import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:simple_permissions/simple_permissions.dart';



class ZoomableImagePage extends StatefulWidget {
  final data;

  ZoomableImagePage(this.data);

  @override
  ZoomableImagePageState createState() {
    return new ZoomableImagePageState();
  }
}

class ZoomableImagePageState extends State<ZoomableImagePage> {
  static const platform = const MethodChannel('test.kinder/s3_image');
  var _photo;
  Permission permission;
  String finalURL;

  @override
  initState() {
    super.initState();
    setState(() {
      _photo = widget.data;
    }
    );
    if (_photo["post_img_url"] == null || _photo["post_img_url"] == "") {
      //do nothing
    } else {
      var pri = _photo["post_img_url"].split(':;');
      if (pri.length > 0) {
        finalURL = "https://d212imxpbiy5j1.cloudfront.net/${pri[0]}";
        print("photo lis isssss  ${_photo["post_img_url"]}");
      }
    }
  }

  Future<void> _photoDownloadLevel(
      MethodChannel channel, String finalUrl) async {
    try {
      final String result = await channel
          .invokeMethod('getPhotoDownloadLevel', {"url": finalUrl});
    } on PlatformException catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    var finalURLs = widget.data['imagesnew']['imagedetails'];

    print('Images list with caption111111 ${widget.data['imagesnew']['imagedetails']}');

    var caption = widget.data['imagesnew']['imagedetails'][0]['caption'];

    List<Widget> widgets = [];
    for (int index = 0; index < finalURLs.length; index++) {
      widgets.add(
        Scaffold(
          appBar: new AppBar(
            title: finalURLs.length > 1
                ? new Text("${index + 1} / ${finalURLs.length}",
                    style: TextStyle(
                      color: Colors.white,
                    ))
                : new Text(''),
            elevation: 0.0,
            backgroundColor: Theme.Colors.app_dark[900],
            actions: <Widget>[
              new IconButton(
                  icon: new Icon(Icons.file_download,
                      color: Theme.Colors.appcolour),
                  onPressed: () async {
                    if (_photo["post_img_url"] != null ||
                        _photo["post_img_url"] != "") {
                      var pri = _photo["post_img_url"].split(':;');

                      if (pri.length > 0) {
                        finalURL =
                            "https://d212imxpbiy5j1.cloudfront.net/${pri[index]}";
                      }
                    }
                    if (Platform.isAndroid) {
                      await SimplePermissions.requestPermission(
                              Permission.WriteExternalStorage)
                          .then((value) {
                        print("request :" + value.toString());

                      });
                      _photoDownloadLevel(platform, finalURL);
                    } else {
                      final ios = const MethodChannel('kinder.flutter/s3');
                      _photoDownloadLevel(ios, finalURL);
                    }
                  }
                  ),
            ],
          ),
          body: new Stack(
            children: <Widget>[
              new Container(
                  child: PhotoViewGallery(
                pageOptions: <PhotoViewGalleryPageOptions>[
                  PhotoViewGalleryPageOptions(
                    imageProvider: NetworkImage(finalURLs[index]['fileurl']),
                    minScale: PhotoViewComputedScale.contained * 1.0,
                    maxScale: PhotoViewComputedScale.covered * 1.1,
                    heroTag: "",
                  ),
                ],
//                backgroundColor: Theme.Colors.app_dark[900],
              )),
              new Align(
                alignment: Alignment.bottomRight,
                child: new Column(
                  // I need to add a column to set the MainAxisSize to min,
                  // otherwise the appbar takes all the screen and the image is no more clickable
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new AppBar(
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      leading: new Container(),
                      // Overrides the go back arrow icon button
                      actions: <Widget>[
                        Expanded(
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Expanded(
                                child: new Text(finalURLs[index]['caption'],
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 12.0)),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }
    return new Scaffold(
      backgroundColor: Theme.Colors.app_dark[900],
      body: new SizedBox.expand(
        child: new PageView(children: widgets),
      ),
    );
  }
}
