import 'package:flutter/rendering.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html_textview/flutter_html_textview.dart';
import 'package:intl/intl.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:flutter_html_textview/html_parser.dart';

const kMonths = <String>[
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

const kMonthsShort = <String>[
  'Jan.',
  'Feb.',
  'Mar.',
  'Apr.',
  'May',
  'Jun.',
  'Jul.',
  'Aug.',
  'Sept.',
  'Oct.',
  'Nov.',
  'Dec.'
];

const kWeekdays = <String>[
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
];

String datetoTimestamp(onlydate) {
  DateTime dateFrom = DateTime.utc(onlydate);
  var timeStamp = new DateFormat("d MMM");
  var dateStamp = new DateFormat("yMMMEd");
  String formatEventStartDate = dateStamp.format(dateFrom);
//  String formatEventStartDate = timeStamp.format(dateFrom);
//  final difference = dateFrom.toUtc();
  return dateStamp.toString();
}

DateTime dateToMonth(DateTime date) {
  return DateTime(date.year, date.month);
}

String dateToShortString(DateTime date) {
  return '${kWeekdays[date.weekday - 1]}, ${kMonthsShort[date.month - 1]} ${date.day}';
}

String dateToShortStringWithTime(DateTime date) {
  return '${kWeekdays[date.weekday - 1]}, ${kMonthsShort[date.month - 1]} ${date.day} at ${_twentyFourToTwelveHour(date.hour)}:${_toMinutesString(date.minute)} ${_AmOrPm(date.hour)}';
}

int _twentyFourToTwelveHour(int hour) {
  if (hour > 12) {
    return hour % 12;
  }

  return hour;
}

String _toMinutesString(int minutes) {
  if (minutes < 10) {
    return '0$minutes';
  }

  return '$minutes';
}

String _AmOrPm(int hour) {
  if (hour < 12) {
    return 'a.m.';
  }

  return 'p.m.';
}

String twoDigits(int n) {
  if (n >= 10) return "${n}";
  return "0${n}";
}

int toTwelveHour(int n) {
  return n > 12 ? n % 12 : (n == 0 ? 12 : n);
}

String amOrPm(int n) {
  return n >= 12 ? 'p.m.' : 'a.m.';
}

final List<String> months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'Devember'
];

/*  Function Use age */
/*//color: Color(getColorHexFromStr('#fff5ea')),*/
int getColorHexFromStr(String colorStr) {
  colorStr = "FF" + colorStr;
  colorStr = colorStr.replaceAll("#", "");
  int val = 0;
  int len = colorStr.length;
  for (int i = 0; i < len; i++) {
    int hexDigit = colorStr.codeUnitAt(i);
    if (hexDigit >= 48 && hexDigit <= 57) {
      val += (hexDigit - 48) * (1 << (4 * (len - 1 - i)));
    } else if (hexDigit >= 65 && hexDigit <= 70) {
      // A..F
      val += (hexDigit - 55) * (1 << (4 * (len - 1 - i)));
    } else if (hexDigit >= 97 && hexDigit <= 102) {
      // a..f
      val += (hexDigit - 87) * (1 << (4 * (len - 1 - i)));
    } else {
      throw new FormatException("An error occurred when converting a color");
    }
  }
  return val;
}

String readTimestamp(int timestamp) {
  var now = new DateTime.now();
  var format = new DateFormat('HH:mm a');
  var date = new DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
  var diff = now.difference(date);
  var time = '';

  if (diff.inSeconds <= 0 ||
      diff.inSeconds > 0 && diff.inMinutes == 0 ||
      diff.inMinutes > 0 && diff.inHours == 0 ||
      diff.inHours > 0 && diff.inDays == 0) {
    time = format.format(date);
  } else if (diff.inDays > 0 && diff.inDays < 7) {
    if (diff.inDays == 1) {
      time = diff.inDays.toString() + ' DAY AGO';
    } else {
      time = diff.inDays.toString() + ' DAYS AGO';
    }
  } else {
    if (diff.inDays == 7) {
      time = (diff.inDays / 7).floor().toString() + ' WEEK AGO';
    } else {
      time = (diff.inDays / 7).floor().toString() + ' WEEKS AGO';
    }
  }

  return time;
}

String computeHowLongAgoText(DateTime timestamp) {
  DateTime now = DateTime.now();

  Duration difference = now.difference(timestamp);

  if (difference.inSeconds < 60) {
    return 'Just Now';
  } else if (difference.inMinutes < 60) {
    return '${difference.inMinutes} minute${difference.inMinutes > 1 ? 's' : ''} ago';
  } else if (difference.inHours < 6) {
    return '${difference.inHours} hour${difference.inHours > 1 ? 's' : ''} ago';
  } else {
    bool sameDay = new DateTime(now.year, now.month, now.day) ==
        new DateTime(timestamp.year, timestamp.month, timestamp.day);

    String onText =
        sameDay ? 'Today' : 'on ${months[timestamp.month]} ${timestamp.day}';
    return 'At ${toTwelveHour(timestamp.hour)}:${twoDigits(timestamp.minute)} ${amOrPm(timestamp.hour)} ${onText}';
  }
}

String computeHowLongAgoTextShort(DateTime timestamp) {
  DateTime now = DateTime.now();

  Duration difference = now.difference(timestamp);

  if (difference.inSeconds < 60) {
    return 'Just Now';
  } else if (difference.inMinutes < 60) {
    return '${difference.inMinutes} minute${difference.inMinutes > 1 ? 's' : ''} ago';
  } else if (difference.inHours < 6) {
    return '${difference.inHours} hour${difference.inHours > 1 ? 's' : ''} ago';
  } else {
    bool sameDay = new DateTime(now.year, now.month, now.day) ==
        new DateTime(timestamp.year, timestamp.month, timestamp.day);

    String onText =
        sameDay ? 'Today' : 'On ${months[timestamp.month]} ${timestamp.day}';
    return '${onText}';
  }
}

String formatText(String str) {
  return str
      .replaceAll(new RegExp('<p>'), '\n\n')
      .replaceAll(new RegExp('&#x2F;'), '/')
      .replaceAll(new RegExp('<i>'), '')
      .replaceAll(new RegExp('</i>'), '')
      .replaceAll(new RegExp('&#x27;'), '\'')
      .replaceAll(new RegExp('&quot;'), '\"')
      .replaceAll(
          new RegExp(r'<a\s+(?:[^>]*?\s+)?href="([^"]*)" rel="nofollow">'), '')
      .replaceAll(new RegExp('</a>'), '');
}

String inlineHtmlWrap(String htmlStr) {
  // print('*** parsing inline html...');

  return htmlStr.replaceAll(new RegExp("<\/*(i|b|span)>"), '');
}

/// Whether or not two times are on the same day.
isSameDay(DateTime a, DateTime b) {
  return a.year == b.year && a.month == b.month && a.day == b.day;
}

isSameWeek(DateTime a, DateTime b) {
  /// Handle Daylight Savings by setting hour to 12:00 Noon
  /// rather than the default of Midnight
  a = new DateTime.utc(a.year, a.month, a.day);
  b = new DateTime.utc(b.year, b.month, b.day);

  var diff = a.toUtc().difference(b.toUtc()).inDays;
  if (diff.abs() >= 7) {
    return false;
  }

  var min = a.isBefore(b) ? a : b;
  var max = a.isBefore(b) ? b : a;
  var result = max.weekday % 7 - min.weekday % 7 >= 0;
  return result;
}

previousMonth(DateTime m) {
  var year = m.year;
  var month = m.month;
  if (month == 1) {
    year--;
    month = 12;
  } else {
    month--;
  }
  return new DateTime(year, month);
}

nextMonth(DateTime m) {
  var year = m.year;
  var month = m.month;

  if (month == 12) {
    year++;
    month = 1;
  } else {
    month++;
  }
  return new DateTime(year, month);
}

previousWeek(DateTime w) {
  return w.subtract(new Duration(days: 7));
}

nextWeek(DateTime w) {
  return w.add(new Duration(days: 7));
}

String timeAgo(DateTime d) {
  Duration diff = DateTime.now().difference(d);
  if (diff.inDays > 365)
    return "${(diff.inDays / 365).floor()} ${(diff.inDays / 365).floor() == 1 ? "year" : "years"} ago";
  if (diff.inDays > 30)
    return "${(diff.inDays / 30).floor()} ${(diff.inDays / 30).floor() == 1 ? "month" : "months"} ago";
  if (diff.inDays > 7)
    return "${(diff.inDays / 7).floor()} ${(diff.inDays / 7).floor() == 1 ? "week" : "weeks"} ago";
  if (diff.inDays > 0)
    return "${diff.inDays} ${diff.inDays == 1 ? "day" : "days"} ago";
  if (diff.inHours > 0)
    return "${diff.inHours} ${diff.inHours == 1 ? "hour" : "hours"} ago";
  if (diff.inMinutes > 0)
    return "${diff.inMinutes} ${diff.inMinutes == 1 ? "minute" : "minutes"} ago";
  return "just now";
}

/* // Usgae code
Navigator.of(context).push(
new FadeRoute(
  builder: (BuildContext context) => new BookDetailsPageFormal(book),
  settings: new RouteSettings(name: '/book_detais_formal', isInitialRoute: false),
)); */

class FadeRoute<T> extends MaterialPageRoute<T> {
  FadeRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;
    // Fades between routes. (If you don't want any animation,
    // just return child.)
    return new FadeTransition(opacity: animation, child: child);
  }
}
