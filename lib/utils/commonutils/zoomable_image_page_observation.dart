import 'dart:async';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:zoomable_image/zoomable_image.dart';


class ZoomableImagePage_Observation extends StatefulWidget {
  final data;

  ZoomableImagePage_Observation(this.data);

  @override
  ZoomableImagePage_observationState createState() {
    return new ZoomableImagePage_observationState();
  }
}

class ZoomableImagePage_observationState extends State<ZoomableImagePage_Observation> {
  static const platform = const MethodChannel('test.kinder/s3_image');
  var _photo;
  Permission permission;

  String finalURL;

  @override
  initState() {
    super.initState();
    setState(() {
      _photo = widget.data;
    }
    );
    if (_photo["post_img_url"] == null || _photo["post_img_url"] == "") {
      //do nothing
    } else {
      var pri = _photo["post_img_url"].split(':;');
      if (pri.length > 0) {
        finalURL = "https://d212imxpbiy5j1.cloudfront.net/${pri[0]}";
      }
    }
  }

  Future<void> _photoDownloadLevel(
      MethodChannel channel, String finalUrl) async {
    try {
      final String result = await channel
          .invokeMethod('getPhotoDownloadLevel', {"url": finalUrl});
    } on PlatformException catch (e) {
      print(e);
    }
  }



  Future<Null> _saveNetworkImage(String url) async {

  }

  @override
  Widget build(BuildContext context) {
    print("HHHHHHHHH${widget.data}");
    var finalURLs = widget.data['imagesnew']['imagedetails'];
    print('Images list with caption 2 222222 ${widget.data['imagesnew']['imagedetails']}');
    print(widget.data);


    List<Widget> widgets = [];
    for (int index = 0; index < finalURLs.length; index++) {
      widgets.add(
        Scaffold(
          appBar: new AppBar(
            title: finalURLs.length > 1
                ? new Text("${index + 1} / ${finalURLs.length}",
                    style: TextStyle(
                      color: Colors.white,
                    ))
                : new Text(''),
            elevation: 0.0,
            backgroundColor: Theme.Colors.app_dark[900],
            leading: new IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: new Icon(Icons.close, color: Theme.Colors.appcolour),
            ),
            actions: <Widget>[
              new IconButton(
                icon: new Icon(Icons.file_download,
                    color: Theme.Colors.appcolour),
                onPressed: () async {
                  if (_photo["post_img_url"] != null ||
                      _photo["post_img_url"] != "") {
                    var pri = _photo["post_img_url"].split(':;');

                    if (pri.length > 0) {
                      finalURL =
                      "https://d212imxpbiy5j1.cloudfront.net/${pri[index]}";
                    }
                  }
                  if (Platform.isAndroid) {
                    await SimplePermissions.requestPermission(
                        Permission.WriteExternalStorage)
                        .then((value) {
                      print("request :" + value.toString());

                    });
                    _photoDownloadLevel(platform, finalURL);
                  } else {
                    final ios = const MethodChannel('kinder.flutter/s3');
                    _photoDownloadLevel(ios, finalURL);
                  }
                }

              ),
            ],
          ),
          body: new Stack(
            children: <Widget>[

            ],
          ),
        ),
      );
    }
    return new Scaffold(
      backgroundColor: Theme.Colors.app_dark[900],
      body: new SizedBox.expand(
        child: new PageView(children: widgets

            ),
      ),
    );
  }


}
