import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:kinderm8/utils/commonutils/zoomable_image_url_only.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:zoomable_image/zoomable_image.dart';


class ZoomableImagePage_Url extends StatefulWidget {
  final data;
  final int i;

  ZoomableImagePage_Url(this.data, this.i);

  @override
  ZoomableImagePageUrlState createState() {
    return new ZoomableImagePageUrlState();
  }
}

class ZoomableImagePageUrlState extends State<ZoomableImagePage_Url> {
  var photoFullData;

  Future<Null> _launched;

  static const platform = const MethodChannel('test.kinder/s3_image');

  Widget _showResult(BuildContext context, AsyncSnapshot<Null> snapshot) {
    if (!snapshot.hasError) {
      return Text('Image is saved');
    } else {
      return const Text('Unable to save image');
    }
  }

  Future<Null> _saveNetworkImage(String url) async {
    try {
    //  await SaveFile().saveImage(url);
    } on Error catch (e) {
      throw 'Error has occured while saving';
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    photoFullData = widget.data;
  }

  Future<void> _photoDownloadLevel(
      MethodChannel channel, String finalUrl) async {
    try {
      final String result = await channel
          .invokeMethod('getPhotoDownloadLevel', {"url": finalUrl});
    } on PlatformException catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
//    var finalURL = widget.data['imagesnew']['imagedetails'][0]['fileurl'];
    print('Images list with caption ${photoFullData}');

    List<Widget> widgets = [];


    String finalUrl;
    for (int index = widget.i; index < photoFullData.length; index++) {
//      print(photoFullData[index]);
      widgets.add(
        Scaffold(
          appBar: new AppBar(
            title: photoFullData.length > 1
                ? new Text("${index + 1} / ${photoFullData.length}",
                style: TextStyle(
                  color: Colors.white,
                ))
                : new Text(''),
            elevation: 0.0,
            backgroundColor: Theme.Colors.appdarkcolour,
            leading: new IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: new Icon(Icons.close, color: Colors.white),
            ),
            actions: <Widget>[
              new IconButton(
                onPressed: () async {
                  String url = photoFullData[index]['fileurl'];
                  print("hi hello welcome to photo ");
                  if (url != null || url != "") {
                    var pri = url.split(':;');

                    if (pri.length > 0) {
                      finalUrl = url;
                    }
                  }
                  if (Platform.isAndroid) {
                    await SimplePermissions.requestPermission(
                        Permission.WriteExternalStorage)
                        .then((value) {
                      print("request :" + value.toString());
                    });
                    _photoDownloadLevel(platform, finalUrl);
                  } else {
                    final ios = const MethodChannel('kinder.flutter/s3');
                    _photoDownloadLevel(ios, finalUrl);
                  }
                },
                icon: new Icon(Icons.file_download,
                    color:Colors.white),
              ),
            ],
          ),
          body: new Stack(
            children: <Widget>[
              new Container(
                child: ZoomableImage(
                  NetworkImage(photoFullData[index]['fileurl']),
                  placeholder:
                  const Center(child: const CircularProgressIndicator()),
                ),

              ),
              new Align(
                alignment: Alignment.bottomRight,
                child: new Column(
                  // I need to add a column to set the MainAxisSize to min,
                  // otherwise the appbar takes all the screen and the image is no more clickable
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new AppBar(
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      leading: new Container(),
                      // Overrides the go back arrow icon button
                      actions: <Widget>[
                        Expanded(
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              photoFullData[index]["caption"] != null &&
                                  photoFullData[index]["caption"] != ""
                                  ? new Expanded(
                                child: new Text(
                                    photoFullData[index]['caption'],
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12.0)),
                              )
                                  : Container(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }
    return new Scaffold(
      backgroundColor: Colors.grey,
      body: new SizedBox.expand(
        /*ZoomableImage(
          NetworkImage(
              'http://carem8bucket.s3-ap-southeast-2.amazonaws.com/img/postuploads/production_1536758523-sun-hat-pink-apple-baby_large.jpg'),
          placeholder: const Center(child: const CircularProgressIndicator()),
        ),*/
        child: new PageView(
          children: widgets,
        ),
      ),
    );
  }
}