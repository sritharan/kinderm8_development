import 'dart:async';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kinderm8/pages/home/modals/child_photos_modal.dart';
import 'package:kinderm8/utils/commonutils/load_image.dart';
import 'package:kinderm8/utils/commonutils/saveimage.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:zoomable_image/zoomable_image.dart';


class ZoomableImagePage_Journal extends StatefulWidget {
  final List<ChildPhotoModal> images;
  final String image;

  ZoomableImagePage_Journal(this.images, {this.image});

  @override
  ZoomableImagePage_journalState createState() {
    return new ZoomableImagePage_journalState();
  }
}

class ZoomableImagePage_journalState extends State<ZoomableImagePage_Journal> {
  static const platform = const MethodChannel('test.kinder/s3_image');

  var _photo;
  Permission permission;
  String finalURL;


  Future<Null> _saveNetworkImage(String url) async {
//    try {
////      await SaveFile().saveImage(url);
//      var date = new DateTime.now();
//      var imagedownloaddate = new DateTime(date.year, date.month, date.day);
//      await SaveFile().saveImagewithDate(url,imagedownloaddate);
//    } on Error catch (e) {
//      throw 'Error has occured while saving';
//    }
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _photo = widget.image;
    });
  }

  Future<void> _photoDownloadLevel(
      MethodChannel channel, String finalUrl) async {
    try {
      final String result = await channel
          .invokeMethod('getPhotoDownloadLevel', {"url": finalUrl});
    } on PlatformException catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
//    print("HHHHHHHHH${widget.images}");
    var finalURLs = widget.images;
     List<Widget> widgets = [];
      for (int index = 0; index < finalURLs.length; index++) {
        widgets.add(
        Scaffold(
          appBar: new AppBar(
            title: finalURLs.length > 1
                ? new Text("${index + 1} / ${widget.images.length}",
                    style: TextStyle(
                      color: Colors.white,
                    ))
                : new Text(''),
            elevation: 0.0,
            backgroundColor: Theme.Colors.app_dark[900],
            leading: new IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: new Icon(Icons.close, color: Theme.Colors.appcolour),
            ),
            actions: <Widget>[
              new IconButton(
                onPressed: () async {
                  if (Platform.isAndroid) {
                    await SimplePermissions.requestPermission(
                        Permission.WriteExternalStorage)
                        .then((value) {
                      print("request :" + value.toString());
                    //  print("heelooooooooooooooooooooooooooo");
                    });
                    _photoDownloadLevel(platform, widget.images[index].fileUrl);
                  } else {
                    final ios = const MethodChannel('kinder.flutter/s3');
                    _photoDownloadLevel(ios, widget.images[index].fileUrl);
                  }
                },
                icon: new Icon(Icons.file_download,
                    color: Theme.Colors.appcolour),
              ),
            ],
          ),
          body: new Stack(
            children: <Widget>[
              new Container(
                  child: PhotoViewGallery(
                pageOptions: <PhotoViewGalleryPageOptions>[
                  PhotoViewGalleryPageOptions(
                    imageProvider: CachedNetworkImageProvider(finalURLs[index].fileUrl),
                    minScale: PhotoViewComputedScale.contained * 1.0,
                    maxScale: PhotoViewComputedScale.covered * 1.1,
                    heroTag: "",
                  ),
                ],
//                backgroundColor: Theme.Colors.app_dark[900],
              )),
              new Align(
              alignment: Alignment.bottomRight,
              child: new Column(

                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new AppBar(
                    elevation: 0.0,
                    backgroundColor: Colors.transparent,
                    leading:
                        new Container(), // Overrides the go back arrow icon button
                    actions: <Widget>[
                      Expanded(
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Expanded(
                              child: new Text(finalURLs[index].caption ?? "",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12.0)),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            ],
          ),
        ),
      );
    }
    return new Scaffold(
      backgroundColor: Theme.Colors.app_dark[900],
      body: new SizedBox.expand(
        child: new PageView(children: widgets),
      ),
    );
  }



}
