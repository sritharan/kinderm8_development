import 'package:flutter/material.dart';
import 'package:kinderm8/pages/Support.dart';
import 'package:kinderm8/pages/calendar/events.dart';
import 'package:kinderm8/pages/calendar/eventslist.dart';
import 'package:kinderm8/pages/childrenmenu.dart';
import 'package:kinderm8/pages/forgetpassword/forgetpassword.dart';
import 'package:kinderm8/pages/home/dailysummarydatalist.dart';
import 'package:kinderm8/pages/home/home.dart';
import 'package:kinderm8/pages/login/login.dart';
import 'package:kinderm8/pages/newsfeed/newsfeedlist.dart';
import 'package:kinderm8/pages/splashScreen.dart';
import 'package:kinderm8/pages/notification/notificationlist.dart';
//import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:kinderm8/Theme.dart' as Theme;

const SupportUrl = 'https://kinderm8.com.au/support';

// Setup all routes
final routes = {
  '/': (BuildContext context) => new SplashPage(),
  '/login': (BuildContext context) => new LoginPage(),
  '/home': (BuildContext context) => new DailySummary(),
  '/newsfeed': (BuildContext context) => new NewsFeedList(),
  '/calendar': (BuildContext context) => new EventsData(),
  '/forgetpassword': (BuildContext context) => new ForgetPassword(),
  '/childrenDrawer': (BuildContext context) => new ChildrenDrawer([]),
  '/notification': (BuildContext context) => new Notifications(),
//  '/supportweb': (BuildContext context) => WebviewScaffold(
//        url: SupportUrl,
//        appBar: AppBar(
//          title: new Text(
//            "Forgot Password",
//            style: new TextStyle(
//                color: Theme.Colors.forgotlabel,
//              fontWeight: FontWeight.w300,
//            ),
//          ),
//        ),
//        withJavascript: true,
//        withZoom: true,
//      ),
  '/support': (BuildContext context) => new Kinderm8Support(),
};
