import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:kinderm8/auth.dart';

class NoConnectionDialog extends StatefulWidget {
  final bool isOffline;
  final Function onAuthStateChanged;
  final AuthState loginState;

  NoConnectionDialog({this.isOffline, this.onAuthStateChanged, this.loginState});

  @override
  _NoConnectionDialogState createState() => _NoConnectionDialogState();
}

class _NoConnectionDialogState extends State<NoConnectionDialog> {

  bool canProceed;
  BuildContext _ctx;

  @override
  void initState() {
    canProceed = false;
    WidgetsBinding.instance.addPostFrameCallback((_) => setState(() => buildAlertDialog()));
    super.initState();
  }

  void buildAlertDialog() {
      if (widget.isOffline) {
        showDialog(
            barrierDismissible: false,
            context: _ctx,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Internet connection cannot be establised!"),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Icon(Icons.portable_wifi_off, color: Colors.redAccent,
                      size: 36.0,),
                    !canProceed ? Text(
                      "Check your internet connection before proceeding.",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 12.0),) : Text(
                      "Please! proceed by connecting to a internet connection",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 12.0, color: Colors.red),),
                  ],
                ),
                actions: <Widget>[
                  RaisedButton(
                    onPressed: () {
                      SystemChannels.platform
                          .invokeMethod('SystemNavigator.pop');
                    },
                    child: Text(
                      "CLOSE THE APP",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  RaisedButton(
                    onPressed: () {
                      if (widget.isOffline) {
                        setState(() {
                          canProceed = true;
                        });
                      } else {
                        Navigator.pop(_ctx);
                        widget.onAuthStateChanged(widget.loginState);
                      }
                    },
                    child: Text(
                      "PROCEED",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              );
            });
        return;
      }
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    return Container();
  }
}
