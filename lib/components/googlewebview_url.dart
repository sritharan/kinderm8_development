import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:kinderm8/Theme.dart' as kinderm8Theme;
import 'package:simple_permissions/simple_permissions.dart';

/* Pass URL to load any documents in new view witih app bar */
class WebUrlPreview extends StatefulWidget {
  final fileurlencode;
  final String fileUrl;

  WebUrlPreview(this.fileurlencode, {this.fileUrl});
  @override
  _WebUrlPreviewState createState() => _WebUrlPreviewState();
}

class _WebUrlPreviewState extends State<WebUrlPreview> {
  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  static const platform = const MethodChannel('test.kinder/s3_image');
  var pdf;
  Permission permission;
  String finalURL;

  @override
  void initState() {
    super.initState();
    setState(() {
      pdf = widget.fileurlencode;
    }
    );
    var pri = pdf;
    if(pri.length > 0) {
    finalURL= widget.fileUrl;
    print("pdf is $pdf");
    print("finalUrl is ${widget.fileUrl} ");
    }
  }

  Future<void> pdfDownload(
      MethodChannel channel, String finalUrl) async {
    try {
      final String result = await channel
          .invokeMethod('pdfDownload', {"url": finalUrl});
       print("$result");
    } on PlatformException catch (e) {
      print(e);
    }
  }





  @override
  Widget build(BuildContext context) {
    print('WebUrlPreview >>> ${widget.fileurlencode}');
    var finalURLs = widget.fileurlencode;
    final double height = MediaQuery.of(context).size.height;
    return Container(
      child: Scaffold(
            body: Container(
              child: WebviewScaffold(
                  url: widget.fileurlencode,
                appBar: AppBar(
                  automaticallyImplyLeading: false, // Don't show the leading button
                  backgroundColor: kinderm8Theme.Colors.appsplashscreen,

                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[

                      IconButton(
                        icon: Icon(Icons.file_download, color: Colors.white),
                        onPressed: () async {
                          if(Platform.isAndroid){
                            await SimplePermissions.requestPermission(
                                Permission.WriteExternalStorage)
                                .then((value) {
                              print("request :" + value.toString());
                              print("android site we are staying ");

                            });
                            pdfDownload(platform, finalURL);
                          } else {
                            final ios = const MethodChannel('kinder.flutter/s3');
                            pdfDownload(ios, finalURL);
                            print("IOS site we are staying........ ");
                          }
                        }

                      ),
                      // Your widgets here
                    ],
                  ),
                  bottom: new PreferredSize(
                      child: new Container(
                        color: kinderm8Theme.Colors.appcolour,
                        padding: const EdgeInsets.all(5.0),
                      ),
                      preferredSize: const Size.fromHeight(20.0)),

                ),
                  withZoom: true,
                  withLocalStorage: true,
                ),
            ),
          ),
        );
  }
}