import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:kinderm8/Theme.dart' as kinderm8Theme;
import 'package:flutter/material.dart';
import 'package:kinderm8/auth.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/centrenotes/centrenoteslist.dart';
import 'package:kinderm8/pages/childprofile/profile.dart';
import 'package:kinderm8/pages/commondrawer.dart';
import 'package:kinderm8/pages/dailychart/dailychartlist.dart';
import 'package:kinderm8/pages/dailyjournal/dailyJournalList.dart';
import 'package:kinderm8/pages/gallery/photolist.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/home/home.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/pages/learningstory/learningstoryList.dart';
import 'package:kinderm8/pages/medication/medicationlist.dart';
import 'package:kinderm8/pages/newsfeed/newsfeedlist.dart';
import 'package:kinderm8/pages/notification/notificationlist.dart';
import 'package:kinderm8/pages/observation/observationList.dart';

class ChildrenModuleMenu extends StatefulWidget {
  final ChildViewModal selectedChild;

  const ChildrenModuleMenu(this.selectedChild);
  @override
  State<StatefulWidget> createState() {
    return new ChildrenModuleMenuState(this.selectedChild);
  }
}

class ChildrenModuleMenuState extends State<ChildrenModuleMenu>
    implements HomePageContract, AuthStateListener {
//  var email, password, client_id;
//  final selectedChild;
  var center;
  var jwt, id, cliId, children, image;
  var deviceSize;
  BuildContext _ctx;
  HomePagePresenter _presenter;
  ChildViewModal selectedChild;
  var childselected;
  bool showMenu = true;
  bool load = true;


  ChildrenModuleMenuState(this.selectedChild) {

    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  logout() {
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
//    Navigator.push(context, new MaterialPageRoute(
//        builder: (context) =>
//        new LoginPage()));
    _presenter.getUserLogout();
  }

  void _openchildrenpopup() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return ChildrenModuleMenu(selectedChild);
        },
        fullscreenDialog: true));
  }

  void _opencommonpopup(i) {
    print("child $i");
    print(selectedChild);
//    print(selectedChild['id']);
//    Navigator.push(
//        context,
//        new MaterialPageRoute(
//            builder: (context) => new CommonDrawer(eventdata)));
  }


  /* ---------------- */
  /* Round icon Menu design */
  /* --------------- */
  @override
  Widget build(BuildContext context) {
    showMenu = true;
//    print('Selected child >> ChildrenModuleMenu $selectedChild + ${selectedChild.length}');
    final Size screenSize = MediaQuery.of(context).size;

    Widget _buildAction(String title, VoidCallback action, Color color,
        Gradient gradient, ImageProvider backgroundImage) {
      final textStyle = new TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.w700,
        fontSize: 16.0,
//          fontFamily: ProfileFontNames.TimeBurner
      );
      final Size screenSize = MediaQuery.of(context).size;
      const double PI = 3.1415926535897932;
      return new GestureDetector(
        onTap: action,
        child: new Container(
          margin: const EdgeInsets.only(right: 5.0, left: 5.0),
//          width: 150.0,
          width: screenSize.width/2,
          decoration: new BoxDecoration(
              color: color,
              shape: BoxShape.rectangle,
              borderRadius: new BorderRadius.circular(10.0),
              boxShadow: <BoxShadow>[
                new BoxShadow(color: Colors.black38,
                    blurRadius: 5.0,
                    spreadRadius: 1.0,
                    offset: new Offset(0.0, 1.0)),
              ],
              gradient: gradient
          ),
          child: new Stack(
            children: <Widget>[
              new Opacity(
                opacity: 0.2,
                child: new Align(
                  alignment: Alignment.centerRight,
                  child: new Transform.rotate(
                    angle: -PI / 5.8,
                    alignment: Alignment.centerRight,
                    child: new ClipPath(
                      clipper: new _BackgroundImageClipper(),
                      child: new Container(
                        padding: const EdgeInsets.only(
                            bottom: 15.0, right: 10.0, left: 75.0),
                        child: new Image(
                          width: 90.0,
                          height: 90.0,
                          image: backgroundImage != null
                              ? backgroundImage
                              : new AssetImage("assets/iconsetpng/d-files.png"),
                        ),
                      ),
                    ),
                  ),
                ),
              ), // END BACKGROUND IMAGE

              new Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.only(left: 10.0, top: 10.0),
                child: new Text(title, style: textStyle),
              ),
            ],
          ),
        ),
      );
    }

    Widget _buildAction_Grid(String title, VoidCallback action, Color color,
        Gradient gradient, ImageProvider backgroundImage) {
      final textStyle = new TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.w500,
        fontSize: 14.0,
//          fontFamily: ProfileFontNames.TimeBurner
      );
      final Size screenSize = MediaQuery.of(context).size;
      const double PI = 3.1415926535897932;
      return new GestureDetector(
        onTap: action,
        child: new Container(
          margin: const EdgeInsets.all(5.0),
//          margin: const EdgeInsets.only(right: 5.0, left: 5.0),
//          width: 150.0,
          width: screenSize.width/2,
          decoration: new BoxDecoration(
              color: color,
              shape: BoxShape.rectangle,
              borderRadius: new BorderRadius.circular(10.0),
              boxShadow: <BoxShadow>[
                new BoxShadow(color: Colors.black38,
                    blurRadius: 5.0,
                    spreadRadius: 1.0,
                    offset: new Offset(0.0, 1.0)),
              ],
              gradient: gradient
          ),
          child: new Stack(
            children: <Widget>[
              new Opacity(
                opacity: 0.4,
                child: new Align(
                  alignment: Alignment.bottomCenter,
                  child: new Transform.rotate(
                    angle: -PI / 360.0,
                    alignment: Alignment.bottomCenter,
                    child: new ClipPath(
                      clipper: new _BackgroundImageClipper(),
                      child: new Container(
//                        padding: const EdgeInsets.only(
//                            bottom: 15.0, right: 10.0, left: 75.0),
                        padding: const EdgeInsets.only(
                            bottom: 10.0),
                        child: new Image(
                          width: 90.0,
                          height: 90.0,
                          image: backgroundImage != null
                              ? backgroundImage
                              : new AssetImage("assets/iconsetpng/d-files.png"),
                        ),
                      ),
                    ),
                  ),
                ),
              ), // END BACKGROUND IMAGE

              new Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.only(left:5.0,bottom: 5.0),
                child: new Text(title, style: textStyle),
              ),
            ],
          ),
        ),
      );
    }

    /* Card design menu*/
    Widget _buildCard(String title,VoidCallback action ,String status, int cardIndex, String backgroundImage) {
      return GestureDetector(
        onTap: action,
        child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)
            ),
            elevation: 7.0,
            child: Column(
              children: <Widget>[
                SizedBox(height: 12.0),
                Stack(
                    children: <Widget>[
                      Container(
                        height: 70.0,
                        width: 70.0,
                        decoration: BoxDecoration(
//                          borderRadius: BorderRadius.circular(30.0),
                          color: Colors.white,
                            image: DecorationImage(

                                image: AssetImage(
                                backgroundImage
                            )
                            )
                        ),
                      ),

/* ------------------------------------------ */
/*  ENABLE THIS FOR SHOW BADGE NOTOFICATION */
/* ------------------------------------------ */
//                    Container(
//                      margin: EdgeInsets.only(left: 40.0),
//                      height: 20.0,
//                      width: 20.0,
//                      decoration: BoxDecoration(
//                          borderRadius: BorderRadius.circular(30.0),
//                          color: status == 'Away' ? Colors.amber : Colors.green,
//                          border: Border.all(
//                              color: Colors.white,
//                              style: BorderStyle.solid,
//                              width: 2.0
//                          )
//                      ),
//                    )

                    ]),
                SizedBox(height: 10.0),
                Text(
                  title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ),
//              SizedBox(height: 5.0),
//              Text(
//                status,
//                style: TextStyle(
//                    fontFamily: 'Quicksand',
//                    fontWeight: FontWeight.bold,
//                    fontSize: 12.0,
//                    color: Colors.grey
//                ),
//              ),
//              SizedBox(height: 15.0),
//              Expanded(
//                  child: Container(
//                      width: 175.0,
//                      decoration: BoxDecoration(
//                        color: status == 'Away' ? kinderm8Theme.Colors.appcolour: kinderm8Theme.Colors.appcolour,
//                        borderRadius: BorderRadius.only
//                          (
//                            bottomLeft: Radius.circular(10.0),
//                            bottomRight: Radius.circular(10.0)
//                        ),
//                      ),
//                      child: Center(
//                        child: Text('Request',
//                          style: TextStyle(
//                              color: Colors.white, fontFamily: 'Quicksand'
//                          ),
//                        ),
//                      )
//                  )
//              )
              ],
            ),
            margin: cardIndex.isEven? EdgeInsets.fromLTRB(25.0, 0.0, 15.0, 15.0):EdgeInsets.fromLTRB(25.0, 0.0, 15.0, 30.0)
        ),
      );
    }
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        margin: EdgeInsets.all(15.0),
        child: selectedChild != null ? new Container(
          height: screenSize.height,
          color: Colors.white30,
          child: new GridView.count(
              crossAxisCount: 2,
              primary: false,
              crossAxisSpacing: 2.0,
              mainAxisSpacing: 2.0,
              shrinkWrap: true,
              children: <Widget>[
                _buildCard(
                    labelsConfig["dailyJournalsLabel"],() {
                  if (showMenu == true) {
                    Navigator.of(context)
                        .pushReplacement(new MaterialPageRoute<Null>(
                        builder: (BuildContext context) {
                          return DailyJournal(widget.selectedChild, jwt);
                        },
                        fullscreenDialog: true));
                  }
                }, "", 1,"assets/iconsetpng/cubes.png"
                ),
                _buildCard(
                    labelsConfig["observationLabel"],() {
                  if (showMenu == true) {
                    Navigator.of(context)
                        .pushReplacement(new MaterialPageRoute<Null>(
                        builder: (BuildContext context) {
                          return ObservationList(widget.selectedChild, jwt);
                        },
                        fullscreenDialog: true));
                  }
                }, "", 1,"assets/iconsetpng/game.png"
                ),
                _buildCard(
                    labelsConfig["noteToCenterLabel"],() {
                  if (showMenu == true) {
                    Navigator.of(context)
                        .pushReplacement(new MaterialPageRoute<Null>(
                        builder: (BuildContext context) {
                          return CentreNotesList(
                              widget.selectedChild, jwt);
                        },
                        fullscreenDialog: true));
                  }
                }, "", 1,"assets/iconsetpng/user.png"
                ),
                _buildCard(
                    labelsConfig["dailyChartLabel"],() {
                  if (showMenu == true) {
                    Navigator.of(context)
                        .pushReplacement(new MaterialPageRoute<Null>(
                        builder: (BuildContext context) {
                          return DailyChart(widget.selectedChild, jwt);
                        },
                        fullscreenDialog: true));
                  }
                }, "", 1,"assets/iconsetpng/notepad_dailychart.png"
                ),
                _buildCard(
                    labelsConfig["learningStoryLabel"],() {
                  if (showMenu == true) {
                    Navigator.of(context)
                        .pushReplacement(new MaterialPageRoute<Null>(
                        builder: (BuildContext context) {
                          return LearningStoryList(widget.selectedChild, jwt);
                        },
                        fullscreenDialog: true));
                  }
                }, "", 1,"assets/iconsetpng/abacus.png"
                ),
                _buildCard(
                    labelsConfig["medicationLabel"],() {
                  if (showMenu == true) {
                    Navigator.of(context)
                        .pushReplacement(new MaterialPageRoute<Null>(
                        builder: (BuildContext context) {
                          return Medications(widget.selectedChild, jwt,1);
                        },
                        fullscreenDialog: true));
                  }
                }, "", 1,"assets/iconsetpng/first-aid-kit.png"
                ),
                _buildCard(
                    labelsConfig["photosLabel"],() {
                  if (showMenu == true) {
                    Navigator.of(context)
                        .pushReplacement(new MaterialPageRoute<Null>(
                        builder: (BuildContext context) {
                          return PhotoList(widget.selectedChild, jwt);
                        },
                        fullscreenDialog: true));
                  }
                }, "", 1,"assets/iconsetpng/image.png"
                ),
                _buildCard(
                    labelsConfig["childProfileLabel"],() {
                  if (showMenu == true) {
                    Navigator.of(context)
                        .pushReplacement(new MaterialPageRoute<Null>(
                        builder: (BuildContext context) {
                          return ChildProfile(widget.selectedChild, jwt);
                        },
                        fullscreenDialog: true));
                  }
                }, "", 1,"assets/iconsetpng/d-files.png"
                )

              ].toList()),
        ): new Container(),
      ),

      persistentFooterButtons: <Widget>[
        new FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text('Close')),
      ],
    );

//    return Scaffold(
//      backgroundColor: Colors.transparent,
//      body: Column(
//        children: <Widget>[
//          new Text('Children Module Meni'),
//
//          /* // ---------------- //
//          // Action button Grid menu Style  //
//          // ---------------- // */
//          /* if One child enable child menu || check child length */
//          selectedChild ? Flexible(
//            child: new Container(
//              color: Colors.white30,
//              child: new GridView.count(
//                  crossAxisCount: 2,
//                  primary: false,
//                  crossAxisSpacing: 2.0,
//                  mainAxisSpacing: 4.0,
//                  shrinkWrap: true,
//                  children: <Widget>[
//                    _buildCard(
//                      "Daily Journal",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return DailyJournal(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                      }, "", 1,"assets/iconsetpng/cubes.png"
//                    ),
//                    _buildCard(
//                        "Observation",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return ObservationList(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/game.png"
//                    ),
//                    _buildCard(
//                        "Centre Notes",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return CentreNotesList(
//                                  selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/user.png"
//                    ),
//                    _buildCard(
//                        "Daily Chart",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return DailyJournal(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/notepad_dailychart.png"
//                    ),
//                    _buildCard(
//                        "Learning Story",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return LearningStoryList(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/abacus.png"
//                    ),
//                    _buildCard(
//                        "Medications",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return DailyJournal(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/first-aid-kit.png"
//                    ),
//                    _buildCard(
//                        "Photos",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return PhotoList(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/image.png"
//                    ),
//                    _buildCard(
//                        "Profile",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return DailyJournal(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/d-files.png"
//                    )
//
//                  ].toList()),
//            ),
//          ): new Container(),
//
//        ],
//      ),
//
//    );

  }



  @override
  void onDisplayUserInfo(User user) {
//    email = user.email;
//    password = user.password;
//    client_id = user.client_id;

//    setState(() {
    try {
      center = user.center;
      final parsed = json.decode(center);
      var details = parsed[0];
      print(details);
      jwt = details["jwt"];
      var users = details["user"];

      id = users["id"];
      cliId = users["client_id"];
      children = details["children"];

      print("Children $children");
      print("Children ${children.length}");
      print("id $id , clientId $cliId");
      setState(() {
        load = false;
      });

    } catch (e) {
      print('That string was null!');
    }
//    });
  }

  @override
  void onErrorUserInfo() {}

  @override
  void onLogoutUser() {
    var authStateProvider = new AuthStateProvider();
    setState(() {
      authStateProvider.notify(AuthState.LOGGED_OUT);
      authStateProvider.dispose(this);
    });
    // TODO: implement onLogoutUser
  }

  @override
  void onAuthStateChanged(AuthState state) {
    print("Navigatesssss");
    print("state $state");
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(_ctx)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }
    // TODO: implement onAuthStateChanged
  }
}

class _BackgroundImageClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = new Path();
    path.moveTo(0.0, 0.0);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;

}