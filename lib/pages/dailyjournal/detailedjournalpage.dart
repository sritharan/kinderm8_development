import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/Theme.dart' as Kinderm8Theme;
import 'package:kinderm8/components/page_transformer.dart';
import 'package:kinderm8/pages/dailyjournal/ArcBannerImage.dart';
import 'package:kinderm8/pages/dailyjournal/diagonally_cut_colored_image.dart';
import 'package:kinderm8/pages/dailyjournal/followupviewlist.dart';
import 'package:kinderm8/pages/dailyjournal/journalcomment.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/home/modals/child_photos_modal.dart';
import 'package:kinderm8/pages/newsfeed/learningoutcomes.dart';
import 'package:kinderm8/pages/newsfeed/learningtags.dart';
import 'package:kinderm8/utils/commonutils/zoomable_image_page.dart';
import 'package:kinderm8/utils/commonutils/zoomable_image_page_journal.dart';
import 'package:html2md/html2md.dart' as html2md;

class JournalDetails extends StatefulWidget {
  final singleJournalData;
  final jwt;

  JournalDetails(this.singleJournalData, this.jwt);
  @override
  JournalDetailsState createState() {
    return new JournalDetailsState(singleJournalData);
  }

//  @override
//  _EventPageState createState() => new _EventPageState(eventdata);
}

class JournalDetailsState extends State<JournalDetails>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  ScrollController scrollController;
  Animation<Color> colorTween1;
  Animation<Color> colorTween2;

  final singleJournalData;
  static const BACKGROUND_IMAGE = 'assets/kinderm8childcare-white-new.png';
  final double statusBarSize = 24.0;
  final double imageSize = 264.0;
  double readPerc = 0.0;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  var learningTag, learningOutcomeDetail;
  var Followup;

  var title, description;
  var learning_set_title = "";
  List learningSetTitle = new List();

  JournalDetailsState(this.singleJournalData);

  @override
  void initState() {
    print('>>>>>>>>>> Single Journal Details $singleJournalData');
    super.initState();
    animationController = new AnimationController(
        duration: new Duration(milliseconds: 500), vsync: this);
    animationController.addListener(() => setState(() {}));

    colorTween1 = new ColorTween(
            begin: Kinderm8Theme.Colors.app_blue[50],
            end: new Color(0xFFB39DDB))
        .animate(new CurvedAnimation(
            parent: animationController, curve: Curves.easeIn));
    colorTween2 = new ColorTween(
            begin: Kinderm8Theme.Colors.app_blue[50],
            end: new Color(0xFF512DA8))
        .animate(new CurvedAnimation(
            parent: animationController, curve: Curves.easeIn));

    scrollController = new ScrollController();
    scrollController.addListener(() {
      setState(() => readPerc =
          scrollController.offset / scrollController.position.maxScrollExtent);

      // Change the appbar colors
      if (scrollController.offset > imageSize - statusBarSize) {
        if (animationController.status == AnimationStatus.dismissed)
          animationController.forward();
      } else if (animationController.status == AnimationStatus.completed)
        animationController.reverse();
    });

    learningTags();
    groupLearningOutComes();
  }

  List<Widget> widgets = [];
  var color, splitColor, tag;
  int colorCode;

  void groupLearningOutComes() {
    print("${singleJournalData["learningsetchildrens"]}");
    if (singleJournalData["learningsetchildrens"].length > 0) {
      learningOutcomeDetail = singleJournalData["learningsetchildrens"];
      for (int i = 0; i < learningOutcomeDetail.length; i++) {
        if (learning_set_title ==
            learningOutcomeDetail[i]["learning_set_title"]) {
        } else {
          learning_set_title = learningOutcomeDetail[i]["learning_set_title"];

          learningSetTitle.add(learningOutcomeDetail[i]["learning_set_title"]);
        }
      }
      print("learningSetTitle______- $learningSetTitle");
      print("${learningSetTitle.length}");
    } else {
      print("else");
    }
  }

  learningTags() {
    if (singleJournalData["learningtagdetails"] != null) {
      print("${singleJournalData["learningtagdetails"].length}");
      learningTag = singleJournalData["learningtagdetails"];

      for (int i = 0; i < learningTag.length; i++) {
        tag = learningTag[i];
        color = tag["color_code"].toString();
        splitColor = '0xFF' + color.substring(1);
        colorCode = int.parse(splitColor);
        widgets.add(
          Padding(
            padding: const EdgeInsets.all(1.0),
            child: new Chip(
              padding: EdgeInsets.all(0.0),
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              label: Text(
                '${tag["name"]}',
                style: TextStyle(color: Colors.white, fontSize: 11.0),
              ),
              backgroundColor: Color(colorCode),
            ),
          ),
        );
      }
    } else {
      print("else tag");
    }
  }

  Widget _buildFollowups(FollowupsData) {
    final Size screenSize = MediaQuery.of(context).size;
    final cardheight = screenSize.height / 2.0;
//    print('_buildFollowups${FollowupsData}');
    Followup = FollowupsData;
    return new SizedBox.fromSize(
      /*size: const Size.fromHeight(250.0),*/
      size: Size.fromHeight(cardheight),
      child: PageTransformer(
        pageViewBuilder: (context, visibilityResolver) {
          return PageView.builder(
            controller: PageController(viewportFraction: 0.85),
            itemCount: FollowupsData.length,
            itemBuilder: (context, index) {
              final item = FollowupsData[index];
              final pageVisibility =
                  visibilityResolver.resolvePageVisibility(index);
              return FollowupPageView(
                followupitem: FollowupsData[index],
                pageVisibility: pageVisibility,
              );
            },
          );
        },
      ),
    );
  }

  Widget _buildVerticalDivider() {
    return new Container(
      height: 40.0,
      width: 1.0,
      color: Kinderm8Theme.Colors.appcolour,
      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
    );
  }

  Widget _buildFollowerStat(String title, String value) {
    final titleStyle = new TextStyle(
        fontSize: 12.0,
//          fontFamily: ProfileFontNames.TimeBurner,
        color: Kinderm8Theme.Colors.appcolour);
    final valueStyle = new TextStyle(
//          fontFamily: ProfileFontNames.TimeBurner,
        fontSize: 12.0,
        fontWeight: FontWeight.w700,
        color: Kinderm8Theme.Colors.darkGrey);
    return Flexible(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Text(
            title,
            style: titleStyle,
            overflow: TextOverflow.ellipsis,
            maxLines: 4,
          ),
          new Text(value,
              style: valueStyle, overflow: TextOverflow.ellipsis, maxLines: 4),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    /* // Format */
    var JournalCreatedDate = DateTime.parse(singleJournalData["created_at"]);
    var timeStamp = new DateFormat("yMMMEd");
    String formatJournalCreatedDate = timeStamp.format(JournalCreatedDate);

    final journalimagecount = this.singleJournalData['journalgallery'].length;
    List<ChildPhotoModal> imgUrl = List();
    var finaljournalImages = List();
    if (journalimagecount > 0) {
      for (int i = 0; i < singleJournalData['journalgallery'].length; i++) {
        var fullurl =
            "https://d212imxpbiy5j1.cloudfront.net/${singleJournalData['journalgallery'][i]['url']}";
        finaljournalImages.add(new CachedNetworkImageProvider(fullurl));
        imgUrl.add(ChildPhotoModal(fileUrl: fullurl, date: "", caption: ""));
      }
    }
//    var finaljournalImages1 = List();
//    for (int i = 0; i < singleJournalData['journalgallery'].length; i++) {
//      var fullurl =
//          "https://d212imxpbiy5j1.cloudfront.net/${singleJournalData['journalgallery'][i]['url']}";
//      finaljournalImages1.add(fullurl);
//    }

//    final followups = this.singleJournalData['followups'];
//    var finalfollowuplist = List();
////    for (var singlefollowup in singleJournalData['followups']) {
//    for (int i = 0; i < singleJournalData['followups'].length; i++) {
//      print('Single followup : ${singleJournalData['followups'][i]['id']}');
//      var singlefollowupimagelist = List();
//      for (int j = 0; j < singleJournalData['followups'][i]['followup_gallery'].length; j++) {
//        print('Single followup_gallery : ${singleJournalData['followups'][i]['followup_gallery'][j]['url']}');
//        var fullurl = "https://d212imxpbiy5j1.cloudfront.net/${followups[j]['url']}";
//        singlefollowupimagelist.add(fullurl);
//      }
//    followups['followupgallery']= singlefollowupimagelist;
//      finalfollowuplist.add(followups);
//    }

    var linearGradient = const BoxDecoration(
      gradient: const LinearGradient(
        begin: FractionalOffset.centerRight,
        end: FractionalOffset.bottomLeft,
        colors: <Color>[
          const Color(0xFF413070),
          const Color(0xFF2B264A),
        ],
      ),
    );

    Widget _buildDiagonalImageBackground(BuildContext context) {
      var screenWidth = MediaQuery.of(context).size.width;

      return new DiagonallyCutColoredImage(
        new Image.asset(
          BACKGROUND_IMAGE,
          width: screenWidth,
          height: screenWidth / 2,
          fit: BoxFit.contain,
        ),
        color: const Color(0xBB8338f4),
      );
    }

//    Widget _buildAvatar() {
//      return new Hero(
//        tag: 'asdasd',
//        child: new CircleAvatar(
//          backgroundImage: new NetworkImage(singleJournalData['staff'][]),
//          radius: 50.0,
//        ),
//      );
//    }

    Widget _createPillButton(
      String text, {
      Color backgroundColor = Colors.transparent,
      Color textColor = Colors.white70,
    }) {
      return new ClipRRect(
        borderRadius: new BorderRadius.circular(30.0),
        child: new MaterialButton(
          minWidth: 140.0,
          color: backgroundColor,
          textColor: textColor,
          onPressed: () {},
          child: new Text(text),
        ),
      );
    }
//

    Widget _buildVerticalDivider() {
      return new Container(
        height: 40.0,
        width: 1.0,
        color: Kinderm8Theme.Colors.appcolour,
        margin: const EdgeInsets.only(left: 10.0, right: 10.0),
      );
    }

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var whatHappened;
    if (singleJournalData['whathappened'] != '' &&
        singleJournalData['whathappened'] != null) {
      whatHappened = html2md.convert(singleJournalData['whathappened']);
    }

    /*List<Widget> widgets = [];
    var color, splitColor, tag;
    int colorCode;

    for (int i = 0; i < learningTag.length; i++) {
      tag = learningTag[i];
      color = tag["color_code"].toString();
      splitColor = '0xFF' + color.substring(1);
      colorCode = int.parse(splitColor);
      widgets.add(
        Padding(
          padding: const EdgeInsets.all(1.0),
          child: new Chip(
            padding: EdgeInsets.all(0.0),
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            label: Text(
              '${tag["name"]}',
              style: TextStyle(color: Colors.white, fontSize: 11.0),
            ),
            backgroundColor: Color(colorCode),
          ),
        ),
      );
    }*/

    ///
    ///
    return new Scaffold(
        body: SingleChildScrollView(
      child: new Stack(
        children: <Widget>[
          new Container(
            margin: const EdgeInsets.only(top: 0.0),
            height: screenSize.height,
            decoration: linearGradient,
            child: new Hero(
              tag: 'JournalDetails',
              child: new Material(
                color: Colors.white,
                child:
                    new Stack // I need to use a stack because otherwise the appbar blocks the content even if transparent
                        (
                  children: <Widget>[
//                        _buildDiagonalImageBackground(context),
                    /// Image and text
                    new ListView(
                      padding: new EdgeInsets.all(0.0),
                      controller: scrollController,
                      children: <Widget>[
//                    singleJournalData['image_url'] != '' ? new SizedBox.fromSize(
//                      size: new Size.fromHeight(imageSize),
//                      child: new Hero(
//                        tag: 'Image',
////                      child: new Image.asset('res/img1.png', fit: BoxFit.cover),
//                        child: new CachedNetworkImage(
//                          imageUrl: S3URL + singleJournalData['image_url'],
//                          placeholder: new CupertinoActivityIndicator(),
////                errorWidget: new Icon(Icons.error),
//                          fadeOutDuration: new Duration(seconds: 1),
//                          fadeInDuration: new Duration(seconds: 3),
//                        ),
//                      ),
//                    ) : new Container(
//                      height:  screenSize.width/3,
////                      margin: EdgeInsets.only(top: screenSize.width/3),
//                    ),
//                    new Padding(
//                      padding: new EdgeInsets.symmetric(vertical: 12.0),
//                      child: new Text('INVISION PRESENTS',
//                          textAlign: TextAlign.center,
//                          style: new TextStyle(
//                              fontSize: 11.0,
//                              color: Colors.black,
//                              fontWeight: FontWeight.w700)),
//                    ),

                        /* ArcBannerImage image for top section*/
//                            singleJournalData['journalgallery'].length > 0 ? new Padding(
//                              padding: const EdgeInsets.only(bottom:2.0),
//                              child: new ArcBannerImage(
//                                  'https://d212imxpbiy5j1.cloudfront.net/${singleJournalData['journalgallery'][0]['url']}'),
//                            ):
//                            new Container(
//                              margin: new EdgeInsets.only(top:screenSize.width/4),
//                            ),

                        new Container(
                          margin:
                              new EdgeInsets.only(top: screenSize.width / 4),
                        ),
                        new Padding(
//                              padding: new EdgeInsets.symmetric(horizontal: 16.0),
                          padding:
                              new EdgeInsets.only(left: 16.0, bottom: 15.0),
                          child: new Hero(
                            tag: 'Title',
                            child: new Text(singleJournalData['journal_title'],
                                textAlign: TextAlign.left,
                                style: new TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600)),
                          ),
                        ),
//                  new Padding
//                    (
//                    padding: new EdgeInsets.only(top: 12.0, bottom: 24.0),
//                    child: new Text(eventdata['date'], textAlign: TextAlign.center, style: new TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.w700)),
//                  ),
//                            new Row(
//                              mainAxisAlignment: MainAxisAlignment.center,
//                              children: <Widget>[
//                                new Column(
//                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                  children: <Widget>[
//                                    Container(
//                                      margin: new EdgeInsets.only(left: 5.0),
//                                      child: new IconButton(
//                                        onPressed: () {},
//                                        icon: new Icon(Icons.today,
//                                          color: Kinderm8Theme.Colors.appcolour,
//                                          size: 35.0,),
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                              ],
//                            ),
//                              new Row(
//                                mainAxisAlignment: MainAxisAlignment.center,
//                                crossAxisAlignment: CrossAxisAlignment.center,
//                                children: <Widget>[
//                                  _buildFollowerStat("Date", formatJournalCreatedDate.toString()),
//                                  _buildVerticalDivider(),
//                                  _buildFollowerStat("Educator", singleJournalData['staff']['fullname']),
//                                  _buildVerticalDivider(),
//                                  _buildFollowerStat("Room", singleJournalData['room']['title']),
//                                ],
//                              ),
//                            new Padding(
////                              padding: const EdgeInsets.only(top: 16.0),
//                              padding: new EdgeInsets.only(left: 16.0,bottom: 2.0),
//                              child: new Row(
//                                children: <Widget>[
//                                  _createCircleBadge(Icons.person, theme.backgroundColor),
//                                  new Text(' Educator :'),
//                                  new Text(singleJournalData['staff']['fullname'])
//                                ],
//                              ),
//                            ),
//                            new Padding(
////                              padding: const EdgeInsets.only(top: 16.0),
//                              padding: new EdgeInsets.only(left: 16.0,bottom: 2.0),
//                              child: new Row(
//                                children: <Widget>[
//                                  _createCircleBadge(Icons.create, theme.backgroundColor),
//                                  new Text(' Date :'),
//                                  new Text(formatJournalCreatedDate.toString())
//                                ],
//                              ),
//                            ),
//                            new Container(
//                              padding: const EdgeInsets.only(right: 16.0),
//                              child: Image.network(journalurl,
//                                  fit: BoxFit.fill, width: 200.0, height: 150.0),
//                            ),
                        Material(
                          elevation: 6.0,
                          color: Kinderm8Theme.Colors.app_white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.0),
                            topRight: Radius.circular(20.0),
//                                bottomLeft: Radius.circular(20.0),
//                                bottomRight: Radius.circular(20.0),
                          ),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              _buildFollowerStat(
                                  labelsConfig["centerManageDateLabel"], formatJournalCreatedDate.toString()),
                              _buildVerticalDivider(),
                              _buildFollowerStat(labelsConfig["educatorLabel"],
                                  singleJournalData['staff']['fullname']),
                              _buildVerticalDivider(),
                              _buildFollowerStat(
                                  labelsConfig["roomLabel"], singleJournalData['room']['title']),
                            ],
                          ),
                        ),
                        finaljournalImages.length > 0
                            ? Card(
                                child: Container(
                                  margin: new EdgeInsets.only(
                                      top: 10.0,
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 5.0),
                                  child: FlatButton(
                                      child: new SizedBox(
                                          height: screenSize.width / 2,
                                          width: 350.0,
                                          child: new Carousel(
                                            animationCurve:
                                                ElasticInCurve(10.0),
                                            images: finaljournalImages,
                                            dotSize: 4.0,
                                            dotSpacing: 15.0,
                                            dotColor: Colors.white,
                                            indicatorBgPadding: 5.0,
//                                dotBgColor: Colors.purple.withOpacity(0.5),
                                            autoplay: false,
//                                  borderRadius: true,
//                                  radius: Radius.circular(5.0),
                                          )),
                                      onPressed: () {
                                        print("Have to Navigate image view..");

                                        Navigator.push(
                                            context,
                                            new MaterialPageRoute(
                                                builder: (context) =>
                                                    new ZoomableImagePage_Journal(
                                                        imgUrl)));
                                      }),
                                ),
                              )
                            : new Container(),

                        // --------------- //
                        // Image List with //
                        // --------------- //
//                            new Container(
//                                height: screenSize.width /2,
//                                child: new ListView.builder(
//                                    scrollDirection: Axis.horizontal,
//                                    shrinkWrap: true,
//                                    physics: const ClampingScrollPhysics(),
//                                    itemCount: journalimagecount,
//                                    // itemExtent: 10.0,
//                                    // reverse: true, //makes the list appear in descending order
//                                    itemBuilder: (BuildContext context, int index) {
//                                      var img = "https://d212imxpbiy5j1.cloudfront.net/${singleJournalData['journalgallery'][index]['url']}";
//                                      return new Container(
//                                        // color: Colors.blue,
////                                        width: 100.0,
////                                        height: 100.0,
////                                        padding: const EdgeInsets.all(10.0),
//                                        child: new Row(
//                                          children: [
//                                            new Row(children: <Widget>[
//                                              FlatButton(
//                                                  child: new Container(
//                                                      width: 100.0,
//                                                      height: 100.0,
//                                                      child: new Image.network(
//                                                          "https://d212imxpbiy5j1.cloudfront.net/${singleJournalData['journalgallery'][index]['url']}"
//                                                      )
//                                                  )
//                                                  , onPressed: () {
//                                                print("Have to Navigate image view..");
////                                                Navigator.push(
////                                                    context,
////                                                    new MaterialPageRoute(
////                                                        builder: (context) =>
////                                                        new ZoomableImagePage(img)
////                                                    )
////                                                );
//                                              }
//                                              ),
//                                            ],)
//                                          ],
//                                        ),
//                                      );
//                                    })),
                        // --------------- //
                        // End of Image List with //
                        // --------------- //
                        new Card(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              /* What Happened */
                              singleJournalData['whathappened'] != '' &&
                                      singleJournalData['whathappened'] != null
                                  ? new Column(
                                      children: <Widget>[
                                        Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
                                          child: new Text(labelsConfig["whatHappendLabel"],
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Kinderm8Theme
                                                      .Colors.app_white,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                        new Container(
                                            margin: new EdgeInsets.only(
                                                top: 10.0,
                                                left: 20.0,
                                                right: 20.0,
                                                bottom: 10.0),
                                            child: Html(
                                              data: whatHappened,
                                              defaultTextStyle: new TextStyle(
                                                  fontSize: 13.0,
                                                  color: Kinderm8Theme
                                                      .Colors.darkGrey,
                                                  fontWeight: FontWeight.w400),
                                            )

                                            /* new Text(whatHappened,
                                              style: new TextStyle(
                                                  fontSize: 13.0,
                                                  color: Kinderm8Theme
                                                      .Colors.darkGrey,
                                                  fontWeight: FontWeight.w400)),*/
                                            ),
                                      ],
                                    )
                                  : Container(
                                      padding: EdgeInsets.all(0.0),
                                    ),
//

                              /* Reflection section */
                              singleJournalData['reflection'] != '' &&
                                      singleJournalData['reflection'] != null
                                  ? Column(
                                      children: <Widget>[
                                        new Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                                          child: new Text(labelsConfig["reflectionLabel"],
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Kinderm8Theme
                                                      .Colors.app_white,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                        new Container(
                                          alignment: Alignment.topLeft,
                                          margin: new EdgeInsets.only(
                                              top: 10.0,
                                              left: 20.0,
                                              right: 20.0,
                                              bottom: 10.0),
                                          child: new Text(
                                              singleJournalData['reflection'],
                                              style: new TextStyle(
                                                  fontSize: 13.0,
                                                  color: Kinderm8Theme
                                                      .Colors.darkGrey,
                                                  fontWeight: FontWeight.w400)),
                                        ),
                                      ],
                                    )
                                  : new Container(
                                      padding: EdgeInsets.all(0.0),
                                    ),

                              /* Reflection section */
                              singleJournalData['intentional_teaching'] != '' &&
                                      singleJournalData[
                                              'intentional_teaching'] !=
                                          null
                                  ? Column(
                                      children: <Widget>[
                                        new Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                                          child: new Text(labelsConfig["followUpLabel"],
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Kinderm8Theme
                                                      .Colors.app_white,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                        new Container(
                                          alignment: Alignment.topLeft,
                                          margin: new EdgeInsets.only(
                                              top: 10.0,
                                              left: 20.0,
                                              right: 20.0,
                                              bottom: 10.0),
                                          child: new Text(
                                              singleJournalData[
                                                  'intentional_teaching'],
                                              style: new TextStyle(
                                                  fontSize: 13.0,
                                                  color: Kinderm8Theme
                                                      .Colors.darkGrey,
                                                  fontWeight: FontWeight.w400)),
                                        ),
                                      ],
                                    )
                                  : Container(
                                      padding: EdgeInsets.all(0.0),
                                    ),

                              ///
                              ///
// new learning Outcomes
                              singleJournalData["learningsetchildrens"].length >
                                      0
                                  ? Column(
                                      children: <Widget>[
                                        new Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
                                          child: new Text(labelsConfig["learningOutcomeLabel"],
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Kinderm8Theme
                                                      .Colors.app_white,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: new ListView.builder(
                                                physics:
                                                    ClampingScrollPhysics(),
                                                shrinkWrap: true,
                                                itemCount:
                                                    learningSetTitle.length > 1
                                                        ? 1
                                                        : learningSetTitle
                                                            .length,
                                                itemBuilder: (context, i) {
                                                  return Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Text(
                                                            "${learningSetTitle[i]}",
                                                            style: TextStyle(
                                                                color: Kinderm8Theme
                                                                        .Colors
                                                                        .app_blue[
                                                                    300],
                                                                fontSize: 14.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                          Container(
                                                              height: 8.0),
                                                          ListView.builder(
                                                            shrinkWrap: true,
                                                            physics:
                                                                ClampingScrollPhysics(),
                                                            itemCount: learningOutcomeDetail
                                                                        .length >
                                                                    5
                                                                ? 5
                                                                : learningOutcomeDetail
                                                                    .length,
                                                            itemBuilder:
                                                                (context, j) {
                                                              var description;
                                                              if (learningOutcomeDetail[
                                                                          j][
                                                                      "description"] !=
                                                                  null) {
                                                                description =
                                                                    html2md.convert(
                                                                        learningOutcomeDetail[j]
                                                                            [
                                                                            "description"]);
                                                              }
                                                              for (int k = 0;
                                                                  k <
                                                                      learningOutcomeDetail
                                                                          .length;
                                                                  k++) {
                                                                return Padding(
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              0.0),
                                                                  child: learningSetTitle[i] ==
                                                                          learningOutcomeDetail[j]
                                                                              [
                                                                              "learning_set_title"]
                                                                      ? Card(
                                                                          elevation:
                                                                              4.0,
                                                                          margin: EdgeInsets.all(
                                                                              2.0),
                                                                          child:
                                                                              Container(
                                                                            padding:
                                                                                EdgeInsets.all(10.0),
                                                                            child:
                                                                                Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: <Widget>[
                                                                                Text(
                                                                                  learningOutcomeDetail[j]["title"],
                                                                                  style: TextStyle(color: Kinderm8Theme.Colors.app_dark[400], fontSize: 13.0, fontWeight: FontWeight.w600),
                                                                                ),
                                                                                learningOutcomeDetail[j]["description"] != null
                                                                                    ? Html(
                                                                                        data: description,
                                                                                        defaultTextStyle: TextStyle(
                                                                                          color: Kinderm8Theme.Colors.app_dark[200],
                                                                                          fontSize: 12.0,
                                                                                        ))
                                                                                    : Text(""),
                                                                              ],
                                                                            ),
                                                                          ))
                                                                      : null,
                                                                );
                                                              }
                                                            },
                                                          )
                                                        ],
                                                      ),
                                                      learningOutcomeDetail
                                                                  .length >
                                                              5
                                                          ? new InkWell(
                                                              child: new Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .end,
                                                                children: <
                                                                    Widget>[
                                                                  new Text(
                                                                    "show more",
                                                                    style: new TextStyle(
                                                                        color: Kinderm8Theme
                                                                            .Colors
                                                                            .appcolour),
                                                                  ),
                                                                ],
                                                              ),
                                                              onTap: () {
                                                                setState(() {
                                                                  Navigator.push(
                                                                      context,
                                                                      MaterialPageRoute(
                                                                          builder: (context) =>
                                                                              new LearningOutcomes(learningOutcomeDetail)));
                                                                });
                                                              },
                                                            )
                                                          : Container(),
                                                      Container(
                                                        height: 10.0,
                                                      )
                                                    ],
                                                  );
                                                },
                                              ),
                                            )
                                          ],
                                        ),
                                      ],
                                    )
                                  : Container(
                                      padding: EdgeInsets.all(0.0),
                                    ),

// new learning Tags
                              singleJournalData["learningtagdetails"] != null
                                  ? Column(
                                      children: <Widget>[
                                        new Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
                                          child: new Text(labelsConfig["learningTagLabel"],
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Kinderm8Theme
                                                      .Colors.app_white,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                        Container(
                                          width: screenSize.width - 10.0,
                                          padding: EdgeInsets.only(
                                              left: 10.0, top: 5.0),
                                          child: Wrap(
                                            spacing: 4.0,
                                            runSpacing: 1.0,
                                            children: widgets,
                                          ),
                                        )
                                      ],
                                    )
                                  : Container(
                                      padding: EdgeInsets.all(0.0),
                                    ),

                              ///
                              ///
                              new Container(height: 5.0),

                              /* Leaning Tag And Leaning Outcome*/

//                              new Container(
//                                padding: EdgeInsets.only(left: 0.0),
//                                child: singleJournalData[
//                                            "learningtagdetails"] !=
//                                        null
//                                    ? Row(
//                                        mainAxisAlignment:
//                                            MainAxisAlignment.spaceAround,
//                                        children: <Widget>[
//                                          Column(
//                                            crossAxisAlignment:
//                                                CrossAxisAlignment.start,
//                                            children: <Widget>[
//                                              GestureDetector(
//                                                child: Container(
//                                                    padding: EdgeInsets.only(
//                                                        left: 0.0),
////                                      width: ((screenSize.width * 3 / 8)),
//                                                    child: Row(
//                                                      children: <Widget>[
//                                                        Icon(
//                                                            FontAwesomeIcons
//                                                                .tags,
//                                                            size: 12.0,
//                                                            color: Colors
//                                                                .grey[600]),
//                                                        Container(width: 5.0),
//                                                        learningTags(),
//                                                        learningTag.length > 5
//                                                            ? Text(
//                                                                " +${learningTag.length - 5}",
//                                                                style: new TextStyle(
//                                                                    fontSize:
//                                                                        12.0),
//                                                              )
//                                                            : Text(""),
//                                                      ],
//                                                    )),
//                                                onTap: () {
//                                                  print(
//                                                      "have to navigte to Learning tags");
//                                                  Navigator.push(
//                                                      context,
//                                                      MaterialPageRoute(
//                                                          builder: (context) =>
//                                                              new LearningTags(
//                                                                  learningTag)));
//                                                },
//                                              )
//                                            ],
//                                          ),
//                                          Column(
//                                            mainAxisAlignment:
//                                                MainAxisAlignment.end,
//                                            children: <Widget>[
//                                              GestureDetector(
//                                                child: Container(
//                                                    padding:
//                                                        EdgeInsets.all(3.0),
//                                                    decoration:
//                                                        new BoxDecoration(
//                                                      borderRadius:
//                                                          new BorderRadius
//                                                              .circular(25.0),
//                                                      border: new Border.all(
//                                                        width: 2.0,
//                                                        color: Kinderm8Theme
//                                                            .Colors.appcolour,
//                                                      ),
//                                                    ),
//                                                    child: Row(
//                                                      mainAxisAlignment:
//                                                          MainAxisAlignment
//                                                              .center,
//                                                      children: <Widget>[
//                                                        Icon(
//                                                            FontAwesomeIcons
//                                                                .listAlt,
//                                                            size: 12.0,
//                                                            color: Colors
//                                                                .grey[600]),
//                                                        Container(width: 6.0),
//                                                        Text(
//                                                            "Learning Outcomes",
//                                                            style:
//                                                                new TextStyle(
//                                                                    fontSize:
//                                                                        12.0)),
//                                                      ],
//                                                    )),
//                                                onTap: () {
//                                                  print(
//                                                      "have to navigte to Learning Outcomes");
//                                                  Navigator.push(
//                                                      context,
//                                                      MaterialPageRoute(
//                                                          builder: (context) =>
//                                                              new LearningOutcomes(
//                                                                  learningOutcomeDetail)));
//                                                },
//                                              )
//                                            ],
//                                          ),
//                                        ],
//                                      )
//                                    : null,
//                              ),
//                              new Container(height: 5.0),
                              /* Leaning Tag */
//                                  new Container(
//                                    padding: EdgeInsets.only(left: 10.0, top: 5.0),
//                                    child: Wrap(
//                                      spacing: 4.0,
//                                      runSpacing: 1.0,
//                                      children: learningTagwidgets,
//                                    ),
//                                  ),
                              /* Leaning Outcome */
//                                  new Column(
//                                    mainAxisAlignment: MainAxisAlignment.end,
//                                    children: <Widget>[
//                                      GestureDetector(
//                                        child: Container(
//                                            padding: EdgeInsets.all(3.0),
////                                            decoration: new BoxDecoration(
////                                              borderRadius:
////                                              new BorderRadius.circular(25.0),
////                                              border: new Border.all(
////                                                width: 2.0,
////                                                color: Kinderm8Theme.Colors.appcolour,
////                                              ),
////                                            ),
////                                      width: ((screenSize.width   / 5)),
//                                            child: Row(
//                                              mainAxisAlignment:
//                                              MainAxisAlignment.center,
//                                              children: <Widget>[
//                                                Icon(FontAwesomeIcons.listAlt,
//                                                    size: 14.0,
//                                                    color: Colors.grey[600]),
//                                                Container(width: 6.0),
//                                                Text("Learning Outcomes",
//                                                    style: new TextStyle(
//                                                        fontSize: 12.0)),
//                                              ],
//                                            )),
//                                        onTap: () {
//                                          print(
//                                              "have to navigte to Learning Outcomes");
//                                          Navigator.push(
//                                              context,
//                                              MaterialPageRoute(
//                                                  builder: (context) =>
//                                                  new LearningOutcomes(
//                                                      learningOutcomeDetail)));
//                                        },
//                                      )
//                                    ],
//                                  ),

                              /* Additional field 1 section - interest_points_for_children */
                              singleJournalData[
                                              'interest_points_for_children'] !=
                                          '' &&
                                      singleJournalData[
                                              'interest_points_for_children'] !=
                                          null
                                  ? new Column(
                                      children: <Widget>[
                                        Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                                          child: new Text(
                                              'Additional Text Field 1',
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Kinderm8Theme
                                                      .Colors.app_white,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                        new Container(
                                          alignment: Alignment.topLeft,
                                          margin: new EdgeInsets.only(
                                              top: 10.0,
                                              left: 20.0,
                                              right: 20.0,
                                              bottom: 10.0),
                                          child: new Text(
                                              singleJournalData[
                                                  'interest_points_for_children'],
                                              style: new TextStyle(
                                                  fontSize: 13.0,
                                                  color: Kinderm8Theme
                                                      .Colors.darkGrey,
                                                  fontWeight: FontWeight.w400)),
                                        ),
                                      ],
                                    )
                                  : Container(
                                      padding: EdgeInsets.all(0.0),
                                    ),

                              /* Additional field 2 section - materials */
                              singleJournalData['materials'] != '' &&
                                      singleJournalData['materials'] != null
                                  ? Column(
                                      children: <Widget>[
                                        new Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                                          child: new Text(
                                              'Additional Text Field 2',
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Kinderm8Theme
                                                      .Colors.app_white,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                        new Container(
                                          alignment: Alignment.topLeft,
                                          margin: new EdgeInsets.only(
                                              top: 10.0,
                                              left: 20.0,
                                              right: 20.0,
                                              bottom: 10.0),
                                          child: new Text(
                                              singleJournalData['materials'],
                                              style: new TextStyle(
                                                  fontSize: 13.0,
                                                  color: Kinderm8Theme
                                                      .Colors.darkGrey,
                                                  fontWeight: FontWeight.w400)),
                                        ),
                                      ],
                                    )
                                  : Container(
                                      padding: EdgeInsets.all(0.0),
                                    ),

                              /* Follow Up section  Start */
//                                  FollowUp(singleJournalData['followups']),
//                                  new Container(
//                                    child: new Text(singleJournalData['followups'][0]['followup_text']),
//                                  ),
                                singleJournalData['followups'].length > 0
                                    ? _buildFollowups(
                                        singleJournalData['followups'])
                                    : Container(),

                              /* last element section bottom */
                              new Container(
                                margin: new EdgeInsets.only(
                                    bottom: screenSize.width / 2),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),

                    /// Appbar
                    new Align(
                      alignment: Alignment.topCenter,
                      child: new SizedBox.fromSize(
                        size: new Size.fromHeight(90.0),
                        child: new Stack(
                          alignment: Alignment.centerLeft,
                          children: <Widget>[
                            new SizedBox
                                    .expand // If the user scrolled over the image
                                (
                              child: new Material(
                                color: colorTween1.value,
                              ),
                            ),
                            new SizedBox.fromSize // TODO: Animate the reveal
                                (
                              size: new Size.fromWidth(
                                  MediaQuery.of(context).size.width * readPerc),
                              child: new Material(
                                color: colorTween2.value,
                              ),
                            ),
                            new Align(
                                alignment: Alignment.center,
                                child: new SizedBox.expand(
                                  child:
                                      new Material // So we see the ripple when clicked on the iconbutton
                                          (
                                    color: Colors.transparent,
                                    child: new Container(
                                        margin: new EdgeInsets.only(top: 24.0),
                                        // For the status bar
                                        child: new Row(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: <Widget>[
                                            new IconButton(
                                              onPressed: () =>
                                                  Navigator.of(context).pop(),
                                              icon: new Icon(Icons.arrow_back,
                                                  color: Colors.white),
                                            ),
//                                        new Hero(
//                                            tag: 'Logo',
//                                            child: new Icon(Icons.event,
//                                                color: Colors.white,
//                                                size: 48.0)),
                                            new IconButton(
                                              onPressed: () {
                                                Navigator.of(context).push(
                                                    new MaterialPageRoute<Null>(
                                                  builder:
                                                      (BuildContext context) {
                                                    return JournalComment(
                                                        singleJournalData,
                                                        widget.jwt);
                                                  },
                                                ));
                                              },
                                              icon: CircleAvatar(
                                                backgroundColor: Colors.white,
                                                child: new Icon(
                                                  Icons.comment,
                                                  color: Kinderm8Theme
                                                      .Colors.appcolour,
                                                  size: 25.0,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )),
                                  ),
                                ))
                          ],
                        ),
                      ),
                    ),

                    /// Fade bottom text
                    new Align(
                      alignment: Alignment.bottomCenter,
                      child: new SizedBox.fromSize(
                          size: new Size.fromHeight(100.0),
                          child: new Container(
                            decoration: new BoxDecoration(
                                gradient: new LinearGradient(
                                    colors: [Colors.white12, Colors.white],
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    stops: [0.0, 1.0])),
                          )),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    )
//        floatingActionButton: new FloatingActionButton(
//          onPressed: () async {
//            final FormState form = _formKey.currentState;
//            if (!form.validate()) {
//              _autovalidate = true; // Start validating on every change.
//              showInSnackBar('Please fix the errors in red before submitting.');
//            } else {
//              form.save();
////              var createEventResult =
////              await _deviceCalendarPlugin.createOrUpdateEvent(_event);
////              if (createEventResult.isSuccess) {
////                Navigator.pop(context, true);
////              } else {
////                showInSnackBar(createEventResult.errorMessages.join(' | '));
////              }
//            }
//          },
//          child: new Icon(Icons.event),
//        )
        );
  }
}
