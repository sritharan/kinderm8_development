import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html_textview/flutter_html_textview.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:html_unescape/html_unescape.dart';
import 'package:kinderm8/pages/dailyjournal/detailedjournalpage.dart';
import 'package:kinderm8/pages/dailyjournal/journalcomment.dart';
import 'package:html2md/html2md.dart' as html2md;

class DailyJournalData extends StatelessWidget {
  final singleJournalData;
  final  jwt;

  DailyJournalData(this.singleJournalData, this.jwt);
  @override
  Widget build(BuildContext context) {
    var S3URL = "https://d212imxpbiy5j1.cloudfront.net/";
/*
    var unescape = new HtmlUnescape();
    var fromData = unescape.convert(singleJournalData['whathappened']);
    var convertedData = fromData.substring(5, fromData.length - 6);
    var whatHappened = convertedData.split('&');
*/

    var date = singleJournalData["created_at"];
    var parsedDate = DateTime.parse(date);
    var convertedDate = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);
    var created_at = convertedDate.split(' ');

    var whatHappened = html2md.convert(singleJournalData['whathappened']);


    return GestureDetector(
      onTap: () {
        print("navigate to detail view..");
        print(singleJournalData);
        Navigator.of(context).push(new MaterialPageRoute<Null>(
          builder: (BuildContext context) {
            return JournalDetails(singleJournalData,jwt);
          },
//                                fullscreenDialog: true
        ));
      },
      child: Card(
          elevation: 1.0,
          color: Colors.white,
          margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 14.0),
          child: Column(
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: const EdgeInsets.only(right: 15.0),
                      width: 100.0,
                      height: 100.0,
                      child: singleJournalData["journalgallery"].length > 0
                          ?
//                  Text(singleData["journalgallery"].toString())
                          new CachedNetworkImage(
                              fit: BoxFit.fitHeight,
                              imageUrl: S3URL +
                                  singleJournalData["journalgallery"][0]['url'],
                              placeholder: new CupertinoActivityIndicator(),
                              errorWidget:
                                  new Image.asset("assets/nophoto.jpg"),
                              fadeOutDuration: new Duration(seconds: 1),
                              fadeInDuration: new Duration(seconds: 3),
                            )
                          : Center(
                              child: new Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Image.asset(
                                    "assets/iconsetpng/cubes.png",
                                    width: 50.0,
                                    height: 50.0,
                                  ),
                                ],
                              ),
                            )),
                  Expanded(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                          margin:
                              const EdgeInsets.only(top: 12.0, bottom: 10.0),
                          child: new Text(
                            singleJournalData['journal_title'],
                            style: new TextStyle(
                              fontSize: 16.0,
//                              color: Colors.grey,
                              color: Theme.Colors.darkGrey,
                              fontWeight: FontWeight.bold,
                            ),
//                      maxLines: 1,
//                      overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        new Container(
                            margin: const EdgeInsets.only(right: 10.0),
                            child: whatHappened.length > 150
                                ? Html(data: whatHappened.substring(0, 150) + '...')
                                : Html(data: whatHappened)
                            /*new Text(
                            whatHappened[0],
//                            singleJournalData['whathappened'],
                            style: new TextStyle(
                              fontSize: 16.0,
                              color: Theme.Colors.darkGrey,
                            ),
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                          ),*/
                            ),
                        new Container(
                          margin:
                              const EdgeInsets.only(top: 10.0, bottom: 10.0),
                          child: new Row(
                            children: <Widget>[
                              Icon(FontAwesomeIcons.home,
                                  size: 12.0, color: Colors.grey),
                              new Text(
                                "  - " +
                                    singleJournalData["room"]["title"]
                                        .toString(),
                                style: new TextStyle(
                                  fontSize: 12.0,
                                  color: Theme.Colors.app_dark[400],
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
//
              Container(
                padding: EdgeInsets.all(4.0),
                color: Theme.Colors.appcolour,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
//                crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
//                        padding: EdgeInsets.all(0.0),
                        onTap: () {
                          Navigator.of(context)
                              .push(new MaterialPageRoute<Null>(
                            builder: (BuildContext context) {
                              return JournalComment(singleJournalData, jwt);
                            },
//                                fullscreenDialog: true
                          ));
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
//                      crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
//                          Padding(padding: EdgeInsets.only(left: 5.0)),
                            new Icon(Icons.chat,
                                size: 12.0, color: Theme.Colors.app_white),
                            Container(
//                          padding: EdgeInsets.only(left: 10.0),
                              child: singleJournalData["comment_count"] != 0
                                  ? Text(
                                      "${ singleJournalData["comment_count"]}",
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          color: Theme.Colors.app_white))
                                  : Text(' No Comments',
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          color: Theme.Colors.app_white)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
//                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Icon(FontAwesomeIcons.clock,
                            size: 12.0, color: Theme.Colors.app_white),
                        Container(
                          padding: EdgeInsets.all(2.0),
                          child: Text(
                            'on ${created_at[0].substring(0, 3)},${created_at[1].substring(0, 3)},${created_at[2]}${created_at[3]}',
                            style: TextStyle(
                                fontSize: 12.0, color: Theme.Colors.app_white),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }
}
