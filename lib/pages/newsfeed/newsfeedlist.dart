import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kinderm8/auth.dart';

import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/calendar/events.dart';
import 'package:kinderm8/pages/childrenmenu.dart';
import 'package:kinderm8/pages/commondrawer.dart';
import 'package:kinderm8/pages/home/dailysummarydatalist.dart';
import 'package:kinderm8/pages/home/data/config.dart';

import 'package:kinderm8/pages/home/home.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/pages/newsfeed/newsfeeddata.dart';
import 'package:kinderm8/utils/commonutils/emptybody.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/fromjson/jsonObj.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:kinderm8/Theme.dart' as Theme;

class NewsFeedList extends StatefulWidget {
  NewsFeedList({this.childrenData});

  final List<ChildViewModal> childrenData;

  @override
  State<StatefulWidget> createState() {
    return new NewsFeedListState();
  }
}

class NewsFeedListState extends State<NewsFeedList>
    implements HomePageContract, AuthStateListener {
  BuildContext _ctx;
  var appuser, jwt, id, client_id, children, users, newsfeedSectionConfig;
  var email, password;
  var load = true;
  bool isLoading;
  HomePagePresenter _presenter;
  var data;
  var k, newsfeedData;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  ScrollController newsFeedScrollController;

  NewsFeedListState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  Future<String> fetchNewsFeedData(int s) async {
    print("data fetched");

    String _newsfeedUrl =
        "http://api.kinderm8.com.au/v2.1.1/newsfeed/$id?step=$s&clientid=$client_id";
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_newsfeedUrl, headers: headers).then((response) {
      print('res get ${response.body}');
      try {
        newsfeedData = json.decode(response.body);
      } catch (ex) {
        print(ex);
        newsfeedData = [];
        return;
      }
      print('map $newsfeedData');

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        print(isLoading);
        isLoading = false;
        print(isLoading);
        if (s == 0) {
          setState(() {
            load = false;
            this.data = newsfeedData;
          });
        } else {
          setState(() {
            load = false;
            data.addAll(newsfeedData);
          });
        }
        k = data.length;
      } else if (response.statusCode == 500 &&
          newsfeedData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      } else {
        print("other errors");
        fetchNewsFeedData(0);
      }
    });
    return null;
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'http://13.55.4.100:7070/v2.1.0/jwt/refresh-token?userid=$id&clientid=$client_id';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken = json.decode(response.body);
      this.jwt = refreshJwtToken;

      if (k != null) {
        fetchNewsFeedData(k);
      } else {
        fetchNewsFeedData(0);
      }
    });
  }

  @override
  void initState() {
    isLoading = false;
    newsFeedScrollController = new ScrollController()..addListener(_scrollListener);
    super.initState();
  }

  @override
  void dispose() {
    newsFeedScrollController = new ScrollController()..removeListener(_scrollListener);
    newsFeedScrollController.dispose();
    super.dispose();
  }

  var progress = new ProgressBar(
    color: Theme.Colors.appdarkcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  void _openchildrenpopup() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return ChildrenDrawer(widget.childrenData);
        },
        fullscreenDialog: true));
  }
  // Pop to ask do you wish to exit
  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          child: new AlertDialog(
            title: new Text('Are you sure?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => exit(0),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ?? false;
  }

  void _modalBottomSheetMenu(){
    showModalBottomSheet(
        context: context,
        builder: (builder){
          return new Container(
            height: 100.0,
            color: Colors.transparent,
            child: new Container(
                decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(10.0),
                        topRight: const Radius.circular(10.0))),
                child: new Center(
                  child: ChildrenDrawer(widget.childrenData),
                )),
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(labelsConfig["newsFeedLabel"],
            style: TextStyle(
              color: Colors.white,
            )),
        backgroundColor: Theme.Colors.appdarkcolour,
        centerTitle: true,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.refresh),
              tooltip: 'Refresh',
              onPressed: () {
                print("Reload..");
                setState(() {
                  k = 0;
                  load = true;
                });
                fetchNewsFeedData(0);
              })
        ],
      ),
      drawer: CommonDrawer(),
      body: WillPopScope(
        onWillPop: _onWillPop,
        child: new RefreshIndicator(
          key: refreshIndicatorKey,
          onRefresh: handleRefresh,
          child: new Center(
            child: load
                ? progress
                : data.length > 0
                    ? new ListView.builder(addAutomaticKeepAlives: true,
                        controller: newsFeedScrollController,
                        itemCount:
                            this.data != null ? (this.data.length + 1) : 0,
                        itemBuilder: (context, i) {
                          if (i == k) {
                            if (newsfeedData.length < 5) {
                              return Container();
                            } else {
                              return _buildCounterButton();
                            }
                          } else {
                            final singleData = this.data[i];
                            return Data(singleData, users, jwt);
                          }
                        },
                      )
                    : EmptyBody("No Newsfeeed yet."),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.menu),
        backgroundColor: Theme.Colors.appcolour,
        onPressed: () {
            _modalBottomSheetMenu();
          },
        ),
        bottomNavigationBar: BottomAppBar(
          shape: CircularNotchedRectangle(),
          notchMargin: 4.0,
          child: new Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new FlatButton(
                onPressed: () {
//                Navigator.pushNamed(context, "/home");
                Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new DailySummary(childrenData: widget.childrenData,)));
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.format_list_bulleted),
                ],
              ),
            ),
            new FlatButton(
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new NewsFeedList()));
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.chrome_reader_mode),
                ],
              ),
            ),
            new FlatButton(
              onPressed: () {
                EventsData();
                Navigator.pushReplacementNamed(context, "/calendar");
                Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new EventsData(childrenData: widget.childrenData,)));
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.date_range),
                ],
              ),
            ),
            new FlatButton(
              onPressed: () {
//                CommonDrawer();
//                Navigator.push(context, new MaterialPageRoute(
//                    builder: (context) =>
//                    new EventsList())
//                );
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.free_breakfast),
                ],
              ),
            ),
//            IconButton(
//              icon: Icon(Icons.format_list_bulleted),
//              onPressed: () {
//                Dailysummary();
//              },
//            ),
//            IconButton(
//                icon: Icon(Icons.chrome_reader_mode),
//                onPressed: () {
//                  NewsfeedList();
//                },
//            ),
//            IconButton(
//              icon: Icon(Icons.date_range),
//              onPressed: () {
//                EventsList();
////                showModalBottomSheet<Null>(
////                  context: context,
////                  builder: (BuildContext context) => const _DemoDrawer(),
////                );
//              },
//            ),
//            IconButton(
//              icon: Icon(Icons.menu),
//              onPressed: () {
//                CommonDrawer();
////                showModalBottomSheet<Null>(
////                  context: context,
////                  builder: (BuildContext context) => CommonDrawer(),
////                );
//              },
//            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: isLoading
            ? new CupertinoActivityIndicator()
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CupertinoActivityIndicator(),
                  SizedBox(width: 6.0),
                  Text('Please wait...',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,
                          color: Theme.Colors.app_white)),
                ],
              ),
        color: Theme.Colors.appcolour.withOpacity(0.75),
        elevation: 4.0,
        onPressed: _counterButtonPress());
  }

  Function _counterButtonPress() {
    if (isLoading) {
      return null;
    } else {
      return () {
        setState(() {
          isLoading = true;
        });
        fetchNewsFeedData(k);
      };
    }
  }

  Future<Null> handleRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      k = 0;
      load = true;
      fetchNewsFeedData(0);
    });
    return null;
  }

  _scrollListener() {
    if (newsFeedScrollController.position.extentAfter <= 0 && isLoading == false) {
      fetchNewsFeedData(k);
    }
  }

  @override
  void onDisplayUserInfo(User user) {
    setState(() {
      password = user.password;
      appuser = user.center;
      children = user.children;

      print("children $children");
      print("appuser $appuser");

      try {
        final parsed = json.decode(appuser);
        var appusers = parsed[0];
        print(appusers);
        print("******${appusers["jwt"]}");
        jwt = appusers["jwt"];
        users = appusers["user"];
        var appConfig = appusers["globalconfig"];

        var child = appusers["children"];

        client_id = users["client_id"];

        id = users["id"];
        email = users["email"];
        print("###########");
        print("child ${child[0]}");

        fetchNewsFeedData(0);
      } catch (e) {
        print(e);
      }
    });
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    var authStateProvider = new AuthStateProvider();
    setState(() {
      authStateProvider.notify(AuthState.LOGGED_OUT);
      authStateProvider.dispose(this);
    });
  }

  @override
  void onAuthStateChanged(AuthState state) {
    print("Navigate");
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(_ctx)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }
  }
}
