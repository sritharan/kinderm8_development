import 'package:flutter/material.dart';

class LearningTags extends StatelessWidget {
  final learningTag;
  LearningTags(this.learningTag);

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [];
    var color, splitColor, tag;
    int colorCode;

      for (int i = 0; i < learningTag.length; i++) {
        tag = learningTag[i];
        color = tag["color_code"].toString();
        splitColor = '0xFF' + color.substring(1);
        colorCode = int.parse(splitColor);
        widgets.add(
          new Chip(
            label: Text(
              '${tag["name"]}',
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Color(colorCode),
          ),
        );
      }

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text("Learning Tags"),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.close),
              tooltip: 'close',
              onPressed: () {
                Navigator.of(context).pop();
              })
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(left: 10.0, top: 5.0),
          child: Wrap(
            spacing: 4.0,
            runSpacing: 1.0,
            children: widgets,
          ),
        ),
      ),
    );
  }
}
