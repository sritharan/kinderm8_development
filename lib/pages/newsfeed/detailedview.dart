import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/newsfeed/comment.dart';
import 'package:kinderm8/pages/newsfeed/learningoutcomes.dart';
import 'package:kinderm8/pages/newsfeed/learningtags.dart';
import 'package:kinderm8/pages/newsfeed/videoplay.dart';
import 'package:kinderm8/utils/commonutils/zoomable_image_page.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:photo_view/photo_view.dart';
import 'package:kinderm8/Theme.dart' as Kinderm8Theme;

class NewsFeedDetails extends StatefulWidget {
  final data;
  final user;
  final String jwt;
  final Function likeClickedFunction;
  final int isLiked;
  final int likeCount;
  final Function incrementCommentCounterFunction;
  final Function decreaseCommentCountFunction;
  final int postCommentCount;
  NewsFeedDetails(this.data, this.user, this.jwt, {this.likeClickedFunction, this.isLiked, this.likeCount, this.incrementCommentCounterFunction, this.postCommentCount, this.decreaseCommentCountFunction});
  @override
  NewsFeedDetailsState createState() => NewsFeedDetailsState(data, user, jwt, likeClickedFunction: likeClickedFunction, isLikeClicked: isLiked, likeCount: likeCount);
}

class NewsFeedDetailsState extends State<NewsFeedDetails> {
  final data;
  final user;
  String jwt;
  var isLikeClicked;
  int likeCount;
  var likes, message;
  bool load = true;
  bool likeHandler = true;
  var learningTag, learningOutcomeDetail;
  var likeduserdata = [];
  var cliId, user_id, likeData, like;
  final Function likeClickedFunction;

  NewsFeedDetailsState(this.data, this.user, this.jwt, {this.likeClickedFunction, this.isLikeClicked, this.likeCount}) {
    if (likeCount == null) {
      assignLiked();
    }
  }

  void assignLiked() {
    likeduserdata = data["likes"]["user"];
    likes = data["likes"];
    likeCount = likes["count"];
    print("checking");

    if (likes["isLike"] == true) {
      isLikeClicked = 1;
    } else if (likes["isLike"] == false) {
      isLikeClicked = 0;
    }
  }

  getLikeData() {
    print("getLikeData");
    var postfeed_id = data["id"];
    cliId = user["client_id"];
    user_id = user["id"];
    String getLikeUrl =
        'http://13.55.4.100:7070/v2.1.0/newsfeedlike/$postfeed_id/$user_id?clientid=$cliId';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(getLikeUrl, headers: headers).then((response) {
      print('res get ${response.body}');
      likeData = json.decode(response.body);

      print('map $likeData');

      print('jwt $jwt');
      print(response.statusCode);

      if (response.statusCode == 200) {
        like = likeData["user"];
        print(like);
        setState(() {
          load = true;
        });
      } else if (response.statusCode == 500 &&
          likeData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      }
    });
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'http://13.55.4.100:7070/v2.1.0/jwt/refresh-token?userid=$user_id&clientid=$cliId';

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      getLikeData();
    });
  }

  roomTags() {
    message = data["tag"]["message"];

    if (data["tag"]["type"] == 2) {
      if (data["tag"]["count"] == 1 && data["tag"]["count"] != null) {
        return Icon(FontAwesomeIcons.child, size: 12.0, color: Colors.grey);
      } else {
        return Icon(FontAwesomeIcons.users, size: 12.0, color: Colors.grey);
      }
    } else if (data["tag"]["type"] == 3) {
      return Icon(FontAwesomeIcons.home, size: 12.0, color: Colors.grey);
    }
  }

  likedStatement() {
    if (likeCount == 1) {
      if (isLikeClicked == 1) {
        return Text("You like this");
      } else {
        return Text("$likeCount like this");
      }
    } else if (likeCount > 1) {
      if (isLikeClicked == 0) {
        return Text("$likeCount like this");
      } else {
        return Text("You and ${likeCount - 1} other like this");
      }
    } else if (likeCount == 0) {
      return null;
    }
  }

  learningTags() {
    learningTag = data["learningtagdetails"];
    learningOutcomeDetail = data["learningoutcome_detail"];
    if (learningTag.length > 5) {
      for (int i = 0; i < 5; i++) {
        return Container(
          width: 60.0,
          height: 25.0,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 5,
              itemBuilder: (context, i) {
                var color = learningTag[i]["color_code"].toString();
                var splitColor = '0xFF' + color.substring(1);
                int colorCode = int.parse(splitColor);
                return Container(
                  child: new CircleAvatar(
                    backgroundColor: Color(colorCode),
                    radius: 6.0,
                  ),
                );
              }),
        );
      }
    } else {
      for (int i = 0; i < learningTag.length; i++) {
        return Container(
          width: 60.0,
          height: 25.0,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: learningTag.length,
              itemBuilder: (context, i) {
                var color = learningTag[i]["color_code"].toString();
                var splitColor = '0xFF' + color.substring(1);
                int colorCode = int.parse(splitColor);
                return new CircleAvatar(
                  backgroundColor: Color(colorCode),
                  radius: 6.0,
                );
              }),
        );
      }
    }
  }

  likedUsersList() {
    print(likeduserdata);
    print("LIKES ${likeduserdata.length}");

    if (likeduserdata.length > 5) {
      for (int i = 0; i < 5; i++) {
        return Container(
          height: 25.0,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 5,
              shrinkWrap: true,
              itemBuilder: (context, j) {
                var image = likeduserdata[j]["image"];
//                print(image);

                return Row(
                  children: <Widget>[
                    Container(
                      child: image != null
                          ? new CircleAvatar(
                        backgroundImage:
                        CachedNetworkImageProvider(image),
                        radius: 12.0,
                      )
                          : CircleAvatar(
                        radius: 12.0,
                        backgroundImage: AssetImage("assets/nophoto.jpg"),
                      ),
                    ),
                    Container(
                      width: 2.0,
                    )
                  ],
                );
              }),
        );
      }
    } else {
      for (int i = 0; i < likeduserdata.length; i++) {
        print(likeduserdata.length);

        return Container(
          height: 25.0,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: likeduserdata.length,
              shrinkWrap: true,
              itemBuilder: (context, j) {
                var image = likeduserdata[j]["image"];
//                print(image);

                return Row(
                  children: <Widget>[
                    Container(
                      child: image != null
                          ? new CircleAvatar(
                        backgroundImage:
                        CachedNetworkImageProvider(image),
                        radius: 12.0,
                      )
                          : CircleAvatar(
                        radius: 12.0,
                        backgroundImage: AssetImage("assets/nophoto.jpg"),
                      ),
                    ),
                    Container(
                      width: 2.0,
                    )
                  ],
                );
              }),
        );

        /*return Container(
          width: 60.0,
          height: 25.0,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: likeduserdata.length,
              itemBuilder: (context, i) {
                return new Image.network(likes["user"][i]["image"]);
//                return new CircleAvatar(
//                  backgroundImage: likeduserdata[i]["image"] != null
//                      ? CachedNetworkImageProvider(likeduserdata[i]["image"])
//                      : Text("HIIII"),
//                  radius: 25.0,
//                );
              }),
        );*/
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

//    var finalURL = "";
    var images = List();
    var finalImages = List();
    var author = data["author"];
//    var likes = data["likes"];

    var parsedDate = DateTime.parse(data["published_date"]);
    var date = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);

    if (data["post_img_url"] == null || data["post_img_url"] == "") {
      //do nothing
    } else {
      var pri = data["post_img_url"].split(':;');
      if (pri.length > 0) {
//        finalURL = "https://d212imxpbiy5j1.cloudfront.net/${pri[0]}";
        for (int i = 0; i < pri.length; i++) {
          images.add("https://d212imxpbiy5j1.cloudfront.net/${pri[i]}");
        }
      }
    }
    for (int i = 0; i < images.length; i++) {
      finalImages.add(new CachedNetworkImageProvider(images[i]));
    }

    TextStyle commentTextStyle = TextStyle(
      color: Kinderm8Theme.Colors.appdarkcolour,
      fontSize: 20.0,
      fontWeight: FontWeight.w300,
      fontStyle: FontStyle.italic,
    );

    var card = new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          child: new ListTile(
            contentPadding: EdgeInsets.only(
                left: 10.0, right: 4.0, top: 0.0, bottom: 0.0),
            leading: author["image"] != null
                ? new CircleAvatar(
              backgroundImage: CachedNetworkImageProvider(author["image"]),
              radius: 25.0,
            )
                : new CircleAvatar(
              backgroundImage: AssetImage("assets/nophoto.jpg"),
              radius: 25.0,
            ),
            title: new Text(
              author["username"],
              style: new TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 16.0,
              ),
            ),
            trailing: null,
            subtitle: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    new Text(
                      date,
                      style: new TextStyle(
                          color: Kinderm8Theme.Colors.darkGrey, fontSize: 11.0),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 5.0),
                      child: Row(
                        children: <Widget>[
                          roomTags(),
                          // Cover device width
                          new Flexible(
                            child: message.length < 30
                                ? Text(" $message",
                                style: new TextStyle(
                                    color: Kinderm8Theme.Colors.lightGrey,
                                    fontSize: 10.0))
                                : Container(
                              width: screenSize.width * 7 / 10,
                              child: Text(
                                " $message",
                                style: new TextStyle(
                                    color: Kinderm8Theme.Colors.darkGrey
                                        .withOpacity(0.5),
                                    fontSize: 10.0),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                )),
            isThreeLine: true,
          ),
        ),
        Container(
          child: new Text(
            data["post_title"],
            style: new TextStyle(
                fontSize: 20.0
            ),
          ),
        ),
        SizedBox(height: 6.0),
        Container(
          padding: EdgeInsets.all(0.0),
          child: data["imagesnew"] != null
              ? FlatButton(
              padding: EdgeInsets.all(0.0),
              child: new SizedBox(
                  height: screenSize.width / 2,
                  width: screenSize.width,
                  child: new Carousel(
                    animationCurve: ElasticInCurve(10.0),
                    images: finalImages,
                    dotSize: 4.0,
                    dotSpacing: 15.0,
                    dotColor: Colors.white,
                    indicatorBgPadding: 5.0,
//                                dotBgColor: Colors.purple.withOpacity(0.5),
                    autoplay: false,
//                    borderRadius: true,
//                    radius: Radius.circular(5.0),
                  )),
              onPressed: () {
                print("Have to Navigate image view..");

                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) =>
                        new ZoomableImagePage(data)));
              })
              : data["videos"] != null
              ? Column(
            children: <Widget>[
              new FlatButton(
//                              child: Text("${data["videos"]["urls"][0]}"),
                child: Container(
                  height: screenSize.width * 2 / 3,
                  color: Colors.black,
                  child: Center(
                      child: Icon(Icons.play_circle_outline,
                          size: 150.0,
                          color: Colors.grey[300])
//                                  child: NetVideo("https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4"),
                  ),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                          new VideoExample(data["videos"]
                          ["urls"][0])));
                },
              ),
            ],
          )
              : Container(),
        ),
        SizedBox(height: 10.0),
        new Container(
          child: new Column(
            children: <Widget>[
              SizedBox(height: 10.0),
              new Container(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Container(
                      child: new Text(data["post_desc"],
                        style: new TextStyle(
                          fontWeight: FontWeight.w300,
                          fontSize: 17.0,
                        ),
                      ),
                    ),
                    Container(
                      height: 10.0,
                    ),
                    Container(
                      padding: EdgeInsets.all(2.0),
                      child: data["learningtagdetails"] != null
                          ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          GestureDetector(
                            child: Container(
                                padding: EdgeInsets.only(left: 0.0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(FontAwesomeIcons.tags,
                                        size: 12.0, color: Colors.grey[600]),
                                    Container(width: 5.0),
                                    learningTags(),
                                    learningTag.length > 5
                                        ? Text(
                                      " +${learningTag.length - 5}",
                                      style:
                                      new TextStyle(fontSize: 12.0),
                                    )
                                        : Text(""),
                                  ],
                                )),
                            onTap: () {
                              print("have to navigte to Learning tags");
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                      new LearningTags(learningTag)));
                            },
                          ),
                        ],
                      ) : Container(),
                    ),
                  ],
                ),
              ),
              Divider(),
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.of(context).push(new MaterialPageRoute<Null>(
                          builder: (BuildContext context) =>
                              Comment(data, user, jwt, 0, commentCountFunction: widget.incrementCommentCounterFunction, decreaseCommentCountFunction: widget.decreaseCommentCountFunction)));
                    },
                    child: (widget?.postCommentCount != null) ? Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        widget.postCommentCount != 0
                            ? new Text("${widget.postCommentCount}", style: commentTextStyle.copyWith(fontSize: 24.0))
                            : Text("0", style: commentTextStyle.copyWith(fontSize: 24.0)),
                        SizedBox(width: 4.0),
                        (widget.postCommentCount == 1) ? Text("Comment", style: commentTextStyle) : Text("Comments", style: commentTextStyle),
                      ],
                    ) : Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        data["post_comments_count"] != 0
                            ? new Text("${data["post_comments_count"]}", style: commentTextStyle.copyWith(fontSize: 24.0))
                            : Text("0", style: commentTextStyle.copyWith(fontSize: 24.0)),
                        SizedBox(width: 4.0),
                        (data["post_comments_count"] == 1) ? Text("Comment", style: commentTextStyle) : Text("Comments", style: commentTextStyle),
                      ],
                    ),
                  ),
                  Container(
                    child: data["learningtagdetails"] != null
                        ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GestureDetector(
                          child: Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(FontAwesomeIcons.listAlt,
                                      size: 16.0, color: Kinderm8Theme.Colors.appdarkcolour),
                                  Container(width: 6.0),
                                  Text("Learning Outcomes", style: commentTextStyle.copyWith(fontSize: 20.0)),
                                ],
                              )),
                          onTap: () {
                            print("have to navigte to Learning Outcomes");
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                    new LearningOutcomes(
                                        learningOutcomeDetail)));
                          },
                        ),
                      ],
                    ) : Container(),
                  ),
                ],
              ),
            ],
          ),
        ),
        new Container(
          child: new Row(
            children: <Widget>[
              new Expanded(
                  child: likeduserdata.length != 0
                      ? GestureDetector(
                    child: Container(
                        padding: EdgeInsets.only(left: 0.0),
                        child: Row(
                          children: <Widget>[
                            Container(width: 10.0),
                            likedUsersList(),
                            likeduserdata.length > 5
                                ? Text(
                              " +${likeduserdata.length - 5}",
                              style:
                              new TextStyle(fontSize: 12.0),
                            )
                                : Text(""),
                          ],
                        )),
                    onTap: () {
                      print("have to navigte to likes user view");
                      Navigator.of(context).push(
                          new MaterialPageRoute<Null>(
                              builder: (BuildContext context) =>
                                  Comment(data, user, jwt, 1)));
                    },
                  )
                      : Row(
                    children: <Widget>[
                      Container(
                        width: 15.0,
                      ),
                    ],
                  )),
//
////
//              likeHandler
//                  ? GestureDetector(
//                onDoubleTap: () {
//                  print('like Control');
//                },
//                child: new Row(
//                  mainAxisAlignment: MainAxisAlignment.start,
//                  children: <Widget>[
//                    likeCount != 0
//                        ? new Text(likeCount.toString())
//                        : Text(""),
//                    isLikeClicked == 1
//                        ? new Icon(Icons.favorite, color: Colors.red)
//                        : new Icon(Icons.favorite_border,
//                        color: Colors.grey),
//                  ],
//                ),
//                onTap: () async {
//                  setState(() {
//                    likeHandler = false;
//                  });
//                  var likeBody = {
//                    "user_id": user["id"],
//                    "postfeed_id": data["id"].toString(),
//                    "status": isLikeClicked
//                  };
//                  var cliId = user["client_id"];
//                  var _likeUrl =
//                      'http://13.55.4.100:7070/v2.1.0/newsfeedlike?clientid=$cliId';
//                  var headers = {"x-authorization": jwt.toString()};
//                  print("data of likes previously $likes");
//
//                  NetworkUtil _netutil = new NetworkUtil();
//                  _netutil
//                      .post(_likeUrl,
//                      headers: headers,
//                      body: likeBody,
//                      encoding: jwt)
//                      .then((response) {
//                    var likeData = json.decode(response);
//                    print("likedObj==$likeData");
//                    this.likes = likeData;
//                    setState(() {
//                      this.likeCount = likeData["count"];
//                      this.isLikeClicked = isLikeClicked;
//                      likeduserdata = likeData["user"];
//                      likeHandler = true;
//                    });
//                    print("after press likes data $likes");
//                    print("*************");
//                  });
//
//                  if (isLikeClicked == 1 && likeCount != 0) {
//                    setState(() {
//                      likeCount = likeCount - 1;
//                      isLikeClicked = 0;
//                    });
//                  } else {
//                    setState(() {
//                      likeCount = likeCount + 1;
//                      isLikeClicked = 1;
//                    });
//                  }
//                },
//              )
//                  : CupertinoActivityIndicator(),
//              FlatButton(
//                onPressed: () {
//                  Navigator.of(context).push(new MaterialPageRoute<Null>(
//                      builder: (BuildContext context) =>
//                          Comment(data, user, jwt, 0)));
//                },
//                child: new Row(
//                  mainAxisAlignment: MainAxisAlignment.end,
//                  children: <Widget>[
//                    data["post_comments_count"] != 0
//                        ? new Text(data["post_comments_count"].toString())
//                        : Text(""),
//                    new Icon(
//                      Icons.comment,
//                      color: Colors.grey,
//                    ),
//                  ],
//                ),
//              ),
            ],
          ),
        ),
      ],
    );

    final container = SingleChildScrollView(
      child: Container(
        margin: new EdgeInsets.all(10.0),
        child: card,
      ),
    );

    final listView = new ListView.builder(
        itemCount: 1,
        itemBuilder: (context, i) {
          return container;
        });

    return new Scaffold(
        appBar: new AppBar(
          title: new Text(labelsConfig["newsFeedDetailLabel"]),
          backgroundColor: Kinderm8Theme.Colors.appseconderycolour,
          centerTitle: true,
        ),
        body: load ? container : CircularProgressIndicator(),
        floatingActionButton: FloatingActionButton(
        onPressed: () async {
          setState(() {
            likeHandler = true;
          });
//          var likeBody = {
//            "user_id": user["id"],
//            "postfeed_id": data["id"].toString(),
//            "status": isLikeClicked
//          };
//          var cliId = user["client_id"];
//          var _likeUrl =
//              'http://13.55.4.100:7070/v2.1.0/newsfeedlike?clientid=$cliId';
//          var headers = {"x-authorization": jwt.toString()};
//          print("data of likes previously $likes");
//
//          NetworkUtil _netutil = new NetworkUtil();
//          _netutil
//              .post(_likeUrl,
//              headers: headers,
//              body: likeBody,
//              encoding: jwt)
//              .then((response) {
//            var likeData = json.decode(response);
//            print("likedObj==$likeData");
//            this.likes = likeData;
//            setState(() {
//              this.likeCount = likeData["count"];
//              this.isLikeClicked = isLikeClicked;
//              likeduserdata = likeData["user"];
//              likeHandler = true;
//            });
//            print("after press likes data $likes");
//            print("*************");
//          });

          likeClickedFunction(data);
          if (isLikeClicked == 1 && likeCount != 0) {
            setState(() {
              likeCount = likeCount - 1;
              isLikeClicked = 0;
            });
          } else {
            setState(() {
              likeCount = likeCount + 1;
              isLikeClicked = 1;
            });
          }
    },
    child: likeHandler
        ? Column(
      children: <Widget>[
        (likeCount != 0)
            ? Icon((isLikeClicked == 1) ? Icons.favorite : Icons.favorite_border, color: (isLikeClicked == 1) ? Kinderm8Theme.Colors.appdarkcolour : Colors.grey, size: 40.0,)
            : Icon(Icons.favorite_border, color: Colors.grey, size: 40.0,),
        new Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            likeCount != 0
                ? new Text(likeCount.toString(), style: TextStyle(color: (isLikeClicked == 1) ? Kinderm8Theme.Colors.appdarkcolour : Colors.grey, fontSize: 12.0))
                : Text("0", style: TextStyle(color: Colors.grey, fontSize: 12.0)),
            SizedBox(width: 4.0),
            isLikeClicked == 1
                ? (likeCount == 1) ? Text("Like", style: TextStyle(color: Kinderm8Theme.Colors.appdarkcolour, fontSize: 12.0)) : Text("Likes", style: TextStyle(color: Kinderm8Theme.Colors.appdarkcolour, fontSize: 12.0),)
                : (likeCount == 1) ? Text("Like", style: TextStyle(color: Colors.grey, fontSize: 12.0)) : Text("Likes", style: TextStyle(color: Colors.grey, fontSize: 12.0),),
          ],
        ),
      ],
    ) : CupertinoActivityIndicator(),
    backgroundColor: Colors.transparent,
    elevation: 0.0,
    highlightElevation: 0.0,
  ),
    );
  }
}

class ViewImage extends StatelessWidget {
  final data1;
  ViewImage(this.data1);

  @override
  Widget build(BuildContext context) {
    var finalURL;
    var images = List();

    if (data1["post_img_url"] == null || data1["post_img_url"] == "") {
      // todo nothing
    } else {
      var pri = data1["post_img_url"].split(':;');
      if (pri.length > 0) {
        finalURL = "https://d212imxpbiy5j1.cloudfront.net/${pri[0]}";
        for (int i = 0; i < pri.length; i++) {
          images.add("https://d212imxpbiy5j1.cloudfront.net/${pri[i]}");
        }
      }
    }

    return new Container(
        child: new PhotoView(
          imageProvider: NetworkImage(finalURL),
          minScale: PhotoViewComputedScale.contained * 0.8,
          maxScale: 4.0,
        ));
  }
}
