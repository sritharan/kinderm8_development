import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/newsfeed/detailedview.dart';
import 'package:kinderm8/pages/newsfeed/comment.dart';
import 'package:kinderm8/pages/newsfeed/learningoutcomes.dart';
import 'package:kinderm8/pages/newsfeed/learningtags.dart';
import 'package:kinderm8/pages/newsfeed/videoplay.dart';
import 'package:kinderm8/utils/commonutils/zoomable_image_page.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:photo_view/photo_view.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kinderm8/Theme.dart' as Kinderm8Theme;
import 'package:video_player/video_player.dart';

class NewsFeedData extends StatefulWidget {
  @override
  _NewsFeedDataState createState() => _NewsFeedDataState();
}

class LikedData {}

class _NewsFeedDataState extends State<NewsFeedData> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class Data extends StatefulWidget {
  final data;
  final user;
  final String jwt;
  Data(this.data, this.user, this.jwt);

  @override
  DataState createState() => DataState(data, user, jwt);
}

class DataState extends State<Data> {
  final data;
  final user;
  final String jwt;
  var isLikeClicked;
  int likeCount;
  var likes, message;
  bool load = true;
  bool likeHandler = true;
  var learningTag, learningOutcomeDetail;
  var likedUserData =[];
  int postCommentCount = 0;

  DataState(this.data, this.user, this.jwt) {
    assignLiked();
    postCommentCount = data["post_comments_count"];
  }

  void assignLiked() {
    likes = data["likes"];
    likedUserData = data["likes"]["user"];
    likeCount = likes["count"];
    print("checking");

    if (likes["isLike"] == true) {
      isLikeClicked = 1;
    } else if (likes["isLike"] == false) {
      isLikeClicked = 0;
    }
  }

  void incrementPostCommentCount() {
    postCommentCount++;
  }

  void decreaseCommentCount() {
    if (postCommentCount > 0) {
//      setState(() {
        postCommentCount--;
//      });
    }
  }

  Widget learningTags() {
    learningTag = data["learningtagdetails"];
    learningOutcomeDetail = data["learningoutcome_detail"];
    if (learningTag != null && learningTag.length > 5) {
      for (int i = 0; i < 5; i++) {
        return Container(
          width: 60.0,
          height: 25.0,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 5,
              itemBuilder: (context, i) {
                var color = learningTag[i]["color_code"].toString();
                var splitColor = '0xFF' + color.substring(1);
                int colorCode = int.parse(splitColor);
                return Container(
                  child: new CircleAvatar(
                    backgroundColor: Color(colorCode),
                    radius: 6.0,
                  ),
                );
              }),
        );
      }
    } else {
      if (learningTag != null) {
        for (int i = 0; i < learningTag.length; i++) {
          return Container(
            width: 60.0,
            height: 25.0,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: learningTag.length,
                itemBuilder: (context, i) {
                  var color = learningTag[i]["color_code"].toString();
                  var splitColor = '0xFF' + color.substring(1);
                  int colorCode = int.parse(splitColor);
                  return new CircleAvatar(
                    backgroundColor: Color(colorCode),
                    radius: 6.0,
                  );
                }),
          );
        }
      }
    }
    return SizedBox();
  }

  likedUsersList() {
//    print(likeduserdata);
//    print("LIKES ${likeduserdata.length}");

    if (likedUserData.length > 5) {
      for (int i = 0; i < 5; i++) {
        return Container(
          height: 25.0,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 5,
              shrinkWrap: true,
              itemBuilder: (context, j) {
                var image = likedUserData[j]["image"];
//                print(image);

                return Row(
                  children: <Widget>[
                    Container(
                      child: image != null
                          ? new CircleAvatar(
                              backgroundImage:
                                  CachedNetworkImageProvider(image),
                              radius: 12.0,
                            )
                          : CircleAvatar(
                              radius: 12.0,
                              backgroundImage: AssetImage("assets/nophoto.jpg"),
                            ),
                    ),
                    Container(
                      width: 2.0,
                    )
                  ],
                );
              }),
        );
      }
    } else {
      for (int i = 0; i < likedUserData.length; i++) {
//        print(likeduserdata.length);

        return Container(
          height: 25.0,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: likedUserData.length,
              shrinkWrap: true,
              itemBuilder: (context, j) {
                var image = likedUserData[j]["image"];
//                print(image);

                return Row(
                  children: <Widget>[
                    Container(
                      child: image != null
                          ? new CircleAvatar(
                              backgroundImage:
                                  CachedNetworkImageProvider(image),
                              radius: 12.0,
                            )
                          : CircleAvatar(
                              radius: 12.0,
                              backgroundImage: AssetImage("assets/nophoto.jpg"),
                            ),
                    ),
                    Container(
                      width: 2.0,
                    )
                  ],
                );
              }),
        );

        /*return Container(
          width: 60.0,
          height: 25.0,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: likeduserdata.length,
              itemBuilder: (context, i) {
                return new Image.network(likes["user"][i]["image"]);
//                return new CircleAvatar(
//                  backgroundImage: likeduserdata[i]["image"] != null
//                      ? CachedNetworkImageProvider(likeduserdata[i]["image"])
//                      : Text("HIIII"),
//                  radius: 25.0,
//                );
              }),
        );*/
      }
    }
  }

  likedStatement() {
    if (likeCount == 1) {
      if (isLikeClicked == 1) {
        return Text("You like this");
      } else {
        return Text("$likeCount like this");
      }
    } else if (likeCount > 1) {
      if (isLikeClicked == 0) {
        return Text("$likeCount like this");
      } else {
        return Text("You and ${likeCount - 1} other like this");
      }
    } else if (likeCount == 0) {
      return null;
    }
  }

  roomTags() {
    message = data["tag"]["message"];

    if (data["tag"]["type"] == 2) {
      if (data["tag"]["count"] == 1 && data["tag"]["count"] != null) {
        return Icon(FontAwesomeIcons.child,
            size: 12.0, color: Kinderm8Theme.Colors.darkGrey.withOpacity(0.5));
      } else {
        return Icon(FontAwesomeIcons.users,
            size: 12.0, color: Kinderm8Theme.Colors.darkGrey.withOpacity(0.5));
      }
    } else if (data["tag"]["type"] == 3) {
      return Icon(FontAwesomeIcons.home,
          size: 12.0, color: Kinderm8Theme.Colors.darkGrey.withOpacity(0.5));
    }
  }

  void whenLikeClicked(var data) async {
      setState(() {
        likeHandler = false;
      });
      var likeBody = {
        "user_id": user["id"],
        "postfeed_id": data["id"].toString(),
        "status": isLikeClicked
      };
      var cliId = user["client_id"];
      var _likeUrl =
          'http://13.55.4.100:7070/v2.1.0/newsfeedlike?clientid=$cliId';
      var headers = {"x-authorization": jwt.toString()};
      print("data of likes previously $likes");

      NetworkUtil _netutil = new NetworkUtil();
      _netutil
          .post(_likeUrl,
          headers: headers,
          body: likeBody,
          encoding: jwt)
          .then((response) {
        var likeData = json.decode(response);
        print("likedObj==$likeData");
        this.likes = likeData;
        setState(() {
          this.likeCount = likeData["count"];
          this.isLikeClicked = isLikeClicked;
          likedUserData = likeData["user"];
          likeHandler = true;
        });
        print("after press likes data $likes");
        print("*************");
      });

      if (isLikeClicked == 1 && likeCount != 0) {
        setState(() {
          likeCount = likeCount - 1;
          isLikeClicked = 0;
        });
      } else {
        setState(() {
          likeCount = likeCount + 1;
          isLikeClicked = 1;
        });
      }
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
//    var finalURL = "";
    var images = List();
    var finalImages = List();
    var author = data["author"];
//    var likes = data["likes"];
    var parsedDate = DateTime.parse(data["published_date"]);
    var date = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);

    int checkislike;
    if (data["post_img_url"] == null || data["post_img_url"] == "") {
      //do nothing
    } else {
      var pri = data["post_img_url"].split(':;');
      if (pri.length > 0) {
//        finalURL = "https://d212imxpbiy5j1.cloudfront.net/${pri[0]}";
        for (int i = 0; i < pri.length; i++) {
          images.add("https://d212imxpbiy5j1.cloudfront.net/${pri[i]}");
        }
      }
    }
    for (int i = 0; i < images.length; i++) {
      finalImages.add(new CachedNetworkImageProvider(images[i]));
    }

    var card = InkWell(
      onTap: () {
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => new NewsFeedDetails(data, user, jwt, likeClickedFunction: whenLikeClicked, isLiked: isLikeClicked, likeCount: likeCount, incrementCommentCounterFunction: incrementPostCommentCount, decreaseCommentCountFunction: decreaseCommentCount, postCommentCount: postCommentCount)));
      },
      child: Card(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(0.0),
              child: new ListTile(
                contentPadding: EdgeInsets.only(
                    left: 10.0, right: 4.0, top: 0.0, bottom: 0.0),
                leading: author["image"] != null
                    ? new CircleAvatar(
                  backgroundImage:
                  CachedNetworkImageProvider(author["image"]),
                  radius: 25.0,
                )
                    : new CircleAvatar(
                  backgroundImage: AssetImage("assets/nophoto.jpg"),
                  radius: 25.0,
                ),
                title: new Text(
                  author["username"],
                  style: new TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 16.0,
                  ),
                ),
                trailing: null,
                subtitle: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        new Text(
                          date,
                          style: new TextStyle(
                              color: Kinderm8Theme.Colors.darkGrey, fontSize: 11.0),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 5.0),
                          child: Row(
                            children: <Widget>[
                              roomTags(),
                              // Cover device width
                              new Flexible(
                                child: message.length < 30
                                    ? Text(" $message",
                                    style: new TextStyle(
                                        color: Kinderm8Theme.Colors.lightGrey,
                                        fontSize: 10.0))
                                    : Container(
                                  width: screenSize.width * 7 / 10,
                                  child: Text(
                                    " $message",
                                    style: new TextStyle(
                                        color: Kinderm8Theme.Colors.darkGrey
                                            .withOpacity(0.5),
                                        fontSize: 10.0),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )),
                isThreeLine: true,
              ),
            ),
            new Divider(
              height: 0.0,
              color: Colors.grey.shade500,
            ),
            new Container(
              padding: new EdgeInsets.only(
                  left: 5.0, top: 2.0, right: 2.0, bottom: 2.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                    child: new Text(
                      data["post_title"],
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: new TextStyle(
                        fontSize: 17.0,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                  Container(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: new Text(data["post_desc"],
                          maxLines: 4,
                          overflow: TextOverflow.ellipsis,
                          style: new TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w300,
                          ))),
                  Container(
                    height: 10.0,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: data["imagesnew"] != null
                        ? InkWell(
                        child: new SizedBox(
                            height: screenSize.width / 2,
                            width: screenSize.width,
                            child: new Carousel(
                              animationCurve: ElasticInCurve(10.0),
                              images: finalImages,
                              dotSize: 4.0,
                              dotSpacing: 15.0,
                              dotColor: Colors.white,
                              indicatorBgPadding: 5.0,
//                                dotBgColor: Colors.purple.withOpacity(0.5),
                              autoplay: false,
                              borderRadius: true,
                              radius: Radius.circular(5.0),
                            )),
                        onTap: () {
                          print("Have to Navigate image view..");

                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) =>
                                  new ZoomableImagePage(data)));
                        })
                        : data["videos"] != null
                        ? Column(
                      children: <Widget>[
                        new FlatButton(
//                              child: Text("${data["videos"]["urls"][0]}"),
                          child: Container(
                            height: screenSize.width * 2 / 3,
                            color: Colors.black,
                            child: Center(
                                child: Icon(Icons.play_circle_outline,
                                    size: 150.0,
                                    color: Colors.grey[300])
//                                  child: NetVideo("https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4"),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                    new VideoExample(
                                        data["videos"]["urls"]
                                        [0])));
                          },
                        ),
                      ],
                    )
                        : Container(),
                  ),
                  Container(
                    padding: EdgeInsets.all(2.0),
                    child: data["learningtagdetails"] != null
                        ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GestureDetector(
                          child: Container(
                              padding: EdgeInsets.only(left: 0.0),
//                                      width: ((screenSize.width * 3 / 8)),
                              child: Row(
                                children: <Widget>[
                                  Icon(FontAwesomeIcons.tags,
                                      size: 12.0,
                                      color: Colors.grey[600]),
                                  Container(width: 5.0),
                                  learningTags(),
                                  learningTag != null && learningTag.length > 5
                                      ? Text(
                                    " +${learningTag.length - 5}",
                                    style: new TextStyle(
                                        fontSize: 12.0),
                                  )
                                      : Text(""),
                                ],
                              )),
                          onTap: () {
                            print("have to navigte to Learning tags");
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                    new LearningTags(learningTag)));
                          },
                        ),
                        GestureDetector(
                          child: Container(
                              padding: EdgeInsets.all(3.0),
                              decoration: new BoxDecoration(
                                borderRadius:
                                new BorderRadius.circular(25.0),
                                border: new Border.all(
                                  width: 2.0,
                                  color: Kinderm8Theme.Colors.appcolour,
                                ),
                              ),
//                                      width: ((screenSize.width   / 5)),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(FontAwesomeIcons.listAlt,
                                      size: 12.0,
                                      color: Colors.grey[600]),
                                  Container(width: 6.0),
                                  Text("Learning Outcomes",
                                      style:
                                      new TextStyle(fontSize: 12.0)),
                                ],
                              )),
                          onTap: () {
                            print("have to navigte to Learning Outcomes");
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                    new LearningOutcomes(
                                        learningOutcomeDetail)));
                          },
                        ),
                      ],
                    )
                        : Container(),
                  ),
                ],
              ),
            ),
            new Container(
              child: new Row(
                children: <Widget>[
                  new Expanded(
                      child: likedUserData.length != 0
                          ? GestureDetector(
                        child: Container(
                            padding: EdgeInsets.only(left: 0.0),
//                                      width: ((screenSize.width * 3 / 8)),
                            child: Row(
                              children: <Widget>[
                                Container(width: 10.0),
                                likedUsersList(),
                                likedUserData.length > 5
                                    ? Text(
                                  " +${likedUserData.length - 5}",
                                  style:
                                  new TextStyle(fontSize: 12.0),
                                )
                                    : Text(""),
                              ],
                            )),
                        onTap: () {
                          print("have to navigte to likes user view");
                          Navigator.of(context).push(
                              new MaterialPageRoute<Null>(
                                  builder: (BuildContext context) =>
                                      Comment(data, user, jwt, 1)));
                        },
                      )
                          : Row(
                        children: <Widget>[
                          Container(
                            width: 15.0,
                          ),
                        ],
                      )),
                  likeHandler
                      ? IconButton(
                    icon: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        likeCount != 0
                            ? new Text(likeCount.toString())
                            : Text(""),
                        isLikeClicked == 1
                            ? new Icon(Icons.favorite, color: Colors.red)
                            : new Icon(Icons.favorite_border,
                            color: Colors.grey),
                      ],
                    ),
                    onPressed: () => whenLikeClicked(data),
                  )
                      : CupertinoActivityIndicator(),
                  IconButton(
                    onPressed: () {
                      Navigator.of(context).push(new MaterialPageRoute<Null>(
                          builder: (BuildContext context) =>
                              Comment(data, user, jwt, 0, commentCountFunction: incrementPostCommentCount, decreaseCommentCountFunction: decreaseCommentCount,)));
                    },
                    icon: new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        postCommentCount != 0
                            ? new Text("$postCommentCount")
                            : Text(""),
                        new Icon(
                          Icons.comment,
                          color: Colors.grey,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 6.0),
                ],
              ),
            ),
          ],
        ),
      ),
    );

    final container = new Container(
      margin: new EdgeInsets.all(5.0),
      child: card,
    );

    return container;
  }
}

//*******************

class ViewImage extends StatelessWidget {
  final data;
  ViewImage(this.data);

  @override
  Widget build(BuildContext context) {
    var finalURL;
    var images = List();
    var finalimages = List();

    if (data["post_img_url"] == null || data["post_img_url"] == "") {
    } else {
      var pri = data["post_img_url"].split(':;');
      if (pri.length > 0) {
        finalURL = "https://d212imxpbiy5j1.cloudfront.net/${pri[0]}";
        for (int i = 0; i < pri.length; i++) {
          images.add("https://d212imxpbiy5j1.cloudfront.net/${pri[i]}");
        }
        print(images);
      }
    }


    for (int i = 0; i < images.length; i++) {}
    var sizedBox = new SizedBox(
        height: 200.0,
        width: 350.0,
        child: new Carousel(
          images: [
            new NetworkImage(
                'https://cdn-images-1.medium.com/max/2000/1*GqdzzfB_BHorv7V2NV7Jgg.jpeg'),
            new NetworkImage(
                'https://cdn-images-1.medium.com/max/2000/1*wnIEgP1gNMrK5gZU7QS0-A.jpeg'),
            new ExactAssetImage("assets/images/LaunchImage.jpg")
          ],
          showIndicator: false,
          borderRadius: false,
          moveIndicatorFromBottom: 180.0,
          noRadiusForIndicator: true,
          overlayShadow: true,
          overlayShadowColors: Colors.white,
          overlayShadowSize: 0.7,
        ));

    print("Images $images");
    return new Container(
        child: new PhotoView(
      imageProvider: NetworkImage(finalURL),
      minScale: PhotoViewComputedScale.contained * 0.8,
      maxScale: 4.0,
    ));
  }
}

class CircleButton extends StatelessWidget {
  final GestureTapCallback onTap;
  final String iconData;
  final Decoration decoration;

  const CircleButton({Key key, this.onTap, this.iconData, this.decoration})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double size = 50.0;

    return new InkResponse(
      onTap: onTap,
      child: new Container(
        width: size,
        height: size,
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
        ),
        child: new Text(iconData),
//          iconData,
//          color: Colors.black,
//        ),
      ),
    );
  }
}
