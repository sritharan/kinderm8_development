import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar/flutter_calendar.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:intl/intl.dart';
import 'package:kinderm8/Theme.dart' as Kinderm8Theme;
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';

class DailyChart extends StatefulWidget {
  final ChildViewModal childData;
  final jwt;

  DailyChart(this.childData, this.jwt);

  @override
  DailyChartState createState() => DailyChartState();
}

class DailyChartState extends State<DailyChart> implements HomePageContract {
  var k, appuser, jwt, id, clientId;
  List data;
  var childId;
  bool isLoading = false;
  HomePagePresenter _presenter;
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var dailychartdate;
  var dailyChartCollection,
      foodcollection,
      bottlefeedcollection,
      nappycollection,
      restcollection,
      sunscreencollection;

  var selectedChild;
  var selectedChildId;

  bool enabled = false;
  bool expanded = false,
      foodexpanded = false,
      nappyexpanded = false,
      bottleexpanded = false,
      restexpanded = false,
      sunscreenexpanded = false,
      sleepCheck = false;

  DailyChartState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  @override
  void initState() {
    var date = new DateTime.now();
    fetchDailyChartData(date);
    super.initState();
  }

  void handleNewDate(date) {
    setState(() {
      isLoading = false;
    });
//    print("handleNewDate ${date}");
    var timeStamp = new DateFormat("dd-MM-yyyy");
    String formatdailychartdate = timeStamp.format(date);
//    print("handleNewDate>>formatdailychartdate ${formatdailychartdate}");
    fetchDailyChartData(date);
  }

  var progress = new ProgressBar(
    color: Kinderm8Theme.Colors.appcolour,
    containerColor: Kinderm8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  // 27-09-2018
  Future<String> fetchDailyChartData(date) async {
    var timeStamp = new DateFormat("dd-MM-yyyy");
    String formatdailychartdate = timeStamp.format(date);

    ///data from GET method
    print("data fetched");
    childId = widget.childData.id;
    String _dailyJournalUrl =
        'http://api.kinderm8.com.au/v2.1.1/getdailychartenteries/$childId?date=$formatdailychartdate&clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_dailyJournalUrl, headers: headers).then((response) {
      var dailyChartData;
      try {
        print(response.statusCode);
        dailyChartData = json.decode(response.body) ?? '';

        print('dailychartcollection UrlData $dailyChartCollection');
      } catch (e) {
        print('That string was null!');
      }

      if (response.statusCode == 200) {
        print('isLoading$isLoading');
        dailyChartCollection = dailyChartData;
        print('dailychartcollection UrlData $dailyChartCollection');
        setState(() {
          isLoading = true;
        });
      } else if (response.statusCode == 500 &&
          dailyChartData["errorType"] == 'ExpiredJwtException') {
        getRefreshToken(date);
      } else {
        fetchDailyChartData(date);
      }
    });
    return null;
  }

  getRefreshToken(date) {
    print("refreshing Token..");
    String _refreshTokenUrl =
        'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;
      if (date != null) {
        fetchDailyChartData(date);
      }
    });
  }

  Widget buildNappyCollectionListWidget() {
    Widget nappyChangeListHeader = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Column(
          children: <Widget>[
            CircleAvatar(
              foregroundColor: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
              backgroundImage: AssetImage("assets/iconsetpng/diaper.png"),
              radius: 12.0,
            ),
            Container(
              width: 2.0,
              height: 10.0,
              margin: EdgeInsets.only(top: 7.0),
              decoration: BoxDecoration(
                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
              ),
            ),
          ],
        ),
        SizedBox(width: 6.0),
        Text("Nappy Changes",
            style: TextStyle(fontSize: 18.0,
                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.5))),
      ],
    );

    if (dailyChartCollection != null
        && dailyChartCollection['dailyChartNappyList'] != null
        && dailyChartCollection['dailyChartNappyList'].length > 0) {
      List dailyChartNappyList = dailyChartCollection['dailyChartNappyList'];
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          nappyChangeListHeader,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: (dailyChartNappyList.length > 0)
                ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: dailyChartNappyList.map<Widget>((item) {
                if (item["dailyChartNappyList"] != null
                    && item["dailyChartNappyList"].length > 0) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: item["dailyChartNappyList"].map<Widget>((nappy) {
                      return Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                width: 10.0,
                                height: 18.0,
                                margin: EdgeInsets.only(top: 7.0),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Kinderm8Theme.Colors.appcolour
                                      .withOpacity(0.3),
                                ),
                              ),
                              Container(
                                width: 2.0,
                                height: 28.0,
                                margin: EdgeInsets.only(top: 7.0),
                                decoration: BoxDecoration(
                                  color: Kinderm8Theme.Colors.appcolour
                                      .withOpacity(0.2),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                (nappy["catagory"] != null)
                                    ? Text(
                                    nappy['catagory'],
                                    style: TextStyle(fontSize: 16.0)
                                ) : SizedBox(),
                                (nappy["trainingtype"] != null &&
                                    nappy["time"] != null)
                                    ? Text(
                                  '${nappy['trainingtype']} @ ${nappy['time']}',
                                  style: TextStyle(fontSize: 14.0),
                                ) : SizedBox(),
                                (nappy["note"] != null)
                                    ? Container(
                                    width: 150.0,
                                    child: Html(
                                        data: html2md.convert(nappy['note']))
                                )
                                    : SizedBox(),
                                (nappy["trainer"] != null)
                                    ? Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    new Icon(FontAwesomeIcons.userCircle,
                                        size: 12.0, color: Colors.grey),
                                    Text(
                                      nappy['trainer'],
                                      style: TextStyle(
                                        color: Colors.grey,
                                      ),
                                    )
                                  ],
                                ) : SizedBox(),
                              ],
                            ),
                          ),
                        ],
                      );
                    }).toList(),
                  );
                } else {
                  return SizedBox();
                }
              },
              ).toList(),
            ) : Text("No nappy changes for this day!"),
          ),
        ],
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          nappyChangeListHeader,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text("No nappy changes for this day!"),
          ),
        ],
      );
    }
  }

  Widget buildRestCollectionUIList() {
    final Size screenSize = MediaQuery.of(context).size;
    List dailyChartRestList = (dailyChartCollection != null
        && dailyChartCollection['dailyChartRestList'] != null
        && dailyChartCollection['dailyChartRestList'].length > 0) ? dailyChartCollection['dailyChartRestList'] : [];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Column(
              children: <Widget>[
                CircleAvatar(
                  foregroundColor: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
                  backgroundImage: AssetImage("assets/iconsetpng/alarm-clock.png"),
                  radius: 12.0,
                ),
                Container(
                  width: 2.0,
                  height: 10.0,
                  margin: EdgeInsets.only(top: 7.0),
                  decoration: BoxDecoration(
                    color: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
                  ),
                ),
              ],
            ),
            SizedBox(width: 6.0),
            Text("Rests",
                style: TextStyle(fontSize: 18.0,
                    color: Kinderm8Theme.Colors.appcolour.withOpacity(0.5))),
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: (dailyChartRestList.length > 0)
              ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: dailyChartRestList.map<Widget>((item) {
              return (item["dailyRestTimeList"] != null
                        && item["dailyRestTimeList"].length > 0) ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: item["dailyRestTimeList"].map<Widget>((rest) {
                        return Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Container(
                                  width: 10.0,
                                  height: 10.0,
                                  margin: EdgeInsets.only(top: 7.0),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
                                  ),
                                ),
                                Container(
                                  width: 2.0,
                                  height: 28.0,
                                  margin: EdgeInsets.only(top: 7.0),
                                  decoration: BoxDecoration(
                                    color: Kinderm8Theme.Colors.appcolour.withOpacity(0.2),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  (rest["start_time"] != null || rest["end_time"] != null)
                                      ? Text(
                                          '${rest['start_time']} ${rest['end_time']}',
                                          style: TextStyle(fontSize: 16.0)
                                      ) : SizedBox(),
                                  (rest["resttype"] != null)
                                      ? Container(
                                        width: screenSize.width / 1.7,
                                        child: Html(
                                          data: html2md.convert(rest['resttype']),
                                          defaultTextStyle: TextStyle(fontSize: 14.0),
                                        ),) : SizedBox(),
                                  (rest["trainer"] != null)
                                      ? Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      new Icon(FontAwesomeIcons.userCircle,
                                          size: 12.0, color: Colors.grey),
                                      Text(
                                        rest['trainer'],
                                        style: TextStyle(
                                          color: Colors.grey,
                                        ),
                                      )
                                    ],
                                  ) : SizedBox(),
                                ],
                              ),
                            ),
                          ],
                        );
                      }).toList(),
                    ) : Text("No rest for this day!");
            },
            ).toList(),)
              : new Text("No rest for this day!"),
        ),
      ],
    );
  }

  Widget buildBottleCollectionListWidget() {
    Widget bottleListHeader = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Column(
          children: <Widget>[
            CircleAvatar(
              foregroundColor: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
              backgroundImage: AssetImage("assets/iconsetpng/bottle-feed.png"),
              radius: 12.0,
            ),
            Container(
              width: 2.0,
              height: 10.0,
              margin: EdgeInsets.only(top: 7.0),
              decoration: BoxDecoration(
                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
              ),
            ),
          ],
        ),
        SizedBox(width: 6.0),
        Text("Bottle Collection",
            style: TextStyle(fontSize: 18.0,
                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.5))),
      ],
    );

    if (dailyChartCollection['dailyChartBottlefeedList'] != null && dailyChartCollection['dailyChartBottlefeedList'].length > 0) {
      List bottleFeedList = dailyChartCollection['dailyChartBottlefeedList'];
      return Column(
        children: <Widget>[
          bottleListHeader,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: (bottleFeedList.length > 0)
                ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: bottleFeedList.map<Widget>((item) {
                if (item["dailyChartBottlefeedList"] != null
                    && item["dailyChartBottlefeedList"].length > 0) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: item["dailyChartBottlefeedList"].map<Widget>((bottle) {
                      return Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                width: 10.0,
                                height: 18.0,
                                margin: EdgeInsets.only(top: 7.0),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
                                ),
                              ),
                              Container(
                                width: 2.0,
                                height: 36.0,
                                margin: EdgeInsets.only(top: 7.0),
                                decoration: BoxDecoration(
                                  color: Kinderm8Theme.Colors.appcolour.withOpacity(0.2),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                (bottle["bottletype"] != null)
                                    ? Text(
                                    bottle['bottletype'],
                                    style: TextStyle(fontSize: 16.0)
                                ) : SizedBox(),
                                (bottle["bottle_qty"] != null)
                                    ? Text(
                                  (bottle['bottle_qty'] == 1)
                                      ? '${bottle['bottle_qty']} bottle'
                                      : '${bottle['bottle_qty']} bottles',
                                  style: TextStyle(fontSize: 14.0),
                                ) : SizedBox(),
                                (bottle["bottle_time"] != null)
                                    ? Text(bottle['bottle_time'], style: TextStyle(fontSize: 14.0))
                                    : SizedBox(),
                                (bottle["trainer"] != null)
                                    ? Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    new Icon(FontAwesomeIcons.userCircle,
                                        size: 12.0, color: Colors.grey),
                                    Text(
                                      bottle['trainer'],
                                      style: TextStyle(
                                        color: Colors.grey,
                                      ),
                                    )
                                  ],
                                ) : SizedBox(),
                              ],
                            ),
                          ),
                        ],
                      );
                    }).toList(),
                  );
                } else {
                  return SizedBox();
                }
              },
              ).toList(),
            ) : Text("No bottle feed for this day!"),
          ),
        ],
      );
//        Row(
//        children: <Widget>[
//          Expanded(
//            child: ListView.builder(
//              physics: ClampingScrollPhysics(),
//              shrinkWrap: true,
//              itemCount: dailyChartCollection['dailyChartBottlefeedList'][0]
//                          ['dailyChartBottlefeedList'] !=
//                      null
//                  ? dailyChartCollection['dailyChartBottlefeedList'][0]
//                          ['dailyChartBottlefeedList']
//                      .length
//                  : 0,
//              itemBuilder: (context, index) {
//                return Card(
//                  elevation: 2.0,
//                  child: Column(
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    children: <Widget>[
//                      ListTile(
//                        subtitle: Text(
//                            dailyChartCollection['dailyChartBottlefeedList'][0]
//                                        ['dailyChartBottlefeedList'][index]
//                                    ['bottle_qty'] +
//                                ' (ml)',
//                            style: TextStyle()),
//                        title: Text(
//                            dailyChartCollection['dailyChartBottlefeedList'][0]
//                                    ['dailyChartBottlefeedList'][index]
//                                ['bottletype'],
//                            style: TextStyle()),
//                      ),
//                      Row(
//                        mainAxisAlignment: MainAxisAlignment.end,
//                        children: <Widget>[
//                          new Icon(FontAwesomeIcons.userCircle,
//                              size: 12.0, color: Colors.grey),
//                          Text(
//                            ' ' +
//                                dailyChartCollection['dailyChartBottlefeedList']
//                                        [0]['dailyChartBottlefeedList'][index]
//                                    ['trainer'],
//                            style: TextStyle(
//                              color: Kinderm8Theme.Colors.darkGrey,
//                            ),
//                          )
//                        ],
//                      ),
//                    ],
//                  ),
//                );
//              },
//            ),
//          )
//        ],
//      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          bottleListHeader,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text("No bottle feed for this day!"),
          ),
        ],
      );
    }
  }

  Widget buildSunScreenListWidget() {
    Widget sunScreenListHeader = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Column(
          children: <Widget>[
            CircleAvatar(
              foregroundColor: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
              backgroundImage: AssetImage("assets/iconsetpng/alarm-clock.png"),
              radius: 12.0,
            ),
            Container(
              width: 2.0,
              height: 10.0,
              margin: EdgeInsets.only(top: 7.0),
              decoration: BoxDecoration(
                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
              ),
            ),
          ],
        ),
        SizedBox(width: 6.0),
        Text("Sunscreen",
            style: TextStyle(fontSize: 18.0,
                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.5))),
      ],
    );

    if (dailyChartCollection['dailyChartSunScreenList'] != null && dailyChartCollection['dailyChartSunScreenList'].length > 0) {
      List dailyChartSunScreenList = dailyChartCollection['dailyChartSunScreenList'];
      return Column(
        children: <Widget>[
          sunScreenListHeader,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: (dailyChartSunScreenList.length > 0)
                ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: dailyChartSunScreenList.map<Widget>((item) {
                return (item["dailySunScreenLists"] != null
                    && item["dailySunScreenLists"].length > 0) ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: item["dailySunScreenLists"].map<Widget>((rest) {
                    return Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              width: 10.0,
                              height: 10.0,
                              margin: EdgeInsets.only(top: 7.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
                              ),
                            ),
                            Container(
                              width: 2.0,
                              height: 20.0,
                              margin: EdgeInsets.only(top: 7.0),
                              decoration: BoxDecoration(
                                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.2),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              (rest["category_name"] != null)
                                  ? Text(
                                  '${rest['category_name']}',
                                  style: TextStyle(fontSize: 16.0)
                              ) : SizedBox(),
                              (rest["educator_name"] != null)
                                  ? Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  new Icon(FontAwesomeIcons.userCircle,
                                      size: 12.0, color: Colors.grey),
                                  Text(
                                    rest['educator_name'],
                                    style: TextStyle(
                                      color: Colors.grey,
                                    ),
                                  )
                                ],
                              ) : SizedBox(),
                            ],
                          ),
                        ),
                      ],
                    );
                  }).toList(),
                ) : SizedBox();
              },
              ).toList(),)
                : new Text("No sunscreen for this day!"),
          ),
        ],
      );
//        Row(
//        children: <Widget>[
//          Expanded(
//            child: ListView.builder(
//              physics: ClampingScrollPhysics(),
//              shrinkWrap: true,
//              itemCount: dailyChartCollection['dailyChartSunScreenList'][0]
//                          ['dailySunScreenLists'] !=
//                      null
//                  ? dailyChartCollection['dailyChartSunScreenList'][0]
//                          ['dailySunScreenLists']
//                      .length
//                  : 0,
//              itemBuilder: (context, index) {
//                return Card(
//                  elevation: 2.0,
//                  child: Column(
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    children: <Widget>[
//                      ListTile(
//                        title: Text(
//                            dailyChartCollection['dailyChartSunScreenList'][0]
//                                ['dailySunScreenLists'][index]['category_name'],
//                            style: TextStyle()),
//                      ),
//                      Row(
//                        mainAxisAlignment: MainAxisAlignment.end,
//                        children: <Widget>[
//                          new Icon(FontAwesomeIcons.userCircle,
//                              size: 12.0, color: Colors.grey),
//                          Text(
//                            dailyChartCollection['dailyChartSunScreenList'][0]
//                                ['dailySunScreenLists'][index]['educator_name'],
//                            style: TextStyle(
//                              color: Kinderm8Theme.Colors.darkGrey,
//                            ),
//                          )
//                        ],
//                      ),
//                    ],
//                  ),
//                );
//              },
//            ),
//          )
//        ],
//      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          sunScreenListHeader,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text("No sunscreen for this day!"),
          ),
        ],
      );
    }
  }

  Widget buildSleepCheckListWidget() {
    Widget sleepCheckListHeader = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Column(
          children: <Widget>[
            CircleAvatar(
              foregroundColor: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
              backgroundImage: AssetImage("assets/iconsetpng/crib-toy.png"),
              radius: 12.0,
            ),
            Container(
              width: 2.0,
              height: 10.0,
              margin: EdgeInsets.only(top: 7.0),
              decoration: BoxDecoration(
                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
              ),
            ),
          ],
        ),
        SizedBox(width: 6.0),
        Text("Sleep Checklist",
            style: TextStyle(fontSize: 18.0,
                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.5))),
      ],
    );

    if (dailyChartCollection['dailyChartSleepCheckList'] != null && dailyChartCollection['dailyChartSleepCheckList'].length > 0) {//[0]['dailyChartSleepCheckList'].length > 0
      List sleepCheckList = dailyChartCollection['dailyChartSleepCheckList'];
      return Column(
        children: <Widget>[
          sleepCheckListHeader,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: (sleepCheckList.length > 0)
                ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: sleepCheckList.map<Widget>((item) {
                return (item["dailyChartSleepCheckList"] != null
                    && item["dailyChartSleepCheckList"].length > 0) ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: item["dailyChartSleepCheckList"].map<Widget>((rest) {
                    return Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              width: 10.0,
                              height: 10.0,
                              margin: EdgeInsets.only(top: 7.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
                              ),
                            ),
                            Container(
                              width: 2.0,
                              height: 25.0,
                              margin: EdgeInsets.only(top: 7.0),
                              decoration: BoxDecoration(
                                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.2),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              (rest["time"] != null)
                                  ? Text(
                                  '${rest['time']}',
                                  style: TextStyle(fontSize: 16.0)
                              ) : SizedBox(),
                              (rest["trainer"] != null)
                                  ? Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  new Icon(FontAwesomeIcons.userCircle,
                                      size: 12.0, color: Colors.grey),
                                  Text(
                                    rest['trainer'],
                                    style: TextStyle(
                                      color: Colors.grey,
                                    ),
                                  )
                                ],
                              ) : SizedBox(),
                            ],
                          ),
                        ),
                      ],
                    );
                  }).toList(),
                ) : SizedBox();
              },
              ).toList(),)
                : new Text("No rest for this day!"),
          ),
        ],
      );
//        Row(
//        children: <Widget>[
//          Expanded(
//            child: ListView.builder(
//              physics: ClampingScrollPhysics(),
//              shrinkWrap: true,
//              itemCount: dailyChartCollection['dailyChartSleepCheckList'][0]
//                          ['dailyChartSleepCheckList'] !=
//                      null
//                  ? dailyChartCollection['dailyChartSleepCheckList'][0]
//                          ['dailyChartSleepCheckList']
//                      .length
//                  : 0,
//              itemBuilder: (context, index) {
//                return Card(
//                  elevation: 2.0,
//                  child: Column(
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    children: <Widget>[
//                      ListTile(
//                        title: Text(
//                            dailyChartCollection['dailyChartSleepCheckList'][0]
//                                ['dailyChartSleepCheckList'][index]['time'],
//                            style: TextStyle()),
//                      ),
//                      Row(
//                        mainAxisAlignment: MainAxisAlignment.end,
//                        children: <Widget>[
//                          new Icon(FontAwesomeIcons.userCircle,
//                              size: 12.0, color: Colors.grey),
//                          Text(
//                            dailyChartCollection['dailyChartSleepCheckList'][0]
//                                ['dailyChartSleepCheckList'][index]['trainer'],
//                            style: TextStyle(
//                              color: Kinderm8Theme.Colors.darkGrey,
//                            ),
//                          )
//                        ],
//                      ),
//                    ],
//                  ),
//                );
//              },
//            ),
//          )
//        ],
//      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          sleepCheckListHeader,
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text("No sleep for this day!"),
          ),
        ],
      );
    }
  }

  Widget buildFoodListWidget() {
    final Size screenSize = MediaQuery.of(context).size;

    List dailyChartFoodList = (dailyChartCollection != null
        && dailyChartCollection['dailyChartFoodList'] != null
        && dailyChartCollection['dailyChartFoodList'].length > 0) ? dailyChartCollection['dailyChartFoodList'] : [];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Column(
              children: <Widget>[
                CircleAvatar(
                  foregroundColor: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
                  backgroundImage: AssetImage("assets/iconsetpng/groceries.png"),
                  radius: 12.0,
                ),
                Container(
                  width: 2.0,
                  height: 10.0,
                  margin: EdgeInsets.only(top: 7.0),
                  decoration: BoxDecoration(
                    color: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
                  ),
                ),
              ],
            ),
            SizedBox(width: 6.0),
            Text("Foods",
                style: TextStyle(fontSize: 20.0,
                  color: Kinderm8Theme.Colors.appcolour.withOpacity(0.5))),
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: (dailyChartFoodList.length > 0)
              ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: dailyChartFoodList.map<Widget>((item) {
              return (item["dailychartfoodlist"] != null
                    && item["dailychartfoodlist"].length > 0) ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: item["dailychartfoodlist"].map<Widget>((food) {
                    return Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              width: 10.0,
                              height: 10.0,
                              margin: EdgeInsets.only(top: 7.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
                              ),
                            ),
                            Container(
                              width: 2.0,
                              height: 32.0,
                              margin: EdgeInsets.only(top: 7.0),
                              decoration: BoxDecoration(
                                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.2),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                (food["MenuCategory"] != null)
                                  ? Text(
                                    food["MenuCategory"],
                                    style: TextStyle(fontSize: 18.0)
                                  ) : SizedBox(),
                                (food["Servetype"] != null)
                                  ? Text(
                                      '${food["Servetype"]}',
                                      style: TextStyle(fontSize: 14.0),
                                  ) : SizedBox(),
                                (food["FoodMenu"] != null)
                                  ? Container(
                                      width: screenSize.width / 1.4,
                                      child: Html(
                                        data: html2md.convert(food["FoodMenu"]),
                                        defaultTextStyle: TextStyle(fontSize: 12.0, color: Colors.grey),
                                      ),
                                    )
                                  : SizedBox(),
                            ],
                          ),
                        ),
                      ],
                    );
                  }).toList(),
//                                              (item['date'] != null)
//                                                  ? Text(
//                                                '${item['date']}',
//                                                textAlign: TextAlign.left,
//                                                style: TextStyle(fontSize: 10.0),
//                                              )
//                                                  : SizedBox(),
                ) : Text("No Foods available in this day!");
            },
            ).toList(),)
//                          new ExpansionPanelList(
//                                  expansionCallback: (i, bool val) {
//                                    setState(() {
//                                      foodexpanded = !val;
//                                    });
//                                  },
//                                  children: [
//                                    new ExpansionPanel(
//                                      body: new Container(
//                                          child: foodcollectionUIList()),
//                                      headerBuilder:
//                                          (BuildContext context, bool val) {
//                                        return new Center(
//                                          child: new ListTile(
//                                            leading: new CircleAvatar(
//                                              foregroundColor: Kinderm8Theme
//                                                  .Colors.appcolour,
//                                              backgroundImage: AssetImage(
//                                                  "assets/iconsetpng/groceries.png"),
//                                              radius: 20.0,
//                                            ),
//                                            title: dailychartcollection[
//                                                            'dailyChartFoodList']
//                                                        [0]['date'] !=
//                                                    null
//                                                ? Text(
//                                                    '${dailychartcollection['dailyChartFoodList'][0]['date']}',
//                                                    textAlign: TextAlign.left,
//                                                    style: TextStyle(
//                                                        fontSize: 20.0,
//                                                        color: Kinderm8Theme
//                                                            .Colors.appcolour),
//                                                  )
//                                                : Text(""),
//                                          ),
//                                        );
//                                      },
//                                      isExpanded: foodexpanded,
//                                    ),
//                                  ],
//                                )
              : new Text("No food usage for this day!"),
        ),
      ],
    );
  }

//  Widget buildFoodListWidget() {
//    List dailyChartFoodList = (dailyChartCollection != null
//        && dailyChartCollection['dailyChartFoodList'] != null
//        && dailyChartCollection['dailyChartFoodList'].length > 0) ? dailyChartCollection['dailyChartFoodList'] : [];
//    return Column(
//      crossAxisAlignment: CrossAxisAlignment.start,
//      children: <Widget>[
//        Row(
//          crossAxisAlignment: CrossAxisAlignment.start,
//          children: <Widget>[
//            Column(
//              children: <Widget>[
//                CircleAvatar(
//                  foregroundColor: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
//                  backgroundImage: AssetImage("assets/iconsetpng/groceries.png"),
//                  radius: 12.0,
//                ),
//                Container(
//                  width: 2.0,
//                  height: 10.0,
//                  margin: EdgeInsets.only(top: 7.0),
//                  decoration: BoxDecoration(
//                    color: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
//                  ),
//                ),
//              ],
//            ),
//            SizedBox(width: 6.0),
//            Text("Foods",
//                style: TextStyle(fontSize: 20.0,
//                  color: Kinderm8Theme.Colors.appcolour.withOpacity(0.5))),
//          ],
//        ),
//        Padding(
//          padding: const EdgeInsets.symmetric(horizontal: 8.0),
//          child: (dailyChartFoodList.length > 0)
//              ? Column(
//            crossAxisAlignment: CrossAxisAlignment.start,
//            children: dailyChartFoodList.map<Widget>((item) {
//              return (item["dailychartfoodlist"] != null
//                    && item["dailychartfoodlist"].length > 0) ? Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  children: item["dailychartfoodlist"].map<Widget>((food) {
//                    return Row(
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        Column(
//                          children: <Widget>[
//                            Container(
//                              width: 10.0,
//                              height: 10.0,
//                              margin: EdgeInsets.only(top: 7.0),
//                              decoration: BoxDecoration(
//                                shape: BoxShape.circle,
//                                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.3),
//                              ),
//                            ),
//                            Container(
//                              width: 2.0,
//                              height: 30.0,
//                              margin: EdgeInsets.only(top: 7.0),
//                              decoration: BoxDecoration(
//                                color: Kinderm8Theme.Colors.appcolour.withOpacity(0.2),
//                              ),
//                            ),
//                          ],
//                        ),
//                        Padding(
//                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
//                            child: Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                (food["MenuCategory"] != null)
//                                  ? Text(
//                                    food["MenuCategory"],
//                                    style: TextStyle(fontSize: 18.0)
//                                  ) : SizedBox(),
//                                (food["Servetype"] != null)
//                                  ? Text(
//                                      '${food["Servetype"]}',
//                                      style: TextStyle(fontSize: 14.0),
//                                  ) : SizedBox(),
//                                (food["FoodMenu"] != null)
//                                  ? Text(
//                                      food["FoodMenu"],
//                                      style: TextStyle(fontSize: 12.0, color: Colors.grey),
//                                    )
//                                  : SizedBox(),
//                            ],
//                          ),
//                        ),
//                      ],
//                    );
//                  }).toList(),
////                                              (item['date'] != null)
////                                                  ? Text(
////                                                '${item['date']}',
////                                                textAlign: TextAlign.left,
////                                                style: TextStyle(fontSize: 10.0),
////                                              )
////                                                  : SizedBox(),
//                ) : Text("No Foods available in this day!");
//            },
//            ).toList(),)
////                          new ExpansionPanelList(
////                                  expansionCallback: (i, bool val) {
////                                    setState(() {
////                                      foodexpanded = !val;
////                                    });
////                                  },
////                                  children: [
////                                    new ExpansionPanel(
////                                      body: new Container(
////                                          child: foodcollectionUIList()),
////                                      headerBuilder:
////                                          (BuildContext context, bool val) {
////                                        return new Center(
////                                          child: new ListTile(
////                                            leading: new CircleAvatar(
////                                              foregroundColor: Kinderm8Theme
////                                                  .Colors.appcolour,
////                                              backgroundImage: AssetImage(
////                                                  "assets/iconsetpng/groceries.png"),
////                                              radius: 20.0,
////                                            ),
////                                            title: dailychartcollection[
////                                                            'dailyChartFoodList']
////                                                        [0]['date'] !=
////                                                    null
////                                                ? Text(
////                                                    '${dailychartcollection['dailyChartFoodList'][0]['date']}',
////                                                    textAlign: TextAlign.left,
////                                                    style: TextStyle(
////                                                        fontSize: 20.0,
////                                                        color: Kinderm8Theme
////                                                            .Colors.appcolour),
////                                                  )
////                                                : Text(""),
////                                          ),
////                                        );
////                                      },
////                                      isExpanded: foodexpanded,
////                                    ),
////                                  ],
////                                )
//              : new Text("No food usage for this day!"),
//        ),
//      ],
//    );
//  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: scaffoldKey,
        appBar: new AppBar(
          title: new Text(labelsConfig["dailyChartLabel"],
              style: TextStyle(
                color: Colors.white,
              )),
          backgroundColor: Kinderm8Theme.Colors.appdarkcolour,
          centerTitle: true,
        ),
        body: new Container(
          margin: new EdgeInsets.symmetric(
            horizontal: 5.0,
            vertical: 10.0,
          ),
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              new Calendar(
                onSelectedRangeChange: (range) =>
                    print("Range is ${range.item1}, ${range.item2}"),
                onDateSelected: (date) => handleNewDate(date),
                isExpandable: true,
              ),
              new Divider(
                height: 50.0,
              ),
              // Daily Chart
              isLoading
                ? Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Column(
                    children: <Widget>[
                      buildFoodListWidget(),
                      SizedBox(height: 5.0),
                      buildNappyCollectionListWidget(),
                      SizedBox(height: 5.0),
                      buildRestCollectionUIList(),
                      SizedBox(height: 5.0),
                      buildSleepCheckListWidget(),
                      SizedBox(height: 5.0),
                      buildSunScreenListWidget(),
                      SizedBox(height: 5.0),
                      buildBottleCollectionListWidget(),
                    ],
                  ),
//
//                          /* Sleep and Rest */
//                          dailychartcollection['dailyChartRestList'].length > 0
//                              ? new ExpansionPanelList(
//                                  expansionCallback: (i, bool val) {
//                                    setState(() {
//                                      restexpanded = !val;
//                                    });
//                                  },
//                                  children: [
//                                    new ExpansionPanel(
//                                      body: new Container(
//                                          child: restcollectionUIList()),
//                                      headerBuilder:
//                                          (BuildContext context, bool val) {
//                                        return new Center(
//                                            child: new ListTile(
//                                                leading: new CircleAvatar(
//                                                  foregroundColor: Kinderm8Theme
//                                                      .Colors.appcolour,
//                                                  backgroundImage: AssetImage(
//                                                      "assets/iconsetpng/alarm-clock.png"),
//                                                  radius: 20.0,
//                                                ),
//                                                title: Text(
//                                                    '${dailychartcollection['dailyChartRestList'][0]['date']}',
//                                                    textAlign: TextAlign.left,
//                                                    style: TextStyle(
//                                                        fontSize: 20.0,
//                                                        color: Kinderm8Theme
//                                                            .Colors
//                                                            .appcolour))));
//                                      },
//                                      isExpanded: restexpanded,
//                                    ),
//                                  ],
//                                )
//                              : new Container(),
//
//                          /* Bottle Feed */
//                          dailychartcollection['dailyChartBottlefeedList']
//                                      .length >
//                                  0
//                              ? new ExpansionPanelList(
//                                  expansionCallback: (i, bool val) {
//                                    setState(() {
//                                      bottleexpanded = !val;
//                                    });
//                                  },
//                                  children: [
//                                    new ExpansionPanel(
//                                      body: new Container(
//                                          child: dailychartcollection[
//                                                          'dailyChartBottlefeedList']
//                                                      [0]['date'] !=
//                                                  null
//                                              ? bottlecollectionUIList()
//                                              : null),
//                                      headerBuilder:
//                                          (BuildContext context, bool val) {
//                                        return new Center(
//                                            child: new ListTile(
//                                                leading: new CircleAvatar(
//                                                  foregroundColor: Kinderm8Theme
//                                                      .Colors.appcolour,
//                                                  backgroundImage: AssetImage(
//                                                      "assets/iconsetpng/bottle-feed.png"),
//                                                  radius: 20.0,
//                                                ),
//                                                title: dailychartcollection[
//                                                                'dailyChartBottlefeedList']
//                                                            [0]['date'] !=
//                                                        null
//                                                    ? Text(
//                                                        '${dailychartcollection['dailyChartBottlefeedList'][0]['date']}',
//                                                        textAlign:
//                                                            TextAlign.left,
//                                                        style: TextStyle(
//                                                            fontSize: 20.0,
//                                                            color: Kinderm8Theme
//                                                                .Colors
//                                                                .appcolour),
//                                                      )
//                                                    : Text("")));
//                                      },
//                                      isExpanded: bottleexpanded,
//                                    ),
//                                  ],
//                                )
//                              : new Container(),
//
//                          /* SunScreen */
//                          dailychartcollection['dailyChartSunScreenList']
//                                      .length >
//                                  0
//                              ? new ExpansionPanelList(
//                                  expansionCallback: (i, bool val) {
//                                    setState(() {
//                                      sunscreenexpanded = !val;
//                                    });
//                                  },
//                                  children: [
//                                    new ExpansionPanel(
//                                      body: new Container(
//                                          child: dailychartcollection[
//                                                          'dailyChartSunScreenList']
//                                                      [0]['date'] !=
//                                                  null
//                                              ? suscreencollectionUIList()
//                                              : Container()),
//                                      headerBuilder:
//                                          (BuildContext context, bool val) {
//                                        return new Center(
//                                            child: new ListTile(
//                                                leading: new CircleAvatar(
//                                                  foregroundColor: Kinderm8Theme
//                                                      .Colors.appcolour,
//                                                  backgroundImage: AssetImage(
//                                                      "assets/iconsetpng/sunscreen.png"),
//                                                  radius: 20.0,
//                                                ),
//                                                title: dailychartcollection[
//                                                                'dailyChartSunScreenList']
//                                                            [0]['date'] !=
//                                                        null
//                                                    ? Text(
//                                                        '${dailychartcollection['dailyChartSunScreenList'][0]['date']}',
//                                                        textAlign:
//                                                            TextAlign.left,
//                                                        style: TextStyle(
//                                                            fontSize: 20.0,
//                                                            color: Kinderm8Theme
//                                                                .Colors
//                                                                .appcolour),
//                                                      )
//                                                    : Text("")));
//                                      },
//                                      isExpanded: sunscreenexpanded,
//                                    ),
//                                  ],
//                                )
//                              : new Container(),
//
//                          /* Sleep Check */
//                          dailychartcollection['dailyChartSleepCheckList']
//                                      .length >
//                                  0
//                              ? new ExpansionPanelList(
//                                  expansionCallback: (i, bool val) {
//                                    setState(() {
//                                      sleepCheck = !val;
//                                    });
//                                  },
//                                  children: [
//                                    new ExpansionPanel(
//                                      body: new Container(
//                                          child: dailychartcollection[
//                                                          'dailyChartSleepCheckList']
//                                                      [0]['date'] !=
//                                                  null
//                                              ? sleepCheckCollectionUIList()
//                                              : Container()),
//                                      headerBuilder:
//                                          (BuildContext context, bool val) {
//                                        return new Center(
//                                            child: new ListTile(
//                                                leading: new CircleAvatar(
//                                                  foregroundColor: Kinderm8Theme
//                                                      .Colors.appcolour,
//                                                  backgroundImage: AssetImage(
//                                                      "assets/iconsetpng/sunscreen.png"),
//                                                  radius: 20.0,
//                                                ),
//                                                title: dailychartcollection[
//                                                                'dailyChartSleepCheckList']
//                                                            [0]['date'] !=
//                                                        null
//                                                    ? Text(
//                                                        '${dailychartcollection['dailyChartSleepCheckList'][0]['date']}',
//                                                        textAlign:
//                                                            TextAlign.left,
//                                                        style: TextStyle(
//                                                            fontSize: 20.0,
//                                                            color: Kinderm8Theme
//                                                                .Colors
//                                                                .appcolour),
//                                                      )
//                                                    : Text("")));
//                                      },
//                                      isExpanded: sleepCheck,
//                                    ),
//                                  ],
//                                )
//                              : new Container(),
//                        ],
//                      ),
                    )
                  : progress
            ],
          ),
        ));
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      jwt = widget.jwt.toString();
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }

    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
