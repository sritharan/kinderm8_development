import 'dart:math';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kinderm8/Theme.dart' as Kinderm8Theme;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/pages/calendar/detailevent.dart';
import 'package:timeago/timeago.dart';
import 'dart:ui' as ui;

class DailyChartData extends StatefulWidget {

  final dailychartData;
  int i;
  final jwt;
  DailyChartData(this.dailychartData, this.jwt);

  @override
  DailyChartDataState createState() {
    return new DailyChartDataState(dailychartData);
  }

}
class DailyChartDataState extends State<DailyChartData>{
  final dailychartData;

  DailyChartDataState(this.dailychartData);

  var dailychartcollection,foodcollection,bottlefeedcollection,nappycollection,restcollection,sunscreencollection;

  bool enabled = false;
  bool expanded = false, foodexpanded = false, nappyexpanded = false,bottleexpanded = false, restexpanded = false,sunscreenexpanded = false;

  // https://stackoverflow.com/questions/52251853/building-and-sorting-a-new-list-based-on-provided-list-and-parameters/52252059
  TimeAgo timeAgoo = new TimeAgo();

  @override
  Widget build(BuildContext context) {
    print('singledailychartdata $dailychartData');
    dailychartcollection = dailychartData;
    print("inside build ${dailychartcollection}");
//    foodcollection = dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'];
//    nappycollection = dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList'];
//    bottlefeedcollection = dailychartcollection['dailyChartBottlefeedList'][0]['dailyChartBottlefeedList'];
//    restcollection = dailychartcollection['dailyChartRestList'][0]['dailyRestTimeList'];
//    sunscreencollection = dailychartcollection['dailyChartSunScreenList'][0]['dailySunScreenLists'];

//    print("journalcollection $journalcollection");
//    print("journeycollection $journeycollection");
//    print("learningstorycollection $learningstorycollection");
//    print("medicationcollection $medicationcollection");
//    print("imagescollection $imagescollection");
//    print("dailychartcollection $dailychartcollection");
//    print("childnotecollection $childnotecollection");


    print(dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist']);
    print(dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList']);
    print(dailychartcollection['dailyChartBottlefeedList'][0]['dailyChartBottlefeedList']);
    print(dailychartcollection['dailyChartRestList'][0]['dailyRestTimeList']);
    print(dailychartcollection['dailyChartSunScreenList'][0]['dailySunScreenLists']);

    /* ---------------- */
    /* Food collection list */
    /* --------------- */
    foodcollectionUIList() {
      final Size screenSize = MediaQuery.of(context).size;
//    print('foodcollectionUIList${dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist']}');
//    print('foodcollectionUIList length${dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'].length}');
      var fooddata = dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'];
      if (dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'].length > 1) {
        return Row(
          children: <Widget>[
            Flexible(
              child: new Container(
//              margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 10.0),
//              padding: EdgeInsets.only(top:10.0),
                height: screenSize.width,
                child: ListView.builder(
                  itemCount: dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'] != null
                      ? dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'].length
                      : 0,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    return Container(
                      child: new Column(
                        children: <Widget>[

                          Card
                            (
                            elevation: 5.0,
                            color: Colors.white,
//                          borderRadius: BorderRadius.only
//                            (
//                            topRight: Radius.circular(20.0),
//                            bottomLeft: Radius.circular(20.0),
//                            bottomRight: Radius.circular(20.0),
//                          ),
                            child: Container
                              (
//                            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
                              child: Container
                                (
                                child: ListTile
                                  (
//                                leading: CircleAvatar
//                                  (
//                                  backgroundColor: Colors.purple,
//                                  child: new Image.asset("assets/iconsetpng/cubes.png")
//                                ),
                                  title: Text(dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'][index]['MenuCategory']+' - '+dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'][index]['FoodMenu'], style: TextStyle()),
                                  subtitle: Text(dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'][index]['Servetype'], style: TextStyle()),

                                ),
                              ),
                            ),
                          ),
//                        new Text(
//                            dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'][index]['MenuCategory'] +
//                                '-' +
//                                dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'][index]['FoodMenu'],style: new TextStyle(
//                        color: Colors.lightBlueAccent, fontSize: 22.0))
                        ],
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        );
      }
    }

    /* ---------------- */
    /* Nappy collection list */
    /* --------------- */
    nappycollectionUIList() {
      final Size screenSize = MediaQuery.of(context).size;
      print('nappycollection $nappycollection');
      print(dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList']);
//    print('nappycollectionUIList${dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList']}');
//    print('nappycollectionUIList length${dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList'].length}');
      var nappydata = dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList'];
      if (dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList'].length > 1) {
        return Row(
          children: <Widget>[
            Flexible(
              child: new Container(
//              margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 10.0),
//              padding: EdgeInsets.only(top:10.0),
                height: screenSize.width,
                child: ListView.builder(
                  itemCount: dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList'] != null
                      ? dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList'].length
                      : 0,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    return Container(
                      child: new Column(
                        children: <Widget>[

                          Card
                            (
                            elevation: 5.0,
                            color: Colors.white,
//                          borderRadius: BorderRadius.only
//                            (
//                            topRight: Radius.circular(20.0),
//                            bottomLeft: Radius.circular(20.0),
//                            bottomRight: Radius.circular(20.0),
//                          ),
                            child: Container
                              (
//                            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
                              child: Container
                                (
                                  child: Row(
                                    children: <Widget>[
                                      ListTile
                                        (
//                                leading: CircleAvatar
//                                  (
//                                  backgroundColor: Colors.purple,
//                                  child: new Image.asset("assets/iconsetpng/cubes.png")
//                                ),
                                        title: Text(dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList'][index]['catagory'], style: TextStyle()),
                                        subtitle: Text(dailychartcollection['dailyChartNappyList'][0]['dailychartfoodlist'][index]['traingingtype'] +' @ '+ dailychartcollection['dailyChartNappyList'][0]['dailychartfoodlist'][index]['time'], style: TextStyle()),

                                      ),
                                      new Text(dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList'][index]['note']),
                                      Row(
                                        children: <Widget>[
                                          new Icon(FontAwesomeIcons.userCircle,
                                              size: 12.0, color: Colors.grey),
                                          Text(
                                            dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList'][index]['trainer'],
                                            style: TextStyle(
                                              color: Kinderm8Theme.Colors.darkGrey,
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  )

                              ),
                            ),
                          ),
//                        new Text(
//                            dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'][index]['MenuCategory'] +
//                                '-' +
//                                dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'][index]['FoodMenu'],style: new TextStyle(
//                        color: Colors.lightBlueAccent, fontSize: 22.0))
                        ],
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        );
      }
    }

    /* ---------------- */
    /* Sleep and rest collection list */
    /* --------------- */
    restcollectionUIList() {
      final Size screenSize = MediaQuery.of(context).size;
      print('restcollection $restcollection');
      print('length ${dailychartcollection['dailyChartSleepCheckList'][0]['dailyChartSleepCheckList'].length}');
//    print('nappycollectionUIList${dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList']}');
//    print('nappycollectionUIList length${dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList'].length}');
      var nappydata = dailychartcollection['dailyChartSleepCheckList'][0]['dailyChartSleepCheckList'];
      if (dailychartcollection['dailyChartSleepCheckList'][0]['dailyChartSleepCheckList'].length > 1) {
        return Row(
          children: <Widget>[
            Flexible(
              child: new Container(
//              margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 10.0),
//              padding: EdgeInsets.only(top:10.0),
                height: screenSize.width,
                child: ListView.builder(
                  itemCount: dailychartcollection['dailyChartSleepCheckList'][0]['dailyChartSleepCheckList'] != null
                      ? dailychartcollection['dailyChartSleepCheckList'][0]['dailyChartSleepCheckList'].length
                      : 0,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    return Container(
                      child: new Column(
                        children: <Widget>[

                          Card
                            (
                            elevation: 5.0,
                            color: Colors.white,
//                          borderRadius: BorderRadius.only
//                            (
//                            topRight: Radius.circular(20.0),
//                            bottomLeft: Radius.circular(20.0),
//                            bottomRight: Radius.circular(20.0),
//                          ),
                            child: Container
                              (
//                            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
                              child: Container
                                (
                                  child: Row(
                                    children: <Widget>[
                                      ListTile
                                        (
//                                leading: CircleAvatar
//                                  (
//                                  backgroundColor: Colors.purple,
//                                  child: new Image.asset("assets/iconsetpng/cubes.png")
//                                ),
                                        title: Text(dailychartcollection['dailyChartSleepCheckList'][0]['dailyChartRestList'][index]['time'], style: TextStyle(color: Kinderm8Theme.Colors.darkGrey)),
                                        subtitle: Text(dailychartcollection['dailyChartSleepCheckList'][0]['dailyChartRestList'][index]['resttype'], style: TextStyle()),

                                      ),
//                                    new Text(restcollection['dailyChartRestList'][index]['note']),
                                      Row(
                                        children: <Widget>[
                                          new Icon(FontAwesomeIcons.userCircle,
                                              size: 12.0, color: Colors.grey),
                                          Text(
                                            dailychartcollection['dailyChartSleepCheckList'][0]['dailyChartRestList'][index]['trainer'],
                                            style: TextStyle(
                                              color: Kinderm8Theme.Colors.darkGrey,
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  )

                              ),
                            ),
                          ),
//                        new Text(
//                            dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'][index]['MenuCategory'] +
//                                '-' +
//                                dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'][index]['FoodMenu'],style: new TextStyle(
//                        color: Colors.lightBlueAccent, fontSize: 22.0))
                        ],
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        );
      }
    }

    /* ---------------- */
    /* Bottlefeed collection list */
    /* --------------- */
    bottlecollectionUIList() {
      final Size screenSize = MediaQuery.of(context).size;
      print('bottlecollection $bottlefeedcollection');
//    print('nappycollectionUIList${dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList']}');
//    print('nappycollectionUIList length${dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList'].length}');
      var nappydata = dailychartcollection['dailyChartBottlefeedList'][0]['dailyChartBottlefeedList'];
      if (dailychartcollection['dailyChartBottlefeedList'][0]['dailyChartBottlefeedList'].length > 1) {
        return Row(
          children: <Widget>[
            Flexible(
              child: new Container(
//              margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 10.0),
//              padding: EdgeInsets.only(top:10.0),
                height: screenSize.width,
                child: ListView.builder(
                  itemCount: dailychartcollection['dailyChartBottlefeedList'][0]['dailyChartBottlefeedList'] != null
                      ? dailychartcollection['dailyChartBottlefeedList'][0]['dailyChartBottlefeedList'].length
                      : 0,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    return Container(
                      child: new Column(
                        children: <Widget>[

                          Card
                            (
                            elevation: 5.0,
                            color: Colors.white,
//                          borderRadius: BorderRadius.only
//                            (
//                            topRight: Radius.circular(20.0),
//                            bottomLeft: Radius.circular(20.0),
//                            bottomRight: Radius.circular(20.0),
//                          ),
                            child: Container
                              (
//                            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
                              child: Container
                                (
                                  child: Row(
                                    children: <Widget>[
                                      ListTile
                                        (
//                                leading: CircleAvatar
//                                  (
//                                  backgroundColor: Colors.purple,
//                                  child: new Image.asset("assets/iconsetpng/cubes.png")
//                                ),
                                        title: Text(dailychartcollection['dailyChartBottlefeedList'][0]['dailyChartBottlefeedList'][index]['bottle_qty']+' (ml)', style: TextStyle()),
                                        subtitle: Text(dailychartcollection['dailyChartBottlefeedList'][0]['dailyChartBottlefeedList'][index]['bottletype'], style: TextStyle()),

                                      ),
//                                    new Text(restcollection['dailyChartRestList'][index]['note']),
                                      Row(
                                        children: <Widget>[
                                          new Icon(FontAwesomeIcons.userCircle,
                                              size: 12.0, color: Colors.grey),
                                          Text(
                                            dailychartcollection['dailyChartBottlefeedList'][0]['dailyChartRestList'][index]['trainer'],
                                            style: TextStyle(
                                              color: Kinderm8Theme.Colors.darkGrey,
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  )

                              ),
                            ),
                          ),
//                        new Text(
//                            dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'][index]['MenuCategory'] +
//                                '-' +
//                                dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'][index]['FoodMenu'],style: new TextStyle(
//                        color: Colors.lightBlueAccent, fontSize: 22.0))
                        ],
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        );
      }
    }

    /* ---------------- */
    /* Sunscreen collection list */
    /* --------------- */
    suscreencollectionUIList() {
      final Size screenSize = MediaQuery.of(context).size;
      print('restcollection $restcollection');
//    print('nappycollectionUIList${dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList']}');
//    print('nappycollectionUIList length${dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList'].length}');
      var nappydata = dailychartcollection['dailyChartSunScreenList'][0]['dailySunScreenLists'];
      if (dailychartcollection['dailyChartSunScreenList'][0]['dailySunScreenLists'].length > 1) {
        return Row(
          children: <Widget>[
            Flexible(
              child: new Container(
//              margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 10.0),
//              padding: EdgeInsets.only(top:10.0),
                height: screenSize.width,
                child: ListView.builder(
                  itemCount: dailychartcollection['dailyChartSunScreenList'][0]['dailySunScreenLists'] != null
                      ? dailychartcollection['dailyChartSunScreenList'][0]['dailySunScreenLists'].length
                      : 0,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    return Container(
                      child: new Column(
                        children: <Widget>[

                          Card
                            (
                            elevation: 5.0,
                            color: Colors.white,
//                          borderRadius: BorderRadius.only
//                            (
//                            topRight: Radius.circular(20.0),
//                            bottomLeft: Radius.circular(20.0),
//                            bottomRight: Radius.circular(20.0),
//                          ),
                            child: Container
                              (
//                            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
                              child: Container
                                (
                                  child: Row(
                                    children: <Widget>[
                                      ListTile
                                        (
//                                leading: CircleAvatar
//                                  (
//                                  backgroundColor: Colors.purple,
//                                  child: new Image.asset("assets/iconsetpng/cubes.png")
//                                ),
                                        title: Text(dailychartcollection['dailyChartSunScreenList'][0]['dailySunScreenLists'][index]['category_name'], style: TextStyle()),
//                                      subtitle: Text(dailychartcollection['dailyChartSunScreenList'][0]['dailySunScreenLists'][index]['category_name'], style: TextStyle()),

                                      ),
                                      new Text(dailychartcollection['dailyChartSunScreenList'][0]['dailySunScreenLists'][index]['note']),
                                      Row(
                                        children: <Widget>[
                                          new Icon(FontAwesomeIcons.userCircle,
                                              size: 12.0, color: Colors.grey),
                                          Text(
                                            dailychartcollection['dailyChartSunScreenList'][0]['dailySunScreenLists'][index]['educator_name'],
                                            style: TextStyle(
                                              color: Kinderm8Theme.Colors.darkGrey,
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  )

                              ),
                            ),
                          ),
//                        new Text(
//                            dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'][index]['MenuCategory'] +
//                                '-' +
//                                dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist'][index]['FoodMenu'],style: new TextStyle(
//                        color: Colors.lightBlueAccent, fontSize: 22.0))
                        ],
                      ),
                    );
                  },
                ),
              ),
            )
          ],
        );
      }
    }

    var S3URL = "https://d212imxpbiy5j1.cloudfront.net/";

    var EventStartDate = DateTime.parse(dailychartData["date"]);
    var EventEndDate = DateTime.parse(dailychartData["to_date"]);

    /* // Format */
    var timeStamp = new DateFormat("d MMM");
    String formatEventStartDate = timeStamp.format(EventStartDate);
    String formatEventEndDate = timeStamp.format(EventEndDate);

    final difference = EventStartDate.difference(EventEndDate).inDays;

    DateTime dob = DateTime.parse(dailychartData["date"]);
    Duration dur =  DateTime.now().difference(dob);
    String differenceInYears = (dur.inDays/365).floor().toString();
    var t = new Text(differenceInYears + ' years');
    print(t);
    String readTimestamp(int timestamp) {
      var now = new DateTime.now();
      var format = new DateFormat('HH:mm a');
      var date = new DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
      var diff = now.difference(date);
      var time = '';

      if (diff.inSeconds <= 0 ||
          diff.inSeconds > 0 && diff.inMinutes == 0 ||
          diff.inMinutes > 0 && diff.inHours == 0 ||
          diff.inHours > 0 && diff.inDays == 0) {
        time = format.format(date);
      } else if (diff.inDays > 0 && diff.inDays < 7) {
        if (diff.inDays == 1) {
          time = diff.inDays.toString() + ' DAY AGO';
        } else {
          time = diff.inDays.toString() + ' DAYS AGO';
        }
      } else {
        if (diff.inDays == 7) {
          time = (diff.inDays / 7).floor().toString() + ' WEEK AGO';
        } else {
          time = (diff.inDays / 7).floor().toString() + ' WEEKS AGO';
        }
      }

      return time;
    }


    return new Card(
      elevation: 1.0,
      color: Colors.white,
      margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 14.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text('data'),
          /* Food */
//          dailychartcollection['dailyChartFoodList'][0].length != null?
//          new ExpansionPanelList(
//            expansionCallback: (i, bool val) {
//              setState(() {
//                foodexpanded = !val;
//              });
//            },
//            children: [
//              new ExpansionPanel(
//                body: new Container(
//                    padding: EdgeInsets.all(5.0),
//                    child: foodcollectionUIList()),
//                headerBuilder: (BuildContext context, bool val) {
//                  return new Center(
//                      child: new ListTile(
//                          leading: new CircleAvatar(
//                            foregroundColor:Kinderm8Theme.Colors.appcolour,
//                            backgroundImage: AssetImage(
//                                "assets/iconsetpng/groceries.png"),
//                            radius: 20.0,
//                          ),
////                          title: dailychartcollection['dailyChartFoodList'][0] != null? Text(
////                            '${dailychartcollection['dailyChartFoodList'][0]['date']}',
////                            textAlign: TextAlign.left,
////                            style: TextStyle(fontSize: 20.0,color: Kinderm8Theme.Colors.appcolour),
////                          ): ''
//                      )
//                  );
//                },
//                isExpanded: foodexpanded,
//              ),
//            ],
//          )
//              : null,
//
//          /* Nappy */
//          dailychartcollection['dailyChartNappyList'][0] != null ?
//          new ExpansionPanelList(
//            expansionCallback: (i, bool val) {
//              setState(() {
//                nappyexpanded = !val;
//              });
//            },
//            children: [
//              new ExpansionPanel(
//                body: new Container(
//                    padding: EdgeInsets.all(20.0),
//                    child: nappycollectionUIList()),
//                headerBuilder: (BuildContext context, bool val) {
//                  return new Center(
//                      child: new ListTile(
//                          leading: new CircleAvatar(
//                            foregroundColor:Kinderm8Theme.Colors.appcolour,
//                            backgroundImage: AssetImage(
//                                "assets/iconsetpng/crib.png"),
//                            radius: 20.0,
//                          ),
//                          title: dailychartcollection['dailyChartNappyList'][0] != null? Text(
//                            '${dailychartcollection['dailyChartNappyList'][0]['date']}',
//                            textAlign: TextAlign.left,
//                            style: TextStyle(fontSize: 20.0,color: Kinderm8Theme.Colors.appcolour),
//                          ): ''
//                      )
//                  );
//                },
//                isExpanded: nappyexpanded,
//              ),
//            ],
//          )
//              : null,
//
//          /* Sleep and Rest */
//          dailychartcollection['dailyChartRestList'][0] != null ?
//          new ExpansionPanelList(
//            expansionCallback: (i, bool val) {
//              setState(() {
//                restexpanded = !val;
//              });
//            },
//            children: [
//              new ExpansionPanel(
//                body: new Container(
//                    padding: EdgeInsets.all(20.0),
//                    child: dailychartcollection['dailyChartRestList'][0]['date'] != null ? restcollectionUIList() : null),
//                headerBuilder: (BuildContext context, bool val) {
//                  return new Center(
//                      child: new ListTile(
//                          leading: new CircleAvatar(
//                            foregroundColor:Kinderm8Theme.Colors.appcolour,
//                            backgroundImage: AssetImage(
//                                "assets/iconsetpng/alarm-clock.png"),
//                            radius: 20.0,
//                          ),
//                          title: dailychartcollection['dailyChartRestList'][0]['date'] != null? Text(
//                            '${dailychartcollection['dailyChartRestList'][0]['date']}',
//                            textAlign: TextAlign.left,
//                            style: TextStyle(fontSize: 20.0,color: Kinderm8Theme.Colors.appcolour),
//                          ): ''
//                      )
//                  );
//                },
//                isExpanded: restexpanded,
//              ),
//            ],
//          )
//              : null,
//
//          /* Bottle Feed */
//          dailychartcollection['dailyChartNappyList'][0] != null ?
//          new ExpansionPanelList(
//            expansionCallback: (i, bool val) {
//              setState(() {
//                bottleexpanded = !val;
//              });
//            },
//            children: [
//              new ExpansionPanel(
//                body: new Container(
//                    padding: EdgeInsets.all(20.0),
//                    child: dailychartcollection['dailyChartSleepCheckList'][0]['date'] != null ? bottlecollectionUIList() : null),
//                headerBuilder: (BuildContext context, bool val) {
//                  return new Center(
//                      child: new ListTile(
//                          leading: new CircleAvatar(
//                            foregroundColor:Kinderm8Theme.Colors.appcolour,
//                            backgroundImage: AssetImage(
//                                "assets/iconsetpng/bottle-feed.png"),
//                            radius: 20.0,
//                          ),
//                          title: dailychartcollection['dailyChartRestList'][0]['date'] != null? Text(
//                            '${dailychartcollection['dailyChartRestList'][0]['date']}',
//                            textAlign: TextAlign.left,
//                            style: TextStyle(fontSize: 20.0,color: Kinderm8Theme.Colors.appcolour),
//                          ): ''
//                      )
//                  );
//                },
//                isExpanded: bottleexpanded,
//              ),
//            ],
//          )
//              : null,
//
//          /* SunScreen */
//          dailychartcollection['dailyChartSunScreenList'][0] != null ?
//          new ExpansionPanelList(
//            expansionCallback: (i, bool val) {
//              setState(() {
//                sunscreenexpanded = !val;
//              });
//            },
//            children: [
//              new ExpansionPanel(
//                body: new Container(
//                    padding: EdgeInsets.all(20.0),
//                    child: dailychartcollection['dailyChartSunScreenList'][0]['date'] != null ? suscreencollectionUIList() : null),
//                headerBuilder: (BuildContext context, bool val) {
//                  return new Center(
//                      child: new ListTile(
//                          leading: new CircleAvatar(
//                            foregroundColor:Kinderm8Theme.Colors.appcolour,
//                            backgroundImage: AssetImage(
//                                "assets/iconsetpng/sunscreen.png"),
//                            radius: 20.0,
//                          ),
//                          title: dailychartcollection['dailyChartSunScreenList'][0]['date'] != null? Text(
//                            '${dailychartcollection['dailyChartSunScreenList'][0]['date']}',
//                            textAlign: TextAlign.left,
//                            style: TextStyle(fontSize: 20.0,color: Kinderm8Theme.Colors.appcolour),
//                          ): ''
//                      )
//                  );
//                },
//                isExpanded: sunscreenexpanded,
//              ),
//            ],
//          )
//              : null,

        ],
      ),
    );


  }
}
