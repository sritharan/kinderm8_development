import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/utils/commonutils/emptybody.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';

class CentreNotesList extends StatefulWidget {
  final ChildViewModal childData;
  final jwt;
  CentreNotesList(this.childData, this.jwt);
  @override
  CentreNotesListState createState() => CentreNotesListState();
}

class CentreNotesListState extends State<CentreNotesList>
    implements HomePageContract {
  var k, appuser, jwt, id, clientId;
  List data;
  var childId;
  bool isLoading;
  bool load = true;
  var centreNotesData;
  HomePagePresenter _presenter;
  CentreNotesListState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  bool submit = true;
  bool delete = false;
  final formKey = new GlobalKey<FormState>();
  final TextEditingController _textController = new TextEditingController();

//  var centreNotesId, centreNotesData;
  var deleteId;
//  var userDetails, commentUserId;

  void displayModal(Size screenSize, centreNotesData, int i) {
    showModalBottomSheet<void>(
        context: context,
        builder:
            (BuildContext context) {
          return Container(
              height:
              screenSize.width /
                  2.2,
              width: screenSize.width,
              child: Padding(
                padding:
                const EdgeInsets
                    .all(32.0),
                child: Column(
                  children: <Widget>[
                    new Padding(
                      padding:
                      const EdgeInsets
                          .all(
                          2.0),
                      child: new Text(
                        "Do you wish to delete",
                        style: new TextStyle(
                            fontSize:
                            22.0,
                            color: Theme
                                .Colors
                                .appsupportlabel,
                            fontStyle:
                            FontStyle
                                .normal),
                      ),
                    ),
                    new GestureDetector(
                      onTap: () {
                        Navigator.of(
                            context)
                            .pop();
                        deleteComment(centreNotesData['id'], i);
                      },
                      child:
                      new Container(
                        width: screenSize
                            .width /
                            2,
                        margin: new EdgeInsets
                            .only(
                            top: 10.0,
                            left: 5.0,
                            right:
                            5.0,
                            bottom:
                            10.0),
                        height: 40.0,
                        decoration: new BoxDecoration(
                            color: Theme
                                .Colors
                                .buttonicon_deletecolor,
                            borderRadius: new BorderRadius
                                .all(new Radius
                                .circular(
                                10.0))),
                        child:
                        new Center(
                            child:
                            new Text(
                              "Delete",
                              style: new TextStyle(
                                  color: Colors
                                      .white,
                                  fontSize:
                                  22.0),
                            )),
                      ),
                    ),
                  ],
                ),
              ));
        });
  }
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(labelsConfig["noteToCenterLabel"],
            style: TextStyle(
              color: Colors.white,
            )),
        backgroundColor: Theme.Colors.appdarkcolour,
        centerTitle: true,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.refresh),
              tooltip: 'Refresh',
              onPressed: () {
                print("Reload..");
                setState(() {
                  k = 0;
                  load = true;
                });
                fetchCentreNotesData(0);
              })
        ],
      ),
//      drawer: new CommonDrawer(),
      body: RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: _counterButtonPress(),
        child: new Center(
          child: Column(
            children: <Widget>[
              //
//                  starting note section
//
            Expanded(
              child: load
                  ? progress
                  : data.length > 0
                      ? new ListView.builder(
                          reverse: true,
                          itemCount:
                              this.data != null ? (this.data.length + 1) : 0,
                          itemBuilder: (context, i) {
                            if (i == data.length) {
                              if (centreNotesData.length < 10) {
                                return Container();
                              } else {
                                return _buildCounterButton();
                              }
//                          return _buildCounterButton();
                            } else {
                              final singleData = this.data[i];

                          var centreNotesData = singleData;
                          var centreNotesId = singleData["id"];
                          var userDetails = centreNotesData["parent_id"];
                          var commentUserId = userDetails["id"];
                          var image = userDetails["image"];
                          var parsedDate =
                              DateTime.parse(centreNotesData["created_at"]);
                          var date = new DateFormat.yMMMMEEEEd()
                              .add_jm()
                              .format(parsedDate);
                          var created_at = date.split(' ');

                          return Center(
                            child: Column(
                              children: <Widget>[
//
//                  starting note section
//
                                    centreNotesData.length > 0
                                        ? Card(
                                            elevation: 0.0,
                                            color: Colors.transparent,
                                            child: Column(
                                              children: <Widget>[
                                                id != commentUserId
                                                    ? Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                            image != null
                                                                ? CircleAvatar(
                                                                    backgroundImage: NetworkImage(
                                                                        'https://d212imxpbiy5j1.cloudfront.net/' +
                                                                            image,
                                                                        scale:
                                                                            10.0),
                                                                    radius:
                                                                        20.0,
                                                                  )
                                                                : CircleAvatar(
                                                                    backgroundImage:
                                                                        AssetImage(
                                                                            "assets/nophoto.jpg"),
                                                                    radius:
                                                                        20.0,
                                                                  ),
                                                            Container(
                                                                width: 10.0),
                                                            Card(
                                                              child: Container(
                                                                child: Column(
                                                                  children: <
                                                                      Widget>[
                                                                    Text(
                                                                        "${centreNotesData["notes"]}"),
                                                                    Container(
                                                                      padding: EdgeInsets
                                                                          .only(
                                                                              top: 2.0),
                                                                      child: Text(
                                                                          "${created_at[0].substring(0, 3)},${created_at[1].substring(0, 3)},${created_at[2]}${created_at[3]}",
                                                                          style:
                                                                              TextStyle(
                                                                            fontSize:
                                                                                12.0,
                                                                          )),
                                                                    )
                                                                  ],
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                ),
                                                                width: screenSize
                                                                        .width *
                                                                    3 /
                                                                    4,
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(
                                                                            10.0),
                                                              ),
                                                            ),
                                                            deleteId == centreNotesData["id"]
                                                                ? InkWell(
                                                              child: Padding(
                                                                padding: const EdgeInsets.only(left: 8.0, right: 4.0, top: 8.0, bottom: 8.0),
                                                                child: Icon(Icons.delete, color: Colors.grey),
                                                              ),
                                                              onTap: () => displayModal(screenSize, centreNotesData, i),
                                                            ) : SizedBox(),
                                                          ],
                                                      )
                                                    : Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .end,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                            deleteId != centreNotesData["id"]
                                                            ? InkWell(
                                                              child: Padding(
                                                                padding: const EdgeInsets.only(left: 8.0, right: 4.0, top: 8.0, bottom: 8.0),
                                                                child: Icon(Icons.delete, color: Theme.Colors.appcolour,),
                                                              ),
                                                              onTap: () => displayModal(screenSize, centreNotesData, i),
                                                            )
                                                            : Container(),
                                                            GestureDetector(
                                                              child: Card(
                                                                color: Theme
                                                                    .Colors
                                                                    .appcolour,
                                                                child:
                                                                    Container(
                                                                  child: Column(
                                                                    children: <
                                                                        Widget>[
                                                                      Text(
                                                                        "${centreNotesData["notes"]}",
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                14.0,
                                                                            color:
                                                                                Theme.Colors.app_white),
                                                                      ),
                                                                      Container(
                                                                        padding:
                                                                            EdgeInsets.only(top: 2.0),
                                                                        child: Text(
                                                                            "${created_at[0].substring(0, 3)},${created_at[1].substring(0, 3)},${created_at[2]}${created_at[3]}",
                                                                            style:
                                                                                TextStyle(fontSize: 12.0, color: Theme.Colors.app_white)),
                                                                      )
                                                                    ],
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                  ),
                                                              width: screenSize.width * 3/4,
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              10.0),
                                                                ),
                                                              ),
                                                              onLongPress: () => displayModal(screenSize, centreNotesData, i),
                                                            ),
                                                            Container(
                                                                width: 10.0),
                                                            image != null
                                                                ? CircleAvatar(
                                                                    backgroundImage: NetworkImage(
                                                                        'https://d212imxpbiy5j1.cloudfront.net/' +
                                                                            image,
                                                                        scale:
                                                                            10.0),
                                                                    radius:
                                                                        20.0,
                                                                  )
                                                                : CircleAvatar(
                                                                    backgroundImage:
                                                                        AssetImage(
                                                                            "assets/nophoto.jpg"),
                                                                    radius:
                                                                        20.0,
                                                                  ),
                                                          ])
                                              ],
                                            ),
                                          )
                                        : Center(
                                            child: Text("No Notes yet"),
                                          ),
                                  ],
                                ),
                              );
//
//                            return CentreNotesData(singleData, jwt);
                            }
                          },
                        )
                      : EmptyBody("No any Notes."),
            ),
//
//                  starting Textformfield section
//
            Container(
              child: Padding(
                padding: EdgeInsets.all(5.0),
                child: Row(
                  children: <Widget>[
                    Flexible(
                      child: Form(
                          key: formKey,
                          child: TextFormField(
                            textInputAction: TextInputAction.done,
                            maxLines: 1,
                            controller: _textController,
                            validator: (val) {
                              return val.length == 0
                                  ? "Comment must not be null"
                                  : null;
                            },
                            decoration: InputDecoration(
                              fillColor: Colors.grey[300],
                              filled: true,
                              contentPadding:
                                  EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 10.0),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0.0),
                              ),
                            ),
                          )),
                    ),
                    new RaisedButton(
                      onPressed: () {
                        if (submit) {
                          setState(() {
                            submit = false;
                          });
                          submitComment();
                        } else {
                          print("main submission $submit");
                        }
                      },
                      color: Theme.Colors.appmenuicon,
                      padding: EdgeInsets.all(10.0),
                      child: submit
                          ? Container(
                              child: Column(
                                // Replace with a Row for horizontal icon + text
                                children: <Widget>[
                                  Icon(
                                    Icons.send,
                                    color: Theme.Colors.buttonicon_darkcolor,
                                  ),
                                  Text("Submit",
                                      style: new TextStyle(
                                          fontSize: 12.0,
                                          color: Theme.Colors.buttonicon_color))
                                ],
                              ),
                            )
                          : CircularProgressIndicator(
                              strokeWidth: 2.0,
                            ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      ),
    );
  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  Future<Null> handleRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
//    setState(() {
      load = true;
      fetchCentreNotesData(0);
//    });
//    return null;
  }

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: isLoading
            ? new CupertinoActivityIndicator()
            : const Text('Load more...',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: Theme.Colors.app_white)),
        color: Theme.Colors.appcolour.withOpacity(0.75),
//        splashColor: Colors.red,
        elevation: 4.0,
        onPressed: _counterButtonPress());
  }

  Function _counterButtonPress() {
    if (isLoading) {
      return null;
    } else {
      return () {
        setState(() {
          isLoading = true;
        });
        fetchCentreNotesData(data.length);
      };
    }
  }

  Future<String> fetchCentreNotesData(int s) async {
    ///data from GET method
    print("CentreNotes data fetched");

    String centreNoteUrl =
        'http://13.55.4.100:7070/getchildnotestep?clientid=$clientId&childid=$childId&step=$s';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(centreNoteUrl, headers: headers).then((response) {
      try {
        centreNotesData = json.decode(response.body);
        print(centreNotesData.length);
        print('res get ${response.body}');
        print('centreNotesData $centreNotesData');
      } catch (e) {
        print('That string was null!');
      }

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        isLoading = false;
        if (s == 0) {
          setState(() {
            load = false;
            this.data = centreNotesData;
          });
        } else {
          setState(() {
            load = false;
            data.addAll(centreNotesData);
          });
        }
        setState(() {
          delete = false;
          submit = true;
          _textController.clear();
        });
        k = data.length;
      } else if (response.statusCode == 500 &&
          centreNotesData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      } else {
        print("fetchCentreNotesData errorType ${centreNotesData["errorType"]}");
//        fetchCentreNotesData(0);
      }
    });
    return null;
  }

  getRefreshToken() {
    print("refreshing Token..");
    String _refreshTokenUrl =
        'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    print(_refreshTokenUrl);
    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      if (k != null) {
        fetchCentreNotesData(k);
      } else {
        fetchCentreNotesData(0);
      }
    });
  }

  void deleteComment(deleteCommentId, index) async {
    setState(() {
      delete = true;
    });
    String deleteUrl =
        'http://13.55.4.100:7070/v2.1.0/childnote/deletechildnote?clientid=$clientId';
    var body = {"id": deleteCommentId, "parent_id": id};
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.apiDeleteRequest(deleteUrl, headers: headers, body: json.encode(body), userId: id.toString(), clientId: clientId).then((res) {
      if (res.statusCode == 200) {
        setState(() {
          data.removeAt(index);
        });
      }
    });
  }

  submitComment() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      FocusScope.of(context).requestFocus(new FocusNode());

      final submitcommentUrl =
          'http://13.55.4.100:7070/childnote?clientid=$clientId';
      var body = {
        "parent_id": id,
        "child_id": childId,
        "notes": _textController.text,
      };
      var headers = {"x-authorization": jwt.toString()};

      NetworkUtil _netUtil = new NetworkUtil();
      _netUtil
          .post(submitcommentUrl,
              body: body, headers: headers, encoding: jwt.toString())
          .then((res) {
        print(res);
//        var result = json.decode(res);
//        print(result[0]);
//        print(data[0]);
//
////        print(data[10]);
//
//        setState(() {
//          data.insert(0, result[0]);
//          submit = true;
//        });
//        print(data);

        fetchCentreNotesData(0);

          });

    } else {
      print("else");
      submit = true;
    }
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    childId = widget.childData.id;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      print(appusers);
      jwt = widget.jwt.toString();
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];

      fetchCentreNotesData(0);
    } catch (e) {
      print(e);
    }

    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
