import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';

class CentreNotesData extends StatefulWidget {
  final singleData;
  final jwt;

  CentreNotesData(this.singleData, this.jwt);
  @override
  CentreNotesDataState createState() => CentreNotesDataState();
}

class CentreNotesDataState extends State<CentreNotesData>
    implements HomePageContract {
  var centreNotesId, centreNotesData, deleteId;
  var userDetails, commentUserId;

//  var commentData, commentList;
  bool load = false;
  var appuser, jwt, userId, clientId;
  HomePagePresenter _presenter;
  bool submit = true;
  bool delete = false;
  final formKey = new GlobalKey<FormState>();
  final TextEditingController _textController = new TextEditingController();
  var image;
  var created_at;

  CentreNotesDataState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  @override
  void initState() {
    centreNotesData = widget.singleData;
    centreNotesId = widget.singleData["id"];
    userDetails = centreNotesData["parent_id"];
    commentUserId = userDetails["id"];
    image = userDetails["image"];
//    print(centreNotesData);
//    print(centreNotesId);
//    print(userDetails);
//    print(commentUserId);
//    print(image);

    var parsedDate = DateTime.parse(centreNotesData["created_at"]);
    var date = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);
    created_at = date.split(' ');
//    print(created_at);

    super.initState();
  }

  @override
  Widget build_(BuildContext context) {
    return Center(
      child: Container(
        child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              image != null
                  ? CircleAvatar(
                      backgroundImage: NetworkImage(
                          'https://d212imxpbiy5j1.cloudfront.net/' + image,
                          scale: 10.0),
                      radius: 20.0,
                    )
                  : CircleAvatar(
                      backgroundImage: AssetImage("assets/nophoto.jpg"),
                      radius: 25.0,
                    ),
              Container(width: 10.0),
              Card(
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Text("${centreNotesData["notes"]}"),
                      Container(
                        padding: EdgeInsets.only(top: 2.0),
                        child: Text(
                            "${created_at[0].substring(0, 3)},${created_at[1].substring(0, 3)},${created_at[2]}${created_at[3]}",
                            style: TextStyle(
                              fontSize: 12.0,
                            )),
                      )
                    ],
                    crossAxisAlignment: CrossAxisAlignment.start,
                  ),
                  width: 300.0,
                  padding: EdgeInsets.all(10.0),
                ),
              )
            ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Center(
      child: Column(
        children: <Widget>[
//
//                  starting note section
//
          centreNotesData.length > 0
              ? Card(
                  elevation: 0.0,
                  color: Colors.transparent,
                  child: Column(
                    children: <Widget>[
                      userId != commentUserId
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                  image != null
                                      ? CircleAvatar(
                                          backgroundImage: NetworkImage(
                                              'https://d212imxpbiy5j1.cloudfront.net/' +
                                                  image,
                                              scale: 10.0),
                                          radius: 20.0,
                                        )
                                      : CircleAvatar(
                                          backgroundImage:
                                              AssetImage("assets/nophoto.jpg"),
                                          radius: 25.0,
                                        ),
                                  Container(width: 10.0),
                                  Card(
                                    child: Container(
                                      child: Column(
                                        children: <Widget>[
                                          Text("${centreNotesData["notes"]}"),
                                          Container(
                                            padding: EdgeInsets.only(top: 2.0),
                                            child: Text(
                                                "${created_at[0].substring(0, 3)},${created_at[1].substring(0, 3)},${created_at[2]}${created_at[3]}",
                                                style: TextStyle(
                                                  fontSize: 12.0,
                                                )),
                                          )
                                        ],
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                      ),
                                      width: 300.0,
                                      padding: EdgeInsets.all(10.0),
                                    ),
                                  )
                                ])
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                  /*       delete
                                      ? deleteId ==
                                              centreNotesData["id"]
                                          ? CupertinoActivityIndicator()
                                          : Container()
                                      : Container(),*/
                                  GestureDetector(
                                    child: Flexible(
                                      flex: 7,
                                      child: Card(
                                        color: Theme.Colors.appcolour,
                                        child: Container(
                                          child: Column(
                                            children: <Widget>[
                                              Text(
                                                "${centreNotesData["comment_text"]}",
                                                style: TextStyle(
                                                    fontSize: 14.0,
                                                    color:
                                                        Theme.Colors.app_white),
                                              ),
                                              Container(
                                                padding:
                                                    EdgeInsets.only(top: 2.0),
                                                child: Text(
                                                    "${created_at[0].substring(0, 3)},${created_at[1].substring(0, 3)},${created_at[2]}${created_at[3]}",
                                                    style: TextStyle(
                                                        fontSize: 12.0,
                                                        color: Theme
                                                            .Colors.app_white)),
                                              )
                                            ],
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                          ),
                                          width: 300.0,
                                          padding: EdgeInsets.all(10.0),
                                        ),
                                      ),
                                    ),
                                    onLongPress: () {
                                      print("delete option");
                                      showModalBottomSheet<void>(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return Container(
                                                height: 180.0,
                                                width: screenSize.width,
                                                child: Padding(
                                                  padding: const EdgeInsets.all(
                                                      32.0),
                                                  child: Column(
                                                    children: <Widget>[
                                                      new Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(2.0),
                                                        child: new Text(
                                                          "Do you wish to delete",
                                                          style: new TextStyle(
                                                              fontSize: 22.0,
                                                              color: Theme
                                                                  .Colors
                                                                  .appsupportlabel,
                                                              fontStyle:
                                                                  FontStyle
                                                                      .normal),
                                                        ),
                                                      ),
                                                      new GestureDetector(
                                                        onTap: () {
                                                          print(
                                                              centreNotesData);
                                                          deleteId =
                                                              centreNotesData[
                                                                  'id'];

                                                          print(deleteId);
                                                          Navigator.of(context)
                                                              .pop();
//                                                                  deleteComment(
//                                                                      deleteId);
                                                        },
                                                        child: new Container(
                                                          width:
                                                              screenSize.width /
                                                                  2,
                                                          margin: new EdgeInsets
                                                                  .only(
                                                              top: 10.0,
                                                              left: 5.0,
                                                              right: 5.0,
                                                              bottom: 10.0),
                                                          height: 30.0,
                                                          decoration: new BoxDecoration(
                                                              color: Theme
                                                                  .Colors
                                                                  .buttonicon_deletecolor,
                                                              borderRadius:
                                                                  new BorderRadius
                                                                      .all(new Radius
                                                                          .circular(
                                                                      10.0))),
                                                          child: new Center(
                                                              child: new Text(
                                                            "Delete",
                                                            style: new TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 22.0),
                                                          )),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ));
                                          });
                                    },
                                  ),
                                  Flexible(
                                    flex: 2,
                                      child: Container(width: 10.0)),
                                  image != null
                                      ? CircleAvatar(
                                          backgroundImage: NetworkImage(
                                              'https://d212imxpbiy5j1.cloudfront.net/' +
                                                  image,
                                              scale: 10.0),
                                          radius: 20.0,
                                        )
                                      : CircleAvatar(
                                          backgroundImage:
                                              AssetImage("assets/nophoto.jpg"),
                                          radius: 20.0,
                                        ),
                                ])
                    ],
                  ),
                )
              : Center(
              child: Text("No Notes yet"),
                ),
//                  starting Textformfield section
        ],
      ),
    );
  }

  var progress = new ProgressBar(
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );
//
  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
//      print(appusers);
      jwt = widget.jwt.toString();
//      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      userId = users["id"];

      print("iddd $userId");
      print(clientId);
//      getCommentData();
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
