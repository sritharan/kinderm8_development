import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:kinderm8/utils/commonutils/zoomable_image_url_only.dart';
import 'package:kinderm8/utils/network_util.dart';

class GridList extends StatefulWidget {
  final ChildViewModal childData;
  final jwt;
  final type;
  GridList(this.childData, this.jwt, this.type);
  @override
  GridListState createState() => GridListState();
}

class GridListState extends State<GridList> implements HomePageContract {
  var appuser, jwt, userId, k, clientId, childId;
  var photoFullData;
  var photoData;
  bool isLoading;
  bool load = true;
  List fileUrl = List();
  HomePagePresenter _presenter;
  GridListState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  createPhotoList() {
    for (int i = 0; i < photoFullData.length; i++) {
      fileUrl.add(photoData[i]['fileurl']);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Photos",
            style: TextStyle(
              color: Colors.white,
            )),
        backgroundColor: Theme.Colors.appdarkcolour,
        centerTitle: true,
      ),
      body: new Center(
        child: load
                ? progress
                : new StaggeredGridView.countBuilder(
                    primary: false,
                    crossAxisCount: 4,
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                    staggeredTileBuilder: (index) => index == k
                        ? new StaggeredTile.fit(4)
                        : new StaggeredTile.fit(2),
                    itemCount: this.photoFullData != null
                        ? (this.photoFullData.length + 1)
                        : 0,
                    itemBuilder: (context, index) {
                      if (index == k) {
                        if (photoData.length == 0 || photoData.length < 20) {
                          print(photoData.length);
                          return Container();
                        } else {
                          return _buildCounterButton();
                        }
                      } else {
                        final photoSingleData = this.photoFullData[index];

                        var date = photoSingleData["date"];
                        var parsedDate = DateTime.parse(date);
                        var convertedDate = new DateFormat.yMMMMEEEEd()
                            .add_jm()
                            .format(parsedDate);
                        var created_at = convertedDate.split(' ');

                        return FlatButton(padding: EdgeInsets.all(0.0),
                          onPressed: () {
                            print(index);
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) =>
                            new ZoomableImagePage_Url(photoFullData, index)));
                          },
                          child: new Card(
                            child: new Column(
                              children: <Widget>[
                                Stack(
                                  children: <Widget>[
                                    new Center(
                                      child: CachedNetworkImage(
                                        imageUrl: photoSingleData["fileurl"],
                                        fit: BoxFit.cover,
                                        errorWidget: new Image.asset(
                                          "assets/nophoto.jpg",
                                          fit: BoxFit.cover,
                                        ),
                                        fadeOutDuration:
                                            new Duration(seconds: 1),
                                        fadeInDuration:
                                            new Duration(seconds: 3),
                                      ),
                                    ),
                                    photoSingleData["caption"] != null &&
                                            photoSingleData["caption"] != ""
                                        ? Positioned(
                                            child: Container(
                                              child: Text(
                                                "${photoSingleData["caption"]}",
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                                style: TextStyle(
                                                    fontSize: 10.0,
                                                    color:
                                                        Theme.Colors.app_white),
                                              ),
                                              decoration: BoxDecoration(
//                                          shape: BoxShape.circle,
                                                  color: Colors.black
                                                      .withOpacity(0.65)),
                                              padding: EdgeInsets.all(2.0),
                                            ),
                                            bottom: 0.0,
                                            right: 0.0,
                                            left: 0.0,
                                          )
                                        : Container()
                                  ],
                                ),
                                new Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: new Row(
                                    children: <Widget>[
                                      new Text(
                                        '${created_at[0].substring(0, 3)}, ${created_at[1]} ${created_at[2]} ${created_at[3]}',
                                        style: TextStyle(fontSize: 10.0),
                                      ),
                                    ],
                                    mainAxisAlignment: MainAxisAlignment.start,
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      }
                    })

            ,
      ),
    );
  }

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: isLoading
            ? new CupertinoActivityIndicator()
            : const Text('Load more...',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: Theme.Colors.app_white)),
        color: Theme.Colors.appcolour.withOpacity(0.75),
//        splashColor: Colors.red,
        elevation: 4.0,
        onPressed: _counterButtonPress());
  }

  Function _counterButtonPress() {
    if (isLoading) {
      return null;
    } else {
      return () {
        setState(() {
          isLoading = true;
        });
        fetchFullPhotoData(k);
      };
    }
  }

  Future<String> fetchFullPhotoData(int s) async {
    ///data from GET method
    print("Gallery photodata fetched");
    String photoDataUrl;
    if (widget.type == 'journal') {
      photoDataUrl =
          'http://13.55.4.100:7070/v4.4.0/getgallery/${widget.type}/$childId?clientid=$clientId&step=$s';
    } else {
      photoDataUrl =
          'http://13.55.4.100:7070/v2.1.0/getgallery/${widget.type}/$childId?clientid=$clientId&step=$s';
    }
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(photoDataUrl, headers: headers).then((response) {
      try {
        photoData = json.decode(response.body);
        print(photoData.length);
        print('res get ${response.body}');
        print('centreNotesData $photoData');
      } catch (e) {
        print('That string was null!');
      }

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        isLoading = false;
        if (s == 0) {
          setState(() {
            load = false;
            this.photoFullData = photoData;
          });
        } else {
          setState(() {
            load = false;
            photoFullData.addAll(photoData);
          });
        }
        k = photoFullData.length;
        createPhotoList();
        print("fileURLS $fileUrl");
        print("length ${fileUrl.length}");
      } else if (response.statusCode == 500 &&
          photoData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      } else {
        print("fetchCentreNotesData errorType ${photoData["errorType"]}");
//        fetchCentreNotesData(0);
      }
    });
    return null;
  }

  getRefreshToken() {
    print("refreshing Token..");
    String _refreshTokenUrl =
        'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

    print(_refreshTokenUrl);
    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      if (k != null) {
        fetchFullPhotoData(k);
      } else {
        fetchFullPhotoData(0);
      }
    });
  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    childId = widget.childData.id;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      print(appusers);
      jwt = widget.jwt.toString();
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      userId = users["id"];

      fetchFullPhotoData(0);
    } catch (e) {
      print(e);
    }

    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }
}

//new FadeInImage.memoryNetwork(
//placeholder: kTransparentImage,
//image: 'https://picsum.photos/${size.width}/${size.height}/',
//)
/**/

//final Uint8List kTransparentImage = new Uint8List.fromList(<int>[
//  0x89,
//  0x50,
//  0x4E,
//  0x47,
//  0x0D,
//  0x0A,
//  0x1A,
//  0x0A,
//  0x00,
//  0x00,
//  0x00,
//  0x0D,
//  0x49,
//  0x48,
//  0x44,
//  0x52,
//  0x00,
//  0x00,
//  0x00,
//  0x01,
//  0x00,
//  0x00,
//  0x00,
//  0x01,
//  0x08,
//  0x06,
//  0x00,
//  0x00,
//  0x00,
//  0x1F,
//  0x15,
//  0xC4,
//  0x89,
//  0x00,
//  0x00,
//  0x00,
//  0x0A,
//  0x49,
//  0x44,
//  0x41,
//  0x54,
//  0x78,
//  0x9C,
//  0x63,
//  0x00,
//  0x01,
//  0x00,
//  0x00,
//  0x05,
//  0x00,
//  0x01,
//  0x0D,
//  0x0A,
//  0x2D,
//  0xB4,
//  0x00,
//  0x00,
//  0x00,
//  0x00,
//  0x49,
//  0x45,
//  0x4E,
//  0x44,
//  0xAE,
//]);

/*
List<IntSize> _createSizes(int count) {
  Random rnd = new Random();

  return new List.generate(count,
      (i) => new IntSize((rnd.nextInt(500) + 200), rnd.nextInt(800) + 200));
}
*/

//class Example08 extends StatelessWidget {
//  final data = [
//    "https://d212imxpbiy5j1.cloudfront.net/img/uploads/production_1534330541-7K0A0259.JPG",
//    "https://d212imxpbiy5j1.cloudfront.net/www.youtube.com/watch%3Fv%3DXqZsoesa55w",
//    "https://d212imxpbiy5j1.cloudfront.net/img/uploads/production_1534327953-lunch.jpg",
//    "https://d212imxpbiy5j1.cloudfront.net/img/uploads/production_1534327955-01-DSC_0197-courtesy-jessica-levinson-760x506.jpg",
//    "https://d212imxpbiy5j1.cloudfront.net/upload_files/stories/images/production_1534327012-Picota-Cherries-Vanilla-Ice-Cream-with-a-Twist.jpg",
//    "https://d212imxpbiy5j1.cloudfront.net/upload_files/stories/camara/production_1534327118_camara_image.png",
//    "https://d212imxpbiy5j1.cloudfront.net/www.youtube.com/watch%3Fv%3DECicjAUJMfA",
//    "https://d212imxpbiy5j1.cloudfront.net/img/uploads/production_1534327953-lunch.jpg",
//    "https://d212imxpbiy5j1.cloudfront.net/img/uploads/production_1534327955-01-DSC_0197-courtesy-jessica-levinson-760x506.jpg",
//    "https://d212imxpbiy5j1.cloudfront.net/upload_files/stories/images/production_1534327012-Picota-Cherries-Vanilla-Ice-Cream-with-a-Twist.jpg",
//    "https://d212imxpbiy5j1.cloudfront.net/upload_files/stories/camara/production_1534327118_camara_image.png",
//    "https://d212imxpbiy5j1.cloudfront.net/www.youtube.com/watch%3Fv%3DECicjAUJMfA",
//    "https://d212imxpbiy5j1.cloudfront.net/img/uploads/production_1534327955-01-DSC_0197-courtesy-jessica-levinson-760x506.jpg",
//    "https://d212imxpbiy5j1.cloudfront.net/upload_files/stories/images/production_1534327012-Picota-Cherries-Vanilla-Ice-Cream-with-a-Twist.jpg",
//    "https://d212imxpbiy5j1.cloudfront.net/upload_files/stories/camara/production_1534327118_camara_image.png",
//    "https://d212imxpbiy5j1.cloudfront.net/img/uploads/production_1534330541-7K0A0259.JPG",
//    "https://d212imxpbiy5j1.cloudfront.net/www.youtube.com/watch%3Fv%3DXqZsoesa55w",
//  ];
//
////  static const int _kItemCount = 20;
//
//  @override
//  Widget build(BuildContext context) {
//    return new Scaffold(
//      appBar: new AppBar(
//        title: new Text('random dynamic tile sizes'),
//      ),
//      body: new StaggeredGridView.countBuilder(
//        primary: false,
//        crossAxisCount: 4,
//        mainAxisSpacing: 4.0,
//        crossAxisSpacing: 4.0,
//        itemCount: 16,
//        itemBuilder: (context, index) => new _Tile(index, data),
//        staggeredTileBuilder: (index) => new StaggeredTile.fit(2),
//      ),
//    );
//  }
//}

/*class IntSize {
  const IntSize(this.width, this.height);

  final int width;
  final int height;
}*/

//class _Tile extends StatelessWidget {
//  final List data;
//  final int index;
//  _Tile(this.index, this.data);
//
//  @override
//  Widget build(BuildContext context) {
//    return new Card(
//      child: new Column(
//        children: <Widget>[
//          /*new Center(
//            child: CachedNetworkImage(
//              imageUrl: data[0],
//              placeholder: new CupertinoActivityIndicator(),
//              fit: BoxFit.cover,
//              errorWidget: new Image.asset("assets/nophoto.jpg"),
//              fadeOutDuration: new Duration(seconds: 1),
//              fadeInDuration: new Duration(seconds: 3),
//            ),
//          ),*/
//          new Padding(
//            padding: const EdgeInsets.all(4.0),
//            child: new Column(
//              children: <Widget>[
//                new Text('Image number $index'),
//              ],
//            ),
//          )
//        ],
//      ),
//    );
//  }
//}
