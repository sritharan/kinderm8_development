import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kinderm8/Theme.dart' as kinderm8Theme;
import 'package:kinderm8/auth.dart';
import 'package:kinderm8/data/database_helper.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/commondrawer.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/settings/editemail.dart';
import 'package:kinderm8/pages/settings/image_picker_handler.dart';
import 'package:kinderm8/pages/settings/resetpassword.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';

class UserSettings extends StatefulWidget {
  @override
  UserSettingsState createState() => new UserSettingsState(image,displayname,displayemail);
  final ImageProvider image;
  final displayname;
  final displayemail;

  UserSettings(this.image, this.displayname, this.displayemail);
}

class UserSettingsState extends State<UserSettings> with TickerProviderStateMixin,ImagePickerListener implements HomePageContract, AuthStateListener {
  BuildContext _ctx;
  final ImageProvider image;
  final displayname;
  final displayemail;




  var appuser,id, jwt,client_id, children, users;
  HomePagePresenter presenter;

  var email, password;
  var load = true;
  bool isLoading;
  HomePagePresenter _presenter;
  ScrollController controller;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  // profile pic change variableskinder.flutter/s3'

  File _image;
  AnimationController _controller;
  ImagePickerHandler imagePicker;
  BuildContext ctx;
  var awsFolder;
  bool isImageUploading = false;
  final platform = const MethodChannel('test.kinder/s3_image');
  final iosplatform =  const MethodChannel('kinder.flutter/s3');

  //to save image update
  User user;

  //  init state for time duration

  @override
  void initState(){
    super.initState();
    _controller = new AnimationController(
        vsync: this,
        duration: const Duration(milliseconds: 500)
    );
    imagePicker=new ImagePickerHandler(this,_controller);
    imagePicker.init();
    print("hello........$image");
    print("hey.........$_image");
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  var progress = new ProgressBar(
    //    backgroundColor: Theme.Colors.progressbackground ,
    color: kinderm8Theme.Colors.appcolour,
    containerColor: kinderm8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );
  UserSettingsState(this.image, this.displayname, this.displayemail) {
    presenter = new HomePagePresenter(this);
    presenter.getUserInfo();
  }

  @override
  userImage(File _image) async {

    setState(() {
      this._image = _image;
    });

    String path ;

    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("imagePath", () => _image.path);

    args.putIfAbsent("clientId", () => client_id);

    String result;
    if (result == null) {
      setState(() {
        isImageUploading = true;
      });
      showDialog(
          context: context,
          barrierDismissible: true,
          //TODO: Make this false after check on double tap
          builder: (context) => AlertDialog(
            title: Center(child: CircularProgressIndicator()),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Uploading...'),
              ],
            ),
          ));

      try {
        if (Platform.isAndroid) {
          result = await platform.invokeMethod('showDialog', args);
        } else if (Platform.isIOS) {
          result = await iosplatform.invokeMethod('showDialog',args);
        }
      } on PlatformException catch (e) {
        print("Failed :'${e.message}'.");
      }
    }

    print("userid $id .......... $client_id");
    final resetUrl ="http://13.55.4.100:7070/v2.1.0/updateusersettingsoneapp?clientid=$client_id";

    String imageName = Platform.isIOS ? "${client_id}_${_image.path.split("/").last}" : _image.path.split("/").last;

    var body ={
      "client_user_id": id,
      "image": "img/uploads/$imageName"
    };
    print("print $body");
    var headers = {"x-authorization": jwt.toString(), "clientid" : client_id};
    NetworkUtil _netUtil = new NetworkUtil();

    _netUtil
        .post(resetUrl,
        body: body, headers: headers, encoding: jwt.toString())
        .then((res) {
      print("ress $res");
      if (res == 1) {
        String _refreshTokenUrl =
            'http://13.55.4.100:7070/v2.1.0/jwt/refresh-token?userid=$id&clientid=$client_id';

        print(_refreshTokenUrl);
        NetworkUtil _netutil = new NetworkUtil();

        _netutil.get(_refreshTokenUrl).then((response) {
          print("refresh get ${response.body}");
          var refreshJwtToken;
          try {
            refreshJwtToken = json.decode(response.body);
          } catch (e) {
            print('That string was null!');
          }
          this.jwt = refreshJwtToken;
        });
      } else if (res == "[true]") {
        print("may be 200");
        print("^^^>>>>>><<<<^^^$client_id");
        Navigator.of(context).pop();
      } else if(res == ''){
        print("may be 200");

      }
      else {
        DatabaseHelper dbHelper = DatabaseHelper();
        var temp = json.decode(user.center);
        temp[0]["user"]["image"] = "https://d212imxpbiy5j1.cloudfront.net/img/uploads/$imageName";
        user.setCenter(json.encode(temp));
        dbHelper.deleteUser();
        dbHelper.saveUser(user);
        setState(() {
          isImageUploading = false;
        });
        Navigator.of(context).pop();
      }
    }
    );
    // return result;
  }

  @override
  Widget build(BuildContext context) {
    ctx = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          elevation: 0.0,
          title: new Text(labelsConfig["settingsLabel"]), //centerTitle: true,
        ),

        body: new ListView(
          children: <Widget>[
            Container(
              color: Colors.deepPurple,
              padding: EdgeInsets.only(top: 10.0, bottom: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Container(
                    width: screenSize.width/4,
                    height: screenSize.width/4,
                    decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: (_image != null) ? ExactAssetImage(_image.path) : image,

                      ),
                      border: Border.all(
                          style: BorderStyle.solid,
                          width: 4.0,
                          color: kinderm8Theme.Colors.app_white),
                    ),
                    child: Container(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            border: Border.all(
                                style: BorderStyle.solid,
                                width: 2.0,
                                color: Colors.white)
                        ),
                        child: new InkWell(
                          child: new Icon(
                            Icons.camera_alt,
                            color: kinderm8Theme.Colors.progressbackground,
                            size: 25.0,
                          ),
                          onTap: () => imagePicker.showDialog(context),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 20.0,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: new Text(
                          labelsConfig["userSettingsLabel"],
                          style: TextStyle(
                            color: kinderm8Theme.Colors.progressbackground,
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),

            ),
            Container(
              height: 10.0,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Card(
                elevation: 1.0,
                child: Stack(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          //   mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    new Icon(
                                      Icons.account_box,
                                      size: 20.0,
                                      color: Colors.black38,
                                    ),
                                    Container(
                                      width: 5.0,
                                    ),
                                    new Text(
                                      "Full Name:",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.0,
                                          color: kinderm8Theme.Colors.progressbackground),
                                      //textScaleFactor: 1.0
                                    ),
                                    Container(
                                      width: 5.0,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    new Icon(
                                      Icons.email,
                                      size: 20.0,
                                      color: Colors.black38,
                                    ),
                                    Container(
                                      width: 5.0,
                                    ),
                                    new Text("Email:",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                            color: kinderm8Theme.Colors.progressbackground)),
                                    Container(
                                      width: 5.0,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),

                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: new Text(
                                widget.displayname,
                                style: TextStyle(
                                  color: Colors.black,
                                  //  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                ),
                              ),
                            ),



                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: new Text(
                                widget.displayemail,
                                style: TextStyle(
                                  color: Colors.black,
                                  //  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Container(
                      alignment: Alignment.topRight,
                      child: new FloatingActionButton(
                          elevation: 5.0,
                          child: new Icon(Icons.edit),
                          backgroundColor: kinderm8Theme.Colors.appdarkcolour,
                          onPressed: (){
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) => new EmailChange()));
                          }
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Container(
                    alignment: Alignment.topRight,
                    decoration: ShapeDecoration(
                        shape: CircleBorder(
                            side: BorderSide(
                              width: 2.0,
                              color: Colors.white,
                            ))),
                  ),
                ),
              ],
            ),
            Container(
              height: 10.0,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                        color: Colors.deepPurple,
                        child: new Center(
                          child: new Row( // Repl
                              mainAxisAlignment: MainAxisAlignment.center,// ace with a Row for horizontal icon + text
                              children: <Widget>[
                                Icon(Icons.vpn_key,color: Colors.white,),
                                Text(" Change password",style: TextStyle(
                                    fontSize: 20.0, color: Colors.white
                                )
                                ),
                              ]
                          ),
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new ResetPassword()
                              )
                          );
                        }
                    ),
                  )
                ],
              ),
            ),
          ],
        )
    );
  }

  @override
  void onDisplayUserInfo(User user) {
    setState(() {
      this.user = user;
      password = user.password;
      appuser = user.center;
      children = user.children;

      try {
        final parsed = json.decode(appuser);
        var appusers = parsed[0];
        print(appusers);
        print("******${appusers["jwt"]}");
        jwt = appusers["jwt"];
        users = appusers["user"];
        var child = appusers["children"];

        client_id = users["client_id"];
//        _image = users["image"];

        id = users["id"];
        email = users["email"];
        user = user;
      } catch (e) {
        print(e);
      }
    });
  }

  @override
  void onErrorUserInfo() {
  }

  @override
  void onLogoutUser() {
    var authStateProvider = new AuthStateProvider();
    setState(() {
      authStateProvider.notify(AuthState.LOGGED_OUT);
      authStateProvider.dispose(this);
    });
  }

  @override
  void onAuthStateChanged(AuthState state) {
    print("Navigate");
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(_ctx)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }
  }

  @override
  void onUpdateJwt() {
  }
}