import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kinderm8/auth.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/settings/image_picker_dialog.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:kinderm8/utils/network_util.dart';

class ImagePickerHandler {
  ImagePickerDialog imagePicker;
  AnimationController _controller;
  ImagePickerListener _listener;

  ImagePickerHandler(this._listener, this._controller);

  var appuser, jwt, clientId, userId, image, token;

  File file;
  var awsFolder,client_id;
  bool isImageUploading = false;
  final platform = const MethodChannel('test.kinder/s3_image');

  openCamera() async {
    imagePicker.dismissDialog();
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    cropImage(image);
  }

  openGallery() async {
    imagePicker.dismissDialog();
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    cropImage(image);
  }

  void init() {
    imagePicker = new ImagePickerDialog(this, _controller);
    imagePicker.initState();
  }

  Future cropImage(File image) async {
    File croppedFile = await ImageCropper.cropImage(
      sourcePath: image.path,
      ratioX: 1.0,
      ratioY: 1.0,
      maxWidth: 512,
      maxHeight: 512,
    );
    _listener.userImage(croppedFile);
  }

  Future<String>  showDialog(BuildContext context) async{
    imagePicker.getImage(context);


  }
}

abstract class ImagePickerListener {
  userImage(File _image);
}
