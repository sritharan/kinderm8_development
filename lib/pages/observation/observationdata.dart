import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:html_unescape/html_unescape.dart';
import 'package:kinderm8/pages/observation/detailedobservationpage.dart';
import 'package:kinderm8/pages/observation/observationcomment.dart';
import 'package:html2md/html2md.dart' as html2md;

class ObservationData extends StatelessWidget {
  final singleData;
  final jwt;

  ObservationData(this.singleData, this.jwt);
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    var S3URL = "https://d212imxpbiy5j1.cloudfront.net/";

/*    var unescape = new HtmlUnescape();

    var fromData = unescape.convert(singleData['observation']);
    var convertedData = fromData.substring(5, fromData.length - 6);
    var observation = convertedData.split('&');*/

    var date = singleData["created_at"];
    var parsedDate = DateTime.parse(date);
    var convertedDate = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);

    var created_at = convertedDate.split(' ');

    var observation = html2md.convert(singleData['observation']);

    return GestureDetector(
      onTap: () {
        print("navigate to detail view..");
        Navigator.of(context).push(new MaterialPageRoute<Null>(
          builder: (BuildContext context) {
            return ObservationDetails(singleData,jwt);
          },
        ));
      },
      child: Card(
          elevation: 1.0,
          color: Colors.white,
          margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 14.0),
          child: Column(
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: const EdgeInsets.all(5.0),
                      width: screenSize.width / 4,
                      height: screenSize.width / 4,
                      child: singleData["journeygallery"].length > 0
                          ? new CachedNetworkImage(
                              fit: BoxFit.cover,
                              imageUrl: S3URL +
                                  singleData["journeygallery"][0]['url'],
                              placeholder: new CupertinoActivityIndicator(),
                              errorWidget:
                                  new Image.asset("assets/nophoto.jpg"),
                              fadeOutDuration: new Duration(seconds: 1),
                              fadeInDuration: new Duration(seconds: 3),
                            )
                          : Center(
                              child: new Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Image.asset(
                                    "assets/iconsetpng/game.png",
                                    width: 50.0,
                                    height: 50.0,
                                  ),
                                ],
                              ),
                            )),
                  Expanded(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                          margin:
                          const EdgeInsets.only(top: 12.0, bottom: 10.0),
                          child: new Text(
                            singleData['journey_title'],
                            style: new TextStyle(
                              fontSize: 16.0,
//                              color: Colors.grey,
                              color: Theme.Colors.darkGrey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        new Container(
                            margin: const EdgeInsets.only(right: 10.0),
                            child: observation.length > 150
                                ? Html(data: observation.substring(0, 150) + '...')
                                : Html(data: observation)
                        ),
                      ],
                    ),
                  ),
                ],
              ),
//
              Container(
                padding: EdgeInsets.all(4.0),
                color: Theme.Colors.appcolour,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
//                crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
//                        padding: EdgeInsets.only(left: 3.0),
                        onTap: () {
                          Navigator.of(context)
                              .push(new MaterialPageRoute<Null>(
                            builder: (BuildContext context) {
                              return ObservationComment(singleData, jwt);
                            },
//                                fullscreenDialog: true
                          ));
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
//                      crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
//                          Padding(padding: EdgeInsets.only(left: 10.0)),
                            new Icon(Icons.comment, size: 12.0,color: Theme.Colors.app_white),
                            Container(
//                          padding: EdgeInsets.only(left: 10.0),
                              child: singleData["journeyComments"].length != 0
                                  ? Text("${singleData["journeyComments"].length}",
                                      style: TextStyle(fontSize: 12.0,color: Theme.Colors.app_white))
                                  : Text('No Comments',
                                      style: TextStyle(fontSize: 12.0,color: Theme.Colors.app_white)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
//                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Icon(FontAwesomeIcons.clock, size: 12.0,color: Theme.Colors.app_white),
                        Container(
                          padding: EdgeInsets.all(2.0),
                          child: Text(
                            'on ${created_at[0].substring(0, 3)},${created_at[1].substring(0, 3)},${created_at[2]}${created_at[3]}',
                            style: TextStyle(fontSize: 12.0,color: Theme.Colors.app_white),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }
}
