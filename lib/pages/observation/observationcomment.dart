import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/Theme.dart' as kinderm8Theme;
import 'package:kinderm8/utils/network_util.dart';

class ObservationComment extends StatefulWidget {
  final singleData;
  final jwt;
  ObservationComment(this.singleData, this.jwt);
  @override
  ObservationCommentState createState() => ObservationCommentState();
}

class ObservationCommentState extends State<ObservationComment>
    implements HomePageContract {
  var observationId, deleteId;
  var commentData, commentList;
  bool load = false;
  var appuser, jwt, id, clientId;
  HomePagePresenter _presenter;
  bool submit = true;
  bool delete = false;
  final formKey = new GlobalKey<FormState>();
  final TextEditingController _textController = new TextEditingController();

  ObservationCommentState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(title: Text("Comments")),
      body: load
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  commentData.length > 0
                      ? Expanded(
                          child: new ListView.builder(
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            padding: const EdgeInsets.all(5.0),
                            itemCount: this.commentData != null
                                ? (this.commentData.length)
                                : 0,
                            itemBuilder: (context, i) {
                              final singlecomment = this.commentData[i];
                              var userDetails = singlecomment["user"];
                              var image = userDetails['image'];
                              var commentUserId = userDetails["id"];
//                              print("id $id commentuser $commentUserId");

                              var parsedDate =
                                  DateTime.parse(singlecomment["created_at"]);
                              var date = new DateFormat.yMMMMEEEEd()
                                  .add_jm()
                                  .format(parsedDate);
                              var created_at = date.split(' ');

                              return new Center(
                                  child: Card(
                                elevation: 0.0,
                                color: Colors.transparent,
                                child: Column(
                                  children: <Widget>[
                                    id != commentUserId
                                        ? Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                                image != null
                                                    ? CircleAvatar(
                                                        backgroundImage:
                                                            NetworkImage(
                                                                'https://d212imxpbiy5j1.cloudfront.net/' +
                                                                    image,
                                                                scale: 10.0),
                                                        radius: 20.0,
                                                      )
                                                    : CircleAvatar(
                                                        backgroundImage: AssetImage(
                                                            "assets/nophoto.jpg"),
                                                        radius: 25.0,
                                                      ),
                                                Container(width: 10.0),
                                                Card(
                                                  child: Container(
                                                    child: Column(
                                                      children: <Widget>[
                                                        Text(
                                                            "${singlecomment["comment_text"]}"),
                                                        Container(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  top: 2.0),
                                                          child: Text(
                                                              "${created_at[0].substring(0, 3)},${created_at[1].substring(0, 3)},${created_at[2]}${created_at[3]}",
                                                              style: TextStyle(
                                                                fontSize: 12.0,
                                                              )),
                                                        )
                                                      ],
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                    ),
                                                    width: 300.0,
                                                    padding:
                                                        EdgeInsets.all(10.0),
                                                  ),
                                                )
                                              ])
                                        : Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                                delete
                                                    ? deleteId ==
                                                            singlecomment["id"]
                                                        ? CupertinoActivityIndicator()
                                                        : Container()
                                                    : Container(),
                                                Expanded(
                                                  child: GestureDetector(
                                                    child: Card(
                                                      color: kinderm8Theme
                                                          .Colors.appcolour,
                                                      child: Container(
                                                        child: Column(
                                                          children: <Widget>[
                                                            Text(
                                                              "${singlecomment["comment_text"]}",
                                                              style: TextStyle(
                                                                  fontSize: 14.0,
                                                                  color: kinderm8Theme
                                                                      .Colors
                                                                      .app_white),
                                                            ),
                                                            Container(
                                                              padding:
                                                                  EdgeInsets.only(
                                                                      top: 2.0),
                                                              child: Text(
                                                                  "${created_at[0].substring(0, 3)},${created_at[1].substring(0, 3)},${created_at[2]}${created_at[3]}",
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          12.0,
                                                                      color: kinderm8Theme
                                                                          .Colors
                                                                          .app_white)),
                                                            )
                                                          ],
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                        ),
//                                                        width: 250.0,
                                                        padding:
                                                            EdgeInsets.all(10.0),
                                                      ),
                                                    ),
                                                    onLongPress: () {
                                                      print("delete option");
                                                      showModalBottomSheet<void>(
                                                          context: context,
                                                          builder: (BuildContext
                                                              context) {
                                                            return Container(
                                                                height: 150.0,
                                                                width: screenSize
                                                                    .width,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .all(
                                                                          32.0),
                                                                  child: Column(
                                                                    children: <
                                                                        Widget>[
                                                                      new Padding(
                                                                        padding:
                                                                            const EdgeInsets.all(
                                                                                2.0),
                                                                        child:
                                                                            new Text(
                                                                          "Do you wish to delete",
                                                                          style: new TextStyle(
                                                                              fontSize:
                                                                                  22.0,
                                                                              color:
                                                                                  kinderm8Theme.Colors.appsupportlabel,
                                                                              fontStyle: FontStyle.normal),
                                                                        ),
                                                                      ),
                                                                      new GestureDetector(
                                                                        onTap:
                                                                            () {
                                                                          print(
                                                                              singlecomment);
                                                                          deleteId =
                                                                              singlecomment['id'];

                                                                          print(
                                                                              deleteId);
                                                                          Navigator.of(context)
                                                                              .pop();
                                                                          deleteComment(
                                                                              deleteId);
                                                                        },
                                                                        child:
                                                                            new Container(
                                                                          width:
                                                                              screenSize.width /
                                                                                  2,
                                                                          margin: new EdgeInsets.only(
                                                                              top:
                                                                                  10.0,
                                                                              left:
                                                                                  5.0,
                                                                              right:
                                                                                  5.0,
                                                                              bottom:
                                                                                  10.0),
                                                                          height:
                                                                              30.0,
                                                                          decoration: new BoxDecoration(
                                                                              color:
                                                                                  kinderm8Theme.Colors.buttonicon_deletecolor,
                                                                              borderRadius: new BorderRadius.all(new Radius.circular(10.0))),
                                                                          child: new Center(
                                                                              child: new Text(
                                                                            "Delete",
                                                                            style: new TextStyle(
                                                                                color: Colors.white,
                                                                                fontSize: 22.0),
                                                                          )),
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ));
                                                          });
                                                    },
                                                  ),
                                                ),
                                                Container(width: 10.0),
                                                image != null
                                                    ? CircleAvatar(
                                                        backgroundImage:
                                                            NetworkImage(
                                                                'https://d212imxpbiy5j1.cloudfront.net/' +
                                                                    image,
                                                                scale: 10.0),
                                                        radius: 20.0,
                                                      )
                                                    : CircleAvatar(
                                                        backgroundImage: AssetImage(
                                                            "assets/nophoto.jpg"),
                                                        radius: 20.0,
                                                      ),
                                              ])

//                                  new ListTile(
//                                    leading: image != null
//                                        ? CircleAvatar(
//                                            backgroundImage: NetworkImage(image,
//                                                scale: 10.0),
//                                            radius: 25.0,
//                                          )
//                                        : CircleAvatar(
//                                            backgroundImage: AssetImage(
//                                                "assets/nophoto.jpg"),
//                                            radius: 25.0,
//                                          ),
////                                      isThreeLine: true,
//                                    trailing: id == commentUserId
//                                        ? Text("true")
//                                        /*new GestureDetector(
//                                        child: Icon(Icons.delete),
//                                        onTap: () {
//                                          showModalBottomSheet<void>(
//                                              context: context,
//                                              builder:
//                                                  (BuildContext context) {
//                                                return Container(
//                                                    height: 150.0,
//                                                    width: screenSize.width,
//                                                    child: Padding(
//                                                      padding:
//                                                      const EdgeInsets
//                                                          .all(32.0),
//                                                      child: Column(
//                                                        children: <Widget>[
//                                                          new Padding(
//                                                            padding:
//                                                            const EdgeInsets
//                                                                .all(
//                                                                2.0),
//                                                            child: new Text(
//                                                              "Do you wish to delete",
//                                                              style: new TextStyle(
//                                                                  fontSize:
//                                                                  22.0,
//                                                                  color: kinderm8Theme
//                                                                      .Colors
//                                                                      .appsupportlabel,
//                                                                  fontStyle:
//                                                                  FontStyle
//                                                                      .normal),
//                                                            ),
//                                                          ),
//                                                          new GestureDetector(
//                                                            onTap: () {
//                                                              print(
//                                                                  singlecommentDetails);
//                                                              print(
//                                                                  singlecommentDetails[
//                                                                  'id']);
//                                                              Navigator.of(
//                                                                  context)
//                                                                  .pop();
//                                                              deleteComment(
//                                                                  singlecommentDetails[
//                                                                  'id']);
//                                                            },
//                                                            child:
//                                                            new Container(
//                                                              width: screenSize
//                                                                  .width /
//                                                                  2,
//                                                              margin: new EdgeInsets
//                                                                  .only(
//                                                                  top: 10.0,
//                                                                  left: 5.0,
//                                                                  right:
//                                                                  5.0,
//                                                                  bottom:
//                                                                  10.0),
//                                                              height: 30.0,
//                                                              decoration: new BoxDecoration(
//                                                                  color: Kinderm8Theme
//                                                                      .Colors
//                                                                      .buttonicon_deletecolor,
//                                                                  borderRadius: new BorderRadius
//                                                                      .all(new Radius
//                                                                      .circular(
//                                                                      10.0))),
//                                                              child:
//                                                              new Center(
//                                                                  child:
//                                                                  new Text(
//                                                                    "Delete",
//                                                                    style: new TextStyle(
//                                                                        color: Colors
//                                                                            .white,
//                                                                        fontSize:
//                                                                        22.0),
//                                                                  )),
//                                                            ),
//                                                          ),
//                                                        ],
//                                                      ),
//                                                    ));
//                                              });
//                                        },
//                                      )*/
//                                        : Text("false"),
//                                    title: new Text(
//                                      "${singlecomment['comment_text']}",
//                                      style: new TextStyle(
//                                          fontWeight: FontWeight.w500,
//                                          fontSize: 14.0),
//                                    ),
//                                    /*subtitle: Container(
//                                          child: Column(
//                                            crossAxisAlignment:
//                                            CrossAxisAlignment.stretch,
//                                            children: <Widget>[
//                                              Container(
//                                                padding: EdgeInsets.only(top: 5.0),
//                                                child: new Text(
//                                                  date,
//                                                  style: new TextStyle(
//                                                      color: Colors.grey.shade500,
//                                                      fontSize: 10.0),
//                                                ),
//                                              ),
//                                              Container(
//                                                padding: EdgeInsets.only(top: 5.0),
//                                                child: Row(
//                                                  children: <Widget>[
//                                                    Flexible(
//                                                      child: Text(" $comments",
//                                                          style: new TextStyle(
//                                                              color: Colors.black,
//                                                              fontSize: 12.0)),
//                                                    )
//                                                  ],
//                                                ),
//                                              )
//                                            ],
//                                          ))*/
//                                  ),
                                  ],
                                ),
                              ));
                              /*return Container(
                              child: Text(
                                commentData[i]['comment_text'],
                                style: TextStyle(
                                    color: kinderm8Theme.Colors.darkGrey),
                              ),
                              padding:
                                  EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                              width: 200.0,
                              decoration: BoxDecoration(
                                  color: kinderm8Theme.Colors.appcolour,
                                  borderRadius: BorderRadius.circular(8.0)),
                              margin: EdgeInsets.only(
//                      bottom: isLastMessageRight(index) ? 20.0 : 10.0,
                                  right: 10.0),
                            );*/
                            },
                          ),
                        )
                      : Expanded(child: Center(child: Text("No Comments yet"))),
                  Container(
                    child: Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            child: Form(
                                key: formKey,
                                child: TextFormField(
                                  textInputAction: TextInputAction.done,
                                  maxLines: 1,
                                  controller: _textController,
                                  validator: (val) {
                                    return val.length == 0
                                        ? "Comment must not be null"
                                        : null;
                                  },
                                  decoration: InputDecoration(
                                    fillColor: Colors.grey[300],
                                    filled: true,
                                    contentPadding: EdgeInsets.fromLTRB(
                                        10.0, 30.0, 10.0, 10.0),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                    ),
                                  ),
                                )),
                          ),
                          new RaisedButton(
                            onPressed: () {
                              if (submit) {
                                setState(() {
                                  submit = false;
                                });
                                submitComment();
                              } else {
                                print("main submission $submit");
                              }
                            },
                            color: kinderm8Theme.Colors.appmenuicon,
                            padding: EdgeInsets.all(10.0),
                            child: submit
                                ? Container(
                                    child: Column(
                                      // Replace with a Row for horizontal icon + text
                                      children: <Widget>[
                                        Icon(
                                          Icons.send,
                                          color: kinderm8Theme
                                              .Colors.buttonicon_darkcolor,
                                        ),
                                        Text("Submit",
                                            style: new TextStyle(
                                                fontSize: 12.0,
                                                color: kinderm8Theme
                                                    .Colors.buttonicon_color))
                                      ],
                                    ),
                                  )
                                : CircularProgressIndicator(
                                    strokeWidth: 2.0,
                                  ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
          : progress,
    );
  }

  var progress = new ProgressBar(
    color: kinderm8Theme.Colors.appcolour,
    containerColor: kinderm8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  getCommentData() {
    print("get observation CommentData");
//    postfeedId = data["id"];
//    cliId = user["client_id"];
//    userId = user["id"];
    String getCommentUrl =
        'http://13.55.4.100:7070/getjourneycomments?journeyid=$observationId&clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};

    print(jwt);

    NetworkUtil _netutil = new NetworkUtil();
//    print('getCommentUrl $getCommentUrl');
    _netutil.get(getCommentUrl, headers: headers).then((response) {
      print('response get $response');
      print('res get ${response.body}');
      commentData = json.decode(response.body);

      print('map $commentData');
//      print(commentData[1]["comment_text"]);
      print(response.statusCode);
      if (response.statusCode == 200) {
//        commentList = commentData[0]["comment_text"];

        setState(() {
          load = true;
          submit = true;
          delete = false;
        });

      } else if (response.statusCode == 500 &&
          commentData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      }
    });
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'http://13.55.4.100:7070/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      getCommentData();
    });
  }

  submitComment() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      FocusScope.of(context).requestFocus(new FocusNode());

      final submitcommentUrl =
          'http://13.55.4.100:7070/jnycomment?clientid=$clientId';
      var body = {
        "journey_id": observationId,
        "user_id": id,
        "comment_text": _textController.text,
      };
      var headers = {"x-authorization": jwt.toString()};

      NetworkUtil _netUtil = new NetworkUtil();
      _netUtil
          .post(submitcommentUrl,
              body: body, headers: headers, encoding: jwt.toString())
          .then((res) {
        print(res);
//        var result = json.decode(res);
        _textController.clear();
        getCommentData();
      });
    } else {
      print("else");
      submit = true;
    }
  }

  void deleteComment(deletecommentid) async {
//    setState(() => load = false);
    setState(() {
      delete = true;
    });
    String deleteUrl =
        'http://13.55.4.100:7070/v2.1.0/journey/deletejourneycomment?clientid=$clientId';
    var body = {"id": deletecommentid, "user_id": id};
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.delete(deleteUrl, headers: headers, body: body).then((res) {
      print(res);

      getCommentData();
    });
  }

  @override
  void onDisplayUserInfo(User user) {
    observationId = widget.singleData["id"];
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
//      print(appusers);
      jwt = widget.jwt.toString();
//      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];

//      print("iddd $id");
//      print(clientId);
      getCommentData();
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
