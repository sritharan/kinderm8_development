import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/Theme.dart' as Kinderm8Theme;
import 'package:kinderm8/components/page_transformer.dart';
import 'package:kinderm8/pages/dailyjournal/followupviewlist.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/home/modals/child_photos_modal.dart';
import 'package:kinderm8/pages/newsfeed/learningoutcomes.dart';
import 'package:kinderm8/pages/newsfeed/learningtags.dart';
import 'package:kinderm8/pages/observation/observationcomment.dart';
import 'package:kinderm8/utils/commonutils/zoomable_image_page_journal.dart';
import 'package:kinderm8/utils/commonutils/zoomable_image_page_observation.dart';
import 'package:html2md/html2md.dart' as html2md;

class ObservationDetails extends StatefulWidget {
  final singleObservationData;
  final jwt;
  ObservationDetails(this.singleObservationData,this.jwt);
  @override
  ObservationDetailsState createState() {
    return new ObservationDetailsState(singleObservationData);
  }

//  @override
//  _EventPageState createState() => new _EventPageState(eventdata);
}

class ObservationDetailsState extends State<ObservationDetails>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  ScrollController scrollController;
  Animation<Color> colorTween1;
  Animation<Color> colorTween2;

  final singleObservationData;
  var learningTag, learningOutcomeDetail;
  final double statusBarSize = 24.0;
  final double imageSize = 264.0;
  double readPerc = 0.0;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  ObservationDetailsState(this.singleObservationData);

  var title, description;
  var learning_set_title = "";
  List learningSetTitle = new List();

  List<Widget> widgets = [];
  var color, splitColor, tag;
  int colorCode;

  @override
  void initState() {
    super.initState();
    animationController = new AnimationController(
        duration: new Duration(milliseconds: 500), vsync: this);
    animationController.addListener(() => setState(() {}));

    colorTween1 = new ColorTween(
            begin: Kinderm8Theme.Colors.app_blue[50],
            end: new Color(0xFFB39DDB))
        .animate(new CurvedAnimation(
            parent: animationController, curve: Curves.easeIn));
    colorTween2 = new ColorTween(
            begin: Kinderm8Theme.Colors.app_blue[50],
            end: new Color(0xFF512DA8))
        .animate(new CurvedAnimation(
            parent: animationController, curve: Curves.easeIn));

    scrollController = new ScrollController();
    scrollController.addListener(() {
      setState(() => readPerc =
          scrollController.offset / scrollController.position.maxScrollExtent);

      // Change the appbar colors
      if (scrollController.offset > imageSize - statusBarSize) {
        if (animationController.status == AnimationStatus.dismissed)
          animationController.forward();
      } else if (animationController.status == AnimationStatus.completed)
        animationController.reverse();
    });
    learningTags();
    groupLearningOutComes();
  }

  void groupLearningOutComes() {
    print("${singleObservationData["learningsetchildrens"]}");
    if (singleObservationData["learningsetchildrens"] != null) {
      learningOutcomeDetail = singleObservationData["learningsetchildrens"];
      for (int i = 0; i < learningOutcomeDetail.length; i++) {
        if (learning_set_title ==
            learningOutcomeDetail[i]["learning_set_title"]) {
        } else {
          learning_set_title = learningOutcomeDetail[i]["learning_set_title"];

          learningSetTitle.add(learningOutcomeDetail[i]["learning_set_title"]);
        }
      }
      print("learningSetTitle______- $learningSetTitle");
      print("${learningSetTitle.length}");
    } else {
      print("else grouping");
    }
  }

  learningTags() {
    if (singleObservationData["learningtagdetails"] != null) {
      print("${singleObservationData["learningtagdetails"].length}");
      learningTag = singleObservationData["learningtagdetails"];

      for (int i = 0; i < learningTag.length; i++) {
        tag = learningTag[i];
        color = tag["color_code"].toString();
        splitColor = '0xFF' + color.substring(1);
        colorCode = int.parse(splitColor);
        widgets.add(
          Padding(
            padding: const EdgeInsets.all(1.0),
            child: new Chip(
              padding: EdgeInsets.all(0.0),
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              label: Text(
                '${tag["name"]}',
                style: TextStyle(color: Colors.white, fontSize: 11.0),
              ),
              backgroundColor: Color(colorCode),
            ),
          ),
        );
      }
    } else {
      print("else tag");
    }
  }

  Widget _buildFollowups(FollowupsData) {
    final Size screenSize = MediaQuery.of(context).size;
    final cardheight = screenSize.height / 2.0;
    return new SizedBox.fromSize(
      size: Size.fromHeight(cardheight),
      child: PageTransformer(
        pageViewBuilder: (context, visibilityResolver) {
          return PageView.builder(
            controller: PageController(viewportFraction: 0.85),
            itemCount: FollowupsData.length,
            itemBuilder: (context, index) {
              final pageVisibility =
                  visibilityResolver.resolvePageVisibility(index);
              return FollowupPageView(
                followupitem: FollowupsData[index],
                pageVisibility: pageVisibility,
              );
            },
          );
        },
      ),
    );
  }

  Widget _buildPrinciples(PrinciplesData) {
    final Size screenSize = MediaQuery.of(context).size;
//    print('_buildPrinciples : ${PrinciplesData.length} | ${PrinciplesData}');
    PrinciplesData = PrinciplesData;
    return new Container(
      padding: EdgeInsets.all(10.0),
      child: new ListView.builder(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemCount: PrinciplesData.length,
          itemBuilder: (context, i) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(
                  PrinciplesData[i]["title"],
                  style: TextStyle(
                      color: Kinderm8Theme.Colors.app_dark[400],
                      fontSize: 13.0,
                      fontWeight: FontWeight.w600),
                ),
                PrinciplesData[i]["description"] != null
                    ? new Text(PrinciplesData[i]["description"],
                        style: TextStyle(
                          color: Kinderm8Theme.Colors.app_dark[200],
                          fontSize: 12.0,
                        ))
                    : Text(""),
              ],
            );
          }),
    );
  }

  Widget _buildPractices(PracticesData) {
    final Size screenSize = MediaQuery.of(context).size;
//    print('_buildPractices: ${PracticesData.length} | ${PracticesData}');
    PracticesData = PracticesData;
    return new Container(
      padding: EdgeInsets.all(10.0),
      child: new ListView.builder(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemCount: PracticesData.length,
          itemBuilder: (context, i) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(
                  PracticesData[i]["title"],
                  style: TextStyle(
                      color: Kinderm8Theme.Colors.app_dark[400],
                      fontSize: 13.0,
                      fontWeight: FontWeight.w600),
                ),
                PracticesData[i]["description"] != null
                    ? new Text(PracticesData[i]["description"],
                        style: TextStyle(
                          color: Kinderm8Theme.Colors.app_dark[200],
                          fontSize: 12.0,
                        ))
                    : Text(""),
              ],
            );
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    var S3URL = "https://d212imxpbiy5j1.cloudfront.net/";
    final Size screenSize = MediaQuery.of(context).size;

    /* // Format */
    var JournalCreatedDate =
        DateTime.parse(singleObservationData["created_at"]);
    var timeStamp = new DateFormat("yMMMEd");
    String formatObservationCreatedDate = timeStamp.format(JournalCreatedDate);

    final observationimagecount =
        this.singleObservationData['journeygallery'].length;
    List<ChildPhotoModal> imgUrl = List();
    var finalobservationImages = List();
    if (observationimagecount > 0) {
      for (int i = 0; i < singleObservationData['journeygallery'].length; i++) {
        var fullurl =
            "https://d212imxpbiy5j1.cloudfront.net/${singleObservationData['journeygallery'][i]['url']}";
        finalobservationImages.add(new CachedNetworkImageProvider(fullurl));
        imgUrl.add(ChildPhotoModal(fileUrl: fullurl, date: "", caption: ""));
      }
    }
//

    var linearGradient = const BoxDecoration(
      gradient: const LinearGradient(
        begin: FractionalOffset.centerRight,
        end: FractionalOffset.bottomLeft,
        colors: <Color>[
          const Color(0xFF413070),
          const Color(0xFF2B264A),
        ],
      ),
    );

    Widget _buildFollowerStat(String title, String value) {
      final titleStyle = new TextStyle(
          fontSize: 12.0,
//          fontFamily: ProfileFontNames.TimeBurner,
          color: Kinderm8Theme.Colors.appcolour);
      final valueStyle = new TextStyle(
//          fontFamily: ProfileFontNames.TimeBurner,
          fontSize: 12.0,
          fontWeight: FontWeight.w700,
          color: Kinderm8Theme.Colors.darkGrey);
      return Flexible(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Text(
              title,
              style: titleStyle,
              overflow: TextOverflow.ellipsis,
              maxLines: 4,
            ),
            new Text(value,
                style: valueStyle,
                overflow: TextOverflow.ellipsis,
                maxLines: 4),
          ],
        ),
      );
    }

    Widget _buildVerticalDivider() {
      return new Container(
        height: 40.0,
        width: 1.0,
        color: Kinderm8Theme.Colors.appcolour,
        margin: const EdgeInsets.only(left: 10.0, right: 10.0),
      );
    }

    var observation;
    if (singleObservationData['observation'] != '' &&
        singleObservationData['observation'] != null) {
      observation = html2md.convert(singleObservationData['observation']);
    }

    return new Scaffold(
        body: SingleChildScrollView(
          child: new Stack(
            children: <Widget>[
              new Container(
                margin: const EdgeInsets.only(top: 0.0),
                height: screenSize.height,
                decoration: linearGradient,
                child: new Hero(
                  tag: 'JournalDetails',
                  child: new Material(
                    color: Colors.white,
                    child:
                    new Stack // I need to use a stack because otherwise the appbar blocks the content even if transparent
                        (
                  children: <Widget>[
//                        _buildDiagonalImageBackground(context),
                    /// Image and text
                    new ListView(
                      padding: new EdgeInsets.all(0.0),
                      controller: scrollController,
                      children: <Widget>[
/*//                    singleJournalData['image_url'] != '' ? new SizedBox.fromSize(
//                      size: new Size.fromHeight(imageSize),
//                      child: new Hero(
//                        tag: 'Image',
////                      child: new Image.asset('res/img1.png', fit: BoxFit.cover),
//                        child: new CachedNetworkImage(
//                          imageUrl: S3URL + singleJournalData['image_url'],
//                          placeholder: new CupertinoActivityIndicator(),
////                errorWidget: new Icon(Icons.error),
//                          fadeOutDuration: new Duration(seconds: 1),
//                          fadeInDuration: new Duration(seconds: 3),
//                        ),
//                      ),
//                    ) : new Container(
//                      height:  screenSize.width/3,
////                      margin: EdgeInsets.only(top: screenSize.width/3),
//                    ),
//                    new Padding(
//                      padding: new EdgeInsets.symmetric(vertical: 12.0),
//                      child: new Text('INVISION PRESENTS',
//                          textAlign: TextAlign.center,
//                          style: new TextStyle(
//                              fontSize: 11.0,
//                              color: Colors.black,
//                              fontWeight: FontWeight.w700)),
//                    ),

                        */ /* ArcBannerImage image for top section*/ /*
//                            singleJournalData['journalgallery'].length > 0 ? new Padding(
//                              padding: const EdgeInsets.only(bottom:2.0),
//                              child: new ArcBannerImage(
//                                  'https://d212imxpbiy5j1.cloudfront.net/${singleJournalData['journalgallery'][0]['url']}'),
//                            ):
//                            new Container(
//                              margin: new EdgeInsets.only(top:screenSize.width/4),
//                            ),*/

                            new Container(
                              margin:
                              new EdgeInsets.only(top: screenSize.width / 4),
                            ),
                            new Padding(
//                              padding: new EdgeInsets.symmetric(horizontal: 16.0),
                              padding:
                              new EdgeInsets.only(left: 16.0, bottom: 15.0),
                          child: new Hero(
                            tag: 'Title',
                            child: new Text(
                                singleObservationData['journey_title'],
                                textAlign: TextAlign.left,
                                style: new TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600)),
                          ),
                        ),
//                  new Padding
//                    (
//                    padding: new EdgeInsets.only(top: 12.0, bottom: 24.0),
//                    child: new Text(eventdata['date'], textAlign: TextAlign.center, style: new TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.w700)),
//                  ),
//                            new Row(
//                              mainAxisAlignment: MainAxisAlignment.center,
//                              children: <Widget>[
//                                new Column(
//                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                  children: <Widget>[
//                                    Container(
//                                      margin: new EdgeInsets.only(left: 5.0),
//                                      child: new IconButton(
//                                        onPressed: () {},
//                                        icon: new Icon(Icons.today,
//                                          color: Kinderm8Theme.Colors.appcolour,
//                                          size: 35.0,),
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                              ],
//                            ),
//                              new Row(
//                                mainAxisAlignment: MainAxisAlignment.center,
//                                crossAxisAlignment: CrossAxisAlignment.center,
//                                children: <Widget>[
//                                  _buildFollowerStat("Date", formatJournalCreatedDate.toString()),
//                                  _buildVerticalDivider(),
//                                  _buildFollowerStat("Educator", singleJournalData['staff']['fullname']),
//                                  _buildVerticalDivider(),
//                                  _buildFollowerStat("Room", singleJournalData['room']['title']),
//                                ],
//                              ),
//                            new Padding(
////                              padding: const EdgeInsets.only(top: 16.0),
//                              padding: new EdgeInsets.only(left: 16.0,bottom: 2.0),
//                              child: new Row(
//                                children: <Widget>[
//                                  _createCircleBadge(Icons.person, theme.backgroundColor),
//                                  new Text(' Educator :'),
//                                  new Text(singleJournalData['staff']['fullname'])
//                                ],
//                              ),
//                            ),
//                            new Padding(
////                              padding: const EdgeInsets.only(top: 16.0),
//                              padding: new EdgeInsets.only(left: 16.0,bottom: 2.0),
//                              child: new Row(
//                                children: <Widget>[
//                                  _createCircleBadge(Icons.create, theme.backgroundColor),
//                                  new Text(' Date :'),
//                                  new Text(formatJournalCreatedDate.toString())
//                                ],
//                              ),
//                            ),
//                            new Container(
//                              padding: const EdgeInsets.only(right: 16.0),
//                              child: Image.network(journalurl,
//                                  fit: BoxFit.fill, width: 200.0, height: 150.0),
//                            ),
                            Material(
                              elevation: 6.0,
                              color: Kinderm8Theme.Colors.app_white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20.0),
                                topRight: Radius.circular(20.0),
//                                bottomLeft: Radius.circular(20.0),
//                                bottomRight: Radius.circular(20.0),
                              ),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  _buildFollowerStat(
                                      labelsConfig["journeyDateLabel"], formatObservationCreatedDate.toString()),
                                  _buildVerticalDivider(),
                                  _buildFollowerStat(labelsConfig["educatorJourneyLabel"],
                                      singleObservationData['staff']['fullname']),
//                                  _buildVerticalDivider(),
//                                  _buildFollowerStat(
//                                      "Room", singleObservationData['room']['title']),
                                ],
                              ),
                            ),
                            finalobservationImages.length > 0
                                ? Card(
                              child: Container(
                                margin: new EdgeInsets.only(
                                    top: 10.0,
                                    left: 5.0,
                                    right: 5.0,
                                    bottom: 5.0),
                                child: FlatButton(
                                    child: new SizedBox(
                                        height: screenSize.width / 2,
                                        width: 350.0,
                                        child: new Carousel(
                                          animationCurve:
                                          ElasticInCurve(10.0),
                                          images: finalobservationImages,
                                          dotSize: 4.0,
                                          dotSpacing: 15.0,
                                          dotColor: Colors.white,
                                          indicatorBgPadding: 5.0,
//                                dotBgColor: Colors.purple.withOpacity(0.5),
                                          autoplay: false,
//                                  borderRadius: true,
//                                  radius: Radius.circular(5.0),
                                        )),
                                    onPressed: () {
                                      print("Have to Navigate image view..");

                                      Navigator.push(
                                          context,
                                          new MaterialPageRoute(
                                              builder: (context) =>
                                              new ZoomableImagePage_Journal(
                                                  imgUrl)));
                                    }),
                              ),
                            )
                                : new Container(),

                            // --------------- //
                            // Image List with //
                            // --------------- //
//                            new Container(
//                                height: screenSize.width /2,
//                                child: new ListView.builder(
//                                    scrollDirection: Axis.horizontal,
//                                    shrinkWrap: true,
//                                    physics: const ClampingScrollPhysics(),
//                                    itemCount: journalimagecount,
//                                    // itemExtent: 10.0,
//                                    // reverse: true, //makes the list appear in descending order
//                                    itemBuilder: (BuildContext context, int index) {
//                                      var img = "https://d212imxpbiy5j1.cloudfront.net/${singleJournalData['journalgallery'][index]['url']}";
//                                      return new Container(
//                                        // color: Colors.blue,
////                                        width: 100.0,
////                                        height: 100.0,
////                                        padding: const EdgeInsets.all(10.0),
//                                        child: new Row(
//                                          children: [
//                                            new Row(children: <Widget>[
//                                              FlatButton(
//                                                  child: new Container(
//                                                      width: 100.0,
//                                                      height: 100.0,
//                                                      child: new Image.network(
//                                                          "https://d212imxpbiy5j1.cloudfront.net/${singleJournalData['journalgallery'][index]['url']}"
//                                                      )
//                                                  )
//                                                  , onPressed: () {
//                                                print("Have to Navigate image view..");
////                                                Navigator.push(
////                                                    context,
////                                                    new MaterialPageRoute(
////                                                        builder: (context) =>
////                                                        new ZoomableImagePage(img)
////                                                    )
////                                                );
//                                              }
//                                              ),
//                                            ],)
//                                          ],
//                                        ),
//                                      );
//                                    })),
                        // --------------- //
                        // End of Image List with //
                        // --------------- //
                        new Card(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              /* observation */
                              singleObservationData['observation'] != '' &&
                                      singleObservationData['observation'] !=
                                          null
                                  ? new Column(
                                      children: <Widget>[
                                        Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                                          child: new Text(labelsConfig["observationSingularLabel"],
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Kinderm8Theme
                                                      .Colors.app_white,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                        new Container(
                                          margin: new EdgeInsets.only(
                                              top: 10.0,
                                              left: 20.0,
                                              right: 20.0,
                                              bottom: 10.0),
                                          child: Html(
                                            data: observation,
                                            defaultTextStyle: new TextStyle(
                                                fontSize: 13.0,
                                                color: Kinderm8Theme
                                                    .Colors.darkGrey,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                      ],
                                    )
                                  : Container(
                                      padding: EdgeInsets.all(0.0),
                                    ),
//

                              /* Reflection section */
                              singleObservationData['reflection'] != '' &&
                                      singleObservationData['reflection'] !=
                                          null
                                  ? Column(
                                      children: <Widget>[
                                        new Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                                        child: new Text(labelsConfig["reflectionJourneyLabel"],
                                            style: new TextStyle(
                                                fontSize: 16.0,
                                                color: Kinderm8Theme
                                                    .Colors.app_white,
                                                fontWeight: FontWeight.w500)),
                                      ),
                                      new Container(
                                        margin: new EdgeInsets.only(
                                            top: 10.0,
                                            left: 20.0,
                                            right: 20.0,
                                            bottom: 10.0),
                                        child: new Text(
                                            singleObservationData['reflection'],
                                            style: new TextStyle(
                                                fontSize: 13.0,
                                                color: Kinderm8Theme
                                                    .Colors.darkGrey,
                                                fontWeight: FontWeight.w400)),
                                      ),
                                    ],
                                  )
                                      : new Container(
                                    padding: EdgeInsets.all(0.0),
                                  ),

                              /* Principles section */
                              //
                              singleObservationData['Principles'] != null
                                  ? Column(
                                      children: <Widget>[
                                        new Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                                          child: new Text(labelsConfig["principlesourneyLabel"],
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Kinderm8Theme
                                                      .Colors.app_white,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                        singleObservationData['Principles']
                                                    .length >
                                                0
                                            ? _buildPrinciples(
                                                singleObservationData[
                                                    'Principles'])
                                            : Container(),
                                      ],
                                    )
                                  : Container(
                                      padding: EdgeInsets.all(0.0),
                                    ),

                              /* Practices section */
                              //

                              singleObservationData['Practices'] != null
                                  ? Column(
                                      children: <Widget>[
                                        new Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                                          child: new Text(labelsConfig["practicesJourneyLabel"],
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Kinderm8Theme
                                                      .Colors.app_white,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                        singleObservationData['Practices']
                                                    .length >
                                                0
                                            ? _buildPractices(
                                                singleObservationData[
                                                    'Practices'])
                                            : Container()
                                      ],
                                    )
                                  : Container(
                                      padding: EdgeInsets.all(0.0),
                                    ),
                              new Container(height: 5.0),

                              ///
                              ///
// new learning Outcomes
                              singleObservationData["learningsetchildrens"] !=
                                      null
                                  ? Column(
                                      children: <Widget>[
                                        new Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
                                          child: new Text(labelsConfig["learningOutcomesJourneyLabel"],
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Kinderm8Theme
                                                      .Colors.app_white,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: new ListView.builder(
                                                physics:
                                                    ClampingScrollPhysics(),
                                                shrinkWrap: true,
                                                itemCount:
                                                    learningSetTitle.length > 1
                                                        ? 1
                                                        : learningSetTitle
                                                            .length,
                                                itemBuilder: (context, i) {
                                                  return Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Text(
                                                            "${learningSetTitle[i]}",
                                                            style: TextStyle(
                                                                color: Kinderm8Theme
                                                                        .Colors
                                                                        .app_blue[
                                                                    300],
                                                                fontSize: 14.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                          Container(
                                                              height: 8.0),
                                                          ListView.builder(
                                                            shrinkWrap: true,
                                                            physics:
                                                                ClampingScrollPhysics(),
                                                            itemCount: learningOutcomeDetail
                                                                        .length >
                                                                    5
                                                                ? 5
                                                                : learningOutcomeDetail
                                                                    .length,
                                                            itemBuilder:
                                                                (context, j) {
                                                              var description;
                                                              if (learningOutcomeDetail[
                                                                          j][
                                                                      "description"] !=
                                                                  null) {
                                                                description =
                                                                    html2md.convert(
                                                                        learningOutcomeDetail[j]
                                                                            [
                                                                            "description"]);
                                                              }

                                                              for (int k = 0;
                                                                  k <
                                                                      learningOutcomeDetail
                                                                          .length;
                                                                  k++) {
                                                                return Padding(
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              0.0),
                                                                  child: learningSetTitle[i] ==
                                                                          learningOutcomeDetail[j]
                                                                              [
                                                                              "learning_set_title"]
                                                                      ? Card(
                                                                          elevation:
                                                                              4.0,
                                                                          margin: EdgeInsets.all(
                                                                              2.0),
                                                                          child:
                                                                              Container(
                                                                            padding:
                                                                                EdgeInsets.all(10.0),
                                                                            child:
                                                                                Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: <Widget>[
                                                                                Text(
                                                                                  learningOutcomeDetail[j]["title"],
                                                                                  style: TextStyle(color: Kinderm8Theme.Colors.app_dark[400], fontSize: 13.0, fontWeight: FontWeight.w600),
                                                                                ),
                                                                                learningOutcomeDetail[j]["description"] != null
                                                                                    ? Html(
                                                                                        data: description,
                                                                                        defaultTextStyle: TextStyle(
                                                                                          color: Kinderm8Theme.Colors.app_dark[200],
                                                                                          fontSize: 12.0,
                                                                                        ),
                                                                                      )
                                                                                    : Text(""),
                                                                              ],
                                                                            ),
                                                                          ))
                                                                      : null,
                                                                );
                                                              }
                                                            },
                                                          )
                                                        ],
                                                      ),
                                                      learningOutcomeDetail
                                                                  .length >
                                                              5
                                                          ? new InkWell(
                                                              child: new Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .end,
                                                                children: <
                                                                    Widget>[
                                                                  new Text(
                                                                    "show more",
                                                                    style: new TextStyle(
                                                                        color: Kinderm8Theme
                                                                            .Colors
                                                                            .appcolour),
                                                                  ),
                                                                ],
                                                              ),
                                                              onTap: () {
                                                                setState(() {
                                                                  Navigator.push(
                                                                      context,
                                                                      MaterialPageRoute(
                                                                          builder: (context) =>
                                                                              new LearningOutcomes(learningOutcomeDetail)));
                                                                });
                                                              },
                                                            )
                                                          : Container(),
                                                      Container(
                                                        height: 10.0,
                                                      )
                                                    ],
                                                  );
                                                },
                                              ),
                                            )
                                          ],
                                        ),
                                      ],
                                    )
                                  : Container(
                                      padding: EdgeInsets.all(0.0),
                                    ),

                              // new learning Tags
                              singleObservationData["learningtagdetails"] !=
                                      null
                                  ? Column(
                                      children: <Widget>[
                                        new Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
                                          child: new Text(labelsConfig["learningTagsJourneyLabel"],
                                              style: new TextStyle(
                                                  fontSize: 16.0,
                                                  color: Kinderm8Theme
                                                      .Colors.app_white,
                                                  fontWeight: FontWeight.w500)),
                                        ),
                                        Container(
                                          width: screenSize.width - 10.0,
                                          padding: EdgeInsets.only(
                                              left: 10.0, top: 5.0),
                                          child: Wrap(
                                            spacing: 4.0,
                                            runSpacing: 1.0,
                                            children: widgets,
                                          ),
                                        )
                                      ],
                                    )
                                  : Container(
                                      padding: EdgeInsets.all(0.0),
                                    ),

                              ///
                              ///
                              /* Leaning Tag And Leaning Outcome*/
//                              new Container(height: 5.0),
                              /* Leaning Tag */
//                                  new Container(
//                                    padding: EdgeInsets.only(left: 10.0, top: 5.0),
//                                    child: Wrap(
//                                      spacing: 4.0,
//                                      runSpacing: 1.0,
//                                      children: learningTagwidgets,
//                                    ),
//                                  ),
                                  /* Leaning Outcome */
//                                  new Column(
//                                    mainAxisAlignment: MainAxisAlignment.end,
//                                    children: <Widget>[
//                                      GestureDetector(
//                                        child: Container(
//                                            padding: EdgeInsets.all(3.0),
////                                            decoration: new BoxDecoration(
////                                              borderRadius:
////                                              new BorderRadius.circular(25.0),
////                                              border: new Border.all(
////                                                width: 2.0,
////                                                color: Kinderm8Theme.Colors.appcolour,
////                                              ),
////                                            ),
////                                      width: ((screenSize.width   / 5)),
//                                            child: Row(
//                                              mainAxisAlignment:
//                                              MainAxisAlignment.center,
//                                              children: <Widget>[
//                                                Icon(FontAwesomeIcons.listAlt,
//                                                    size: 14.0,
//                                                    color: Colors.grey[600]),
//                                                Container(width: 6.0),
//                                                Text("Learning Outcomes",
//                                                    style: new TextStyle(
//                                                        fontSize: 12.0)),
//                                              ],
//                                            )),
//                                        onTap: () {
//                                          print(
//                                              "have to navigte to Learning Outcomes");
//                                          Navigator.push(
//                                              context,
//                                              MaterialPageRoute(
//                                                  builder: (context) =>
//                                                  new LearningOutcomes(
//                                                      learningOutcomeDetail)));
//                                        },
//                                      )
//                                    ],
//                                  ),
//
                                  /* Additional field 1 section - materials */
                                  singleObservationData['comments'] != '' &&
                                      singleObservationData['comments'] != null
                                      ? Column(
                                    children: <Widget>[
                                      new Container(
                                        alignment: Alignment.center,
                                        width: screenSize.width,
                                        padding: const EdgeInsets.only(
                                            top: 5.0, bottom: 5.0),
                                        color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                                        child: new Text(
                                            'Additional Text Field 1',
                                            style: new TextStyle(
                                                fontSize: 16.0,
                                                color: Kinderm8Theme
                                                    .Colors.app_white,
                                                fontWeight: FontWeight.w500)),
                                      ),
                                      new Container(
                                        margin: new EdgeInsets.only(
                                            top: 10.0,
                                            left: 20.0,
                                            right: 20.0,
                                            bottom: 10.0),
                                        child: new Text(
                                            singleObservationData['comments'],
                                            style: new TextStyle(
                                                fontSize: 13.0,
                                                color: Kinderm8Theme
                                                    .Colors.darkGrey,
                                                fontWeight: FontWeight.w400)),
                                      ),
                                    ],
                                  )
                                      : Container(
                                    padding: EdgeInsets.all(0.0),
                                  ),

                              /* Additional field 2 section - materials */
                              singleObservationData['additional_text_2'] !=
                                          '' &&
                                      singleObservationData[
                                              'additional_text_2'] !=
                                          null
                                  ? Column(
                                      children: <Widget>[
                                        new Container(
                                          alignment: Alignment.center,
                                          width: screenSize.width,
                                          padding: const EdgeInsets.only(
                                              top: 5.0, bottom: 5.0),
                                          color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                                        child: new Text(
                                            'Additional Text Field 2',
                                            style: new TextStyle(
                                                fontSize: 16.0,
                                                color: Kinderm8Theme
                                                    .Colors.app_white,
                                                fontWeight: FontWeight.w500)),
                                      ),
                                      new Container(
                                        margin: new EdgeInsets.only(
                                            top: 10.0,
                                            left: 20.0,
                                            right: 20.0,
                                            bottom: 10.0),
                                        child: new Text(
                                            singleObservationData['additional_text_2'],
                                            style: new TextStyle(
                                                fontSize: 13.0,
                                                color: Kinderm8Theme
                                                    .Colors.darkGrey,
                                                fontWeight: FontWeight.w400)),
                                      ),
                                    ],
                                  )
                                      : Container(
                                    padding: EdgeInsets.all(0.0),
                                  ),

                                  /* Follow Up section  Start */
                                  singleObservationData['followups'].length > 0
                                      ? _buildFollowups(
                                      singleObservationData['followups'])
                                      : Container(),

                                  /* last element section bottom */
                                  new Container(
                                    margin: new EdgeInsets.only(
                                        bottom: screenSize.width / 2),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),

                        /// Appbar
                        new Align(
                          alignment: Alignment.topCenter,
                          child: new SizedBox.fromSize(
                            size: new Size.fromHeight(90.0),
                            child: new Stack(
                              alignment: Alignment.centerLeft,
                              children: <Widget>[
                                new SizedBox
                                    .expand // If the user scrolled over the image
                                  (
                                  child: new Material(
                                    color: colorTween1.value,
                                  ),
                                ),
                                new SizedBox.fromSize // TODO: Animate the reveal
                                  (
                                  size: new Size.fromWidth(
                                      MediaQuery.of(context).size.width * readPerc),
                                  child: new Material(
                                    color: colorTween2.value,
                                  ),
                                ),
                                new Align(
                                    alignment: Alignment.center,
                                    child: new SizedBox.expand(
                                      child:
                                      new Material // So we see the ripple when clicked on the iconbutton
                                        (
                                        color: Colors.transparent,
                                        child: new Container(
                                            margin: new EdgeInsets.only(
                                                top: 24.0), // For the status bar
                                            child: new Row(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              children: <Widget>[
                                                new IconButton(
                                                  onPressed: () =>
                                                      Navigator.of(context).pop(),
                                                  icon: new Icon(Icons.arrow_back,
                                                      color: Colors.white),
                                                ),
//                                        new Hero(
//                                            tag: 'Logo',
//                                            child: new Icon(Icons.event,
//                                                color: Colors.white,
//                                                size: 48.0)),
                                                new IconButton(
                                                  onPressed: () {
                                                    Navigator.of(context).push(
                                                        new MaterialPageRoute<Null>(
                                                          builder:
                                                              (BuildContext context) {
                                                            return ObservationComment(
                                                                singleObservationData,
                                                                widget.jwt);
                                                          },
                                                        ));
                                                  },
                                                  icon: CircleAvatar(
                                                    backgroundColor: Colors.white,
                                                    child: new Icon(
                                                      Icons.comment,
                                                      color: Kinderm8Theme
                                                          .Colors.appcolour,
                                                      size: 25.0,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            )),
                                      ),
                                    ))
                              ],
                            ),
                          ),
                        ),

                        /// Fade bottom text
                        new Align(
                          alignment: Alignment.bottomCenter,
                          child: new SizedBox.fromSize(
                              size: new Size.fromHeight(100.0),
                              child: new Container(
                                decoration: new BoxDecoration(
                                    gradient: new LinearGradient(
                                        colors: [Colors.white12, Colors.white],
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                        stops: [0.0, 1.0])),
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
//        floatingActionButton: new FloatingActionButton(
//          onPressed: () async {
//            final FormState form = _formKey.currentState;
//            if (!form.validate()) {
//              _autovalidate = true; // Start validating on every change.
//              showInSnackBar('Please fix the errors in red before submitting.');
//            } else {
//              form.save();
////              var createEventResult =
////              await _deviceCalendarPlugin.createOrUpdateEvent(_event);
////              if (createEventResult.isSuccess) {
////                Navigator.pop(context, true);
////              } else {
////                showInSnackBar(createEventResult.errorMessages.join(' | '));
////              }
//            }
//          },
//          child: new Icon(Icons.event),
//        )
    );
  }
}
