import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/Theme.dart' as Kinderm8Theme;
import 'package:flutter/material.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/commondrawer.dart';
import 'package:kinderm8/pages/dailychart/dailychartdata.dart';
import 'package:kinderm8/pages/dailyjournal/dailyjournaldata.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:flutter_calendar/flutter_calendar.dart';

class ChildProfile extends StatefulWidget {
  final ChildViewModal childData;
  final jwt;
  ChildProfile(this.childData, this.jwt);
  @override
  ChildProfileState createState() => ChildProfileState();
}

class ChildProfileState extends State<ChildProfile>
    with SingleTickerProviderStateMixin
    implements HomePageContract {
  var k, appuser, jwt, id, clientId;
  List data;
  var childId;
  bool isLoading;
  bool load = true;
  HomePagePresenter _presenter;
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  ChildViewModal childProfileData;
  var attendanceTag, attendanceDetail;
  TabController controller;
  var pri;

  bool enabled = false;
  bool expanded = false;

  ChildProfileState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isLoading = false;
    load = false;
  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Kinderm8Theme.Colors.appcolour,
    containerColor: Kinderm8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  attendanceTags() {
    attendanceTag = widget.childData.attendance;
    pri = attendanceTag?.split(',');
    if (pri != null) {

      if (pri.length > 0) {
        for (int i = 0; i < pri.length; i++) {
          return Container(
              width: 170.0,
              height: 25.0,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: pri.length,
                  itemBuilder: (context, i) {
                    if (pri[i] == 'monday') {
                      return Padding(
                        padding: const EdgeInsets.all(1.0),
                        child: Container(
                          child: new CircleAvatar(
                            radius: 11.0,
                            backgroundColor: Colors.black,
                            child: Text(
                              "MON",
                              style: TextStyle(fontSize: 8.0),
                            ),
                          ),

                        ),
                      );


                    }else if (pri[i] == 'tuesday') {
                      return Padding(
                        padding: const EdgeInsets.all(1.0),
                        child: Container( //width: 1.0,
                          child: new CircleAvatar(
                            radius: 11.0,
                            backgroundColor: Colors.black,
                            child: Text(
                              "TUE",
                              style: TextStyle(fontSize: 8.0),
                            ),
                          ),
                        ),
                      );
                    }else if (pri[i] == 'wednesday') {
                      return Padding(
                        padding: const EdgeInsets.all(1.0),
                        child: Container(
                          child: new CircleAvatar(
                            radius: 11.0,
                            backgroundColor: Colors.black,
                            child: Text(
                              "WED",
                              style: TextStyle(fontSize: 8.0),
                            ),
                          ),
                        ),
                      );
                    }else if (pri[i] == 'thursday') {
                      return Padding(
                        padding: const EdgeInsets.all(1.0),
                        child: Container(
                          child: new CircleAvatar(
                            radius: 11.0,
                            backgroundColor: Colors.black,
                            child: Text(
                              "THU",
                              style: TextStyle(fontSize: 8.0),
                            ),
                          ),
                        ),
                      );
                    }
                    else if (pri[i] == 'friday') {
                      return Padding(
                        padding: const EdgeInsets.all(1.0),
                        child: Container(
                          child: new CircleAvatar(
                            radius: 11.0,
                            backgroundColor: Colors.black,
                            child: Text(
                              "FRI",
                              style: TextStyle(fontSize: 8.0),
                            ),
                          ),
                        ),
                      );
                    }
                    else if (pri[i] == 'saturday') {
                      return Padding(
                        padding: const EdgeInsets.all(1.0),
                        child: Container(
                          child: new CircleAvatar(
                            radius: 11.0,
                            backgroundColor: Colors.black,
                            child: Text(
                              "SAT",
                              style: TextStyle(fontSize: 8.0),
                            ),
                          ),
                        ),
                      );
                    } else  {
                      return Container(
                        child: new CircleAvatar(
                          radius: 11.0,
                          backgroundColor: Colors.black,
                          child: Text(
                            "SUN",
                            style: TextStyle(fontSize: 8.0),
                          ),
                        ),
                      );
                    }
                  }));
        }
      } else {
        for (int i = 0; i < pri.length; i++) {
          return Container(
            width: 60.0,
            height: 25.0,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: pri.length,
                itemBuilder: (context, i) {
                  return new CircleAvatar(
                    backgroundColor: Colors.grey,
                    radius: 6.0,
                  );
                }),
          );
        }
      }
    }
  }

  int getColorHexFromStr(String colorStr) {
    colorStr = "FF" + colorStr;
    colorStr = colorStr.replaceAll("#", "");
    int val = 0;
    int len = colorStr.length;
    for (int i = 0; i < len; i++) {
      int hexDigit = colorStr.codeUnitAt(i);
      if (hexDigit >= 48 && hexDigit <= 57) {
        val += (hexDigit - 48) * (1 << (4 * (len - 1 - i)));
      } else if (hexDigit >= 65 && hexDigit <= 70) {
        // A..F
        val += (hexDigit - 55) * (1 << (4 * (len - 1 - i)));
      } else if (hexDigit >= 97 && hexDigit <= 102) {
        // a..f
        val += (hexDigit - 87) * (1 << (4 * (len - 1 - i)));
      } else {
        throw new FormatException("An error occurred when converting a color");
      }
    }
    return val;
  }
   upload(){

   }
  @override
  Widget build(BuildContext context) {
    childProfileData = widget.childData ?? '';
    Size screenSize = MediaQuery.of(context).size;
    var childimageURL =
        "https://d212imxpbiy5j1.cloudfront.net/${childProfileData.iconAssetPath}";
    var fullname = "${widget.childData.firstName} ${widget.childData.firstName}";
    final dobFormat = new DateFormat('dd-MM-yyyy');
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        elevation: 0.0,leading: Container(child: IconButton(icon: Icon(Icons.arrow_back),
          onPressed: (){
            Navigator.of(context).pop();
          }),),
        title: new Text("Child Profile",
          style: TextStyle(
              color: Colors.white
          ),),

      ),
      body: ListView(children: <Widget>[
        Column(crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Stack(children: <Widget>[
                Container(
                  height: 160.0,
                  width: double.infinity,
                  color: Kinderm8Theme.Colors.appcolour,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 151.0,
                        width: screenSize.width,
                        child: Container(
                          //  color: Kinderm8Theme.Colors.appcolour,
                          color: Colors.deepPurple,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[

                              Flexible(
                                child: SizedBox(
                                  height: 151.0,
                                  width: screenSize.width / 2.0,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      right: 20.0,
                                      bottom: 40.0,
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        new Container(
                                          width: 100.0,
                                          height: 100.0,
                                          decoration: new BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: new DecorationImage(
                                              fit: BoxFit.fill,
                                              image: new NetworkImage(
                                                //widget.childData['image']
                                                  "https://d212imxpbiy5j1.cloudfront.net/${widget.childData.iconAssetPath}"
                                              ),
                                            ),
                                            border: Border.all(
                                                style: BorderStyle.solid,
                                                width: 4.0,
                                                color: Colors.white
                                            ),
                                          ),
                                          child: Container(
                                            alignment: Alignment.bottomRight,
                                            child: Container(
                                              decoration: BoxDecoration(  color: Colors.white,
                                                  shape: BoxShape.circle,
                                                  border: Border.all(
                                                      style: BorderStyle.solid,
                                                      width: 3.0,
                                                      color: Colors.white)
                                              ),
                                              child: new InkWell(
                                                  child: new Icon(
                                                    Icons.camera_alt,
                                                    color: Colors.purple,
                                                    size: 20.0,
                                                  ),
                                                  onTap: upload(),
                                                  ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              //        Container(height: 20.0,)
                            ],
                          ),
                        ),
                      ),
                      Container(
                        height: 15.0,

                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: screenSize.width>500.0? EdgeInsets.only(left: screenSize.width/4):EdgeInsets.only(left: 10.0),
                            child: new Text(
                              "General",
                              style: TextStyle(
                                fontSize: 20.0,
                                color: Kinderm8Theme.Colors.appcolour,

                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(height: 10.0,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[

                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: <Widget>[
                                    //   Container(width: 20.0,),
                                    new Icon(
                                      Icons.accessibility,
                                      size: 20.0,
                                      color: Colors.grey,
                                    ),
                                    Container(
                                      width: 5.0,
                                    ),
                                    new Text(
                                      'First Name:',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: <Widget>[
                                    // Container(width: 20.0,),
                                    new Icon(
                                      Icons.account_box,
                                      size: 20.0,
                                      color: Colors.grey,
                                    ),
                                    Container(
                                      width: 5.0,
                                    ),
                                    Text(
                                      'Last Name:',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.start,
                                    ),

                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: <Widget>[
                                    // Container(width: 20.0,),
                                    new Icon(
                                      Icons.calendar_today,
                                      size: 20.0,
                                      color: Colors.grey,
                                    ),
                                    Container(
                                      width: 5.0,
                                    ),
                                    Text(
                                      'Date Of Birth:',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.start,
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: <Widget>[
                                    // Container(width: 20.0,),
                                    new Icon(
                                      Icons.face,
                                      size: 20.0,
                                      color: Colors.grey,
                                    ),
                                    Container(
                                      width: 5.0,
                                    ),
                                    Text(
                                      'Gender:',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.start,
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: <Widget>[
                                    //  Container(width: 20.0,),
                                    new Icon(
                                      Icons.home,
                                      size: 20.0,
                                      color: Colors.grey,
                                    ),
                                    Container(
                                      width: 5.0,
                                    ),
                                    Text(
                                      'Room:',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.start,
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: <Widget>[
                                    //Container(width: 10.0,),
                                    new Icon(
                                      Icons.work,
                                      size: 20.0,
                                      color: Colors.grey,
                                    ),
                                    Container(
                                      width: 5.0,
                                    ),
                                    Text(
                                      'Attendance:',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.start,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  widget.childData.firstName ?? "",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16.0,
                                    //       fontWeight: FontWeight.bold
                                  ),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  widget.childData.lastName ?? "",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16.0,
                                    //      fontWeight: FontWeight.bold
                                  ),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  '${dobFormat.format(DateTime.parse(widget.childData.dob)) ?? ''}',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16.0,
                                    //      fontWeight: FontWeight.bold
                                  ),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  widget.childData.gender ?? "",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16.0,
                                    //      fontWeight: FontWeight.bold
                                  ),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  (widget.childData?.room != null) ? widget.childData?.room['title'] : "",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16.0,
                                    //        fontWeight: FontWeight.bold
                                  ),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    attendanceTags() ?? SizedBox(),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),

                    ],
                  ),
                ),


              ]),
              Container(
                height: 10.0,
              ),

//          SizedBox(height: 15.0),
              new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(   padding: screenSize.width>500.0? EdgeInsets.only(left: screenSize.width/4):EdgeInsets.only(left: 10.0),
                  ),
                  Text(
                    "Parent Upload",
                    style: TextStyle(
                      color: Kinderm8Theme.Colors.appcolour,
                      fontSize: 18.0,
                    ),
                  ),
                ],
              ),

              Container(
                child: Padding(
                  padding: screenSize.width>500.0? EdgeInsets.only(left: screenSize.width/4,right: screenSize.width/4):EdgeInsets.all( 10.0),
                  child: ListTile(
                    title: Text("Pictures",style: TextStyle(fontWeight: FontWeight.bold),),
                    leading: CircleAvatar(child: Icon(Icons.image),backgroundColor:Kinderm8Theme.Colors.appcolour ,radius: 25.0,),
                    trailing: Icon(Icons.navigate_next),
                  ),
                ),
              ),
              Container(
                child: Padding(
                  padding: screenSize.width>500.0? EdgeInsets.only(left: screenSize.width/4,right: screenSize.width/4):EdgeInsets.all( 10.0),
                  child: ListTile(
                    title: Text("Videos",style: TextStyle(fontWeight: FontWeight.bold),),
                    leading: CircleAvatar(child: Icon(Icons.video_library),backgroundColor:Kinderm8Theme.Colors.appcolour ,radius: 25.0,),
                    trailing: Icon(Icons.navigate_next),
                  ),
                ),
              ),
              Container(
                child: Padding(
                  padding: screenSize.width>500.0? EdgeInsets.only(left: screenSize.width/4,right: screenSize.width/4):EdgeInsets.all( 10.0),
                  child: ListTile(
                    title: Text("Documents",style: TextStyle(fontWeight: FontWeight.bold),),
                    leading: CircleAvatar(child: Icon(Icons.insert_drive_file),backgroundColor:Kinderm8Theme.Colors.appcolour ,radius: 25.0,),
                    trailing: Icon(Icons.navigate_next),
                  ),
                ),
              ),

            ]
        )
      ]
      ),
    );
  }

  Widget listItem(String title, Color buttonColor, iconButton) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Row(
        children: <Widget>[
          Container(
            height: 50.0,
            width: 50.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25.0),
                color: buttonColor.withOpacity(0.3)),
            child: Icon(iconButton, color: buttonColor, size: 25.0),
          ),
          SizedBox(width: 25.0),
          Container(
            width: MediaQuery.of(context).size.width - 100.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 15.0,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
                Icon(Icons.arrow_forward_ios, color: Colors.black, size: 20.0)
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget cardDetails(String title, String imgPath, String valueCount) {
    return Material(
      elevation: 4.0,
      borderRadius: BorderRadius.circular(7.0),
      child: Container(
        height: 125.0,
        width: (MediaQuery.of(context).size.width / 2) - 20.0,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(7.0), color: Colors.white),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 10.0),
            Padding(
              padding: EdgeInsets.only(left: 15.0),
              child: Image.asset(
                imgPath,
                fit: BoxFit.cover,
                height: 50.0,
                width: 50.0,
              ),
            ),
            SizedBox(height: 2.0),
            Padding(
              padding: EdgeInsets.only(left: 15.0),
              child: Text(title,
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontSize: 15.0,
                    color: Colors.black,
                  )),
            ),
            SizedBox(height: 3.0),
            Padding(
              padding: EdgeInsets.only(left: 15.0),
              child: Text(valueCount,
                  style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontSize: 15.0,
                      color: Colors.red,
                      fontWeight: FontWeight.bold)),
            )
          ],
        ),
      ),
    );
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      print(appusers);
      jwt = widget.jwt.toString();
//      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];

      print("iddd $id");
      print(clientId);
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }

    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
