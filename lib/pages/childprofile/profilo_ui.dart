import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kinderm8/Theme.dart' as kinderm8Theme;
import 'package:kinderm8/auth.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/settings/editemail.dart';
import 'package:kinderm8/pages/settings/resetpassword.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';

class ProfiloUI extends StatefulWidget {
  @override
  ProfiloUIState createState() => new ProfiloUIState();
  final userData;
  var displayname;
  var displayemail;
  final jwt;
  ProfiloUI(this.userData, this.jwt, this.displayname, this.displayemail);
}

class ProfiloUIState extends State<ProfiloUI>
    implements HomePageContract, AuthStateListener {
  BuildContext _ctx;

  var appuser, jwt, id, client_id, children, users;

  var email, password;
  var load = true;
  bool isLoading;
  HomePagePresenter _presenter;
  ScrollController controller;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  var progress = new ProgressBar(
    //    backgroundColor: Theme.Colors.progressbackground ,
    color: kinderm8Theme.Colors.appcolour,
    containerColor: kinderm8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  // Pop to ask do you wish to exit
  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          child: new AlertDialog(
            title: new Text('Are you sure?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => exit(0),
//                    Navigator.pushReplacementNamed(context, "/home"),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Settings"), //centerTitle: true,
//          actions: <Widget>
//          [
//            new IconButton
//              (
//              icon: new Icon(Icons.exit_to_app),
//              onPressed: () => null,
//            )
//          ],
          backgroundColor: kinderm8Theme.Colors.appcolour,
        ),
     //   backgroundColor: kinderm8Theme.Colors.appcolour,
        body: new ListView(
          children: <Widget>[
            new Container(
               margin:
                  new EdgeInsets.symmetric(horizontal: 24.0, vertical: 24.0),
              child: new Container(
                margin: new EdgeInsets.symmetric(
                    vertical: 24.0, horizontal: 24.0),
                child: new Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new CircleAvatar /* User image */ (
                          radius: screenSize.width/2,
                          backgroundImage: new NetworkImage(widget.userData),
                          child: Container(
                            alignment: Alignment.bottomRight,
//                                        alignment: Alignment(1, 0),
//                                        padding: const EdgeInsets.all(90.0),
                            child: Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      style: BorderStyle.solid,
                                      width: 2.0,
                                      color: Colors.lightBlue)),
                              child: new InkWell(
                                  child: new Icon(
                                    Icons.camera_alt,
                                    color: Colors.purple,
                                    size: 26.0,
                                  ),
                                  onTap: () {
                                    print('upload');
                                  }),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: 20.0,
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(

                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: new Text(
                                  "Profile Settings",
                                  style: TextStyle(
                                    color: kinderm8Theme
                                        .Colors.progressbackground,
                                    fontSize: 20.0,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row( mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: Container(
                                  decoration: ShapeDecoration(
                                      shape: CircleBorder(
                                          side: BorderSide(
                                            width: 2.0,
                                            color: Colors.lightBlue,
                                          )
                                      )
                                  ),
                                  child: new IconButton(
                                    icon: new Icon(Icons.edit),
                                    iconSize: 20.0,
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          new MaterialPageRoute(
                                              builder: (context) =>
                                              new EmailChange()));
                                    },
                                    padding: const EdgeInsets.all(8.0),
                                    color: kinderm8Theme.Colors.progressbackground,
                                  ),
                                ),
                              ),
                            ],

                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
//                          Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: <Widget>[
//                              Padding(
//                                padding: const EdgeInsets.only(right: 10.0,left:10.0),
//                                child: new IconButton(
//                                    icon: new Icon(Icons.account_box),
//
//                                    onPressed: null),
//                              ),
//                              Padding(
//                                padding: const EdgeInsets.all(8.0),
//                                child: new IconButton(
//                                    icon: new Icon(Icons.email),
//                                    color: kinderm8Theme.Colors.progressbackground,
//
//
//                                    onPressed: null),
//                              ),
//                            ],
//                          ),

                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          //   mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    new IconButton(
                                      onPressed: null,
                                      icon: new Icon(Icons.account_box),
                                    ),
                                    new Text(
                                      "Full Name:",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.0,
                                          color: kinderm8Theme
                                              .Colors.progressbackground),
                                      //textScaleFactor: 1.0
                                    ),
                                    Container(
                                      width: 5.0,
                                    ),
                                    new Text(
                                      "" + widget.displayname,
                                      style: TextStyle(
                                        color: kinderm8Theme
                                            .Colors.progressbackground,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    new IconButton(
                                        icon: new Icon(Icons.email),
                                        onPressed: null),
                                    new Text("Email:",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                            color: kinderm8Theme
                                                .Colors.progressbackground)),
                                    Container(
                                      width: 5.0,
                                    ),
                                    new Text(
                                      "" + widget.displayemail,
                                      style: TextStyle(
                                        color: kinderm8Theme
                                            .Colors.progressbackground,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          width: 10.0,
                        ), //                          Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: <Widget>[
//                              Padding(
//                                padding: const EdgeInsets.all(8.0),
//                                child: Text(
//                                  "" + widget.displayname,
//                                  style: TextStyle(
//                                      fontWeight: FontWeight.bold,
//                                      fontSize: 14.0,
//                                      color: kinderm8Theme
//                                          .Colors.progressbackground),
//                                ),
//                              ),
//                              Padding(
//                                padding: const EdgeInsets.all(8.0),
//                                child: Text(
//                                  "" + widget.displayemail,
//                                  style: TextStyle(
//                                      fontWeight: FontWeight.bold,
//                                      fontSize: 14.0,
//                                      color: kinderm8Theme
//                                          .Colors.progressbackground),
//                                ),
//                              ),
//                            ],
//                          ),
                      ],
                    ),
                    Container(
                      height: 15.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
//                            decoration: ShapeDecoration(
//                                shape: CircleBorder(
//                                    side: BorderSide(
//                              width: 2.0,
//                              color: Colors.lightBlue,
//                            ))),
                          //  alignment: Alignment(10.0, 0.0),
//                        alignment: Alignment.bottomRight,
//                            child: new IconButton(
//                              icon: new Icon(Icons.edit),
//                              iconSize: 20.0,
//                              onPressed: () {
//                                Navigator.push(
//                                    context,
//                                    new MaterialPageRoute(
//                                        builder: (context) =>
//                                            new EmailChange()));
//                              },
//                              padding: const EdgeInsets.all(8.0),
//                              color: kinderm8Theme.Colors.progressbackground,
//                            ),
                        ),
                        Container(
                          width: 5.0,
                        )
                      ],
                    ),
                    Container(
                      height: 10.0,
                    ),
                    new SizedBox(
                      height: 30.0,
                      width: 300.0,
                      child: new RaisedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new ResetPassword()));
                        },
                        padding: const EdgeInsets.all(8.0),
                        textColor: Colors.white,
                        color: kinderm8Theme.Colors.progressbackground,
                        child: new Text("Change Password"),
                      ),
                    )
                  ],
                ),
              ),
            ), //            new Container
//              (
//              margin: new EdgeInsets.symmetric(horizontal: 16.0),
//              child: new Card
//                (
//                child: new Column
//                  (
//                  mainAxisAlignment: MainAxisAlignment.start,
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  children: <Widget>
//                  [
//                    new Container
//                      (
//                      margin: new EdgeInsets.all(16.0),
//                      child: new Text("Ultimi 5 ordini"),
//                    ),
//                    new ListTile
//                      (
//                      leading: new Icon(Icons.local_cafe),
//                      title: new Text("Caffe espresso"),
//                      subtitle: new Text("09:58h 24/10/2017"),
//                    ),
//                    new ListTile
//                      (
//                      leading: new Icon(Icons.local_cafe),
//                      title: new Text("Caffe espresso"),
//                      subtitle: new Text("09:58h 24/10/2017"),
//                    ),
//                    new ListTile
//                      (
//                      leading: new Icon(Icons.local_cafe),
//                      title: new Text("Caffe espresso"),
//                      subtitle: new Text("09:58h 24/10/2017"),
//                    ),
//                    new ListTile
//                      (
//                      leading: new Icon(Icons.local_cafe),
//                      title: new Text("Caffe espresso"),
//                      subtitle: new Text("09:58h 24/10/2017"),
//                    ),
//                    new ListTile
//                      (
//                      leading: new Icon(Icons.local_cafe),
//                      title: new Text("Caffe espresso"),
//                      subtitle: new Text("09:58h 24/10/2017"),
//                    ),
//                  ],
//                ),
//              ),
//            ),
//            new Container
//              (
//                margin: new EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
//                child: new Card
//                  (
//                    child: new Column
//                      (
//                      mainAxisAlignment: MainAxisAlignment.start,
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>
//                      [
//                        new Container
//                          (
//                          margin: new EdgeInsets.all(16.0),
//                          child: new Text("Ultime 5 consegne"),
//                        ),
//                        new ListTile
//                          (
//                          leading: new Icon(Icons.local_cafe),
//                          title: new Text("Caffe espresso"),
//                          subtitle: new Text("09:58h 24/10/2017"),
//                        ),
//                        new ListTile
//                          (
//                          leading: new Icon(Icons.local_cafe),
//                          title: new Text("Caffe espresso"),
//                          subtitle: new Text("09:58h 24/10/2017"),
//                        ),
//                        new ListTile
//                          (
//                          leading: new Icon(Icons.local_cafe),
//                          title: new Text("Caffe espresso"),
//                          subtitle: new Text("09:58h 24/10/2017"),
//                        ),
//                        new ListTile
//                          (
//                          leading: new Icon(Icons.local_cafe),
//                          title: new Text("Caffe espresso"),
//                          subtitle: new Text("09:58h 24/10/2017"),
//                        ),
//                        new ListTile
//                          (
//                          leading: new Icon(Icons.local_cafe),
//                          title: new Text("Caffe espresso"),
//                          subtitle: new Text("09:58h 24/10/2017"),
//                        ),
//                      ],
//                    )
//                )
//            ),
          ],
        ));
  }

  @override
  void onDisplayUserInfo(User user) {
    setState(() {
      password = user.password;
      appuser = user.center;
      children = user.children;

      print("children $children");
      print("appuser $appuser");

      try {
        final parsed = json.decode(appuser);
        var appusers = parsed[0];
        print(appusers);
        print("******${appusers["jwt"]}");
        jwt = appusers["jwt"];
        users = appusers["user"];
        var child = appusers["children"];

        client_id = users["client_id"];

        id = users["id"];
        email = users["email"];
        print("###########");
        print("child ${child[0]}");
      } catch (e) {
        print(e);
      }
    });
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    var authStateProvider = new AuthStateProvider();
    setState(() {
      authStateProvider.notify(AuthState.LOGGED_OUT);
      authStateProvider.dispose(this);
    });
  }

  @override
  void onAuthStateChanged(AuthState state) {
    print("Navigate");
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(_ctx)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
