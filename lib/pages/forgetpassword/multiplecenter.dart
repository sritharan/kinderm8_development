import 'dart:io';
import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kinderm8/pages/forgetpassword/forgetcenter.dart';
import 'package:kinderm8/pages/login/login.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:kinderm8/Theme.dart' as Theme;

class MultipleCenter extends StatefulWidget {
  final email;
  var res;
  MultipleCenter(this.email, this.res);

  @override
  MultipleCenterState createState() => MultipleCenterState(email, res);
}

class MultipleCenterState extends State<MultipleCenter> {
  final email;
  var res;
  var _isLoading = false;
  List id = new List();
  List client_id = List();
  List center = List();
  List image = List();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  MultipleCenterState(this.email, this.res);

  getClientId() {
    setState(() {
      _isLoading = true;
    });

    for (int i = 0; i < res.length; i++) {
      var forgetCenter = new ForgetCenter.fromJson(res[i]);
      id.add(forgetCenter.id);
      client_id.add(forgetCenter.clientId);
      center.add(forgetCenter.client.name);
      image.add(forgetCenter.client.logo);
      print("id $center");
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getClientId();
  }
  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  void _showDialog(text) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Passsword reset"),
          content: new Text(text),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/login', (Route<dynamic> route) => false);
              },
            )
          ],
        );
      },
    );
  }

  Future<dynamic> submit(result) async {
    var clientId = result["client_id"];
    final LOGIN_URL = 'http://13.55.4.100:7070/v2.1.0/forgotpassword';
    var body = {"email": email};

    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(LOGIN_URL));
    request.headers.set('clientid', clientId);
    request.add(utf8.encode(json.encode(body)));
    HttpClientResponse response = await request.close();
    var reply = await response.transform(utf8.decoder).join();
    List decodedList = json.decode(reply);
    print("decodedList $decodedList");
    print(decodedList.length);
    try {
      setState(() {
        showDialog(
          context: context,
          child: progress,
        );
        _isLoading = true;
      });
      if (decodedList.length != 0 && decodedList[0] == true) {
        _showDialog(
            "Your password was reset successfully");
      } else if (decodedList.length != 0 && decodedList[0] == false) {
        _showDialog("Somthing went wrong\nPlease try again later");
      } else {
        print("error happened..");
        return null;
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    var listView = new ListView.builder(
        itemCount: this.id != null ? this.id.length : 0,
        itemBuilder: (context, i) {
          return new Card(
            elevation: 2.0,
            semanticContainer: false,
            child: FlatButton(
              child: image[i] != null
                  ? new ListTile(
                      leading: CircleAvatar(
//                        radius: 42.0,
                        backgroundImage: NetworkImage(image[i],scale: 10.0),
                      ),
                      title: new Text(
                        center[i].toString(),
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 14.0),
                      ),
                    )
                  : new ListTile(
                      leading: CircleAvatar(
//                        radius: 42.0,
                        backgroundImage: AssetImage("assets/nophoto.jpg"),
                      ),
                      title: new Text(
                        center[i].toString(),
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 14.0),
                      ),
                    ),
              onPressed: () {
                var result = {
                  "email": email,
                  "client_id": client_id[i],
                  "center": center[i],
                  "image": image[i]
                };
                submit(result);
                setState(() {
                  _isLoading = false;
                });
              },
            ),
          );
        });

    return Center(
        child: new Scaffold(
      appBar: new AppBar(
        title: Text("Choose Your Center"),
        centerTitle: true,
      ),
      key: scaffoldKey,
      body: id.length > 0
          ? new Container(
              padding: EdgeInsets.all(16.0),
              child: _isLoading
                  ? new Center(child: listView)
                  : Center(
                  child: progress
              ),
            )
          : new Center(
              child: RaisedButton(
                child: Text("Press here to Login"),
                onPressed: () {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/login', (Route<dynamic> route) => false);
                },
              ),
            ),
    ));
  }
}
