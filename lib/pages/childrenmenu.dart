import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:kinderm8/Theme.dart' as kinderm8Theme;
import 'package:flutter/material.dart';
import 'package:kinderm8/auth.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/centrenotes/centrenoteslist.dart';
import 'package:kinderm8/pages/childrenmodulemenu.dart';
import 'package:kinderm8/pages/commondrawer.dart';
import 'package:kinderm8/pages/dailyjournal/dailyJournalList.dart';
import 'package:kinderm8/pages/gallery/photolist.dart';
import 'package:kinderm8/pages/home/home.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/pages/learningstory/learningstoryList.dart';
import 'package:kinderm8/pages/newsfeed/newsfeedlist.dart';
import 'package:kinderm8/pages/notification/notificationlist.dart';
import 'package:kinderm8/pages/observation/observationList.dart';

class ChildrenDrawer extends StatefulWidget {
  final List<ChildViewModal> childrenData;

  ChildrenDrawer(this.childrenData);

  @override
  State<StatefulWidget> createState() {
    return new ChildrenDrawerState();
  }
}

class ChildrenDrawerState extends State<ChildrenDrawer>
    implements HomePageContract, AuthStateListener {
//  var email, password, client_id;
  var center;
  var jwt, id, cliId, image;
  List<ChildViewModal> children = [];
  var deviceSize;
  BuildContext _ctx;
  HomePagePresenter _presenter;
  ChildViewModal selectedChild;
  int childSelected;
  bool showMenu = false;
  bool load = true;
  List list = [
    {
      "id": "Centre Notes",
      "name": "Centre Notes",
      "icon": Icons.directions_bike,
      "image": "assets/iconsetpng/user.png",
      "color": Colors.deepOrange
    },
    {
      "id": "Daily Journal",
      "name": "Daily Journal",
      "icon": Icons.videogame_asset,
      "image": "assets/iconsetpng/cubes.png",
      "color": Colors.orange
    },
    {
      "id": "Observations",
      "name": "Observations",
      "icon": Icons.people,
      "image": "assets/iconsetpng/game.png",
      "color": Colors.cyan
    },
    {
      "id": "Leanring Story",
      "name": "Leanring Story",
      "icon": Icons.local_movies,
      "image": "assets/iconsetpng/abacus.png",
      "color": Colors.purple
    },
    {
      "id": "Daily Chart",
      "name": "Daily Chart",
      "icon": Icons.local_hospital,
      "image": "assets/iconsetpng/notepad_dailychart.png",
      "color": Colors.red
    },
    {
      "id": "Medications",
      "name": "Medications",
      "icon": Icons.music_note,
      "image": "assets/iconsetpng/first-aid-kit.png",
      "color": Colors.amber
    },
//    {
//      "id": "Profile",
//      "name": "Profile",
//      "icon": Icons.account_circle,
//      "image": "assets/iconsetpng/settings.png",
//      "color": Colors.blueGrey
//    },
//    {
//      "id": "Photo Alubms",
//      "name": "Photo Alubms extra words with for testing",
//      "icon": Icons.assistant_photo,
//      "image": "assets/iconsetpng/notification.png",
//      "color": Colors.blueGrey,
//      "navigator": "/notification"
//    },
  ];

  ChildrenDrawerState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  @override
  void initState() {
    children = widget.childrenData;
    super.initState();
  }

  logout() {
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
//    Navigator.push(context, new MaterialPageRoute(
//        builder: (context) =>
//        new LoginPage()));
    _presenter.getUserLogout();
  }

  void _openchildrenpopup(selectedChild) {
    print('Selected child >> ChildrenDrawer$selectedChild');
    Navigator.of(context).pop(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return ChildrenModuleMenu(selectedChild);
        },
        fullscreenDialog: true));
  }

  void _opencommonpopup(selectedChild) {
    print("child $selectedChild");
    print(selectedChild);
    print(selectedChild.id);
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new ChildrenModuleMenu(selectedChild)));
  }

  void _navigateToRoute(selectedChild) {
    Navigator.of(context).pushReplacement(new MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return new Scaffold(
          body: ChildrenModuleMenu(selectedChild),
        );
      },
    ));
  }

  /* ---------------- */
  /* children list */
  /* --------------- */
  childList() {
    if (children.length > 1) {
      return Container(
        height: 80.0,
        color: Colors.white,
        child: ListView.builder(
          itemCount: this.children.length > 0 ? this.children.length : 0,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
//          return Text("Text $index");
//        print(children[index]["image"]);

            return Container(
              width: 80.0,
              height: 80.0,
              color: Colors.transparent,
              child: GestureDetector(
                onDoubleTap: () {
                  setState(() {
                    showMenu = false;
                    childSelected=null;
                  });
                },
                onTap: () {
                  setState(() {
                    showMenu = true;
                    selectedChild = children[index];
                    childSelected = selectedChild.id;
                    print("$childSelected");
                    print(children[index].id);
                    _navigateToRoute(selectedChild);
//                    _openchildrenpopup(selectedChild);
//                    ChildrenModuleMenu(selectedChild);
//                    Navigator.of(context).push(new MaterialPageRoute<Null>(
//                        builder: (BuildContext context) {
//                          return ChildrenModuleMenu(selectedChild);
//                        },
////                        fullscreenDialog: true
//                    ));
                  });
                },
                child: childSelected != children[index].id
                    ? Container(
                  margin: const EdgeInsets.all(5.0),
//            padding: const EdgeInsets.all(26.0),
                  decoration: new BoxDecoration(
                    color: const Color(0xff7c94b6),
                    image: new DecorationImage(
                      image: CachedNetworkImageProvider(
                          'https://d212imxpbiy5j1.cloudfront.net/${children[index].iconAssetPath}'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius:
                    new BorderRadius.all(new Radius.circular(50.0)),
                    border: new Border.all(
                      color: kinderm8Theme.Colors.appcolour,
                      width: 1.0,
                    ),
                  ),
                )
                    : Container(
                  margin: const EdgeInsets.all(5.0),
//            padding: const EdgeInsets.all(26.0),
                  decoration: new BoxDecoration(
                    color: const Color(0xff7c94b6),
                    image: new DecorationImage(
                      image: CachedNetworkImageProvider(
                          'https://d212imxpbiy5j1.cloudfront.net/${children[index].iconAssetPath}'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius:
                    new BorderRadius.all(new Radius.circular(50.0)),
                    border: new Border.all(
                      color: Colors.green,
                      width: 3.0,
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      );
    }
    else if(children.length == 1 ) {
      setState(() {
        showMenu = true;
        selectedChild = children[0];
        _navigateToRoute(selectedChild);

      });
      return Container();

    }
    else{ new Container();}
  }


  /* ---------------- */
  /* Round icon Menu design */
  /* --------------- */
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    final blueGradient = const LinearGradient(
        colors: const <Color>[
          const Color(0xFF0075D1),
          const Color(0xFF00A2E3),
        ],
        stops: const <double>[0.4, 0.6],
        begin: Alignment.topRight,
        end: Alignment.bottomLeft);
    final purpleGraient = const LinearGradient(
        colors: const <Color>[
          const Color(0xFF882DEB),
          const Color(0xFF9A4DFF)
        ],
        stops: const <double>[0.5, 0.7],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight);
    final redGradient = const LinearGradient(
        colors: const <Color>[
          const Color(0xFFBA110E),
          const Color(0xFFCF3110),
        ],
        stops: const <double>[0.6, 0.8],
        begin: Alignment.bottomRight,
        end: Alignment.topLeft);
    final whiteGradient = const LinearGradient(
        colors: const <Color>[
          const Color(0xFFFFFFFF),
          const Color(0xFFFFFFFF),
        ],
        stops: const <double>[0.6, 0.8],
        begin: Alignment.bottomRight,
        end: Alignment.topLeft);
    final appGradient = const LinearGradient(
        colors: const <Color>[
          const Color(0xFF8860EB),
          const Color(0xFF8881EB),
        ],
        stops: const <double>[0.6, 0.8],
        begin: Alignment.bottomRight,
        end: Alignment.topLeft);

    Widget _buildAction(String title, VoidCallback action, Color color,
        Gradient gradient, ImageProvider backgroundImage) {
      final textStyle = new TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.w700,
        fontSize: 16.0,
//          fontFamily: ProfileFontNames.TimeBurner
      );
      final Size screenSize = MediaQuery.of(context).size;
      const double PI = 3.1415926535897932;
      return new GestureDetector(
        onTap: action,
        child: new Container(
          margin: const EdgeInsets.only(right: 5.0, left: 5.0),
//          width: 150.0,
          width: screenSize.width/2,
          decoration: new BoxDecoration(
              color: color,
              shape: BoxShape.rectangle,
              borderRadius: new BorderRadius.circular(10.0),
              boxShadow: <BoxShadow>[
                new BoxShadow(color: Colors.black38,
                    blurRadius: 5.0,
                    spreadRadius: 1.0,
                    offset: new Offset(0.0, 1.0)),
              ],
              gradient: gradient
          ),
          child: new Stack(
            children: <Widget>[
              new Opacity(
                opacity: 0.2,
                child: new Align(
                  alignment: Alignment.centerRight,
                  child: new Transform.rotate(
                    angle: -PI / 5.8,
                    alignment: Alignment.centerRight,
                    child: new ClipPath(
                      clipper: new _BackgroundImageClipper(),
                      child: new Container(
                        padding: const EdgeInsets.only(
                            bottom: 15.0, right: 10.0, left: 75.0),
                        child: new Image(
                          width: 90.0,
                          height: 90.0,
                          image: backgroundImage != null
                              ? backgroundImage
                              : new AssetImage("assets/iconsetpng/d-files.png"),
                        ),
                      ),
                    ),
                  ),
                ),
              ), // END BACKGROUND IMAGE

              new Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.only(left: 10.0, top: 10.0),
                child: new Text(title, style: textStyle),
              ),
            ],
          ),
        ),
      );
    }

    Widget _buildAction_Grid(String title, VoidCallback action, Color color,
        Gradient gradient, ImageProvider backgroundImage) {
      final textStyle = new TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.w500,
        fontSize: 14.0,
//          fontFamily: ProfileFontNames.TimeBurner
      );
      final Size screenSize = MediaQuery.of(context).size;
      const double PI = 3.1415926535897932;
      return new GestureDetector(
        onTap: action,
        child: new Container(
          margin: const EdgeInsets.all(5.0),
//          margin: const EdgeInsets.only(right: 5.0, left: 5.0),
//          width: 150.0,
          width: screenSize.width/2,
          decoration: new BoxDecoration(
              color: color,
              shape: BoxShape.rectangle,
              borderRadius: new BorderRadius.circular(10.0),
              boxShadow: <BoxShadow>[
                new BoxShadow(color: Colors.black38,
                    blurRadius: 5.0,
                    spreadRadius: 1.0,
                    offset: new Offset(0.0, 1.0)),
              ],
              gradient: gradient
          ),
          child: new Stack(
            children: <Widget>[
              new Opacity(
                opacity: 0.4,
                child: new Align(
                  alignment: Alignment.bottomCenter,
                  child: new Transform.rotate(
                    angle: -PI / 360.0,
                    alignment: Alignment.bottomCenter,
                    child: new ClipPath(
                      clipper: new _BackgroundImageClipper(),
                      child: new Container(
//                        padding: const EdgeInsets.only(
//                            bottom: 15.0, right: 10.0, left: 75.0),
                        padding: const EdgeInsets.only(
                            bottom: 10.0),
                        child: new Image(
                          width: 90.0,
                          height: 90.0,
                          image: backgroundImage != null
                              ? backgroundImage
                              : new AssetImage("assets/iconsetpng/d-files.png"),
                        ),
                      ),
                    ),
                  ),
                ),
              ), // END BACKGROUND IMAGE

              new Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.only(left:5.0,bottom: 5.0),
                child: new Text(title, style: textStyle),
              ),
            ],
          ),
        ),
      );
    }

    /* Card design menu*/
    Widget _buildCard(String title,VoidCallback action ,String status, int cardIndex, String backgroundImage) {
      return GestureDetector(
        onTap: action,
        child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)
            ),
            elevation: 7.0,
            child: Column(
              children: <Widget>[
                SizedBox(height: 12.0),
                Stack(
                    children: <Widget>[
                      Container(
                        height: 70.0,
                        width: 70.0,
                        decoration: BoxDecoration(
//                          borderRadius: BorderRadius.circular(30.0),
                          color: Colors.white,
                            image: DecorationImage(
//                              image: NetworkImage(
//                                  'https://pixel.nymag.com/imgs/daily/vulture/2017/06/14/14-tom-cruise.w700.h700.jpg'
//                              )
                                image: AssetImage(
                                backgroundImage
                            )
                            )
                        ),
                      ),
//                    Container(
//                      margin: EdgeInsets.only(left: 40.0),
//                      height: 20.0,
//                      width: 20.0,
//                      decoration: BoxDecoration(
//                          borderRadius: BorderRadius.circular(30.0),
//                          color: status == 'Away' ? Colors.amber : Colors.green,
//                          border: Border.all(
//                              color: Colors.white,
//                              style: BorderStyle.solid,
//                              width: 2.0
//                          )
//                      ),
//                    )
                    ]),
                SizedBox(height: 8.0),
                Text(
                  title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ),
//              SizedBox(height: 5.0),
//              Text(
//                status,
//                style: TextStyle(
//                    fontFamily: 'Quicksand',
//                    fontWeight: FontWeight.bold,
//                    fontSize: 12.0,
//                    color: Colors.grey
//                ),
//              ),
//              SizedBox(height: 15.0),
//              Expanded(
//                  child: Container(
//                      width: 175.0,
//                      decoration: BoxDecoration(
//                        color: status == 'Away' ? kinderm8Theme.Colors.appcolour: kinderm8Theme.Colors.appcolour,
//                        borderRadius: BorderRadius.only
//                          (
//                            bottomLeft: Radius.circular(10.0),
//                            bottomRight: Radius.circular(10.0)
//                        ),
//                      ),
//                      child: Center(
//                        child: Text('Request',
//                          style: TextStyle(
//                              color: Colors.white, fontFamily: 'Quicksand'
//                          ),
//                        ),
//                      )
//                  )
//              )
              ],
            ),
//            margin: cardIndex.isEven? EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 15.0):EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 30.0)
        ),
      );
    }

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        children: <Widget>[
          childList(),

          /* if One child enable child menu */
//          selectedChild != null ? new Container(): childList(),
//          Divider(),
          /* // ---------------- //
          // Action button Grid menu Style  //
          // ---------------- // */
          /* if One child enable child menu || check child length */
//          showMenu ? Flexible(
//            child: new Container(
////          constraints: const BoxConstraints(maxHeight: 200.0,maxWidth:200.0),
////          margin: const EdgeInsets.only(top: 20.0),
//              color: Colors.white30,
//              child: new GridView.count(
//                  crossAxisCount: 2,
//                  primary: false,
//                  crossAxisSpacing: 2.0,
//                  mainAxisSpacing: 4.0,
//                  shrinkWrap: true,
//                  children: <Widget>[
//                    _buildCard(
//                      "Daily Journal",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return DailyJournal(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                      }, "", 1,"assets/iconsetpng/cubes.png"
//                    ),
//                    _buildCard(
//                        "Observation",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return ObservationList(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/game.png"
//                    ),
//                    _buildCard(
//                        "Centre Notes",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return CentreNotesList(
//                                  selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/user.png"
//                    ),
//                    _buildCard(
//                        "Daily Chart",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return DailyJournal(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/notepad_dailychart.png"
//                    ),
//                    _buildCard(
//                        "Learning Story",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return LearningStoryList(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/abacus.png"
//                    ),
//                    _buildCard(
//                        "Medications",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return DailyJournal(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/first-aid-kit.png"
//                    ),
//                    _buildCard(
//                        "Photos",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return PhotoList(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/image.png"
//                    ),
//                    _buildCard(
//                        "Profile",() {
//                      if (showMenu == true) {
//                        Navigator.of(context)
//                            .pushReplacement(new MaterialPageRoute<Null>(
//                            builder: (BuildContext context) {
//                              return DailyJournal(selectedChild, jwt);
//                            },
//                            fullscreenDialog: true));
//                      }
//                    }, "", 1,"assets/iconsetpng/d-files.png"
//                    )
//
//                  ].toList()),
//            ),
//          ): new Container(),

//          new Container(
//              alignment: Alignment.center,
////              padding: const EdgeInsets.only(top: 100.0),
//              child: new Text(
//                "Hi! Your Child not selected..",
//                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 18.0),
//              )
//          ),
//          new ListTile(
//            title: Text(
//              "LogOut",
//              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
//            ),
//            leading: Icon(
//              Icons.keyboard_return,
//              color: Colors.redAccent,
//            ),
//            onTap: () {
//              logout();
//            },
//          ),
//          new GestureDetector(
//            onTap: () {
//              Navigator.of(context).pop();
//            },
//            child: new Container(
//              width: screenSize.width/2,
//              margin: new EdgeInsets.only(
//                  top:10.0,left: 5.0, right: 5.0,bottom: 10.0),
//              height: 30.0,
//              decoration: new BoxDecoration(
//                  color: kinderm8Theme.Colors.appcolour,
//                  borderRadius: new BorderRadius.all(
//                      new Radius.circular(10.0))),
//              child: new Center(
//                  child: new Text(
//                    "Close",
//                    style: new TextStyle(
//                        color: Colors.white, fontSize: 22.0),
//                  )),
//            ),
//          ),

          /* // ---------------- //
          // Action button menu Style  //
          // ---------------- // */
          /*showMenu ? new Container(
            constraints: const BoxConstraints(maxHeight: 120.0),
            margin: const EdgeInsets.only(top: 20.0),
            child: new Align(
              alignment: Alignment.center,
              child: new ListView(
                  shrinkWrap: true,
                  padding: const EdgeInsets.only(
                      left: 10.0, bottom: 20.0, right: 10.0, top: 10.0),
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    _buildAction(
                        "Daily Journal", () {
                      if (showMenu == true) {
                        Navigator.of(context)
                            .pushReplacement(new MaterialPageRoute<Null>(
                            builder: (BuildContext context) {
                              return DailyJournal(selectedChild, jwt);
                            },
                            fullscreenDialog: true));
                      }
                    }, Colors.blue, appGradient,
                        new AssetImage("assets/iconsetpng/cubes.png")),
                    _buildAction(
                        "Observation", () {}, Colors.purple, appGradient,
                        new AssetImage("assets/iconsetpng/game.png")),
                    _buildAction(
                        "Centre Notes", () {}, Colors.red, appGradient,
                        new AssetImage("assets/iconsetpng/user.png")),
                    _buildAction(
                        "Daily Chart", () {}, Colors.red, appGradient,
                        new AssetImage("assets/iconsetpng/notepad_dailychart.png")),
                    _buildAction(
                        "Learning Story", () {}, Colors.red, appGradient,
                        new AssetImage("assets/iconsetpng/abacus.png")),
                    _buildAction(
                        "Medications", () {}, Colors.red, appGradient,
                        new AssetImage("assets/iconsetpng/first-aid-kit.png")),
                  ]
              ),
            ),
          ): new Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.only(top: 100.0),
            child: new Text(
              "Hi! You didn't Select Child..",
              style: TextStyle(fontWeight: FontWeight.w400, fontSize: 18.0),
            )
          ),*/

          /*// ---------------- //
          // List menu Style //
          // ---------------- // */
//          showMenu ? new Column(
//            children: <Widget>[
//              new ListTile(
//                  title: Text(
//                    "Daily Journal",
//                    style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
//                  ),
//                  leading: Icon(
//                    Icons.home,
//                    color: kinderm8Theme.Colors.appmenuicon,
//                  ),
//                  onTap: () {
//                    if (showMenu == true) {
//                  Navigator.of(context)
//                      .pushReplacement(new MaterialPageRoute<Null>(
//                          builder: (BuildContext context) {
//                            return DailyJournal(selectedChild, jwt);
//                          },
//                          fullscreenDialog: true));
//                    } else {
//                      print("not selected..");
//                    }
//                  }),
//              new ListTile(
//                  title: Text(
//                    "Observation",
//                    style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
//                  ),
//                  leading: Icon(
//                    Icons.home,
//                    color: kinderm8Theme.Colors.appmenuicon,
//                  ),
//                  onTap: () {
//                    if (showMenu == true) {
//                      Navigator.of(context).push(new MaterialPageRoute<Null>(
//                          builder: (BuildContext context) {
//                            return ObservationList(selectedChild);
//                          },
//                          fullscreenDialog: true));
//                    } else {
//                      print("not selected..");
//                    }
//                  })
//            ],
//          ): new Container()


/*          new ListTile(
              title: Text(
                "Main Menu",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
              ),
              leading: Icon(
                Icons.home,
                color: kinderm8Theme.Colors.appmenuicon,
              ),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute<Null>(
                    builder: (BuildContext context) {
                      return CommonDrawer();
                    },
                    fullscreenDialog: true));
              }),
          new ListTile(
            title: Text(
              "LogOut",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.keyboard_return,
              color: Colors.redAccent,
            ),
            onTap: () {
              logout();
            },
          ),*/
/*        new Padding(
          padding: new EdgeInsets.all(5.0),
          child: new BackButton(
            color: kinderm8Theme.Colors.appcolour,
          ),
        ),
        new Text(
          'Go Back',
          style: Theme.of(context).textTheme.headline.copyWith(
              color: kinderm8Theme.Colors.appmenuicon, fontSize: 26.0),
        ),*/
        ],
      ),
//      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
//      floatingActionButton: FloatingActionButton(
//        child: const Icon(Icons.close),
//        backgroundColor: kinderm8Theme.Colors.appcolour,
//        onPressed: () {
////            showModalBottomSheet<Null>(
////                context: context,
////                builder: (BuildContext context) {
////                  return ChildrenDrawer();
////                });
//          Navigator.of(context).pop();
////            Navigator.push(
////                context, new MaterialPageRoute(builder: (context) => new ChildrenDrawer()));
//        },
//      ),
    );
  }

  /* ---------------- */
  /* Half icon square Menu design */
  /* --------------- */
  /* Square half icon set*/
  @override
  Widget build_1(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    final blueGradient = const LinearGradient(
        colors: const <Color>[
          const Color(0xFF0075D1),
          const Color(0xFF00A2E3),
        ],
        stops: const <double>[0.4, 0.6],
        begin: Alignment.topRight,
        end: Alignment.bottomLeft);
    final purpleGraient = const LinearGradient(
        colors: const <Color>[
          const Color(0xFF882DEB),
          const Color(0xFF9A4DFF)
        ],
        stops: const <double>[0.5, 0.7],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight);
    final redGradient = const LinearGradient(
        colors: const <Color>[
          const Color(0xFFBA110E),
          const Color(0xFFCF3110),
        ],
        stops: const <double>[0.6, 0.8],
        begin: Alignment.bottomRight,
        end: Alignment.topLeft);
    final whiteGradient = const LinearGradient(
        colors: const <Color>[
          const Color(0xFFFFFFFF),
          const Color(0xFFFFFFFF),
        ],
        stops: const <double>[0.6, 0.8],
        begin: Alignment.bottomRight,
        end: Alignment.topLeft);
    final appGradient = const LinearGradient(
        colors: const <Color>[
          const Color(0xFF8860EB),
          const Color(0xFF8881EB),
        ],
        stops: const <double>[0.6, 0.8],
        begin: Alignment.bottomRight,
        end: Alignment.topLeft);

    Widget _buildAction(String title, VoidCallback action, Color color,
        Gradient gradient, ImageProvider backgroundImage) {
      final textStyle = new TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w700,
          fontSize: 16.0,
//          fontFamily: ProfileFontNames.TimeBurner
      );
      final Size screenSize = MediaQuery.of(context).size;
      const double PI = 3.1415926535897932;
      return new GestureDetector(
        onTap: action,
        child: new Container(
          margin: const EdgeInsets.only(right: 5.0, left: 5.0),
//          width: 150.0,
          width: screenSize.width/2,
          decoration: new BoxDecoration(
              color: color,
              shape: BoxShape.rectangle,
              borderRadius: new BorderRadius.circular(10.0),
              boxShadow: <BoxShadow>[
                new BoxShadow(color: Colors.black38,
                    blurRadius: 5.0,
                    spreadRadius: 1.0,
                    offset: new Offset(0.0, 1.0)),
              ],
              gradient: gradient
          ),
          child: new Stack(
            children: <Widget>[
              new Opacity(
                opacity: 0.2,
                child: new Align(
                  alignment: Alignment.centerRight,
                  child: new Transform.rotate(
                    angle: -PI / 5.8,
                    alignment: Alignment.centerRight,
                    child: new ClipPath(
                      clipper: new _BackgroundImageClipper(),
                      child: new Container(
                        padding: const EdgeInsets.only(
                            bottom: 15.0, right: 10.0, left: 75.0),
                        child: new Image(
                          width: 90.0,
                          height: 90.0,
                          image: backgroundImage != null
                              ? backgroundImage
                              : new AssetImage("assets/iconsetpng/d-files.png"),
                        ),
                      ),
                    ),
                  ),
                ),
              ), // END BACKGROUND IMAGE

              new Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.only(left: 10.0, top: 10.0),
                child: new Text(title, style: textStyle),
              ),
            ],
          ),
        ),
      );
    }

    Widget _buildAction_Grid(String title, VoidCallback action, Color color,
        Gradient gradient, ImageProvider backgroundImage) {
      final textStyle = new TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.w500,
        fontSize: 14.0,
//          fontFamily: ProfileFontNames.TimeBurner
      );
      final Size screenSize = MediaQuery.of(context).size;
      const double PI = 3.1415926535897932;
      return new GestureDetector(
        onTap: action,
        child: new Container(
          margin: const EdgeInsets.all(5.0),
//          margin: const EdgeInsets.only(right: 5.0, left: 5.0),
//          width: 150.0,
          width: screenSize.width/2,
          decoration: new BoxDecoration(
              color: color,
              shape: BoxShape.rectangle,
              borderRadius: new BorderRadius.circular(10.0),
              boxShadow: <BoxShadow>[
                new BoxShadow(color: Colors.black38,
                    blurRadius: 5.0,
                    spreadRadius: 1.0,
                    offset: new Offset(0.0, 1.0)),
              ],
              gradient: gradient
          ),
          child: new Stack(
            children: <Widget>[
              new Opacity(
                opacity: 0.4,
                child: new Align(
                  alignment: Alignment.bottomCenter,
                  child: new Transform.rotate(
                    angle: -PI / 360.0,
                    alignment: Alignment.bottomCenter,
                    child: new ClipPath(
                      clipper: new _BackgroundImageClipper(),
                      child: new Container(
//                        padding: const EdgeInsets.only(
//                            bottom: 15.0, right: 10.0, left: 75.0),
                        padding: const EdgeInsets.only(
                            bottom: 10.0),
                        child: new Image(
                          width: 90.0,
                          height: 90.0,
                          image: backgroundImage != null
                              ? backgroundImage
                              : new AssetImage("assets/iconsetpng/d-files.png"),
                        ),
                      ),
                    ),
                  ),
                ),
              ), // END BACKGROUND IMAGE

              new Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.only(left:5.0,bottom: 5.0),
                child: new Text(title, style: textStyle),
              ),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        children: <Widget>[
          /* if One child enable child menu */
          childList(),
          Divider(),
//          Container(
//            child: Flexible(
//              child: showMenu
//                  ? new GridView.builder(
//                      gridDelegate:
//                          const SliverGridDelegateWithFixedCrossAxisCount(
//                              crossAxisCount: 4, mainAxisSpacing: 0.0),
//                      padding: const EdgeInsets.only(top: 5.0),
//                      itemCount: list.length,
//                      shrinkWrap: false,
//                      physics: ClampingScrollPhysics(),
//                      itemBuilder: (BuildContext context, int index) {
//                        return new GridTile(
//                          footer: new Row(
//                              mainAxisAlignment: MainAxisAlignment.center,
//                              children: <Widget>[
//                                Flexible(
//                                  child: new Text(
//                                    list[index]["name"],
//                                    maxLines: 2,
//                                    textAlign: TextAlign.center,
//                                    overflow: TextOverflow.ellipsis,
//                                  ),
//                                )
//                              ]),
//                          child: new GestureDetector(
//                              child: new Column(
//                                crossAxisAlignment: CrossAxisAlignment.center,
//                                children: [
//                                  new CircleAvatar(
//                                    backgroundColor: Colors.white,
//                                    radius: 40.0,
//                                    child: new Image.asset(
//                                      list[index]['image'],
//                                      width: 50.0,
//                                      height: 50.0,
//                                    ),
//                                  ),
////                                  new Icon(list[index]["icon"],
////                                      size: 40.0, color: list[index]["color"]),
//                                ],
//                              ),
//                              onTap: () {
//                                print(
//                                    "Have to Navigate ${list[index]['name']}");
//                                print(selectedChild);
////                          Navigator.of(context).push(list[index]['navigator']);
///*//                    Navigator.push(
////                        context,
////                        new MaterialPageRoute(
////                            builder: (_) =>
////                            new ArticleSourceScreen.ArticleSourceScreen(
////                              sourceId: categoriesList.list[index]
////                              ['id'],
////                              sourceName: categoriesList.list[index]
////                              ["name"],
////                              isCategory: true,
////                            )));
////                    },*/
//                              }),
//                        );
//                      },
//                    )
//                  : Container(),
//            ),
//          ),
          /* // ---------------- //
          // Action button Grid menu Style  //
          // ---------------- // */
          /* if One child enable child menu || check child length */
          showMenu ? Flexible(
            child: new Container(
//          constraints: const BoxConstraints(maxHeight: 200.0,maxWidth:200.0),
//          margin: const EdgeInsets.only(top: 20.0),
            color: Colors.white30,
            child: new GridView.count(
                crossAxisCount: 4,
//              childAspectRatio: 1.0,
                padding: const EdgeInsets.all(16.0),
                mainAxisSpacing: 2.0,
                crossAxisSpacing: 2.0,
                shrinkWrap: true,
                children: <Widget>[
                  _buildAction_Grid(
                      "Daily Journal", () {
                    if (showMenu == true) {
                      Navigator.of(context)
                          .pushReplacement(new MaterialPageRoute<Null>(
                          builder: (BuildContext context) {
                            return DailyJournal(selectedChild, jwt);
                          },
                          fullscreenDialog: true));
                    }
                  }, Colors.blue, purpleGraient,
                      new AssetImage("assets/iconsetpng/cubes.png")),
                  _buildAction_Grid(
                      "Observation", () {
                    if (showMenu == true) {
                      Navigator.of(context)
                          .pushReplacement(new MaterialPageRoute<Null>(
                          builder: (BuildContext context) {
                            return ObservationList(selectedChild, jwt);
                          },
                          fullscreenDialog: true));
                    }
                  }, Colors.purple, purpleGraient,
                      new AssetImage("assets/iconsetpng/game.png")),
                  _buildAction_Grid(
                      "Centre Notes", () {}, Colors.red, purpleGraient,
                      new AssetImage("assets/iconsetpng/user.png")),
                  _buildAction_Grid(
                      "Daily Chart", () {}, Colors.red, purpleGraient,
                      new AssetImage("assets/iconsetpng/notepad_dailychart.png")),
                  _buildAction_Grid(
                      "Learning Story", () {}, Colors.red, purpleGraient,
                      new AssetImage("assets/iconsetpng/abacus.png")),
                  _buildAction_Grid(
                      "Medications", () {}, Colors.red, purpleGraient,
                      new AssetImage("assets/iconsetpng/first-aid-kit.png")),
                  _buildAction_Grid(
                      "Photos", () {}, Colors.red, purpleGraient,
                      new AssetImage("assets/iconsetpng/image.png")),
                  _buildAction_Grid(
                      "Profile", () {}, Colors.red, purpleGraient,
                      new AssetImage("assets/iconsetpng/d-files.png")),
//                  _buildAction_Grid(
//                      "Priority Notification", () {}, Colors.red, purpleGraient,
//                      new AssetImage("assets/iconsetpng/notification.png")),
                ].toList()),
        ),
          ): new Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.only(top: 100.0),
              child: new Text(
                "Hi! You didn't Select Child..",
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 18.0),
              )
          ),
          new ListTile(
            title: Text(
              "LogOut",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.keyboard_return,
              color: Colors.redAccent,
            ),
            onTap: () {
              logout();
            },
          ),
          new GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
            child: new Container(
              width: screenSize.width/2,
              margin: new EdgeInsets.only(
                  top:10.0,left: 5.0, right: 5.0,bottom: 10.0),
              height: 30.0,
              decoration: new BoxDecoration(
                  color: kinderm8Theme.Colors.appcolour,
                  borderRadius: new BorderRadius.all(
                      new Radius.circular(10.0))),
              child: new Center(
                  child: new Text(
                    "Close",
                    style: new TextStyle(
                        color: Colors.white, fontSize: 22.0),
                  )),
            ),
          ),

          /* // ---------------- //
          // Action button menu Style  //
          // ---------------- // */
          /*showMenu ? new Container(
            constraints: const BoxConstraints(maxHeight: 120.0),
            margin: const EdgeInsets.only(top: 20.0),
            child: new Align(
              alignment: Alignment.center,
              child: new ListView(
                  shrinkWrap: true,
                  padding: const EdgeInsets.only(
                      left: 10.0, bottom: 20.0, right: 10.0, top: 10.0),
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    _buildAction(
                        "Daily Journal", () {
                      if (showMenu == true) {
                        Navigator.of(context)
                            .pushReplacement(new MaterialPageRoute<Null>(
                            builder: (BuildContext context) {
                              return DailyJournal(selectedChild, jwt);
                            },
                            fullscreenDialog: true));
                      }
                    }, Colors.blue, appGradient,
                        new AssetImage("assets/iconsetpng/cubes.png")),
                    _buildAction(
                        "Observation", () {}, Colors.purple, appGradient,
                        new AssetImage("assets/iconsetpng/game.png")),
                    _buildAction(
                        "Centre Notes", () {}, Colors.red, appGradient,
                        new AssetImage("assets/iconsetpng/user.png")),
                    _buildAction(
                        "Daily Chart", () {}, Colors.red, appGradient,
                        new AssetImage("assets/iconsetpng/notepad_dailychart.png")),
                    _buildAction(
                        "Learning Story", () {}, Colors.red, appGradient,
                        new AssetImage("assets/iconsetpng/abacus.png")),
                    _buildAction(
                        "Medications", () {}, Colors.red, appGradient,
                        new AssetImage("assets/iconsetpng/first-aid-kit.png")),
                  ]
              ),
            ),
          ): new Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.only(top: 100.0),
            child: new Text(
              "Hi! You didn't Select Child..",
              style: TextStyle(fontWeight: FontWeight.w400, fontSize: 18.0),
            )
          ),*/

          /*// ---------------- //
          // List menu Style //
          // ---------------- // */
//          showMenu ? new Column(
//            children: <Widget>[
//              new ListTile(
//                  title: Text(
//                    "Daily Journal",
//                    style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
//                  ),
//                  leading: Icon(
//                    Icons.home,
//                    color: kinderm8Theme.Colors.appmenuicon,
//                  ),
//                  onTap: () {
//                    if (showMenu == true) {
//                  Navigator.of(context)
//                      .pushReplacement(new MaterialPageRoute<Null>(
//                          builder: (BuildContext context) {
//                            return DailyJournal(selectedChild, jwt);
//                          },
//                          fullscreenDialog: true));
//                    } else {
//                      print("not selected..");
//                    }
//                  }),
//              new ListTile(
//                  title: Text(
//                    "Observation",
//                    style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
//                  ),
//                  leading: Icon(
//                    Icons.home,
//                    color: kinderm8Theme.Colors.appmenuicon,
//                  ),
//                  onTap: () {
//                    if (showMenu == true) {
//                      Navigator.of(context).push(new MaterialPageRoute<Null>(
//                          builder: (BuildContext context) {
//                            return ObservationList(selectedChild);
//                          },
//                          fullscreenDialog: true));
//                    } else {
//                      print("not selected..");
//                    }
//                  })
//            ],
//          ): new Container()


/*          new ListTile(
              title: Text(
                "Main Menu",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
              ),
              leading: Icon(
                Icons.home,
                color: kinderm8Theme.Colors.appmenuicon,
              ),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute<Null>(
                    builder: (BuildContext context) {
                      return CommonDrawer();
                    },
                    fullscreenDialog: true));
              }),
          new ListTile(
            title: Text(
              "LogOut",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.keyboard_return,
              color: Colors.redAccent,
            ),
            onTap: () {
              logout();
            },
          ),*/
/*        new Padding(
          padding: new EdgeInsets.all(5.0),
          child: new BackButton(
            color: kinderm8Theme.Colors.appcolour,
          ),
        ),
        new Text(
          'Go Back',
          style: Theme.of(context).textTheme.headline.copyWith(
              color: kinderm8Theme.Colors.appmenuicon, fontSize: 26.0),
        ),*/
        ],
      ),
//      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
//      floatingActionButton: FloatingActionButton(
//        child: const Icon(Icons.close),
//        backgroundColor: kinderm8Theme.Colors.appcolour,
//        onPressed: () {
////            showModalBottomSheet<Null>(
////                context: context,
////                builder: (BuildContext context) {
////                  return ChildrenDrawer();
////                });
//          Navigator.of(context).pop();
////            Navigator.push(
////                context, new MaterialPageRoute(builder: (context) => new ChildrenDrawer()));
//        },
//      ),
    );
  }


  /* ---------------- */
  /* Old Menu design */
  /* --------------- */
//  @override
  /*Widget build_old(BuildContext context) {
    _ctx = context;
    deviceSize = MediaQuery.of(context).size;
    return new Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
            child: ListView(
              scrollDirection: Axis.vertical,
              children: <Widget>[
                Container(width: 100.0, color: Colors.redAccent),
                Container(width: 100.0, color: Colors.amberAccent),
                Container(width: 100.0, color: Colors.cyan),
                Container(width: 100.0, color: Colors.black),
              ],
            ),
//            child: ListView.builder(
//              scrollDirection: Axis.horizontal,
//              itemCount: this.children != null ? this.children.length : 0,
//              itemBuilder: (context, i) {
//                final children = this.children[i];
//                return new Container(
//                  child: new Text(children["firstname"],
//                      style: new TextStyle(fontWeight: FontWeight.w500)),
//                );
//              },
//            ),
          ),

          Container(
            height: deviceSize.height * 0.24,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        print('notification');
                        Navigator.of(context).pop();
                        Navigator.pushNamed(context, "/notification");
                      },
                      child: new Container(
                        width: 50.0,
                        height: 50.0,
                        decoration: new BoxDecoration(
                          color: const Color(0xff7c94b6),
                          image: new DecorationImage(
                            image: AssetImage(
                                "assets/iconsetpng/notification.png"),
                            fit: BoxFit.cover,
                          ),
                          borderRadius:
                              new BorderRadius.all(new Radius.circular(50.0)),
                          border: new Border.all(
                            color: Colors.red,
                            width: 1.0,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius:
                            new BorderRadius.all(new Radius.circular(50.0)),
                        border: new Border.all(
                          color: Colors.black,
                          width: 2.0,
                        ),
                      ),
                      child: CircleAvatar(
                        backgroundImage: AssetImage("assets/Kinderm8.png"),
                        foregroundColor: Colors.black,
                        radius: 40.0,
                      ),
                    ),
                    Container(
                      width: 50.0,
                      height: 50.0,
                      margin: const EdgeInsets.all(6.0),
                      padding: const EdgeInsets.all(26.0),
                      decoration: new BoxDecoration(
                        color: const Color(0xff7c94b6),
                        image: new DecorationImage(
                          image: AssetImage("assets/iconsetpng/settings.png"),
                          fit: BoxFit.contain,
                        ),
                        borderRadius:
                            new BorderRadius.all(new Radius.circular(50.0)),
                        border: new Border.all(
                          color: Colors.red,
                          width: 2.0,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Container(
//            child: Column(
//              children: <Widget>[Text("$email"), Text("$client_id")],
//            ),
          ),
          Divider(),
          Container(
            child: new Column(
              children: <Widget>[
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {
                        Navigator.pushNamed(context, "/home");
                        print("home");
                      },
                      child: Column(
                        children: <Widget>[
                          new Container(
                            width: 50.0,
                            height: 50.0,
                            decoration: new BoxDecoration(
                              color: const Color(0xff7c94b6),
                              image: new DecorationImage(
                                image: new AssetImage(
                                    'assets/iconsetpng/house.png'),
                                fit: BoxFit.contain,
                              ),
                              borderRadius: new BorderRadius.all(
                                  new Radius.circular(5.0)),
//                              border: new Border.all(
//                                color: Colors.purpleAccent,
//                                width: 1.0,
//                              ),
                            ),
                          ),
                          new Text(
                            "Home",
                            style: TextStyle(fontSize: 13.0),
                          ),
                        ],
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        print("home");
                      },
                      child: Column(
                        children: <Widget>[
                          new Container(
                            width: 50.0,
                            height: 50.0,
                            decoration: new BoxDecoration(
                              color: const Color(0xff7c94b6),
                              image: new DecorationImage(
                                image: new AssetImage('assets/nophoto.jpg'),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: new BorderRadius.all(
                                  new Radius.circular(100.0)),
                              border: new Border.all(
                                color: Colors.purpleAccent,
                                width: 1.0,
                              ),
                            ),
                          ),
                          new Text(
                            "Message",
                            style: TextStyle(fontSize: 13.0),
                          ),
                        ],
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        print("home");
                      },
                      child: Column(
                        children: <Widget>[
                          new Container(
                            width: 50.0,
                            height: 50.0,
                            decoration: new BoxDecoration(
                              color: const Color(0xff7c94b6),
                              image: new DecorationImage(
                                image: new AssetImage('assets/icon.jpg'),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: new BorderRadius.all(
                                  new Radius.circular(100.0)),
                              border: new Border.all(
                                color: Colors.purpleAccent,
                                width: 1.0,
                              ),
                            ),
                          ),
                          new Text(
                            "Centre to Note",
                            style: TextStyle(fontSize: 13.0),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
//
                Padding(
                  padding: EdgeInsets.all(2.0),
                ),
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {
                        print("home");
                      },
                      child: Column(
                        children: <Widget>[
                          new Container(
                            width: 50.0,
                            height: 50.0,
                            decoration: new BoxDecoration(
                              color: const Color(0xff7c94b6),
                              image: new DecorationImage(
                                image: new AssetImage('assets/nophoto.jpg'),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: new BorderRadius.all(
                                  new Radius.circular(50.0)),
                              border: new Border.all(
                                color: Colors.purpleAccent,
                                width: 1.0,
                              ),
                            ),
                          ),
                          new Text(
                            "Newsletter",
                            style: TextStyle(fontSize: 13.0),
                          ),
                        ],
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        print("home");
                      },
                      child: Column(
                        children: <Widget>[
                          new Container(
                            width: 50.0,
                            height: 50.0,
                            decoration: new BoxDecoration(
                              color: const Color(0xff7c94b6),
                              image: new DecorationImage(
                                image: new AssetImage('assets/nophoto.jpg'),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: new BorderRadius.all(
                                  new Radius.circular(100.0)),
                              border: new Border.all(
                                color: Colors.purpleAccent,
                                width: 1.0,
                              ),
                            ),
                          ),
                          new Text(
                            "Gerneral forms",
                            style: TextStyle(fontSize: 13.0),
                          ),
                        ],
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        print("home");
                      },
                      child: Column(
                        children: <Widget>[
                          new Container(
                            width: 50.0,
                            height: 50.0,
                            decoration: new BoxDecoration(
                              color: const Color(0xff7c94b6),
                              image: new DecorationImage(
                                image: new AssetImage('assets/icon.jpg'),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: new BorderRadius.all(
                                  new Radius.circular(100.0)),
                              border: new Border.all(
                                color: Colors.purpleAccent,
                                width: 1.0,
                              ),
                            ),
                          ),
                          new Text(
                            "Policies",
                            style: TextStyle(fontSize: 13.0),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
//
          Divider(),

          new ListTile(
            title: Text(
              "Profile",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.person,
              color: Colors.blue,
            ),
          ),
          new ListTile(
            title: Text(
              "Dashboard",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.dashboard,
              color: Colors.red,
            ),
          ),
          new ListTile(
            title: Text(
              "Timeline",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.timeline,
              color: Colors.cyan,
            ),
          ),
          Divider(),
          new ListTile(
            title: Text(
              "Settings",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.settings,
              color: Colors.brown,
            ),
          ),
          new ListTile(
            title: Text(
              "LogOut",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.keyboard_return,
              color: Colors.redAccent,
            ),
            onTap: () {
              logout();
            },
          ),
          Divider(),
//          MyAboutTile()
        ],
      ),
    );

    //      Drawer(
//      child: ListView(
//        padding: EdgeInsets.zero,
//        children: <Widget>[
//          UserAccountsDrawerHeader(
//            accountName: Text(
//              "Pawan Kumar",
//            ),
//            accountEmail: Text(
//              "mtechviral@gmail.com",
//            ),
//            currentAccountPicture: new CircleAvatar(
////              backgroundImage: new AssetImage("http://i.pravatar.cc/300"),
//            ),
//          ),
//          new ListTile(
//            title: Text(
//              "Profile",
//              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
//            ),
//            leading: Icon(
//              Icons.person,
//              color: Colors.blue,
//            ),
//          ),
//          new ListTile(
//            title: Text(
//              "Shopping",
//              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
//            ),
//            leading: Icon(
//              Icons.shopping_cart,
//              color: Colors.green,
//            ),
//          ),
//          new ListTile(
//            title: Text(
//              "Dashboard",
//              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
//            ),
//            leading: Icon(
//              Icons.dashboard,
//              color: Colors.red,
//            ),
//          ),
//          new ListTile(
//            title: Text(
//              "Timeline",
//              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
//            ),
//            leading: Icon(
//              Icons.timeline,
//              color: Colors.cyan,
//            ),
//          ),
//          Divider(),
//          new ListTile(
//            title: Text(
//              "Settings",
//              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
//            ),
//            leading: Icon(
//              Icons.settings,
//              color: Colors.brown,
//            ),
//          ),
//          Divider(),
////          MyAboutTile()
//        ],
//      ),
//    );
  }*/

  @override
  void onDisplayUserInfo(User user) {
//    email = user.email;
//    password = user.password;
//    client_id = user.client_id;

//    setState(() {
    try {
      center = user.center;
      final parsed = json.decode(center);
      var details = parsed[0];
      print(details);
      jwt = details["jwt"];
      var users = details["user"];

      id = users["id"];
      cliId = users["client_id"];
      children = details["children"];

      print("Children $children");
      print("Children ${children.length}");
      print("id $id , clientId $cliId");
      setState(() {
        load = false;
      });
//        print(jwt);
//        image = children[0]["image"];
//        print(id);

    } catch (e) {
      print('That string was null!');
    }
//    });
  }

  @override
  void onErrorUserInfo() {}

  @override
  void onLogoutUser() {
    var authStateProvider = new AuthStateProvider();
    setState(() {
      authStateProvider.notify(AuthState.LOGGED_OUT);
      authStateProvider.dispose(this);
    });
    // TODO: implement onLogoutUser
  }

  @override
  void onAuthStateChanged(AuthState state) {
    print("Navigatesssss");
    print("state $state");
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(_ctx)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }
    // TODO: implement onAuthStateChanged
  }
}

class LabelBelowIcon extends StatelessWidget {
  final label;
  final IconData icon;
  final iconColor;
  final onPressed;
  final circleColor;
  final isCircleEnabled;
  final betweenHeight;

  LabelBelowIcon(
      {this.label,
        this.icon,
        this.onPressed,
        this.iconColor = Colors.white,
        this.circleColor,
        this.isCircleEnabled = true,
        this.betweenHeight = 5.0});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onPressed,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          isCircleEnabled
              ? CircleAvatar(
            backgroundColor: circleColor,
            radius: 20.0,
            child: Icon(
              icon,
              size: 12.0,
              color: iconColor,
            ),
          )
              : Icon(
            icon,
            size: 23.0,
            color: iconColor,
          ),
          SizedBox(
            height: betweenHeight,
          ),
          Text(
            label,
            textAlign: TextAlign.center,
//            style: TextStyle(fontFamily:f),
          )
        ],
      ),
    );
  }
}
//
//class DashboardMenuRow extends StatelessWidget {
//  final firstLabel;
//  final IconData firstIcon;
//  final firstIconCircleColor;
//  final secondLabel;
//  final IconData secondIcon;
//  final secondIconCircleColor;
//  final thirdLabel;
//  final IconData thirdIcon;
//  final thirdIconCircleColor;
//  final fourthLabel;
//  final IconData fourthIcon;
//  final fourthIconCircleColor;
//
//  const DashboardMenuRow(
//      {Key key,
//      this.firstLabel,
//      this.firstIcon,
//      this.firstIconCircleColor,
//      this.secondLabel,
//      this.secondIcon,
//      this.secondIconCircleColor,
//      this.thirdLabel,
//      this.thirdIcon,
//      this.thirdIconCircleColor,
//      this.fourthLabel,
//      this.fourthIcon,
//      this.fourthIconCircleColor})
//      : super(key: key);
//  @override
//  Widget build(BuildContext context) {
//    return Padding(
//      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
//      child: Row(
//        mainAxisAlignment: MainAxisAlignment.spaceAround,
//        mainAxisSize: MainAxisSize.max,
//        children: <Widget>[
//          LabelBelowIcon(
//            icon: firstIcon,
//            label: firstLabel,
//            circleColor: firstIconCircleColor,
//          ),
//          LabelBelowIcon(
//            icon: secondIcon,
//            label: secondLabel,
//            circleColor: secondIconCircleColor,
//          ),
//          LabelBelowIcon(
//            icon: thirdIcon,
//            label: thirdLabel,
//            circleColor: thirdIconCircleColor,
//          ),
//          LabelBelowIcon(
//            icon: fourthIcon,
//            label: fourthLabel,
//            circleColor: fourthIconCircleColor,
//          ),
//        ],
//      ),
//    );
//  }
//}*/

/*

class MyAboutTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AboutListTile(
      applicationIcon: FlutterLogo(
        colors: Colors.yellow,
      ),
      icon: FlutterLogo(
        colors: Colors.yellow,
      ),
      aboutBoxChildren: <Widget>[
        SizedBox(
          height: 10.0,
        ),
        Text(
          "Developed By Proitzen",
        ),
      ],
//      applicationName: UIData.appName,
//      applicationVersion: "1.0.1",
//      applicationLegalese: "Apache License 2.0",
    );
  }
}
*/

class _BackgroundImageClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = new Path();
    path.moveTo(0.0, 0.0);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;

}