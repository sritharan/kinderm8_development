import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/medication/medicationlist.dart';
import 'package:kinderm8/pages/medication/nonprescribedlist.dart';
import 'package:kinderm8/utils/network_util.dart';

class CreateNonPrescribed extends StatefulWidget {
  final childData;
  final jwt;
  CreateNonPrescribed(this.childData, this.jwt);
  @override
  CreateNonPrescribedState createState() => CreateNonPrescribedState();
}

class CreateNonPrescribedState extends State<CreateNonPrescribed>
    implements HomePageContract {
  var appuser, jwt, clientId, id;
  final formKey1 = new GlobalKey<FormState>();
  final formKey2 = new GlobalKey<FormState>();
  final TextEditingController _textController1 = new TextEditingController();
  final TextEditingController _textController2 = new TextEditingController();
  TextEditingController dateInputController;
  DateTime _date;
  bool submit = true;
  bool errorCal = false;

  HomePagePresenter _presenter;

  CreateNonPrescribedState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(labelsConfig["createNonPrescribedMediLabel"]),
        elevation: 0.5,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Column(
            children: <Widget>[
              Container(
                height: 20.0,
              ),
              Container(
                child: Column(
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          " * ",
                          style: TextStyle(
                              fontSize: 18.0,
                              color: Colors.redAccent,
                              fontWeight: FontWeight.w500),
                        ),
                        Flexible(
                          child: Text(
                            "${labelsConfig["medsToApplyAdministeredLabel"]} :",
                            style: TextStyle(
                                fontSize: 16.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                    Container(height: 5.0),
                    Padding(
                      padding: const EdgeInsets.only(left: 14.0, right: 10.0),
                      child: Container(
                        child: Form(
                            key: formKey1,
                            child: TextFormField(
                              textInputAction: TextInputAction.done,
                              controller: _textController1,
                              validator: (val) {
                                return val.length == 0
                                    ? "This field must not be null"
                                    : null;
                              },
                              decoration: InputDecoration(
                                  hintText: labelsConfig["methodToBeAdministeredLabel"]),
                            )),
                      ),
                    ),
                  ],
                ),
              ),
              Container(height: 5.0),
//
              Padding(
                padding: const EdgeInsets.only(left: 14.0, right: 10.0),
                child: InkWell(
                  onTap: () {
                    print("onTap start");
                    _showDatePicker();
                  },
                  child: Row(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          labelsConfig["dateAuthorizedLabel"],
                          style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.black,
                              fontWeight: FontWeight.w500),
                        ),
                        Text(
                          " * ",
                          style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.redAccent,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          Icons.event,
                          color: errorCal ? Colors.redAccent : Colors.deepPurple,
                        ),
                      ),
                    Expanded(
                        child: TextFormField(
                      controller: dateInputController,
                      enabled: false,
                      decoration: InputDecoration(
                          fillColor: Colors.grey[300],
                          filled: true,
                      ),),),
                  ],
                ),),
              ),
              Container(height: 5.0),
//
              Padding(
                padding: const EdgeInsets.only(left: 14.0, right: 8.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "${labelsConfig["giveUnderCircumstanceLabel"]} :",
                          style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.black,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    Container(height: 5.0),
                    Container(
                      child: Form(
                          key: formKey2,
                          child: TextFormField(
                            textInputAction: TextInputAction.done,
                            maxLines: 1,
                            controller: _textController2,
                            validator: (val) {
                              return val.length == 0
                                  ? "This field must not be null"
                                  : null;
                            },
                            decoration: InputDecoration(
                              hintText: "Give under following circumstance :",
                            ),
                          )),
                    ),
                    Container(height: 20.0),
                    Text(
                      labelsConfig["nonPrescribedAcknowledgmentLabel"],
                      style: TextStyle(
                          fontSize: 13.0),
                    ),
                    Container(height: 5.0),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: RaisedButton(
                              color: Colors.deepPurple,
                              child: Text("SUBMIT",style: TextStyle(
                                  fontSize: 20.0, color: Colors.white)),
                              onPressed: () {
                                if (submit) {
                                  setState(() {
                                    submit = false;
                                  });
                                  submitMedi();
                                  print("submit");
                                } else {
                                  submitMedi();
                                  print("else//");
                                }
                              }),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  submitMedi() {
    final form1 = formKey1.currentState;
    if (form1.validate() && _date != null) {
      form1.save();
      FocusScope.of(context).requestFocus(new FocusNode());
      var childId = widget.childData.id;

      var timeStamp = new DateFormat("yyyy-MM-dd");
      String formatdailychartdate = timeStamp.format(_date);

      var body = {
        "medication_to_administer": _textController1.text.toString(),
        "circumstances": _textController2.text.toString(),
        "child_id": childId,
        "authorized_date": formatdailychartdate,
        "requested_by": id
      };
      print(body);
      final submitMediUrl =
          'http://api.kinderm8.com.au/v2.1.0/medication/non-prescribed/create?clientid=$clientId';
      var headers = {"x-authorization": jwt.toString()};

      NetworkUtil _netUtil = new NetworkUtil();
      _netUtil
          .post(submitMediUrl,
              body: body, headers: headers, encoding: jwt.toString())
          .then((res) {
        print("________ ${res.length}");
        if (res == null) {
          setState(() {
            submit = true;
          });
        } else {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      Medications(widget.childData, jwt,1)));

          _textController1.clear();
          _textController2.clear();
          dateInputController.clear();
        }
      });
    } else {
      setState(() {
        if (_date == null) {
          errorCal = true;
        }
      });
      print("else");
      submit = true;
    }
  }

  _showDatePicker() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(DateTime.now().year - 1, 1),
        lastDate: DateTime(DateTime.now().year, 12));

    if (picked != null) {
      setState(() {
        _date = picked;
        setState(() {
          errorCal = false;
        });
        print("%%%%%%%%%%% $_date");
        dateInputController = new TextEditingController(
            text: "${picked.day}/${picked.month}/${picked.year}");
      });
    }
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      jwt = widget.jwt.toString();
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}