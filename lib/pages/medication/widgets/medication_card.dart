import 'package:flutter/material.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:kinderm8/pages/medication/modals/medication_card_model.dart';

class MedicationCard extends StatelessWidget {
  MedicationCard({
    this.dateMonthHeader,
    this.medicationCardData,
    this.medicationCardDataGroup,
    this.isPrevMonths,
  });

  final String dateMonthHeader;
  final MedicationCardData medicationCardData;
  final List medicationCardDataGroup;
  final bool isPrevMonths;

  final TextStyle textStyle = TextStyle(color: Colors.black54);

  Widget _buildMedicationName(MedicationCardData med) {
    return med.name != "" && med.name != null
        ? Text(med.name, style: textStyle)
        : SizedBox(); //"Name of the Medication"
  }

  Widget _buildMedicationTime(MedicationCardData med) {
    return med.time != "" && med.time != null
        ? Text(med.time, style: textStyle)
        : SizedBox(); //"Time of the Medication"
  }

  Widget _buildMedicationDosage(MedicationCardData med) {
    return med.dosage != "" && med.dosage != null
        ? Text(med.dosage, style: textStyle)
        : SizedBox(); //"Time of the Medication"
  }

  Widget _buildMedicationStatus(MedicationCardData med) {
    if (med.status != 0 && med.status != null) {
      Text statusText = Text("Status: ");

      return (med.status > 0)
          ? Row(
              children: <Widget>[
                statusText,
                Text("Pending", style: textStyle.copyWith(color: Colors.green))
              ],
            )
          : Row(
              children: <Widget>[
                statusText,
                Text("Completed",
                    style: textStyle.copyWith(color: Colors.orange))
              ],
            );
    }

    return SizedBox(); //"Time of the Medication"
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 5.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: new BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
                color: Colors.black12, offset: Offset(3, 3), blurRadius: 5.0),
          ],
        ),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Container(
            padding:
                const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
//                      decoration: BoxDecoration(
//                        border: Border.all(width: 1.0, color: Colors.black12),
//                      ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 0.0),
                  child: Text(
                    dateMonthHeader,
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Theme.Colors.appdarkcolour.withAlpha(200),
                    ),
                  ),
                ),
                Container(
                  height: 1.3,
                  color: Color(0xFFCCCCCC),
                ),
                SizedBox(height: 8.0),
                Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 10.0,
                              height: 10.0,
                              margin: EdgeInsets.only(top: 4.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.black54,
                              ),
                            ),
                            SizedBox(width: 8.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                _buildMedicationName(medicationCardData),
                                SizedBox(height: 2.0),
                                _buildMedicationTime(medicationCardData),
                                SizedBox(height: 2.0),
                                _buildMedicationDosage(medicationCardData),
                                SizedBox(height: 2.0),
                                _buildMedicationStatus(medicationCardData),
                                SizedBox(height: 2.0),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: 8.0),
                      ],
                    ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
