import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/medication/create_prescribed.dart';
import 'package:kinderm8/pages/medication/createnonprescribed.dart';

class MyFloatingMenuPage extends StatefulWidget {
  MyFloatingMenuPage({
    this.childData,
    this.jwt,
  });

  final childData;
  final jwt;

  @override
  State createState() => new MyFloatingMenuPageState();
}

class MyFloatingMenuPageState extends State<MyFloatingMenuPage>
    with TickerProviderStateMixin {
  AnimationController _controller;

  static const List<IconData> icons = const [
    Icons.local_hospital,
    Icons.do_not_disturb
  ];
  List<Text> texts = [
    Text(labelsConfig["prescribedMedicationLabel"].split(" ")[0], style: TextStyle(color: Colors.white)),
    Text(labelsConfig["nonPrescribedMedicationLabel"].split(" ")[0], style: TextStyle(color: Colors.white))
  ];

  @override
  void initState() {
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void closeAndNavigateToCreate(int index) {
    if (_controller.isDismissed) {
      _controller.forward();
    } else {
      _controller.reverse();
    }

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) {
            return index == 0
                ? CreatePrescribed(widget.childData, widget.jwt)
                : CreateNonPrescribed(widget.childData, widget.jwt);
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    Color backgroundColor = Theme.of(context).cardColor;
    Color foregroundColor = Theme.of(context).accentColor;
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisSize: MainAxisSize.min,
      children: new List.generate(icons.length, (int index) {
        Widget child = new Container(
          height: 50.0,
          width: 220.0,
          margin: EdgeInsets.only(top: 5.0),
          alignment: FractionalOffset.topCenter,
          child: new ScaleTransition(
            scale: new CurvedAnimation(
              parent: _controller,
              curve: new Interval(0.0, 1.0 - index / icons.length / 2.0,
                  curve: Curves.easeOut),
            ),
            child: Container(
              width: 162.0,
              padding: EdgeInsets.only(left: 10.0),
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
//                border: Border.all(),
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: InkWell(
                onTap: () => closeAndNavigateToCreate(index),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    texts[index],
                    new FloatingActionButton(
                      heroTag: null,
                      backgroundColor: backgroundColor,
                      mini: true,
                      child: new Icon(icons[index], color: foregroundColor),
                      onPressed: () => closeAndNavigateToCreate(index),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
        return child;
      }).toList()
        ..add(
          new FloatingActionButton(
            heroTag: null,
            child: new AnimatedBuilder(
              animation: _controller,
              builder: (BuildContext context, Widget child) {
                return new Transform(
                  transform:
                      new Matrix4.rotationZ(_controller.value * 0.5 * math.pi),
                  alignment: FractionalOffset.center,
                  child: new Icon(
                      _controller.isDismissed ? Icons.add : Icons.close),
                );
              },
            ),
            onPressed: () {
              if (_controller.isDismissed) {
                _controller.forward();
              } else {
                _controller.reverse();
              }
            },
          ),
        ),
    );
  }
}