import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/medication/detailviewpriscribed.dart';
import 'package:kinderm8/pages/medication/edit_prescribed.dart';
import 'package:kinderm8/pages/medication/medicationlist.dart';
import 'package:kinderm8/pages/medication/modals/medication_card_model.dart';
import 'package:kinderm8/pages/medication/repeat_prescribed.dart';
import 'package:kinderm8/pages/medication/repeat_prescribed_detaillist.dart';
import 'package:kinderm8/pages/medication/widgets/medication_card.dart';
import 'package:kinderm8/pages/medication/widgets/medication_card_options.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';

class PrescribedMediData extends StatefulWidget {
  final childData;
  final singlemedicationData;
  final jwt;
  final users;
  final List medicationDataGroupByDay;
  final bool isPrevMonths;

  PrescribedMediData (this.childData, this.singlemedicationData, this.jwt,
                      this.users, {this.medicationDataGroupByDay, this.isPrevMonths = false});

  @override
  PrescribedMediDataState createState () =>
      PrescribedMediDataState(childData, singlemedicationData, jwt, users);
}

class PrescribedMediDataState extends State<PrescribedMediData> {
  final singlemedicationData;
  final users;
  final childData;
  var jwt;

  PrescribedMediDataState (this.childData, this.singlemedicationData, this.jwt,
                           this.users);

  bool delete = false;
  BuildContext ctx;

  void deleteMedication (mediId) async {
    showDialog(
        context: ctx,
        barrierDismissible: true,
        builder: (context) => AlertDialog(
          title: Center(child: CircularProgressIndicator()),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Deleting...'),
            ],
          ),
        ));
    var childId = childData.id;
    String deleteUrl =
        'http://13.55.4.100:7070/v2.1.1/medication/delete?clientid=${users["client_id"]}';
    var body = {
      "id": mediId,
      "requested_by": users['id'],
      "child_id": childId
    };
    var headers = {"x-authorization": jwt.toString()};
    print(body);
    print(deleteUrl);

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.apiDeleteRequest(deleteUrl, headers: headers, body: json.encode(body)).then((res){
      if(res.statusCode == 200) {
        Navigator.pop(ctx);
        Navigator.pushReplacement(ctx, MaterialPageRoute(builder: (BuildContext context) => Medications(widget.childData, jwt, 0)));
      }
    });
//    .then((res) {
//      print(res);
//      if (res.statusCode == 200) {
//        Navigator.pushReplacement(
//            context,
//            MaterialPageRoute(
//                builder: (context) => Medications(childData, jwt, 0)));
//        print("DELETED..!");
//      } else if (res.statusCode == 500) {
//        print("refreshing Token..");
//        String _refreshTokenUrl =
//            'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=${users['id']}&clientid=${users["client_id"]}';
//
//        _netutil.get(_refreshTokenUrl).then((response) {
//          print('refresh get ${response.body}');
//          var refreshJwtToken = json.decode(response.body);
//          this.jwt = refreshJwtToken;
//          deleteMedication(medi_id);
//        });
//      }
//    });
  }

  //Asanka created global variables here
  MedicationCardData medicationCardData;

  String dateMonthHeader;
  void generateDayWithMonth() {
    if (singlemedicationData["started_on"] != null &&
        singlemedicationData["started_on"] != "") {
      DateTime dateTime = DateTime.parse(singlemedicationData["started_on"]);

      setState(() {
        String generatedMonth = DateFormat('MMMM')
            .format(DateTime(dateTime.year, dateTime.month, dateTime.day))
            .substring(0, 3);
        dateMonthHeader = "${dateTime.day} $generatedMonth";
      });
    }
  }

  @override
  void initState () {
    dateMonthHeader = "";
    if (widget.medicationDataGroupByDay.length > 0) {
      final firstMedication = singlemedicationData;
      generateDayWithMonth();
      setState(() {
        medicationCardData = MedicationCardData(
          name: firstMedication["medication_name"],
          time: "${labelsConfig["medicationStartedOnLabel"]}: ${firstMedication["started_on"]}",
          dosage: "${(labelsConfig["dosageLabel"].contains(" ")) ? labelsConfig["dosageLabel"].split(" ").first : labelsConfig["dosageLabel"]}: ${firstMedication["dosage"]}",
          status: firstMedication["pending_request_count"],
          dateMonthHeader: dateMonthHeader,
        );
      });
    } else {
      setState(() {
        medicationCardData = MedicationCardData(
          name: "No Data available at this day",
        );
      });
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ctx = context;
    generateDayWithMonth();
    final Size screenSize = MediaQuery
        .of(context)
        .size;

    var canEdit;
    if (singlemedicationData['canedit'] == true) {
      canEdit = 1;
    } else {
      canEdit = 0;
    }

    var canDelete;
    if (singlemedicationData['candelete'] == true) {
      canDelete = 1;
    } else {
      canDelete = 0;
    }

//    here u need to design card for one meditation and validate it.
    return (medicationCardData != null)
        ? InkWell(
            onTap: () {
              if (singlemedicationData != null) {
                showDialog(context: context,
                  builder: (BuildContext context) {
                    return MedicationOptionsDialog(childData: childData, jwt: jwt, medData: singlemedicationData, deleteMedi: deleteMedication, canMediDelete: canDelete, canMediEdit: canEdit);
                  }
                );
              }
//              Navigator.push(
//                context,
//                MaterialPageRoute(
//                    builder: (context) =>
//                        ViewPrescribed(singlemedicationData)),
//              );
            },
            child: MedicationCard(dateMonthHeader: dateMonthHeader, medicationCardData: medicationCardData, medicationCardDataGroup: widget.medicationDataGroupByDay, isPrevMonths: widget.isPrevMonths,),
//                child:  Card(
//                    margin:
//                        const EdgeInsets.symmetric(vertical: 8.0, horizontal: 14.0),
//                    child: new Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                        crossAxisAlignment: CrossAxisAlignment.start,
//                        children: <Widget>[
//                          Container(
//                            child: Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: <Widget>[
//                                new Container(),
//                                Padding(
//                                  padding:
//                                      const EdgeInsets.only(top: 2.0, bottom: 2.0),
//                                  child: new Text("Medication Name:",
//                                      style: TextStyle(
//                                        fontSize: 14.0,
//                                        color: Theme.Colors.app_dark[400],
//                                        fontWeight: FontWeight.w600,
//                                      ),
//                                      textAlign: TextAlign.left),
//                                ),
//                                Text(
//                                  "${singlemedicationData["medication_name"]}",
//                                  style: TextStyle(
//                                      fontSize: 14.0, color: Theme.Colors.app_dark),
//                                ),
//                                Padding(
//                                  padding:
//                                      const EdgeInsets.only(top: 2.0, bottom: 2.0),
//                                  child: new Text("Started on: ",
//                                      style: TextStyle(
//                                        fontSize: 14.0,
//                                        color: Theme.Colors.app_dark[400],
//                                        fontWeight: FontWeight.w600,
//                                      ),
//                                      textAlign: TextAlign.left),
//                                ),
//                                Container(
//                                  padding: EdgeInsets.only(top: 2.0),
//                                  child: Text(
//                                    "${singlemedicationData["started_on"]}",
//                                    style: TextStyle(
//                                        fontSize: 14.0,
//                                        color: Theme.Colors.app_dark),
//                                  ),
//                                ),
//                                singlemedicationData["manner"] != null &&
//                                        singlemedicationData["manner"].toString() !=
//                                            ""
//                                    ? Padding(
//                                        padding: const EdgeInsets.only(
//                                            top: 2.0, bottom: 2.0),
//                                        child: new Text("Mehod to be administered:",
//                                            style: TextStyle(
//                                              fontSize: 14.0,
//                                              color: Theme.Colors.app_dark[400],
//                                              fontWeight: FontWeight.w600,
//                                            ),
//                                            textAlign: TextAlign.left),
//                                      )
//                                    : new Container(),
//                                singlemedicationData["manner"] != null &&
//                                        singlemedicationData["manner"].toString() !=
//                                            ""
//                                    ? Container(
//                                        padding: EdgeInsets.only(top: 2.0),
//                                        child: Text(
//                                          "${singlemedicationData["manner"]}",
//                                          style: TextStyle(
//                                              fontSize: 14.0,
//                                              color: Theme.Colors.app_dark),
//                                        ),
//                                      )
//                                    : new Container(),
//
//                                // Completed Status
//                                singlemedicationData["activity_status"] ==
//                                        "completed"
//                                    ? new Row(
//                                        children: <Widget>[
//    //                            Padding(
//    //                              padding: const EdgeInsets.only(top:2.0,bottom: 2.0),
//    //                              child: new Text("Status", style: TextStyle(fontSize: 14.0,color: Theme.Colors.app_dark[400],
//    //                                fontWeight: FontWeight.w600,),textAlign: TextAlign.left),
//    //                            ),
//                                          Chip(
//                                            label: new Text("Completed"),
//                                            onDeleted: () {},
//                                            labelPadding: EdgeInsets.all(2.0),
//                                            backgroundColor:
//                                                Theme.Colors.progresscontent,
//                                          )
//                                        ],
//                                      )
//                                    : Container(),
//
//                                // Pending Status
//                                singlemedicationData["pending_request_count"] >=
//                                            1 &&
//                                        singlemedicationData["repeat_medication"]
//                                                .length >=
//                                            1
//                                    ? new Row(
//                                        children: <Widget>[
//    //                            Padding(
//    //                              padding: const EdgeInsets.only(top:2.0,bottom: 2.0),
//    //                              child: new Text("Status", style: TextStyle(fontSize: 14.0,color: Theme.Colors.app_dark[400],
//    //                                fontWeight: FontWeight.w600,),textAlign: TextAlign.left),
//    //                            ),
//    //                            Chip(
//    //                              label: new Text("${singlemedicationData['pending_request_count']} Pending"),
//    //                              onDeleted: () {},
//    //                              labelPadding: EdgeInsets.all(2.0),
//    //                              backgroundColor: Theme.Colors.darkGrey,
//    //                              shape: StadiumBorder(side: BorderSide()),
//    //                            ),
//    //                            Transform(
//    //                              transform: new Matrix4.identity()..scale(0.8),
//    //                              child: new Chip(
//    //                                label: new Text(
//    //                                  "Chip",
//    //                                  overflow: TextOverflow.ellipsis,
//    //                                  style: new TextStyle(color: Colors.white),
//    //                                ),
//    //                                backgroundColor: const Color(0xFFa3bdc4),
//    //                              ),
//    //                            ),
//                                          new Container(
//    //                              decoration: new BoxDecoration(
//    //                                color: Colors.blue.shade100,
//    //                                borderRadius: new BorderRadius.only(
//    //                                    topRight: Radius.circular(30.0), bottomRight: Radius.circular(30.0)),
//    //                                border: new Border.all(color: Color.fromRGBO(0, 0, 0, 0.0)),
//    //                              ),
//                                            child: new Chip(
//                                              backgroundColor: Colors.blue.shade100,
//                                              label: new Text(
//                                                  "${singlemedicationData['pending_request_count']} Pending",
//                                                  style: new TextStyle(
//                                                      fontSize: 15.0,
//                                                      color: Colors.blueGrey)),
//                                            ),
//                                          )
//                                        ],
//                                      )
//                                    : Container(
//                                        height: 0.0,
//                                      ),
//
//                                // On Going Status
//                                singlemedicationData["activity_status"]
//                                                .toString() ==
//                                            'ongoing' &&
//                                        singlemedicationData["request_status"] == 1
//                                    ? new Row(
//                                        children: <Widget>[
//    //                            Padding(
//    //                              padding: const EdgeInsets.only(top:2.0,bottom: 2.0),
//    //                              child: new Text("Status", style: TextStyle(fontSize: 14.0,color: Theme.Colors.app_dark[400],
//    //                                fontWeight: FontWeight.w600,),textAlign: TextAlign.left),
//    //                            ),
//                                          Chip(
//                                            label: new Text("On Going"),
//                                            onDeleted: () {},
//                                            labelPadding: EdgeInsets.all(2.0),
//                                            backgroundColor:
//                                                Theme.Colors.progresscontent,
//                                          )
//                                        ],
//                                      )
//                                    : Container(
//                                        height: 0.0,
//                                      ),
//                              ],
//                            ),
//                            padding: EdgeInsets.all(10.0),
//                          ),
//
//                          ///                     second Row
//                          ///
//                          new Column(
//                            children: <Widget>[
//                              // Repeat History
//                              GestureDetector(
//                                onTap: () {
//                                  Navigator.push(
//                                      context,
//                                      MaterialPageRoute(
//                                          builder: (context) =>
//                                              ViewRepeatPrescribed(childData,
//                                                  singlemedicationData, jwt)));
//                                },
//                                child: Container(
//                                  margin: const EdgeInsets.symmetric(
//                                      vertical: 10.0, horizontal: 10.0),
//    //                                padding: EdgeInsets.all(5.0),
//                                  child: Padding(
//                                    padding: const EdgeInsets.all(3.0),
//                                    child: new Icon(
////                                      FontAwesomeIcons.briefcaseMedical,
//                                    Icons.mode_edit,
//                                      color: Theme.Colors.app_white,
//                                      size: 30.0,
//                                    ),
//                                  ),
//                                  color: Theme.Colors.appBarGradientEnd,
//                                ),
//                              ),
//
//                              // Edit
//                              canEdit == 1
//                                  ? GestureDetector(
//                                      onTap: () {
//                                        Navigator.push(
//                                            context,
//                                            MaterialPageRoute(
//                                                builder: (context) =>
//                                                    EditPrescribed(
//                                                        childData,
//                                                        singlemedicationData,
//                                                        jwt)));
//                                      },
//                                      child: Container(
//                                        margin: const EdgeInsets.symmetric(
//                                            vertical: 10.0, horizontal: 10.0),
//    //                                padding: EdgeInsets.all(5.0),
//                                        child: Padding(
//                                          padding: const EdgeInsets.all(2.0),
//                                          child: new Icon(
////                                            FontAwesomeIcons.edit,
//                                          Icons.edit,
//                                            color: Theme.Colors.app_white,
//                                            size: 30.0,
//                                          ),
//                                        ),
//                                        color: Theme.Colors.appBarGradientEnd,
//                                      ),
//                                    )
//                                  : Container(),
//
//                              // CreateRepeatPrescribed
//                              GestureDetector(
//                                onTap: () {
//                                  Navigator.push(
//                                      context,
//                                      MaterialPageRoute(
//                                          builder: (context) =>
//                                              CreateRepeatPrescribed(childData,
//                                                  singlemedicationData, jwt)));
//                                },
//                                child: Container(
//                                  margin: const EdgeInsets.symmetric(
//                                      vertical: 10.0, horizontal: 10.0),
//    //                                padding: EdgeInsets.all(5.0),
//                                  child: Padding(
//                                    padding: const EdgeInsets.all(2.0),
//                                    child: Icon(
//                                      Icons.update,
//                                      color: Theme.Colors.app_white,
//                                      size: 30.0,
//                                    ),
//                                  ),
//                                  color: Theme.Colors.appBarGradientEnd,
//                                ),
//                              ),
//
//                              // Delete Prescribed
//                              canDelete == 1
//                                  ? GestureDetector(
//                                onTap: () {
//                                  showModalBottomSheet<void>(
//                                      context: context,
//                                      builder: (BuildContext context) {
//                                        return Container(
//                                            height: 150.0,
//                                            width: screenSize.width,
//                                            child: Padding(
//                                              padding:
//                                              const EdgeInsets.all(32.0),
//                                              child: Column(
//                                                children: <Widget>[
//                                                  new Padding(
//                                                    padding:
//                                                    const EdgeInsets.all(
//                                                        2.0),
//                                                    child: new Text(
//                                                      "Do you wish to delete",
//                                                      style: new TextStyle(
//                                                          fontSize: 22.0,
//                                                          color: Theme.Colors
//                                                              .appsupportlabel,
//                                                          fontStyle: FontStyle
//                                                              .normal),
//                                                    ),
//                                                  ),
//                                                  new GestureDetector(
//                                                    onTap: () {
//                                                      setState(() {
//                                                        delete = true;
//                                                      });
//                                                      print(
//                                                          singlemedicationData);
//                                                      Navigator.of(context)
//                                                          .pop();
//                                                      deleteMedication(
//                                                          singlemedicationData[
//                                                          'medication_id']);
//                                                    },
//                                                    child: new Container(
//                                                      width:
//                                                      screenSize.width /
//                                                          2,
//                                                      margin:
//                                                      new EdgeInsets.only(
//                                                          top: 10.0,
//                                                          left: 5.0,
//                                                          right: 5.0,
//                                                          bottom: 10.0),
//                                                      height: 30.0,
//                                                      decoration: new BoxDecoration(
//                                                          color: Theme.Colors
//                                                              .buttonicon_deletecolor,
//                                                          borderRadius:
//                                                          new BorderRadius
//                                                              .all(new Radius
//                                                              .circular(
//                                                              10.0))),
//                                                      child: new Center(
//                                                          child: new Text(
//                                                            "Delete",
//                                                            style: new TextStyle(
//                                                                color:
//                                                                Colors.white,
//                                                                fontSize: 22.0),
//                                                          )),
//                                                    ),
//                                                  ),
//                                                ],
//                                              ),
//                                            ));
//                                      });
//                                },
//                                child: Container(
//                                  margin: const EdgeInsets.symmetric(
//                                      vertical: 10.0, horizontal: 10.0),
//    //                                padding: EdgeInsets.all(5.0),
//                                  child: Padding(
//                                    padding: const EdgeInsets.all(2.0),
//                                    child: delete
//                                        ? CupertinoActivityIndicator(
//                                        radius: 15.0)
//                                        : Icon(Icons.delete,
//                                        color: Theme.Colors.app_white,
//                                        size: 30.0),
//                                  ),
//                                  color: Theme.Colors.form_requiredicon,
//                                ),
//                              )
//                                  : Container(),
//                            ],
//                          ),
//                        ]),
//                  ),
          )
        : Center(
          child: Text("No Medications are available in this day"),
        );
  }

  var progress = new ProgressBar(
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );
}