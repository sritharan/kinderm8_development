import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/Theme.dart' as kinderm8Theme;
import 'package:flutter/material.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/pages/medication/createnonprescribed.dart';
import 'package:kinderm8/pages/medication/nonprescribedmedicationdata.dart';
import 'package:kinderm8/pages/medication/widgets/date_pick_pager.dart';
import 'package:kinderm8/pages/medication/widgets/floating_actions.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:flutter/scheduler.dart';

class NonPrescribed extends StatefulWidget {
  final ChildViewModal childData;
  final jwt;
  NonPrescribed(this.childData, this.jwt);
  @override
  NonPrescribedState createState() => NonPrescribedState();
}

class NonPrescribedState extends State<NonPrescribed>
    with SingleTickerProviderStateMixin
    implements HomePageContract {
  var k, len, appuser, jwt, id, clientId;

  var delete = true;

  var childId;
  bool isLoading;
  bool load = true;
  HomePagePresenter _presenter;
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var medidate;
  var casualattendancedate;
  var casualattendancedata;
  bool enabled = false;
  bool expanded = false;

  TabController tabController;

  //To assign day relevant data that coming from parent widget(cannot use generics: MedicationCardData again the way written, have to do more work)
  List medicationCardDataGroup = [];
  ScrollController nonPrescribedScrollController;
  bool isPreviousButtonActive = false;
  bool isDateHolderActive = true;
  bool isNextButtonActive = false;
  bool isPreviouslyPrescribed = false;
  bool isFuturePrescribing = false;

  List longLiveList = [];

  NonPrescribedState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  @override
  void dispose() {
    nonPrescribedScrollController.removeListener(_scrollListener);
    nonPrescribedScrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    nonPrescribedScrollController = ScrollController()..addListener(_scrollListener);
    super.initState();
//    isLoading = false;
//    load = false;
    // Get Casualattendance data while load view
//    fetchCasualattendanceData(clientId,date);
  }

  var progress = new ProgressBar(
    color: kinderm8Theme.Colors.appcolour,
    containerColor: kinderm8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  var medicationData;
  //Keeping a map to display date holders if med available
  Map<int, bool> medicationAvailabilityMap = {};

  void toggleIsPrevActive(bool flag) {
    setState(() {
      isPreviousButtonActive = flag;
    });
  }

  void toggleNextBtActive(bool flag) {
    setState(() {
      isFuturePrescribing = flag;
    });
  }

  void toggleDateHolderActive(bool flag) {
    setState(() {
      isDateHolderActive = flag;
    });
  }

  void updateMedicationDataByDay([int selectedDay = 0]) {
    DateTime pagerDate = DateTime.now();
    final int today = pagerDate.day;
    setState(() {
      medicationCardDataGroup = [];
    });
    SchedulerBinding.instance.addPostFrameCallback((_) => setState(() {
      if (medicationData != null) {
        if (!(medicationData is Map)) {
          medicationCardDataGroup = medicationData.where((value) {
//            print("This is the value: $value");
            DateTime upcomingDate = DateTime.parse(value["authorized_date"]);
//            print("This is upcomingDate: $upcomingDate");
//            print("Selected day: $selectedDay");
            return (upcomingDate.year == pagerDate.year && upcomingDate.month == pagerDate.month) &&
                (upcomingDate.day ==
                    ((selectedDay == 0) ? today : selectedDay));
          }).toList();
//          print("Here is the available data: $medicationCardDataGroup");
        }
      }
    }));
  }

  Future<String> fetchNonPrescribedMedicationData(int s) async {
    print('fetchMedicationData $childId');
    print('fetchMedicationData $clientId');
    var timeStamp = new DateFormat("yyyy-MM-dd");
//    medidate = new DateTime(date.year, date.month, date.day);
//    String formatdailychartdate = timeStamp.format(medidate);
//    print("formatdailychartdate ${formatdailychartdate}");
    ///data from GET method
    print("data fetched");
    childId = widget.childData.id;
    String _MedicationUrl =
        'http://13.55.4.100:7070/v2.1.1/medication/non-prescribed/$childId?step=$s&clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};
    print(_MedicationUrl);
    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_MedicationUrl, headers: headers).then((response) {
//      try {
      medicationData = json.decode(response.body);
//      } catch (e) {
//        print('That string was null!');
//      }

      if (medicationData != null) {
        if (!(medicationData is Map)) {
          setState(() {
            int index = 1;
            DateTime pagerDate = DateTime.now();
            final int thisMonth = pagerDate.month;
            medicationData.forEach((item) {
              if (item["authorized_date"] != null) {
                DateTime upcomingDate = DateTime.parse(item["authorized_date"]);//["started_on"]);
                if (upcomingDate.month == thisMonth) {
                  medicationAvailabilityMap[upcomingDate.day] = true;
                }

                if (upcomingDate.month < thisMonth) {
                  isPreviouslyPrescribed = true;
                }
              } else {
                medicationAvailabilityMap[index] = false;
              }
              index++;
            });
          });
        }
      }

      if (response.statusCode == 200) {
        longLiveList.addAll(json.decode(response.body));
        print(isLoading);
        isLoading = false;
        print(isLoading);
        if (s == 0) {
          setState(() {
            load = false;
            len = medicationData.length;
            medicationCardDataGroup = medicationData;
          });
        } else {
          setState(() {
            load = false;
            len = medicationData.length;
            medicationCardDataGroup.addAll(medicationData);
          });
        }
        setState(() {
          k = medicationCardDataGroup.length;
        });
      } else if (response.statusCode == 500 &&
          medicationData["errorType"] == 'ExpiredJwtException') {
        isLoading = false;
//        if(clientId !=null) {
        getRefreshToken();
//        }
      } else {
        fetchNonPrescribedMedicationData(0);
      }
    });
    return null;
  }

  getRefreshToken() {
    print("refreshing Token..");
    String _refreshTokenUrl =
        'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      if (k != null) {
        fetchNonPrescribedMedicationData(k);
      } else {
        fetchNonPrescribedMedicationData(0);
      }
    });
  }

  void whenPreviousButtonPressed([String type = "previous"]) {
//    DateTime pagerDate = DateTime.now();
//    final int thisMonth = pagerDate.month;
    var temp = DateTime.now().toUtc();
    var pagerDate = DateTime.utc(temp.year,temp.month);
    setState(() {
      medicationCardDataGroup = [];
    });
    SchedulerBinding.instance.addPostFrameCallback((_) => setState(() {
      isPreviousButtonActive = true;
      isDateHolderActive = false;
      if (medicationData != null) {
        if (!(medicationData is Map)) {
          medicationCardDataGroup = longLiveList.where((value) {
//            DateTime upcomingDate = DateTime.parse(value["authorized_date"]);//value["authorized_date"]);
//            return (upcomingDate.month < thisMonth);
            DateTime upcomingDateUtc = DateTime.parse(value["authorized_date"]).toUtc();
            DateTime upcomingDate = DateTime.utc(upcomingDateUtc.year, upcomingDateUtc.month);
            return (upcomingDate.compareTo(pagerDate) < 0);
          }).toList();
        }
      }
      print("medicationCardDataGroup ${longLiveList.length}");
    }));
  }

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text('Please wait..',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: kinderm8Theme.Colors.app_white)),
            SizedBox(width: 6.0),
            Container(
              width: 20.0,
              height: 20.0,
              child: CircularProgressIndicator(strokeWidth: 2.0, backgroundColor: Colors.white24,
                valueColor: new AlwaysStoppedAnimation(Colors.white),),
            ),
          ],
        ),
        color: kinderm8Theme.Colors.appcolour.withOpacity(0.85),
//        splashColor: Colors.red,
        elevation: 4.0,
        onPressed: _counterButtonPress());
  }

  Function _counterButtonPress() {
    if (isLoading) {
      return null;
    } else {
      return () {
        setState(() {
          isLoading = true;
          fetchNonPrescribedMedicationData(k);
        });
      };
    }
  }

  _scrollListener() {
    if (nonPrescribedScrollController.position.extentAfter <= 0 && isLoading == false) {
      fetchNonPrescribedMedicationData(k);
    }
  }

  Widget buildDataChild() {
    return (medicationCardDataGroup.length > 0)
        ? ListView.builder(
      controller: nonPrescribedScrollController,
      itemCount: medicationCardDataGroup.length,
      itemBuilder: (context, index) {
        if (medicationCardDataGroup.length < 5) {
          return NonPrescribedMediData(widget.childData,
              this.medicationCardDataGroup[index], jwt,
              isPrevMonths: isPreviousButtonActive);
        } else if (medicationCardDataGroup.length == index+1) {
          return _buildCounterButton();
        } else {
          return NonPrescribedMediData(widget.childData,
              this.medicationCardDataGroup[index], jwt,
              isPrevMonths: isPreviousButtonActive,);
        }
      },
    )
        : Container(
      height: MediaQuery.of(context).size.height / 1.5,
      padding: EdgeInsets.all(8.0),
      child: Text(
        "No medications are available in this day",
        textAlign: TextAlign.center,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      body: new RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: handleRefresh,
        child: new Center(
          child: load
              ? progress
              : DatePickPager(
            child: buildDataChild(),
            updateMedicationDataByDay: updateMedicationDataByDay,
            whenPrevBtPress: whenPreviousButtonPressed,
            isDateHolderActive: isDateHolderActive,
            isPrevActive: isPreviousButtonActive,
            isNextActive: isNextButtonActive,
            isPreviouslyPrescribed: isPreviouslyPrescribed,
            isNextMonthPrescribing: isFuturePrescribing,
            dayAvailabilityMap: medicationAvailabilityMap,
            toggleIsPrevActive: toggleIsPrevActive,
            toggleIsDateHolderActive: toggleDateHolderActive,
            toggleIsNextActive: toggleNextBtActive,
          ),
        ),
      ),
      floatingActionButton: MyFloatingMenuPage(childData: widget.childData, jwt: widget.jwt),
    );
  }

  void deleteMedi(mediId, index) async {
    setState(() {
      delete = false;
    });
    String deleteUrl =
        'http://api.kinderm8.com.au/v2.1.0/medication/non-prescribed/delete?clientid=$clientId';
    var body = {
      "id": mediId,
      "requested_by": id,
      "child_id": widget.childData.id
    };

    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.delete(deleteUrl, headers: headers, body: body).then((res) {
      print("res$res");
      print(res.statusCode);
      if (res.statusCode == 200) {
        setState(() {
          delete = true;
          medicationCardDataGroup.removeAt(index);
          print(medicationCardDataGroup.length);
        });
      } else if (res.statusCode == 500) {
        print("refreshing Token..");
        String _refreshTokenUrl =
            'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

        NetworkUtil _netutil = new NetworkUtil();

        _netutil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          var refreshJwtToken = json.decode(response.body);
          this.jwt = refreshJwtToken;
          deleteMedi(mediId, index);
        });
      }
    });
  }

  FloatingActionButton _getMailFloatingActionButton() {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CreateNonPrescribed(widget.childData, jwt)),
        );
//        Navigator.push(
//          context,
//          MaterialPageRoute(builder: (context) => CreateMessage()),
//        );
      },
      child: Icon(Icons.edit),
      backgroundColor: kinderm8Theme.Colors.appcolour,
    );
  }

  Future<Null> handleRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      load = true;
      fetchNonPrescribedMedicationData(0);
    });
    return null;
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      print(appusers);
      jwt = widget.jwt.toString();
//      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];

      print("iddd $id");
      print(clientId);
      fetchNonPrescribedMedicationData(0);
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }

    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
