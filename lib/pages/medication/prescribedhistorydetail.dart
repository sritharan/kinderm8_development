import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/medication/medicationlist.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:kinderm8/Theme.dart' as Theme;

class PrescribedHistoryDetail extends StatefulWidget {
  final singlehistoryData;
  PrescribedHistoryDetail(this.singlehistoryData);
  @override
  PrescribedHistoryDetailState createState() =>
      PrescribedHistoryDetailState(singlehistoryData);
}

class PrescribedHistoryDetailState extends State<PrescribedHistoryDetail> {
  final singlehistoryData;
  PrescribedHistoryDetailState(this.singlehistoryData);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final UriData parent_signature =
        singlehistoryData["parent_signature"] != null &&
                singlehistoryData["parent_signature"] != ''
            ? Uri.parse(singlehistoryData["parent_signature"]).data
            : null;
    final UriData signature_of_witness =
        singlehistoryData["signature_of_witness"] != null &&
                singlehistoryData["signature_of_witness"] != ''
            ? Uri.parse(singlehistoryData["signature_of_witness"]).data
            : null;
    final UriData signature_of_educator_administering =
        singlehistoryData['signature_of_educator_administering'] != null &&
                singlehistoryData["signature_of_educator_administering"] != ''
            ? Uri.parse(
                    singlehistoryData['signature_of_educator_administering'])
                .data
            : null;
    final Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple.shade50,
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Container(
                height: 5.0,
              ),

              /// have to validate
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
//
                  ///Completed by Parent
                  ///
                  Row(
                    children: <Widget>[
                      new Container(
                        width: 5.0,
                        height: 28.0,
                        color: Colors.deepPurpleAccent,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 5.0, bottom: 15.0),
                        child: Text(
                          "Completed by Parent",
                          style: TextStyle(fontSize: 22.0),
                        ),
                      ),
                    ],
                  ),
                  singlehistoryData['last_administered'] == ""
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 5.0, bottom: 5.0),
                              child: Text(
                                "Last administered :",
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                width: screenSize.width,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 6.0, vertical: 6.0),
                                      child: Text(
                                          "${singlehistoryData['last_administered']}"),
                                    ),
                                  ],
                                ),
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                  singlehistoryData['to_be_administered'] == ""
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 5.0, bottom: 5.0),
                              child: Text(
                                "To be administered :",
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                width: screenSize.width,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 6.0, vertical: 6.0),
                                      child: Text(
                                          "${singlehistoryData['to_be_administered']}"),
                                    ),
                                  ],
                                ),
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                  singlehistoryData['dosge'] == ""
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 5.0, bottom: 5.0),
                              child: Text(
                                "Dosage to be administered :",
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                width: screenSize.width,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 6.0, vertical: 6.0),
                                      child:
                                          Text("${singlehistoryData['dosge']}"),
                                    ),
                                  ],
                                ),
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                  singlehistoryData['manner'] == ""
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 5.0, bottom: 5.0),
                              child: Text(
                                "Method to be administered :",
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                width: screenSize.width,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 6.0, vertical: 6.0),
                                      child: Text(
                                          "${singlehistoryData['manner']}"),
                                    ),
                                  ],
                                ),
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),

                  singlehistoryData["parent_signature"] != null
                      ? Padding(
                          padding:
                              const EdgeInsets.only(left: 5.0, bottom: 5.0),
                          child: Text(
                            "Signature of parent/guardian :",
                            style: TextStyle(fontSize: 18.0),
                          ),
                        )
                      : new Container(),
                  singlehistoryData["parent_signature"] != null
                      ? Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            width: screenSize.width,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0, vertical: 16.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.deepPurple,
                                            style: BorderStyle.solid)),
                                    child: new Image.memory(
                                        parent_signature.contentAsBytes()),
                                  ),
                                ),
                              ],
                            ),
                            color: Colors.white,
                          ),
                        )
                      : new Container(),

                  ///Completed by Staff
                  ///
                  singlehistoryData['signature_of_educator_administering'] !=
                          null
                      ? Row(
                          children: <Widget>[
                            new Container(
                              width: 5.0,
                              height: 28.0,
                              color: Colors.deepPurpleAccent,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 5.0, bottom: 15.0),
                              child: Text(
                                "Completed by Staff",
                                style: TextStyle(fontSize: 22.0),
                              ),
                            ),
                          ],
                        )
                      : Container(),

                  singlehistoryData['medication_administered'] == null
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 5.0, bottom: 5.0),
                              child: Text(
                                "Medication Administered: ",
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                width: screenSize.width,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 6.0, vertical: 6.0),
                                      child: Text(
                                          "${singlehistoryData['medication_administered']}"),
                                    ),
                                  ],
                                ),
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),

                  singlehistoryData['dosage_given'] == null
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 5.0, bottom: 5.0),
                              child: Text(
                                "Dosage administration: ",
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                width: screenSize.width,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 6.0, vertical: 6.0),
                                      child: Text(
                                          "${singlehistoryData['dosage_given']}"),
                                    ),
                                  ],
                                ),
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),

                  singlehistoryData['manner_given'] == null
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 5.0, bottom: 5.0),
                              child: Text(
                                "Method of administration: ",
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                width: screenSize.width,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 6.0, vertical: 6.0),
                                      child: Text(
                                          "${singlehistoryData['manner_given']}"),
                                    ),
                                  ],
                                ),
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),

                  singlehistoryData['name_of_educator_administering'] == null
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 5.0, bottom: 5.0),
                              child: Text(
                                "Name of educator administrating :",
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                width: screenSize.width,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 6.0, vertical: 6.0),
                                      child: Text(
                                          "${singlehistoryData['name_of_educator_administering']}"),
                                    ),
                                  ],
                                ),
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),

                  singlehistoryData['signature_of_educator_administering'] !=
                          null
                      ? Padding(
                          padding:
                              const EdgeInsets.only(left: 5.0, bottom: 5.0),
                          child: Text(
                            "Signature of educator administrating :",
                            style: TextStyle(fontSize: 18.0),
                          ),
                        )
                      : new Container(),
                  singlehistoryData['signature_of_educator_administering'] !=
                          null
                      ? Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            width: screenSize.width,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0, vertical: 16.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.deepPurple,
                                            style: BorderStyle.solid)),
                                    child: new Image.memory(
                                        signature_of_educator_administering
                                            .contentAsBytes()),
                                  ),
                                ),
                              ],
                            ),
                            color: Colors.white,
                          ),
                        )
                      : new Container(),

                  singlehistoryData['name_of_witness'] == null
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 5.0, bottom: 5.0),
                              child: Text(
                                "Name of witness :",
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                width: screenSize.width,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 6.0, vertical: 6.0),
                                      child: Text(
                                          "${singlehistoryData['name_of_witness']}"),
                                    ),
                                  ],
                                ),
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),

                  singlehistoryData["signature_of_witness"] != null
                      ? Padding(
                          padding:
                              const EdgeInsets.only(left: 5.0, bottom: 5.0),
                          child: Text(
                            "Signature of Witnes :",
                            style: TextStyle(fontSize: 18.0),
                          ),
                        )
                      : new Container(),
                  singlehistoryData["signature_of_witness"] != null
                      ? Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            width: screenSize.width,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0, vertical: 16.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.deepPurple,
                                            style: BorderStyle.solid)),
                                    child: new Image.memory(
                                        signature_of_witness.contentAsBytes()),
                                  ),
                                ),
                              ],
                            ),
                            color: Colors.white,
                          ),
                        )
                      : new Container(),
                  singlehistoryData['comments'] == null
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 5.0, bottom: 5.0),
                              child: Text(
                                "Commets :",
                                style: TextStyle(fontSize: 18.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(
                                width: screenSize.width,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 6.0, vertical: 6.0),
                                      child: Text(
                                          "${singlehistoryData['comments']}"),
                                    ),
                                  ],
                                ),
                                color: Colors.white,
                              ),
                            ),
                          ],
                        )
                ],
              )

              ///
              ///
            ],
          ),
        ),
      ),
    );
  }
}
