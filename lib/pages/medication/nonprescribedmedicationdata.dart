import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/medication/detailviewnonpriscribeddata.dart';
import 'package:kinderm8/pages/medication/modals/medication_card_model.dart';
import 'package:kinderm8/pages/medication/updatenonprescribed.dart';
import 'package:kinderm8/pages/medication/widgets/medication_card.dart';
import 'package:kinderm8/pages/medication/widgets/medication_card_options.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:groovin_widgets/groovin_widgets.dart';

class NonPrescribedMediData extends StatefulWidget {
  final childData;
  final singlemedicationData;
  final jwt;
  final bool isPrevMonths;

  NonPrescribedMediData(this.childData, this.singlemedicationData, this.jwt, {this.isPrevMonths = false});
  @override
  NonPrescribedMediDataState createState() =>
      NonPrescribedMediDataState(childData, singlemedicationData, jwt);
}

class NonPrescribedMediDataState extends State<NonPrescribedMediData> {
  final singlemedicationData;
  var jwt;
  final childData;

  var authorized_date;

  bool isExpanded = false;

  NonPrescribedMediDataState(
      this.childData, this.singlemedicationData, this.jwt);

  //Asanka created global variables here
  MedicationCardData medicationCardData;

  String dateMonthHeader;
  void generateDayWithMonth() {
    if (singlemedicationData["authorized_date"] != null &&
        singlemedicationData["authorized_date"] != "") {
      DateTime dateTime = DateTime.parse(singlemedicationData["authorized_date"]);

      setState(() {
        String generatedMonth = DateFormat('MMMM')
            .format(DateTime(dateTime.year, dateTime.month, dateTime.day))
            .substring(0, 3);
        dateMonthHeader = "${dateTime.day} $generatedMonth";
      });
    }
  }

  @override
  void initState() {
    dateMonthHeader = "";
  if (singlemedicationData != null) {
    final firstMedication = singlemedicationData;
    generateDayWithMonth();
    setState(() {
      medicationCardData = MedicationCardData(
        name: firstMedication["medication_to_administer"],
        time: "${labelsConfig["dateAuthorizedLabel"]}: ${firstMedication["authorized_date"]}",
        dosage: firstMedication["dosage"],
        status: firstMedication["pending_request_count"],
        dateMonthHeader: dateMonthHeader,
      );
    });
  } else {
    setState(() {
      medicationCardData = MedicationCardData(
        name: "No Data available at this day",
      );
    });
  }

  super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var parsedDate = DateTime.parse(singlemedicationData["authorized_date"]);
    var date = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);
    authorized_date = date.split(' ');

    var canEdit;
    if (singlemedicationData['candelete'] == true) {
      canEdit = 1;
    } else {
      canEdit = 0;
    }

    var medicationName =
        html2md.convert(singlemedicationData["medication_to_administer"]);

//    here u need to design card for one meditation and validate it.
    return singlemedicationData.length > 0
        ? FlatButton(
            padding: EdgeInsets.all(0.0),
            onPressed: () {
              if (singlemedicationData != null) {
                showDialog(context: context,
                    builder: (BuildContext context) {
                      return NonMedicationOptionsDialog(childData: childData, jwt: jwt, medData: singlemedicationData, deleteMedi: () {}, canMediDelete: 0, canMediEdit: canEdit);
                    }
                );
              }
//              Navigator.push(
//                context,
//                MaterialPageRoute(
//                    builder: (context) =>
//                        ViewNonPrescribed(singlemedicationData)),
//              );
            },
            child: MedicationCard(dateMonthHeader: dateMonthHeader, medicationCardData: medicationCardData, isPrevMonths: widget.isPrevMonths,),
//            Card(
//              elevation: 1.0,
//              margin:
//                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 14.0),
//              child: new Row(
//                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                  children: <Widget>[
//                    Expanded(
//                      child: Container(
//                        padding: EdgeInsets.all(10.0),
//                        child: Column(
//                          children: <Widget>[
//                            Padding(
//                              padding:
//                                  const EdgeInsets.only(top: 2.0, bottom: 2.0),
//                              child: new Text("Medication Name:",
//                                  style: TextStyle(
//                                    fontSize: 14.0,
//                                    color: Theme.Colors.app_dark[400],
//                                    fontWeight: FontWeight.w600,
//                                  ),
//                                  textAlign: TextAlign.left),
//                            ),
//                            Html(
//                              data: medicationName,
//                              defaultTextStyle: TextStyle(
//                                  fontSize: 14.0, color: Theme.Colors.app_dark),
//                            ),
//                            Padding(
//                              padding:
//                                  const EdgeInsets.only(top: 2.0, bottom: 2.0),
//                              child: new Text("Started on:",
//                                  style: TextStyle(
//                                    fontSize: 14.0,
//                                    color: Theme.Colors.app_dark[400],
//                                    fontWeight: FontWeight.w600,
//                                  ),
//                                  textAlign: TextAlign.left),
//                            ),
//                            Container(
//                              padding: EdgeInsets.only(top: 2.0),
//                              child: Text(
//                                "${singlemedicationData["authorized_date"]}",
//                                style: TextStyle(
//                                    fontSize: 14.0,
//                                    color: Theme.Colors.app_dark),
//                              ),
//                            )
//                          ],
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                        ),
//                      ),
//                    ),
//                    canEdit == 1
//                        ? GestureDetector(
//                            onTap: () {
//                              Navigator.push(
//                                  context,
//                                  MaterialPageRoute(
//                                      builder: (context) => UpdateNonPrescribed(
//                                          singlemedicationData,
//                                          childData,
//                                          jwt)));
//                            },
//                            child: Container(
//                              margin: const EdgeInsets.symmetric(
//                                  vertical: 10.0, horizontal: 10.0),
////                                padding: EdgeInsets.all(5.0),
//                              child: Padding(
//                                padding: const EdgeInsets.all(2.0),
//                                child: Icon(
//                                  Icons.mode_edit,
//                                  color: Theme.Colors.app_white,
//                                  size: 30.0,
//                                ),
//                              ),
//                              color: Theme.Colors.appBarGradientEnd,
//                            ),
//                          )
//                        : Container(),
//                  ]),
//            ),
          )
        : Center(
            child: Text("No Medi yet"),
          );
  }

  var progress = new ProgressBar(
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );
}
