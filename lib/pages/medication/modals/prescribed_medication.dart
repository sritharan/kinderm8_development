class PrescribedMedication {
  final String id;
  final String medicationName;
  final String startedOn;
  final String dosage;
  final String pendingRequestCount;
  final bool canDelete;

  PrescribedMedication({
    this.id,
    this.medicationName,
    this.startedOn,
    this.dosage,
    this.pendingRequestCount,
    this.canDelete,
  });

  PrescribedMedication.fromJson(Map<String, dynamic> map) :
      id = map["id"],
      medicationName = map["medication_name"],
      startedOn = map["started_on"],
      dosage = map["dosage"],
      pendingRequestCount = map["pending_request_count"],
      canDelete = map["can_delete"];
}