import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/Theme.dart' as kinderM8Theme;
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/pages/medication/prescribedmedicationData.dart';
import 'package:kinderm8/pages/medication/widgets/date_pick_pager.dart';
import 'package:kinderm8/pages/medication/widgets/floating_actions.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';

class Prescribed extends StatefulWidget {
  final ChildViewModal childData;
  final jwt;

  Prescribed(this.childData, this.jwt);

  @override
  PrescribedState createState() => PrescribedState();
}

class PrescribedState extends State<Prescribed>
    with SingleTickerProviderStateMixin
    implements HomePageContract {
  int apiPaginateSize, len = 0;
  var users, jwt, id, clientId;
  var childId;
  bool isLoading;
  bool load = true;
  HomePagePresenter _presenter;

  NetworkUtil _netUtil;

  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  //To assign day relevant data that coming from parent widget(cannot use generics: MedicationCardData again the way written, have to do more work)
  List medicationCardDataGroup = [];
  List prevMonthMedicationList = [];
  List nextMonthMedicationList = [];
  ScrollController prescribedScrollController;
  List longLiveList = [];

  DateTime pagerDate;

  PrescribedState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  var medicationData;

  var progress = new ProgressBar(
    color: kinderM8Theme.Colors.appcolour,
    containerColor: kinderM8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  //Asanka's edits cannot add top, all are mess

  bool isPreviousButtonActive;
  bool isDateHolderActive;
  bool isNextButtonActive;

  var medData;

  //Keeping a map to display date holders if med available
  Map<int, bool> medicationAvailabilityMap = {};
  bool isPreviouslyPrescribed = false;
  bool isFuturePrescribing = false;

  @override
  void initState() {
    prescribedScrollController = ScrollController()
      ..addListener(_scrollListener);
    pagerDate = DateTime.now();
    _netUtil = new NetworkUtil();
    isPreviousButtonActive = false;
    isNextButtonActive = false;
    isDateHolderActive = true;
    apiPaginateSize = 0;
    fetchMedicationData(apiPaginateSize);
    super.initState();
  }

//  void fetchThirtyMed() async {
//    for (int i = 0; i < 36; i+=5) {
//      print("long length $apiPaginateSize i $i");
//      if (apiPaginateSize == i) {
//        await fetchMedicationData(apiPaginateSize).then((onValue) {
//          apiPaginateSize = i;
//        });
//      }
//    }
//  }

  @override
  void dispose() {
    prescribedScrollController.removeListener(_scrollListener);
    prescribedScrollController.dispose();
    super.dispose();
  }

  void toggleIsPrevActive(bool flag) {
    setState(() {
      isPreviousButtonActive = flag;
    });
  }

  void toggleDateHolderActive(bool flag) {
    setState(() {
      isDateHolderActive = flag;
    });
  }

  void toggleNextBtActive(bool flag) {
    setState(() {
      isNextButtonActive = flag;
    });
  }

  void updateMedicationDataBasedOnAction([int selectedDay = 0]) {
    DateTime pagerDateWithDay = DateTime.now();
    final int today = pagerDateWithDay.day;
    toggleDateHolderActive(true);
    toggleIsPrevActive(false);
    setState(() {
      medicationCardDataGroup = [];
      prevMonthMedicationList = [];
      nextMonthMedicationList = [];
    });
    if (medData != null) {
      if (!(medData is Map)) {
        if (selectedDay != -1) {
          SchedulerBinding.instance.addPostFrameCallback((_) => setState(() {
                medicationCardDataGroup = longLiveList.where((value) {
                  DateTime upcomingDate = DateTime.parse(value["started_on"]);
                  return (upcomingDate.year == pagerDateWithDay.year) &&
                      (upcomingDate.month == pagerDate.month) &&
                      (upcomingDate.day ==
                          ((selectedDay == 0) ? today : selectedDay));
                }).toList();
              }));
        }
      }
    }
  }

  void whenPreviousButtonPressed([String type = "previous"]) {
    toggleDateHolderActive(false);
    toggleIsPrevActive(true);
    setState(() {
      medicationCardDataGroup = [];
      prevMonthMedicationList = [];
      nextMonthMedicationList = [];
    });
    if (medData != null) {
      if (!(medData is Map)) {
        SchedulerBinding.instance.addPostFrameCallback((_) => setState(() {
          prevMonthMedicationList = longLiveList.where((e) {
            if (e["started_on"] != null) {
              DateTime upcomingDateUtc = DateTime.parse(e["started_on"]).toUtc();
              DateTime upcomingDate = DateTime.utc(upcomingDateUtc.year, upcomingDateUtc.month);
              DateTime pagerDateUtc = DateTime.now().toUtc();
              DateTime pagerDateUtcFormated = DateTime.utc(pagerDateUtc.year, pagerDateUtc.month);
//              return (upcomingDate.year <= pagerDate.year && upcomingDate.month < pagerDate.month);
              return upcomingDate.compareTo(pagerDateUtcFormated) < 0;
            } else return false;
          }).toList();
        }));
      }
    }
  }

  void whenNextButtonPressed([String type = "next"]) {
    toggleDateHolderActive(false);
    toggleIsPrevActive(false);
    toggleNextBtActive(true);
    setState(() {
      medicationCardDataGroup = [];
      prevMonthMedicationList = [];
      nextMonthMedicationList = [];
    });
    if (medData != null) {
      if (!(medData is Map)) {
        SchedulerBinding.instance.addPostFrameCallback((_) => setState(() {
          nextMonthMedicationList = longLiveList.where((e) {
            if (e["started_on"] != null) {
              DateTime upcomingDate =
              DateTime.parse(e["started_on"]);
//              DateTime upcomingDate =
//              DateTime.utc(upcomingDateUtc.year, upcomingDateUtc.month);
              return (upcomingDate.compareTo(pagerDate) > 0);
            } else return false;
          }).toList();
        }));
      }
    }
  }

  void saveMedicationAvailabilityMap() {
    //for to display day holder
    if (medData != null) {
      if (!(medData is Map)) {
        int index = 1;
        medData.forEach((item) {
          if (item["started_on"] != null) {
            DateTime upcomingDefault = DateTime.parse(item["started_on"]);
//            DateTime upcomingDateUtc =
//            DateTime.parse(item["started_on"]).toUtc();
//            DateTime upcomingDate =
//            DateTime.utc(upcomingDateUtc.year, upcomingDateUtc.month);
            if (pagerDate.month == upcomingDefault.month) {
              setState(() {
                medicationAvailabilityMap[upcomingDefault.day] = true;
              });
            }

            if (upcomingDefault.compareTo(pagerDate) < 0) {
              setState(() {
                isPreviouslyPrescribed = true;
              });
            }

            if (upcomingDefault.compareTo(pagerDate) > 0) {
              setState(() {
                isFuturePrescribing = true;
              });
            }
          } else {
            setState(() {
              medicationAvailabilityMap[index] = false;
            });
          }
          index++;
        });
      }
    }
  }

  Future<String> fetchMedicationData(int paginateLength) async {
    childId = widget.childData.id;
    String _medicationUrl =
        'http://13.55.4.100:7070/v2.1.1/medication/$childId?step=$paginateLength&clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};
    _netUtil.get(_medicationUrl, headers: headers).then((response) {
      try {
        medData = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }

      saveMedicationAvailabilityMap();

      if (response.statusCode == 200) {
        longLiveList.addAll(medData);
        isLoading = false;
        if (paginateLength == 0) {
          setState(() {
            load = false;
            len = longLiveList.length;
          });
//          medData.forEach((item) {
//            if (item["started_on"] != null) {
//              DateTime upcomingDateUtc =
//                  DateTime.parse(item["started_on"]).toUtc();
//              DateTime upcomingDate =
//                  DateTime.utc(upcomingDateUtc.year, upcomingDateUtc.month);
//              if (upcomingDate.compareTo(pagerDate) == 0) {
//                setState(() {
//                  medicationCardDataGroup.add(item);
//                });
//              }
//
//              if (upcomingDate.compareTo(pagerDate) < 0) {
//                setState(() {
//                  prevMonthMedicationList.add(item);
//                });
//              }
//
//              if (upcomingDate.compareTo(pagerDate) > 0) {
//                setState(() {
//                  nextMonthMedicationList.add(longLiveList);
//                });
//              }
//            }
//          });
          setState(() {
            apiPaginateSize = longLiveList.length;
          });
        } else {
          setState(() {
            load = false;
            len = medData.length;
          });

//          medData.forEach((item) {
//            if (item["started_on"] != null) {
//              DateTime upcomingDateUtc =
//                  DateTime.parse(item["started_on"]).toUtc();
//              DateTime upcomingDate =
//                  DateTime.utc(upcomingDateUtc.year, upcomingDateUtc.month);
//              if (upcomingDate.compareTo(pagerDate) == 0) {
//                setState(() {
//                  medicationCardDataGroup.add(item);
//                });
//              }
//
//              if (upcomingDate.compareTo(pagerDate) < 0) {
//                setState(() {
//                  prevMonthMedicationList.add(item);
//                });
//              }
//
//              if (upcomingDate.compareTo(pagerDate) > 0) {
//                setState(() {
//                  nextMonthMedicationList.add(item);
//                });
//              }
//            }
//          });
        }
        setState(() {
          apiPaginateSize = longLiveList.length;
        });
      } else if (response.statusCode == 500 &&
          medData["errorType"] == 'ExpiredJwtException') {
        setState(() {
          isLoading = true;
        });
        getRefreshToken();
      } else {
        print("error");
      }
    });

    setState(() {
      isLoading = false;
    });
  }

  getRefreshToken() {
    print("refreshing Token..");
    String _refreshTokenUrl =
        'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netUtil = new NetworkUtil();

    _netUtil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      if (apiPaginateSize != null) {
        fetchMedicationData(apiPaginateSize);
      } else {
        fetchMedicationData(apiPaginateSize);
      }
    });
  }

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text('Please wait..',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: kinderM8Theme.Colors.app_white)),
            SizedBox(width: 6.0),
            Container(
              width: 20.0,
              height: 20.0,
              child: CircularProgressIndicator(
                strokeWidth: 2.0,
                backgroundColor: Colors.white24,
                valueColor: new AlwaysStoppedAnimation(Colors.white),
              ),
            ),
          ],
        ),
        color: kinderM8Theme.Colors.appcolour.withOpacity(0.85),
//        splashColor: Colors.red,
        elevation: 4.0,
        onPressed: () {});
  }

  _scrollListener() {
    if (prescribedScrollController.position.extentAfter <= 0 &&
        isLoading == false) {
      fetchMedicationData(apiPaginateSize);
    }
  }

  Widget buildMedicationList(List data) {
    return data.length > 0
        ? ListView.builder(
            controller: prescribedScrollController,
            itemCount: data.length,
            itemBuilder: (context, index) {
              if (data.length > 3 && data.length == index + 1) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    PrescribedMediData(
                        widget.childData, data[index], jwt, users,
                        medicationDataGroupByDay: data,
                        isPrevMonths: isPreviousButtonActive),
                    _buildCounterButton(),
                  ],
                );
              } else if (data.length > index) {
                return PrescribedMediData(
                    widget.childData, data[index], jwt, users,
                    medicationDataGroupByDay: data,
                    isPrevMonths: isPreviousButtonActive);
              }
            },
          )
        : Container(
            height: MediaQuery.of(context).size.height / 1.5,
            padding: EdgeInsets.all(8.0),
            child: Text(
              "No medications are available in this day",
              textAlign: TextAlign.center,
            ),
          );
  }

  Widget buildDataChild() {
    if (isPreviousButtonActive && prevMonthMedicationList.length > 0) {
      return buildMedicationList(prevMonthMedicationList);
    } else if (isNextButtonActive && nextMonthMedicationList.length > 0) {
      return buildMedicationList(nextMonthMedicationList);
    } else if (isDateHolderActive && medicationCardDataGroup.length > 0) {
      return buildMedicationList(medicationCardDataGroup);
    } else {
      return Container(
        height: MediaQuery.of(context).size.height / 1.5,
        padding: EdgeInsets.all(8.0),
        child: Text(
          "No medications are available in this day",
          textAlign: TextAlign.center,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
//    PrescribedMediData(
//        widget.childData, this.medicationCardDataGroup[0], jwt, users, medicationDataGroupByDay: medicationCardDataGroup)
    return Scaffold(
      key: scaffoldKey,
      body: load
        ? progress
        : new RefreshIndicator(
            key: refreshIndicatorKey,
            onRefresh: handleRefresh,
            child: DatePickPager(
              child: buildDataChild(),
              updateMedicationDataByDay: updateMedicationDataBasedOnAction,
              whenPrevBtPress: whenPreviousButtonPressed,
              whenNxtBtPress: whenNextButtonPressed,
              isDateHolderActive: isDateHolderActive,
              isPrevActive: isPreviousButtonActive,
              isNextActive: isNextButtonActive,
              isPreviouslyPrescribed: isPreviouslyPrescribed,
              isNextMonthPrescribing: isFuturePrescribing,
              dayAvailabilityMap: medicationAvailabilityMap,
              toggleIsPrevActive: toggleIsPrevActive,
              toggleIsDateHolderActive: toggleDateHolderActive,
              toggleIsNextActive: toggleNextBtActive,
            ),
        ), //Sending whole data may not be efficient but I cant reproduced this bcuz of the way have done everything
      floatingActionButton:
          MyFloatingMenuPage(childData: widget.childData, jwt: widget.jwt),
    );
  }

  Future<String> handleRefresh() async {
    setState(() {
      longLiveList = [];
      medicationCardDataGroup = [];
      load = true;
    });
    return await fetchMedicationData(0);
  }

  @override
  void onDisplayUserInfo(User user) {
    var appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];

      jwt = widget.jwt.toString();
      print("******$jwt");
      users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }
  }

  @override
  void onErrorUserInfo() {}

  @override
  void onLogoutUser() {}
}
