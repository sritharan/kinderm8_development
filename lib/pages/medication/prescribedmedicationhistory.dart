import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/medication/medicationlist.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:kinderm8/Theme.dart' as Theme;

class ViewPrescribedHistory extends StatefulWidget {
  final singlemedicationData;
  ViewPrescribedHistory(this.singlemedicationData);
  @override
  PrescribedHistoryState createState() =>
      PrescribedHistoryState(singlemedicationData);
}

class PrescribedHistoryState extends State<ViewPrescribedHistory> {
  final singlemedicationData;

  var report;
  List<Widget> history = [];
  PrescribedHistoryState(this.singlemedicationData);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    report = singlemedicationData['repeat_medication_report'];
//    createReport();
  }

//  createReport() {
//    if (report.length > 0) {
//      for (int i = 0; i < report.length; i++) {
//        history.add(Column(
//          children: <Widget>[
//            Container(
//              child: Column(
//                children: <Widget>[
//                  Text(
//                    "Date given",
//                    style: TextStyle(
//                        fontSize: 14.0,
//                        color: Colors.black,
//                        fontWeight: FontWeight.w500),
//                  ),
//                  Container(height: 5.0),
//                  Container(
//                    child: Text(
//                      "${report[i]['given_date']}",
//                      style: TextStyle(
//                          fontSize: 14.0,
//                          color: Colors.black,
//                          fontWeight: FontWeight.w500),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            Container(
//              height: 5.0,
//            ),
//            Container(
//              child: Column(
//                children: <Widget>[
//                  Text(
//                    "Time given",
//                    style: TextStyle(
//                        fontSize: 14.0,
//                        color: Colors.black,
//                        fontWeight: FontWeight.w500),
//                  ),
//                  Container(height: 5.0),
//                  Container(
//                    child: Text(
//                      "${report[i]['given_time']}",
//                      style: TextStyle(
//                          fontSize: 14.0,
//                          color: Colors.black,
//                          fontWeight: FontWeight.w500),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            Container(
//              height: 5.0,
//            ),
//            Container(
//              child: Column(
//                children: <Widget>[
//                  Text(
//                    "Dosage",
//                    style: TextStyle(
//                        fontSize: 14.0,
//                        color: Colors.black,
//                        fontWeight: FontWeight.w500),
//                  ),
//                  Container(height: 5.0),
//                  Container(
//                    child: Text(
//                      "${report[i]['dosage']}",
//                      style: TextStyle(
//                          fontSize: 14.0,
//                          color: Colors.black,
//                          fontWeight: FontWeight.w500),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            Container(
//              child: Column(
//                children: <Widget>[
//                  Text(
//                    "Medicine given by",
//                    style: TextStyle(
//                        fontSize: 14.0,
//                        color: Colors.black,
//                        fontWeight: FontWeight.w500),
//                  ),
//                  Container(height: 5.0),
//                  Container(
//                    child: Text(
//                      "${report[i]['given_user']}",
//                      style: TextStyle(
//                          fontSize: 14.0,
//                          color: Colors.black,
//                          fontWeight: FontWeight.w500),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//
//
//
//          ],
//        ));
//      }
//    }
//  }


  ListView _buildList(context) {
    return ListView.builder(
      // Must have an item count equal to the number of items!
      itemCount: singlemedicationData['repeat_medication'].length,
      // A callback that will return a widget.
      itemBuilder: (context, int) {
//        return new Text(singlemedicationData['repeat_medication'][int].toString());
        // In our case, a DogCard for each doggo.
        return Card(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child:  new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text("${singlemedicationData["candelete"]}"),
                Padding(
                  padding: const EdgeInsets.only(top:2.0,bottom: 2.0),
                  child: new Text("Medication Date1:", style: TextStyle(fontSize: 14.0,color: Theme.Colors.app_dark[400],
                    fontWeight: FontWeight.w600,),textAlign: TextAlign.left),
                ),
                Container(
                  padding: EdgeInsets.only(top: 2.0),
                  child: Text(
                    "${singlemedicationData["repeat_medication"][int]["main_date"]}",
                    style: TextStyle(
                        fontSize: 14.0, color: Theme.Colors.app_dark),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(top:2.0,bottom: 2.0),
                  child: new Text("Last administered: ", style: TextStyle(fontSize: 14.0,color: Theme.Colors.app_dark[400],
                    fontWeight: FontWeight.w600,),textAlign: TextAlign.left),
                ),
                Container(
                  padding: EdgeInsets.only(top: 2.0),
                  child: Text(
                    "${singlemedicationData["repeat_medication"][int]["last_administered_datetime"]}",
                    style: TextStyle(
                        fontSize: 14.0, color: Theme.Colors.app_dark),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(top:2.0,bottom: 2.0),
                  child: new Text("Dosage of medication:", style: TextStyle(fontSize: 14.0,color: Theme.Colors.app_dark[400],
                    fontWeight: FontWeight.w600,),textAlign: TextAlign.left),
                ),
                Container(
                  padding: EdgeInsets.only(top: 2.0),
                  child: Text(
                    "${singlemedicationData["repeat_medication"][int]["dosage"]}",
                    style: TextStyle(
                        fontSize: 14.0, color: Theme.Colors.app_dark),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(top:2.0,bottom: 2.0),
                  child: new Text("Method to be administered:", style: TextStyle(fontSize: 14.0,color: Theme.Colors.app_dark[400],
                    fontWeight: FontWeight.w600,),textAlign: TextAlign.left),
                ),
                Container(
                  padding: EdgeInsets.only(top: 2.0),
                  child: Text(
                    "${singlemedicationData["repeat_medication"][int]["dosage"]}",
                    style: TextStyle(
                        fontSize: 14.0, color: Theme.Colors.app_dark),
                  ),
                ),

                new Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top:2.0,bottom: 2.0),
                      child: new Text("Acknowledged:", style: TextStyle(fontSize: 14.0,color: Theme.Colors.app_dark[400],
                        fontWeight: FontWeight.w600,),textAlign: TextAlign.left),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 2.0),
                      child: Text(
                        "${singlemedicationData["repeat_medication"][int]["medication_status"]}",
                        style: TextStyle(
                            fontSize: 14.0, color: Theme.Colors.darkGrey),
                      ),
                    ),
                  ],
                ),

                singlemedicationData["medication_status"].toString() == 'Acknowledged'?
                new Row(
                  children: <Widget>[
//                            Padding(
//                              padding: const EdgeInsets.only(top:2.0,bottom: 2.0),
//                              child: new Text("Status", style: TextStyle(fontSize: 14.0,color: Theme.Colors.app_dark[400],
//                                fontWeight: FontWeight.w600,),textAlign: TextAlign.left),
//                            ),
                    Chip(
                      label: new Text("${singlemedicationData["medication_status"].toString()}"),
                      labelPadding: EdgeInsets.all(2.0),
                      backgroundColor: Theme.Colors.medication_acknowledged,
                    )
                  ],
                ): Container(height: 0.0,),

                singlemedicationData["medication_status"] == 'Completed'?
                new Row(
                  children: <Widget>[
//                            Padding(
//                              padding: const EdgeInsets.only(top:2.0,bottom: 2.0),
//                              child: new Text("Status", style: TextStyle(fontSize: 14.0,color: Theme.Colors.app_dark[400],
//                                fontWeight: FontWeight.w600,),textAlign: TextAlign.left),
//                            ),
                    Chip(
                      label: new Text("${singlemedicationData["medication_status"].toString()}"),
                      labelPadding: EdgeInsets.all(2.0),
                      backgroundColor: Theme.Colors.medication_completed,
                    )
                  ],
                ): Container(height: 0.0,),

                singlemedicationData["medication_status"].toString() == 'Pending'?
                new Row(
                  children: <Widget>[
//                            Padding(
//                              padding: const EdgeInsets.only(top:2.0,bottom: 2.0),
//                              child: new Text("Status", style: TextStyle(fontSize: 14.0,color: Theme.Colors.app_dark[400],
//                                fontWeight: FontWeight.w600,),textAlign: TextAlign.left),
//                            ),
                    Chip(
                      label: new Text("${singlemedicationData["medication_status"].toString()}"),
                      labelPadding: EdgeInsets.all(2.0),
                      backgroundColor: Theme.Colors.medication_pending,
                    )
                  ],
                ): Container(height: 0.0,),

              ],
            ),
          ),
        );
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Prescribed Medication History"),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Container(
                height: 20.0,
              ),
//

              ///
              /// have to validate

              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(),

                  Padding(
                    padding: const EdgeInsets.only(left:5.0,top:5.0,bottom: 5.0),
                    child: Text("Name of medication", style: TextStyle(fontSize: 16.0),),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      width: screenSize.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                            child: Text("${singlemedicationData['medication_name']}"),
                          ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
//                      child: Row(
//                        children: <Widget>[
//                          Text("Use my location")
//                        ],
//                      ),
//                    ),
                        ],
                      ), color: Colors.white,),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(left:5.0,top:5.0,bottom: 5.0),
                    child: Text("Medication in original packaging", style: TextStyle(fontSize: 16.0),),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      width: screenSize.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                            child: Text("${singlemedicationData["medic_original_pack"] == 0 ? 'No' : 'Yes'}"),
                          ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
//                      child: Row(
//                        children: <Widget>[
//                          Text("Use my location")
//                        ],
//                      ),
//                    ),
                        ],
                      ), color: Colors.white,),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(left:5.0,top:5.0,bottom: 5.0),
                    child: Text("Medical practitioner/chemist :", style: TextStyle(fontSize: 16.0),),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      width: screenSize.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                            child: Text("${singlemedicationData['medical']}"),
                          ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
//                      child: Row(
//                        children: <Widget>[
//                          Text("Use my location")
//                        ],
//                      ),
//                    ),
                        ],
                      ), color: Colors.white,),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(left:5.0,top:5.0,bottom: 5.0),
                    child: Text("Date prescribed:", style: TextStyle(fontSize: 16.0),),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      width: screenSize.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                            child: Text("${singlemedicationData['date_prescribed']}"),
                          ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
//                      child: Row(
//                        children: <Widget>[
//                          Text("Use my location")
//                        ],
//                      ),
//                    ),
                        ],
                      ), color: Colors.white,),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(left:5.0,top:5.0,bottom: 5.0),
                    child: Text("Expiry Date:", style: TextStyle(fontSize: 16.0),),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      width: screenSize.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                            child: Text("${singlemedicationData['expiry_date']}"),
                          ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
//                      child: Row(
//                        children: <Widget>[
//                          Text("Use my location")
//                        ],
//                      ),
//                    ),
                        ],
                      ), color: Colors.white,),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(left:5.0,top:5.0,bottom: 5.0),
                    child: Text("Storage :", style: TextStyle(fontSize: 16.0),),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      width: screenSize.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                            child: Text("${singlemedicationData['storage']}"),
                          ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
//                      child: Row(
//                        children: <Widget>[
//                          Text("Use my location")
//                        ],
//                      ),
//                    ),
                        ],
                      ), color: Colors.white,),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(left:5.0,top:5.0,bottom: 5.0),
                    child: Text("Reason for medication:", style: TextStyle(fontSize: 16.0),),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      width: screenSize.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                            child: Text("${singlemedicationData['reason']}"),
                          ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
//                      child: Row(
//                        children: <Widget>[
//                          Text("Use my location")
//                        ],
//                      ),
//                    ),
                        ],
                      ), color: Colors.white,),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(left:5.0,top:5.0,bottom: 5.0),
                    child: Text("Instructions:", style: TextStyle(fontSize: 16.0),),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      width: screenSize.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                            child: Text("${singlemedicationData['instructions']}"),
                          ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
//                      child: Row(
//                        children: <Widget>[
//                          Text("Use my location")
//                        ],
//                      ),
//                    ),
                        ],
                      ), color: Colors.white,),
                  ),

//                        new Text(singlemedicationData["repeat_medication"].toString()),
//                  Padding(
//                    padding: const EdgeInsets.all(8.0),
//                    child: Card(
//                      color: Colors.green,
//                      child: Padding(
//                        padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal:5.0),
//                        child: Row(
//                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                          children: <Widget>[
//                            Expanded(child: Icon(Icons.local_hospital, size: 60.0, color: Colors.white,), flex: 1,),
//                            Expanded(child: Text("Medication History", style: TextStyle(fontSize: 22.0, color: Colors.white),), flex: 3,),
//                          ],
//                        ),
//                      ),
//                    ),
//                  ),
                  singlemedicationData['repeat_medication'] != null ?
                  Container(
                      height: 400.0,child: _buildList(singlemedicationData['repeat_medication'])
                  ):new Container(),
//                  new Text(singlemedicationData['repeat_medication_report'].toString()),
//                  Container(
//                    height: 400.0,
//                    child: ListView.builder(
//                      itemBuilder: (context, position) {
//                        return ListView.builder(
//                          // Must have an item count equal to the number of items!
//                          itemCount: singlemedicationData['repeat_medication'].length,
//                          // A callback that will return a widget.
//                          itemBuilder: (context, i) {
//                            final singleData = this.singlemedicationData["repeat_medication"][i];
//                            // In our case, a DogCard for each doggo.
//                            return Padding(
//                              padding: const EdgeInsets.all(16.0),
//                              child: Text(singlemedicationData["repeat_medication"].toString(), style: TextStyle(color: Colors.black,fontSize: 20.0),),
//                            );
//                          },
//                        );
//                      },
//                    ),
//                  ),
                ],
              )

              ///
              ///
            ],
          ),
        ),
      ),
    );
  }
}
