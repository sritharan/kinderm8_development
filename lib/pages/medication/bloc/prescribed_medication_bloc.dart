import 'dart:async';
import 'package:kinderm8/pages/medication/modals/prescribed_medication.dart';
import 'package:rxdart/rxdart.dart';

class PrescribedBloc {
  Stream<List<PrescribedMedication>> get prescribedMedications => _articlesSubject.stream;

  final _articlesSubject = BehaviorSubject<List<PrescribedMedication>>();
}