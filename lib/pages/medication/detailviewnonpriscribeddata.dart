import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:kinderm8/pages/home/data/config.dart';

class ViewNonPrescribed extends StatefulWidget {
  final singlemedicationData;
  ViewNonPrescribed(this.singlemedicationData);
  @override
  ViewNonPrescribedState createState() =>
      ViewNonPrescribedState(singlemedicationData);
}

class ViewNonPrescribedState extends State<ViewNonPrescribed> {
  final singlemedicationData;

  var report;
  List<Widget> history = [];
  ViewNonPrescribedState(this.singlemedicationData);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    report = singlemedicationData['report'];
  }

  @override
  Widget build(BuildContext context) {
    var medicationName =
        html2md.convert(singlemedicationData["medication_to_administer"]);

    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("View Non-Prescribed Medication", style: TextStyle(fontSize: 18.0),),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Container(
                height: 5.0,
              ),
              ///
              /// have to validate

              Container(
//                decoration: BoxDecoration(
//                    borderRadius: BorderRadius.circular(20.0),
//                    border: Border.all(
//                      color: Colors.deepPurple,
//                      style: BorderStyle.solid,
//                    )),
                padding: EdgeInsets.only(top: 2.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 5.0, top: 5.0, bottom: 0.0),
                      child: Text(
                        "${labelsConfig["medsToApplyAdministeredLabel"]} :",
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 0.0, vertical: 16.0),
                              child: Html(data: medicationName),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 5.0, top: 5.0, bottom: 0.0),
                      child: Text(
                        "${labelsConfig["dateAuthorizedLabel"]} :",
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 0.0, vertical: 8.0),
                              child: Text(
                                  "${singlemedicationData['authorized_date']}"),
                            ),
                          ],
                        ),
                      ),
                    ),
                    singlemedicationData['circumstances'] != null
                        ? Padding(
                            padding: const EdgeInsets.only(
                                left: 5.0, top: 5.0, bottom: 0.0),
                            child: Text(
                              "${labelsConfig["giveUnderCircumstanceLabel"]} :",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.w500),
                            ),
                          )
                        : Container(),
                    singlemedicationData['circumstances'] != null
                        ? Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 0.0, vertical: 8.0),
                                    child: Text(
                                        "${singlemedicationData['circumstances']}"),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),

              ///
              ///

              report.length > 0
                  ? Column(
                      children: <Widget>[
//                        Card(
//                          color: Colors.deepPurple.shade50,
//                          child: Padding(
//                            padding: const EdgeInsets.symmetric(
//                                vertical: 1.0, horizontal: 5.0),
//                            child: Row(
//                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                              children: <Widget>[
//                                Expanded(
//                                  child: Icon(
//                                    Icons.local_hospital,
//                                    size: 50.0,
//                                    color: Colors.black,
//                                  ),
//                                  flex: 1,
//                                ),
//                                Expanded(
//                                  child: Text(
//                                    "Medication History",
//                                    style: TextStyle(
//                                        fontSize: 20.0, color: Colors.black),
//                                  ),
//                                  flex: 3,
//                                ),
//                              ],
//                            ),
//                          ),
//                        ),

                        report.length > 0
                            ? Column(
                          children: <Widget>[
                            Card(
                              color: Colors.deepPurple.shade50,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 1.0, horizontal: 5.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Icon(
                                      Icons.local_hospital,
                                      size: 50.0,
                                      color: Colors.black,
                                    ),
                                    Expanded(
                                      child: Text(
                                        labelsConfig["medicationHistoryLabel"],
                                        style: TextStyle(
                                            fontSize: 20.0, color: Colors.black),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                    child: ListView.builder(
                                        shrinkWrap: true,
                                        physics: ClampingScrollPhysics(),
                                        itemCount: report.length,
                                        itemBuilder: (context, i) {
                                          final UriData data =
                                              Uri.parse(report[i]['signature'])
                                                  .data;
                                          return Container(margin: EdgeInsets.only(bottom: 3.0),
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                BorderRadius.circular(10.0),
                                                border: Border.all(
                                                  color: Colors.deepPurple,
                                                  style: BorderStyle.solid,
                                                )),
                                            padding: EdgeInsets.only(bottom: 3.0),
                                            child: Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Padding(
                                                  padding: const EdgeInsets.only(
                                                      left: 5.0,
                                                      top: 5.0,
                                                      bottom: 3.0),
                                                  child: Text(
                                                    labelsConfig["dateAuthorizedLabel"],
                                                    style: TextStyle(
                                                        fontSize: 16.0,
                                                        fontWeight:
                                                        FontWeight.w500),
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.all(5.0),
                                                  child: Container(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: const EdgeInsets
                                                              .symmetric(
                                                              horizontal: 1.0,
                                                              vertical: 8.0),
                                                          child: Text(
                                                              "${report[i]['given_date']}"),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.only(
                                                      left: 5.0,
                                                      top: 5.0,
                                                      bottom: 3.0),
                                                  child: Text(
                                                    "Time given",
                                                    style: TextStyle(
                                                        fontSize: 16.0,
                                                        fontWeight:
                                                        FontWeight.w500),
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.all(3.0),
                                                  child: Container(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: const EdgeInsets
                                                              .symmetric(
                                                              horizontal: 2.0,
                                                              vertical: 8.0),
                                                          child: Text(
                                                              "${report[i]['given_time']}"),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.only(
                                                      left: 5.0,
                                                      top: 5.0,
                                                      bottom: 3.0),
                                                  child: Text(
                                                    labelsConfig["dosageLabel"],
                                                    style: TextStyle(
                                                        fontSize: 16.0,
                                                        fontWeight:
                                                        FontWeight.w500),
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.all(3.0),
                                                  child: Container(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: const EdgeInsets
                                                              .symmetric(
                                                              horizontal: 1.0,
                                                              vertical: 8.0),
                                                          child: Text(
                                                              "${report[i]['dosage']}"),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.only(
                                                      left: 5.0,
                                                      top: 5.0,
                                                      bottom: 3.0),
                                                  child: Text(
                                                    "Medicine given by",
                                                    style: TextStyle(
                                                        fontSize: 16.0,
                                                        fontWeight:
                                                        FontWeight.w500),
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.all(5.0),
                                                  child: Container(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: const EdgeInsets
                                                              .symmetric(
                                                              horizontal: 1.0,
                                                              vertical: 8.0),
                                                          child: Text(
                                                              "${report[i]['given_user']}"),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.only(
                                                      left: 5.0,
                                                      top: 5.0,
                                                      bottom: 3.0),
                                                  child: Text(
                                                    "${labelsConfig["staffSignatureLabel"]}:",//TODO: need clarification on type of signature
                                                    style: TextStyle(
                                                        fontSize: 16.0,
                                                        fontWeight:
                                                        FontWeight.w500),
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.all(5.0),
                                                  child: Container(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                      children: <Widget>[
                                                        Padding(
                                                          padding: const EdgeInsets
                                                              .symmetric(
                                                              horizontal: 1.0,
                                                              vertical: 8.0),
                                                          child: Container(decoration: BoxDecoration(border: Border.all(color: Colors.deepPurple,style: BorderStyle.solid) ),
                                                            child: (data != null) ? new Image.memory(
                                                                data.contentAsBytes()) : SizedBox(),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          );
                                        }))
                              ],
                            )
                          ],
                        )
                            : Container(),
                      ],
                    )
                  : Container(),

              ///
              ///
            ],
          ),
        ),
      ),
    );
  }
}
