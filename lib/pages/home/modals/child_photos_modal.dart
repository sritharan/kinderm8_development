class ChildPhotoModal {
  final String fileUrl;
  final String date;
  final String caption;

  ChildPhotoModal({
    this.fileUrl,
    this.date,
    this.caption,
  });

  ChildPhotoModal.fromJson(Map<String, dynamic> map)
      : fileUrl = map["fileurl"],
        date = map["date"],
        caption = map["caption"];
}
