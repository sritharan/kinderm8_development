import 'package:kinderm8/pages/home/modals/child_summary_modal.dart';

class SummaryViewModal {
  final String assetPath;
  final String type;
  final String label;
  final String value;
  ChildSummaryModal childSummaryModal;
  final Function function;

  SummaryViewModal({
    this.assetPath,
    this.type,
    this.label,
    this.value,
    this.childSummaryModal,
    this.function,
  });
}
