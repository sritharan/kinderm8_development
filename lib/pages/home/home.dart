import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:kinderm8/auth.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/calendar/events.dart';
import 'package:kinderm8/pages/childrenmenu.dart';
import 'package:kinderm8/pages/home/dailysummary.dart';
import 'package:kinderm8/pages/home/dailysummarylist.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kinderm8/pages/commondrawer.dart';

import 'package:kinderm8/pages/newsfeed/newsfeedlist.dart';
import 'package:kinderm8/pages/calendar/eventslist.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';

class HomePage extends StatefulWidget {

  final String Selectedchildvalue;
  HomePage({Key key, this.Selectedchildvalue}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin
    implements HomePageContract, AuthStateListener {
  var deviceSize;
  HomePagePresenter _presenter;
  String _homeText, email, password, client_id, center;
  var k, appuser, jwt, id, clientId;
  BuildContext _ctx;
  bool isLoading;
  int selectedchildid;
  List dailysummary_data;
  bool load = true;
  TabController controller;

  HomePageState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  logout() {
    _presenter.getUserLogout();
  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Colors.white,
    containerColor: Theme.Colors.progresscontent,
    borderRadius: 5.0,
    text: 'Loading...',
  );


  @override
  void initState() {
    controller = new TabController(length: 4, vsync: this);
//    fetchDailysummaryData(2);
    super.initState();
  }

  void _openchildrenpopup() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return CommonDrawer();
        },
        fullscreenDialog: true));
  }

  // get Dailysummary data
  Future<DailysummaryUI> fetchDailysummaryData(int childid) async {
    print(childid);
    selectedchildid = childid;
    ///data from GET method
    print("dailysummary data fetched $childid");

    String _eventsUrl =
        'http://13.55.4.100:7070/v4.4.0/getchildsummary/$childid&clientid=$client_id ';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_eventsUrl, headers: headers).then((response) {
      var dailysummaryData;
      try {
        dailysummaryData = json.decode(response.body);
        print(dailysummaryData.length);
        print('res get ${response.body}');
        print('events Data $dailysummaryData');
      } on FormatException catch (e) {
        print("That string didn't look like Json.");
      } on NoSuchMethodError catch (e) {
        print('That string was null!');
      }


      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        print(isLoading);
        isLoading = false;
        print(isLoading);
//        if (childid != null) {
        setState(() {
          load = false;
          this.dailysummary_data = dailysummaryData;
          print('<<<<<<<<<< dailysummaryData >>>>>>>>>>>>> $dailysummaryData');
        });
//        }
//        else {
//          setState(() {
//            load = false;
//            data.addAll(eventsData);
//          });
//        }
//        k = data.length;
      } else if (response.statusCode == 500 &&
          dailysummaryData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      } else {
        fetchDailysummaryData(selectedchildid);
      }
    });
    return null;
  }
  // Refresh JWT
  getRefreshToken() {
    String _refreshTokenUrl =
        'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$id&clientid=$client_id';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);

      } on FormatException catch (e) {
        print("That string didn't look like Json.");
      } on NoSuchMethodError catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      if (selectedchildid != null) {
        fetchDailysummaryData(selectedchildid);
      } else {
        fetchDailysummaryData(selectedchildid);
      }
    });
  }
  // end of Dailysummary

  // Pop to ask do you wish to exit
  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          child: new AlertDialog(
            title: new Text('Are you sure?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => exit(0),
//                    Navigator.pushReplacementNamed(context, "/home"),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    deviceSize = MediaQuery.of(context).size;
    return new Scaffold(
//      appBar: new AppBar(
//        title: new Text("Home"),
//        centerTitle: true,
//      ),
//      drawer: CommonDrawer(),
      body: WillPopScope(
        onWillPop: _onWillPop,
        child: new Dailysummary(),
//        child: new TabBarView(
//          // Add tabs as widgets
//          children: <Widget>[
////            new Dailysummary(),
////            new NewsfeedList(),
////            new EventsList(),
////            new EventsList()
//          ],
//          // set the controller
//          controller: controller,
//        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.menu),
        onPressed: () {
          _openchildrenpopup();
//          ChildrenDrawer();
//          new Dailysummary();
//          showModalBottomSheet<Null>(
//            context: context,
//            builder: (BuildContext context) => const _DemoDrawer(),
//          );
        },
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 4.0,
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new FlatButton(
              onPressed: () {
//                Navigator.pushNamed(context, "/home");
                Navigator.push(context, new MaterialPageRoute(
                    builder: (context) =>
                    new Dailysummary())
                );
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.format_list_bulleted),
//                  new Text("Dailysummary")
                ],
              ),
            ),
            new FlatButton(
              onPressed: () {
//                Navigator.pushNamed(context, "/newsfeed");
                Navigator.push(context, new MaterialPageRoute(
                    builder: (context) =>
                    new NewsFeedList())
                );
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.chrome_reader_mode),
//                  new Text("Newsfeed")
                ],
              ),
            ),
            new FlatButton(
              onPressed: () {
                EventsData();
                Navigator.pushNamed(context, "/calendar");
                Navigator.push(context, new MaterialPageRoute(
                    builder: (context) =>
                    new EventsData())
                );
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.date_range),
//                  new Text("Calendar")
                ],
              ),
            ),
            new FlatButton(
              onPressed: () {
//                CommonDrawer();
//                Navigator.push(context, new MaterialPageRoute(
//                    builder: (context) =>
//                    new EventsList())
//                );
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.free_breakfast),
//                  new Text("Family ..")
                ],
              ),
            ),
//            IconButton(
//              icon: Icon(Icons.format_list_bulleted),
//              onPressed: () {
//                Dailysummary();
//              },
//            ),
//            IconButton(
//                icon: Icon(Icons.chrome_reader_mode),
//                onPressed: () {
//                  NewsfeedList();
//                },
//            ),
//            IconButton(
//              icon: Icon(Icons.date_range),
//              onPressed: () {
//                EventsList();
////                showModalBottomSheet<Null>(
////                  context: context,
////                  builder: (BuildContext context) => const _DemoDrawer(),
////                );
//              },
//            ),
//            IconButton(
//              icon: Icon(Icons.menu),
//              onPressed: () {
//                CommonDrawer();
////                showModalBottomSheet<Null>(
////                  context: context,
////                  builder: (BuildContext context) => CommonDrawer(),
////                );
//              },
//            ),
          ],
        ),
      ),
//      bottomNavigationBar: new Material(
//        color: Colors.blueGrey.shade300,
//        child: new TabBar(
//          controller: controller,
//          tabs: <Widget>[
//            new Tab(
//              child: Container(
//                margin: EdgeInsets.all(5.0),
//                child: Column(
//                  children: <Widget>[
//                    new Image(
//                      image: AssetImage("assets/iconsetpng/newspaper2.png"),
//                      height: 22.0,
//                      width: 22.0,
//                    ),
//                    new Text(
//                      "Daily Summary",
//                      style: TextStyle(color: Colors.white,fontSize: 10.00),
//                    ),
//                  ],
//                ),
////                padding: EdgeInsets.only(top: 2.0),
//              ),
//            ),
//            new Tab(
//              child: Container(
//                margin: EdgeInsets.all(5.0),
//                child: Column(
//                  children: <Widget>[
//                    new Image(
//                      image: AssetImage("assets/iconsetpng/computer_nsfeed.png"),
//                      height: 20.0,
//                      width: 22.0,
//                    ),
//                    new Text(
//                      "Children",
//                      style: TextStyle(color: Colors.white,fontSize: 14.00),
//                    ),
//                  ],
//                ),
//                padding: EdgeInsets.symmetric(),
//              ),
//            ),
//            new Tab(
//              child: Container(
//                margin: EdgeInsets.all(5.0),
//                child: Column(
//                  children: <Widget>[
//                    new Image(
//                      image: AssetImage("assets/iconsetpng/newspaper3.png"),
//                      height: 20.0,
//                      width: 22.0,
//                    ),
//                    new Text(
//                      "Events",
//                      style: TextStyle(color: Colors.white,fontSize: 14.00),
//                    ),
//                  ],
//                ),
//                padding: EdgeInsets.symmetric(),
//              ),
//            ),
//            new Tab(
//              child: Container(
//                margin: EdgeInsets.all(5.0),
//                child: Column(
//                  children: <Widget>[
//                    new Image(
//                      image: AssetImage("assets/iconsetpng/newspaper3.png"),
//                      height: 20.0,
//                      width: 22.0,
//                    ),
//                    new Text(
//                      "Events",
//                      style: TextStyle(color: Colors.white,fontSize: 14.00),
//                    ),
//                  ],
//                ),
//                padding: EdgeInsets.symmetric(),
//              ),
//            ),
//          ],
//        ),
//      ),
    );
  }

  @override
  void onDisplayUserInfo(User user) {
    setState(() {
      email = user.email;
      password = user.password;
      client_id = user.client_id;
      center = user.center;
      try {
        final parsed = json.decode(center);
        var details = parsed[0];
        var users = details["user"];

        jwt = details["jwt"];
        id = users["id"];
        client_id = users["client_id"];

//        print(jwt);
//        print(id);
//        print(cliId);
//        print("******${details["jwt"]}");

      } on FormatException catch (e) {
        print("That string didn't look like Json.");
      } on NoSuchMethodError catch (e) {
        print('That string was null!');
      }

//      var childrenData = getProperty(center);
//      print("childrenData $childrenData");
    });
  }

  @override
  void onErrorUserInfo() {
    setState(() {
      _homeText = 'There was an error retrieving user info';
      print(_homeText);
    });
  }

  @override
  void onLogoutUser() {
    var authStateProvider = new AuthStateProvider();
    setState(() {
      authStateProvider.notify(AuthState.LOGGED_OUT);
      authStateProvider.dispose(this);
    });
//
  }

  @override
  void onAuthStateChanged(AuthState state) {
    print("Navigate");
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(_ctx)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }
  }
}

// A drawer that pops up from the bottom of the screen.
class _DemoDrawer extends StatelessWidget {
  const _DemoDrawer();

  @override
  Widget build(BuildContext context) {
//    return CommonDrawer();
//    return ChildrenDrawer();

//    return Drawer(
//      child: Column(
//        children: const <Widget>[
//          ListTile(
//            leading: Icon(Icons.search),
//            title: Text('Search'),
//          ),
//          ListTile(
//            leading: Icon(Icons.threed_rotation),
//            title: Text('3D'),
//          ),
//        ],
//      ),
//    );
  }
}
/*
class MenuTile extends StatelessWidget {
  final title;
  final subtitle;
  final textColor;
  MenuTile({this.title, this.subtitle, this.textColor = Colors.black});
  @override
  Widget build(BuildContext context) {
    return Column(
      // crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
              fontSize: 20.0, fontWeight: FontWeight.w700, color: textColor),
        ),
        SizedBox(
          height: 5.0,
        ),
        Text(
          subtitle,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.normal, color: textColor),
        ),
      ],
    );
  }
}*/
