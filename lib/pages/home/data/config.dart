//Containing daily summary screen config default data, which are replaced by the api globals
Map<String, String> labelsConfig = {
  "dailySummaryLabel": "Daily Summary", //client config
  "learningStoryLabel": "Learning Story",
  "dailyJournalsLabel": "Daily Journals",
  "observationLabel": "Observations",
  "dailyChartLabel": "Daily Chart",
  "medicationLabel": "Medications",
  "noteToCenterLabel": "Note To Center",
  "photosLabel": "Photos",
  "galleryLabel": "Gallery",
  "calendarLabel": "Calendar",
  "eventsLabel": "Events",
  "programsLabel": "Programs",
  "privateMessageLabel": "Private Message",
  "newsletterLabel": "Newsletters",
  "generalFormsLabel": "General Forms",
  "policiesLabel": "Policies",
  "settingsLabel": "Settings",
  "notificationsLabel": "Notifications",
  "logoutLabel": "Logout",
  "newsFeedLabel": "Newsfeed",
  "childProfileLabel": "Profile",
  "prescribedMedicationLabel": "Prescribed",
  "nonPrescribedMedicationLabel": "Non-Prescribed",
  "journalsLabel": "Journals",
  "composeMessageLabel": "Compose",
  "messageLabel": "Message",
  "userSettingsLabel": "Profile Settings",
  "medicationStartedOnLabel": "Started On",
  "dosageLabel": "Dosage",
  "lastMedicationOnLabel": "Last medication on",
  "repeatMedicationLabel": "Repeat",
  "administeredMedicationLabel": "Administered",
  "viewPrescribedMedicationLabel": "View Prescribed Medication",
  "medicationHistoryLabel": "Medication History",
  "nameOfMedicationLabel": "Name of mediction",
  "originalPackagingLabel": "Medication in original packaging",
  "medicalPractitionerLabel": "Medical practitioner/chemist",
  "repeatPrescribedLabel": "Repeat Prescribed Medication",
  "datePrescribedLabel": "Date Prescribed",
  "expiryDateLabel": "Expiry Date",
  "medicationStorageLabel": "Storage",
  "reasonForMedicationLabel": "Reason for medication",
  "methodToBeAdministeredLabel": "Method to be administered",
  "medicationInstructionLabel": "Instructions",
  "dateLabel": "Date",
  "timesLabel": "Times",
  "editMedicationLabel": "Edit Medication",
  "editMediDetailsLabel": "Edit Prescribed Medication Details",
  "acknowledgmentLabel": "I request that the above medication to be given in accordance with the instructions given.",
  "createPrescribedMediLabel": "Create Medication",
  "createNonPrescribedMediLabel": "Create Medication",
  "medsToApplyAdministeredLabel": "Non Prescribed Medication to apply/administer",
  "dateAuthorizedLabel": "Date authorized",
  "giveUnderCircumstanceLabel": "Give under following circumstance",
  "nonPrescribedAcknowledgmentLabel": "I authorize to apply/administer above mendication to my child in the above circumstances.",
  "administeredLabel": "Administered",
  "whatHappendLabel": "What Happened",
  "educatorLabel": "Educator",
  "roomLabel": "Room",
  "centerManageDateLabel": "Date",
  "learningOutcomeLabel": "Learning outComes",
  "learningTagLabel": "Learning tags",
  "reflectionLabel": "Reflection",
  "followUpPhotoLabel": "Follow up photos",
  "followUpLabel": "Follow Up",
  "journalDateLabel": "Journal Date",
  "followUpDateLabel": "Follow Up Date",
  "observationSingularLabel": "Observation",
  "learningOutcomesJourneyLabel": "Learning Outcomes",
  "journeyDateLabel": "Date",
  "learningTagsJourneyLabel": "Learning tags",
  "principlesourneyLabel": "Principles",
  "practicesJourneyLabel": "Practices",
  "reflectionJourneyLabel": "Reflection",
  "educatorJourneyLabel": "Educator",
  "journeyFollowUpLabel": "Follow Up",
  "learningStoryDetailLabel": "Learnig story detail",
  "newsFeedDetailLabel": "View News Feed",
  "programSubjectLabel": "Subject Area",
  "programTypeLabel": "Program Type",
  "programLearningOutcomeLabel": "Learning Outcome",
  "programDetailViewLabel": "Program Detail View",
  "programStartDate": "Start Date",
  "programEndDate": "End Date",
};

Map<String, bool> appSettingsConfig = {
  "enable_master_nav_learning_story": true,
};