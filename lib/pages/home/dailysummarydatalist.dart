import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:kinderm8/Theme.dart' as Kinderm8Theme;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:kinderm8/auth.dart';
import 'package:kinderm8/data/database_helper.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/calendar/events.dart';
import 'package:kinderm8/pages/centrenotes/centrenoteslist.dart';
import 'package:kinderm8/pages/childrenmenu.dart';
import 'package:kinderm8/pages/childrenmodulemenu.dart';
import 'package:kinderm8/pages/commondrawer.dart';
import 'package:kinderm8/pages/dailychart/dailychartlist.dart';
import 'package:kinderm8/pages/dailyjournal/dailyJournalList.dart';
import 'package:kinderm8/pages/gallery/photolist.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_summary_modal.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/pages/home/modals/summary_view_modal.dart';
import 'package:kinderm8/pages/home/services/daily_summary_service.dart';
import 'package:kinderm8/pages/home/widgets/child_indicator.dart';
import 'package:kinderm8/pages/home/widgets/childs.dart';
import 'package:kinderm8/pages/home/widgets/page_dragger.dart';
import 'package:kinderm8/pages/learningstory/learningstoryList.dart';
import 'package:kinderm8/pages/login/login.dart';
import 'package:kinderm8/pages/medication/medicationlist.dart';
import 'package:kinderm8/pages/newsfeed/newsfeedlist.dart';
import 'package:kinderm8/pages/observation/observationList.dart';
import 'package:kinderm8/pages/splashScreen.dart'
    show UserChildDataInheritedWidget;
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DailySummary extends StatefulWidget {
  final String text;
  final List<ChildViewModal> childrenData;

  const DailySummary({Key key, this.text, this.childrenData}) : super(key: key);

  @override
  _DailySummaryState createState() => new _DailySummaryState();
}

class _DailySummaryState extends State<DailySummary>
    with TickerProviderStateMixin
    implements HomePageContract, AuthStateListener {
  var childId, appUser, jwt, userId;
  var email, password, clientId, center;
  var image;
  var deviceSize;
  HomePagePresenter _presenter;

  //Asanka
  BuildContext ctx;
  StreamController<SlideUpdate> slideUpdateStream;
  AnimatedPageDragger animatedPageDragger;

  int activeIndex = 0;
  int nextPageIndex = 0;
  SlideDirection slideDirection = SlideDirection.none;
  double slidePercent = 0.0;

  SlideUpdate slideUpdate =
      SlideUpdate(UpdateType.doneDragging, SlideDirection.none, 0.0);

  //fetch children data to a List from api
  List<ChildViewModal> childrenData = [];
  List<SummaryViewModal> childSummary = [];
  bool isLoading = false;

  List<Function> navigationFunctions = [];
  ScrollController _scrollViewController;
  bool isOnRefresh = false;

  _DailySummaryState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();

    //Asanka
    slideUpdateStream = new StreamController<SlideUpdate>();

    slideUpdateStream.stream.listen((SlideUpdate event) {
      setState(() {
        slideUpdate = event;
        if (event.updateType == UpdateType.dragging) {
          slideDirection = event.direction;
          slidePercent = event.slidePercent;

          if (slideDirection == SlideDirection.leftToRight) {
            nextPageIndex = activeIndex - 1;
          } else if (slideDirection == SlideDirection.rightToLeft) {
            nextPageIndex = activeIndex + 1;
          } else {
            nextPageIndex = activeIndex;
          }
        } else if (event.updateType == UpdateType.doneDragging) {
          if (slidePercent > 0.5) {
            animatedPageDragger = new AnimatedPageDragger(
              slideDirection: slideDirection,
              transitionGoal: TransitionGoal.open,
              slidePercent: slidePercent,
              slideUpdateStream: slideUpdateStream,
              vsync: this,
            );
          } else {
            animatedPageDragger = new AnimatedPageDragger(
              slideDirection: slideDirection,
              transitionGoal: TransitionGoal.close,
              slidePercent: slidePercent,
              slideUpdateStream: slideUpdateStream,
              vsync: this,
            );

            nextPageIndex = activeIndex;
          }

          animatedPageDragger.run();
        } else if (event.updateType == UpdateType.animating) {
          slideDirection = event.direction;
          slidePercent = event.slidePercent;
        } else if (event.updateType == UpdateType.doneAnimating) {
          activeIndex = nextPageIndex;

          slideDirection = SlideDirection.none;
          slidePercent = 0.0;

          animatedPageDragger.dispose();
        }
      });
    });
  }

  Widget buildParentWidget() {
    return isLoading
        ? isOnRefresh ? SizedBox() : progress
        : MediaQuery.removePadding(
            context: ctx,
            removeTop: true,
            child: ListView(
              children: <Widget>[
                Child(
                  viewModel: (childrenData.length > 0)
                      ? childrenData[activeIndex]
                      : ChildViewModal(), //childrenData[activeIndex],
                  slideDirection: slideDirection,
                  slideUpdate: slideUpdate,
                  percentVisible: (slideDirection == SlideDirection.none)
                      ? 1.0
                      : slidePercent, //slideUpdateType == UpdateType.doneAnimating &&
                ),
              ],
            ),
          );
  }

  void changeSummaryWhenIndicatorTap(int index) {
    setState(() {
      activeIndex = index;
    });
  }

  void navigateToDailyJournal() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                new DailyJournal(childrenData[activeIndex], jwt)));
  }

  void navigateToObservation() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                new ObservationList(childrenData[activeIndex], jwt)));
  }

  void navigateToDailyChart() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                new DailyChart(childrenData[activeIndex], jwt)));
  }

  //TODO: where's my readability: cannot divide for now bcuz of setState
  void getGlobalConfig() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String savedClientConfig = prefs.getString('globalConfig_clientSection') ?? null;
    final decodedClientConfigs = json.decode(savedClientConfig);
    String savedNavigationConfig = prefs.getString("globalConfig_navigationSet") ?? null;
    final decodedNavigationConfigs = json.decode(savedNavigationConfig);
    String savedProgramConfig = prefs.getString('globalConfig_programPlanSection') ?? null;
    final decodedProgramConfig = json.decode(savedProgramConfig);
    String savedNotificationConfig = prefs.getString("globalConfig_notificationSection") ?? null;
    final decodedNotificationConfig = json.decode(savedNotificationConfig);
    String savedPrivateMessageConfig = prefs.getString("globalConfig_privateMessageSection") ?? null;
    final decodedPrivateMessageConfig = json.decode(savedPrivateMessageConfig);
    String savedChildProfileConfig = prefs.getString("globalConfig_childProfileSection") ?? null;
    final decodedChildProfileConfig = json.decode(savedChildProfileConfig);
    String savedStaffDocumentsSection = prefs.getString("globalConfig_StaffDocumentsSection") ?? null;
    final decodedStaffDocumentsSection = json.decode(savedStaffDocumentsSection);
    String savedMedicationSection = prefs.getString("globalConfig_medicationSection") ?? null;
    final decodedMedicationSection = json.decode(savedMedicationSection);
    String savedNonPrescribedMedicationSection = prefs.getString("globalConfig_nonPrescribedMedicationSection") ?? null;
    final decodedNonPrescribedMedicationSection = json.decode(savedNonPrescribedMedicationSection);
    String savedCenterManagementSection = prefs.getString("globalConfig_centerManagementJournalSection");
    final decodedCenterManagementJournalSection = json.decode(savedCenterManagementSection);
    String savedCenterManagementJourneySection = prefs.getString("globalConfig_centerManagementJournalSection");
    final decodedCenterManagementJourneySection = json.decode(savedCenterManagementJourneySection);
    String savedNewsFeedSection = prefs.getString("globalConfig_newsFeedSection");
    final decodedNewsFeedSection = json.decode(savedNewsFeedSection);
    String savedNewsFeedDetail = prefs.getString("globalConfig_newsFeedDetailLabel");
    final decodedNewsFeedDetailLabel = json.decode(savedNewsFeedDetail);
    String savedAppSettings = prefs.getString("globalConfig_appSettings");
    final decodedAppSettings = json.decode(savedAppSettings);

    //If upcoming configs are null or if there is error, I should show default labels again.
    Map<String, String> backup = labelsConfig;
    Map<String, bool> appSettingsBackup = appSettingsConfig;


    setState(() {
      //client config
      labelsConfig["dailySummaryLabel"] = decodedClientConfigs["nav_client_daily_summary"] ?? backup["dailySummaryLabel"];
      labelsConfig["learningStoryLabel"] = decodedClientConfigs["nav_client_photos_story_tab"] ?? backup["learningStoryLabel"];
      labelsConfig["dailyJournalsLabel"] = decodedClientConfigs["nav_client_journal"] ?? backup["dailyJournalsLabel"];
      labelsConfig["observationLabel"] = decodedClientConfigs["nav_client_journey"] ?? backup["observationLabel"];
      labelsConfig["dailyChartLabel"] = decodedClientConfigs["nav_client_dailychart"] ?? backup["dailyChartLabel"];
      labelsConfig["medicationLabel"] = decodedClientConfigs["dashboard_notifications_medication"] ?? backup["medicationLabel"];
      labelsConfig["noteToCenterLabel"] = decodedClientConfigs["nav_centermanagement_remarks_tab_label_s"] ?? backup["noteToCenterLabel"];
      labelsConfig["photosLabel"] = decodedClientConfigs["nav_client_photos"] ?? backup["photosLabel"];
      labelsConfig["calendarLabel"] = decodedClientConfigs["nav_client_events"] ?? backup["calendarLabel"];
      labelsConfig["eventsLabel"] = decodedClientConfigs["nav_client_events_tab"] ?? backup["eventsLabel"];
      labelsConfig["journalsLabel"] = decodedClientConfigs["nav_client_mobile_journal"] ?? backup["journalsLabel"];

      //newsfeed config
      labelsConfig["newsFeedLabel"] = decodedNewsFeedSection ?? backup["newsFeedLabel"];
      labelsConfig["newsFeedDetailLabel"] = decodedNewsFeedDetailLabel ?? backup["newsFeedDetailLabel"];

      //programs config
      labelsConfig["programsLabel"] = decodedProgramConfig["label_program_plan_client_list_tab_heading"] ?? backup["programsLabel"];
      labelsConfig["programSubjectLabel"] = decodedProgramConfig["label_program_plan_subjectarea"] ?? backup["programSubjectLabel"];
      labelsConfig["programTypeLabel"] = decodedProgramConfig["label_program_plan_type"] ?? backup["programTypeLabel"];
      labelsConfig["programLearningOutcomeLabel"] = decodedProgramConfig["label_program_plan_learningtags"] ??backup["programLearningOutcomeLabel"];
      labelsConfig["programStartDate"] = decodedProgramConfig["label_program_plan_starting_date"] ??backup["programStartDate"];
      labelsConfig["programEndDate"] = decodedProgramConfig["label_program_plan_ending_date"] ?? backup["programEndDate"];

      //navigation_set configs
      labelsConfig["newsletterLabel"] = decodedNavigationConfigs["nav_newsletter"] ?? backup["newsletterLabel"];
      labelsConfig["generalFormsLabel"] = decodedNavigationConfigs["nav_generalforms"] ?? backup["generalFormsLabel"];
      labelsConfig["policiesLabel"] = decodedNavigationConfigs["nav_policies"] ?? backup["policiesLabel"];
      labelsConfig["settingsLabel"] = decodedNavigationConfigs["nav_user_settings"] ?? backup["settingsLabel"];
      labelsConfig["userSettingsLabel"] = decodedNavigationConfigs["nav_user_settings"] ?? backup["userSettingsLabel"];

      //notification config
      labelsConfig["notificationsLabel"] = decodedNotificationConfig ?? backup["notificationsLabel"];

      //privateMessage config
      labelsConfig["privateMessageLabel"] = decodedPrivateMessageConfig["nav_messageportal_active_log_label"] ?? backup["privateMessageLabel"];
      labelsConfig["composeMessageLabel"] = decodedPrivateMessageConfig["nav_messageportal_button_new"] ?? backup["composeMessageLabel"];
      labelsConfig["messageLabel"] = decodedPrivateMessageConfig["nav_messageportal_label"] ?? backup["messageLabel"];

      //childProfile config
      labelsConfig["childProfileLabel"] = decodedChildProfileConfig ?? backup["childProfileLabel"];

      //staff_documents_upload_section medication config
      labelsConfig["medicationLabel"] = decodedStaffDocumentsSection["nav_client_medications"] ?? backup["medicationLabel"];
      labelsConfig["createNonPrescribedMediLabel"] = decodedMedicationSection["nav_client_nonprescribed_medication_create"] ?? backup["createNonPrescribedMediLabel"];
      //medication section
      labelsConfig["prescribedMedicationLabel"] = decodedMedicationSection["administered_medication_history_sub_medications_page_title"] ?? backup["prescribedMedicationLabel"];
      labelsConfig["nonPrescribedMedicationLabel"] = decodedNonPrescribedMedicationSection["nonprescribed_medication_title"] ?? backup["nonPrescribedMedicationLabel"];
      labelsConfig["medicationStartingOnLabel"] = decodedMedicationSection["medication_label_startingon"] ?? backup["medicationStartingOnLabel"];
      labelsConfig["dosageLabel"] = decodedMedicationSection["medication_label_dosageofmedication"] ?? backup["dosageLabel"];
      labelsConfig["lastMedicationOnLabel"] = decodedMedicationSection["medication_label_lastadministered"] ?? backup["lastMedicationOnLabel"];
      labelsConfig["repeatMedicationLabel"] = decodedMedicationSection["nav_medicationmanagement_dchart_option_label_action_repeat"] ?? backup["repeatMedicationLabel"];
      labelsConfig["viewPrescribedMedicationLabel"] = decodedMedicationSection["administered_medication_main_title"] ?? backup["viewPrescribedMedicationLabel"];
      labelsConfig["medicationHistoryLabel"] = decodedMedicationSection["nav_medication_action_index_title"] ?? backup["medicationHistoryLabel"];
      labelsConfig["nameOfMedicationLabel"] = decodedMedicationSection["medication_label_nameofmedication"] ?? backup["nameOfMedicationLabel"];
      labelsConfig["originalPackagingLabel"] = decodedMedicationSection["medication_label_medicationinoriginalpackaging"] ?? backup["originalPackagingLabel"];
      labelsConfig["medicalPractitionerLabel"] = decodedMedicationSection["medication_label_medicalpractitioner"] ?? backup["medicalPractitionerLabel"];
      labelsConfig["repeatPrescribedLabel"] = decodedMedicationSection["administered_medication_sub_medications_clone_btn_title"] ?? backup["repeatPrescribedLabel"];
      labelsConfig["acknowledgmentLabel"] = decodedMedicationSection["medication_label_create_helpertext"] ?? backup["acknowledgmentLabel"];
      labelsConfig["datePrescribedLabel"] = decodedMedicationSection["medication_label_dateprescribed"] ?? backup["datePrescribedLabel"];
      labelsConfig["expiryDateLabel"] = decodedMedicationSection["medication_label_expirydate"] ?? backup["expiryDateLabel"];
      labelsConfig["medicationStorageLabel"] = decodedMedicationSection["medication_label_storage"] ?? backup["medicationStorageLabel"];
      labelsConfig["reasonForMedicationLabel"] = decodedMedicationSection["medication_label_reasonformedication"] ?? backup["reasonForMedicationLabel"];
      labelsConfig["dateLabel"] = decodedMedicationSection["medication_label_maindate"] ?? backup["dateLabel"];
      labelsConfig["methodToBeAdministeredLabel"] = decodedMedicationSection["medication_label_methodtobeadministered"] ?? backup["methodToBeAdministeredLabel"];
      labelsConfig["timesLabel"] = decodedMedicationSection["medication_label_times"] ?? backup["timesLabel"];
      labelsConfig["createPrescribedMediLabel"] = decodedMedicationSection["nav_client_prescribed_medication_create"] ?? backup["createPrescribedMediLabel"];
      labelsConfig["staffSignatureLabel"] = decodedMedicationSection["medication_label_staffsignature"] ?? backup["staffSignatureLabel"];

      //non prescribed medication section
      labelsConfig["createNonPrescribedMediLabel"] = decodedMedicationSection["nonprescribed_medication_label_medication_to_administered"] ?? backup["createNonPrescribedMediLabel"];
      labelsConfig["medsToApplyAdministeredLabel"] = decodedNonPrescribedMedicationSection["nonprescribed_medication_label_medication_to_administered"] ?? backup["medsToApplyAdministeredLabel"];
      labelsConfig["dateAuthorizedLabel"] = decodedNonPrescribedMedicationSection["nonprescribed_medication_label_date_authorized"] ?? backup["dateAuthorizedLabel"];
      labelsConfig["giveUnderCircumstanceLabel"] = decodedNonPrescribedMedicationSection["nonprescribed_medication_label_give_under_circumstances"] ?? backup["giveUnderCircumstanceLabel"];
      labelsConfig["nonPrescribedAcknowledgmentLabel"] = decodedNonPrescribedMedicationSection["nonprescribed_medication_label_helpertext"] ?? backup["nonPrescribedAcknowledgmentLabel"];
      labelsConfig["administeredLabel"] = decodedNonPrescribedMedicationSection["medication_label_medicationadministered"] ?? backup["administeredLabel"];

      //center management journal section
      labelsConfig["whatHappendLabel"] = decodedCenterManagementJournalSection["nav_centermanagement_journal_form_label_whathappened"] ?? backup["whatHappendLabel"];
      labelsConfig["educatorLabel"] = decodedCenterManagementJournalSection["nav_centermanagement_journal_form_label_educator"] ?? backup["educatorLabel"];
      labelsConfig["roomLabel"] = decodedCenterManagementJournalSection["nav_centermanagement_journal_label_table_room"] ?? backup["roomLabel"];
      labelsConfig["centerManageDateLabel"] = decodedCenterManagementJournalSection["nav_stafreflection_form_label_date"] ?? backup["centerManageDateLabel"];
      labelsConfig["learningOutcomeLabel"] = decodedCenterManagementJournalSection["nav_centermanagement_journal_form_label_learningtags_followup"] ?? backup["learningOutcomeLabel"];
      labelsConfig["learningTagLabel"] = decodedCenterManagementJournalSection["nav_centermanagement_journal_form_label_new_learningtags"] ?? backup["learningTagLabel"];
      labelsConfig["reflectionLabel"] = decodedCenterManagementJournalSection["nav_centermanagement_journal_form_label_reflection"] ?? backup["reflectionLabel"];
      labelsConfig["followUpLabel"] = decodedCenterManagementJournalSection["nav_centermanagement_journal_form_label_followup"] ?? backup["followUpLabel"];
      labelsConfig["followUpPhotoLabel"] = decodedCenterManagementJournalSection["nav_centermanagement_journal_form_label_followup_images"] ?? backup["followUpPhotoLabel"];
      labelsConfig["journalDateLabel"] = decodedCenterManagementJournalSection["nav_centermanagement_journal_form_label_observationdate"] ?? backup["journalDateLabel"];
      labelsConfig["followUpDateLabel"] = decodedCenterManagementJournalSection["nav_centermanagement_journal_form_label_followupdate"] ?? backup["followUpDateLabel"];

      //center management journey section
      labelsConfig["observationSingularLabel"] = decodedCenterManagementJourneySection["nav_centermanagement_journey_form_label_whathappened"] ?? backup["observationSingularLabel"];
      labelsConfig["educatorJourneyLabel"] = decodedCenterManagementJourneySection["nav_centermanagement_journey_form_label_observer"] ?? backup["educatorJourneyLabel"];
      labelsConfig["learningOutcomesJourneyLabel"] = decodedCenterManagementJourneySection["nav_centermanagement_journey_form_label_learningtags_followup"] ?? backup["learningOutcomesJourneyLabel"];
      labelsConfig["learningTagsJourneyLabel"] = decodedCenterManagementJourneySection["nav_stafreflection_form_label_learningtags"] ?? backup["learningTagsJourneyLabel"];
      labelsConfig["journeyDateLabel"] = decodedCenterManagementJourneySection["nav_centermanagement_journey_form_label_observationdate"] ?? backup["journeyDateLabel"];
      labelsConfig["principlesourneyLabel"] = decodedCenterManagementJourneySection["nav_centermanagement_journey_form_label_principles"] ?? backup["principlesourneyLabel"];
      labelsConfig["practicesJourneyLabel"] = decodedCenterManagementJourneySection["nav_centermanagement_journey_form_label_practice"] ?? backup["practicesJourneyLabel"];
      labelsConfig["reflectionJourneyLabel"] = decodedCenterManagementJourneySection["nav_centermanagement_journey_form_label_reflection"] ?? backup["reflectionJourneyLabel"];

//      appSettingsConfig["enable_master_nav_learning_story"] = decodedAppSettings ?? backup["enable_master_nav_learning_story"];;
    });
  }

  void navigateToNoteToCenter() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                new CentreNotesList(childrenData[activeIndex], jwt)));
  }

  void navigateToLearningStory() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                new LearningStoryList(childrenData[activeIndex], jwt)));
  }

  void navigateToMedication() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                new Medications(childrenData[activeIndex], jwt, 0)));
  }

  void navigateToPhotoList() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                new PhotoList(childrenData[activeIndex], jwt)));
  }

  @override
  void initState() {
    super.initState();
    _scrollViewController = new ScrollController();
    getGlobalConfig();
    setState(() {
      //Asanka
      if (widget.childrenData == null) {
        setState(() {
          isLoading = true;
        });
        fetchData();
      } else {
        childrenData = widget.childrenData;
      }
      activeIndex = childrenData.length > 2 ? 1 : 0;
      navigationFunctions.addAll([
        navigateToDailyJournal,
        navigateToObservation,
        navigateToDailyChart,
        navigateToNoteToCenter,
        navigateToLearningStory,
        navigateToMedication,
        navigateToPhotoList
      ]);
    });

    for (int i = 0; i < childrenData.length; i++) {
      childrenData[i].navigateFunctions = navigationFunctions;
    }
  }

  @override
  void dispose() {
    _scrollViewController.dispose();
    super.dispose();
  }

  var progress = new ProgressBar(
    color: Kinderm8Theme.Colors.appcolour,
    containerColor: Kinderm8Theme.Colors.progresscontent,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  // // Pop to ask do you wish to exit
  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          child: new AlertDialog(
            title: new Text('Do you want to close Kinder m8?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => exit(0),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }

  void _modalBottomSheetMenu() {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return new Container(
            height: 100.0,
            color: Colors.transparent,
            child: new Container(
                decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(10.0),
                        topRight: const Radius.circular(10.0))),
                child: new Center(
                  child: ChildrenDrawer(childrenData),
                )),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    ctx = context;
    final myInheritedWidget = UserChildDataInheritedWidget.of(context);
    if (myInheritedWidget?.data?.childrenData != null &&
        myInheritedWidget.data.childrenData.length > 0) {
      childrenData = myInheritedWidget.data.childrenData;
    }
    return DailySummaryStateContainer(
      data: this,
      child: Scaffold(
      drawer: CommonDrawer(childrenData: childrenData),
      body: NestedScrollView(
        controller: _scrollViewController,
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            new SliverAppBar(
              elevation: 0.7,
              centerTitle: true,
              iconTheme: new IconThemeData(color: Color(0xFFB39DDB)),
              title: Text(
                labelsConfig["dailySummaryLabel"],
                style: TextStyle(color: Color(0xFFB39DDB), fontSize: 18.5),
              ),
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              pinned: true,
              floating: true,
              forceElevated: innerBoxIsScrolled,
              bottom: PreferredSize(
                preferredSize: Size.fromHeight(124.0),
                child: Padding(
                  padding: EdgeInsets.only(top: 4.0),
                  child: ChildIndicator(
                    viewModel: new ChildIndicatorViewModel(
                      childrenData,
                      activeIndex,
                      slideDirection,
                      slidePercent,
                    ),
                  ),
                ),
              ),
            ),
          ];
        },
        body: WillPopScope(
          onWillPop: _onWillPop,
          child: RefreshIndicator(
            onRefresh: () async {
              setState(() {
                isOnRefresh = true;
              });
              return await fetchData().then((value) {
                setState(() {
                  isOnRefresh = false;
                });
              });
            },
            child: Stack(
              children: <Widget>[
                PageDragger(
                  canDragLeftToRight: activeIndex > 0,
                  canDragRightToLeft: activeIndex < childrenData.length - 1,
                  slideUpdateStream: this.slideUpdateStream,
                  child: buildParentWidget(),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.menu),
        backgroundColor: Kinderm8Theme.Colors.appcolour,
        onPressed: () {
          if (childrenData.length == 1) {
            Navigator.of(context).pushReplacement(new MaterialPageRoute<Null>(
              builder: (BuildContext context) {
                return new Scaffold(
                  body: ChildrenModuleMenu(childrenData[activeIndex]),
                );
              },
            ));
          } else {
            _modalBottomSheetMenu();
          }
        },
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        color: Kinderm8Theme.Colors.app_white,
        notchMargin: 4.0,
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new FlatButton(
              onPressed: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new DailySummary(
                              childrenData: childrenData,
                            )));
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.format_list_bulleted),
                ],
              ),
            ),
            new FlatButton(
              onPressed: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new NewsFeedList(
                              childrenData: childrenData,
                            )));
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.chrome_reader_mode),
                ],
              ),
            ),
            new FlatButton(
              onPressed: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new EventsData(
                              childrenData: childrenData,
                            )));
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.date_range),
                ],
              ),
            ),
            new FlatButton(
              onPressed: () {},
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.free_breakfast),
                  //                  new Text("Family ..")
                ],
              ),
            ),
          ],
        ),
      ),
    ),
    );
  }

  @override
  Future onDisplayUserInfo(User user) async {
    email = user.email;
    password = user.password;
    clientId = user.client_id;
    var appUser = user.center;
    var details;
//    setState(() {
      try {
        final parsed = json.decode(appUser);
        details = parsed[0];
        var users = details["user"];
        jwt = details["jwt"];
        userId = users["id"];
        clientId = users["client_id"];
      } catch (e) {
        print('That string was null!');
      }
//    });
  }

  Future fetchData() async {
    bool isImageAvailable = true;

    isLoading = true;
    var db = new DatabaseHelper();
    User user = await db.getFirstUser();
    final parsed = json.decode(user.center);
    List childDetails = parsed[0]['children'];

//    await getRefreshToken(userId: childDetails[0]["id"].toString(), clientId: clientId);
    setState(() {
      childrenData = childDetails.map<ChildViewModal>((element) {
        precacheImage(
            NetworkImage(
                "https://d212imxpbiy5j1.cloudfront.net/${element["image"]}"),
            ctx,
            onError: (err, stackT) => isImageAvailable = false);
        return ChildViewModal(
            id: element["id"],
            iconAssetPath: isImageAvailable ? element["image"] : '',
            firstName: element["firstname"],
            lastName: element["lastname"],
            dob: element["dob"],
            attendance: element["attendance"],
            gender: element["gender"],
            room: element["room"],);
      }).toList();
    });

    navigationFunctions.addAll([
      navigateToDailyJournal,
      navigateToObservation,
      navigateToDailyChart,
      navigateToNoteToCenter,
      navigateToLearningStory,
      navigateToMedication,
      navigateToPhotoList
    ]);
    for (int i = 0; i < childrenData.length; i++) {
      await fetchDailySummaryData(
              childId: childrenData[i].id, clientId: clientId, userId: userId.toString())
          .then((value) {
        ChildSummaryModal childSummaryModal = value;
//        precacheImage(
//            NetworkImage(childSummaryModal.childPhotoModals[i].fileUrl), ctx,
//            onError: (err, trace) {
//              print(err);
//            });
        setState(() {
          childrenData[i].childSummaryVM = childSummaryModal;
          childrenData[i].navigateFunctions = navigationFunctions;
        });
//        print("Fetching... ${childrenData[i].id}");
//        print(childSummaryModal.journal.whatHappened);
      });
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  void onLogoutUser() {
    var authStateProvider = new AuthStateProvider();
    setState(() {
      authStateProvider.notify(AuthState.LOGGED_OUT);
      authStateProvider.dispose(this);
    });
  }

  @override
  void onAuthStateChanged(AuthState state) {
    print("daily summary Navigate $state");
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
      Navigator.push(context,
          new MaterialPageRoute(builder: (context) => new LoginPage()));
    }
  }

  @override
  void onErrorUserInfo() {}
}

class DailySummaryStateContainer extends InheritedWidget {
  final _DailySummaryState data;
  const DailySummaryStateContainer ({
             Key key,
             this.data,
             @required Widget child,
           })
      : assert(child != null),
        super(key: key, child: child);

  static DailySummaryStateContainer of (BuildContext context) {
    return context.inheritFromWidgetOfExactType(DailySummaryStateContainer) as DailySummaryStateContainer;
  }

  @override
  bool updateShouldNotify (DailySummaryStateContainer old) {
    return true;
  }
}