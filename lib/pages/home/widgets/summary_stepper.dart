import 'package:kinderm8/pages/home/modals/daily_chart_modal.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SummaryStepper extends StatelessWidget {

  final DailyChartModal dailyChartModal;

  SummaryStepper({
    this.dailyChartModal,
  });

  Widget _buildFoodListWidget(BuildContext context) {
    return Column(
      children: dailyChartModal.dailyChartFoodList.map<Widget>((element) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 1.2,
              height: 15.0,
              color: Colors.grey,
              margin: EdgeInsets.only(left: 18.0),
            ),
            SizedBox(height: 4.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 4.0),
                Container(
                  width: 30.0,
                  height: 30.0,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    backgroundImage: AssetImage("assets/iconsetpng/groceries.png"),
                    radius: 20.0,
                  ),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 1.7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      (element.menuCategory != null && element.menuCategory != "") ? Text(
                        element.menuCategory,
                        style: TextStyle(fontSize: 16.0),
                      ) : SizedBox(),
                      (element.date != null && element.date != "") ? Text(
                        element.date,
                        style: TextStyle(fontSize: 12.0),
                      ) : SizedBox(),//, fontWeight: FontWeight.w500
                      (element.foodMenu != null && element.foodMenu != "") ? Text(
                        element.foodMenu,
                        style: TextStyle(fontSize: 12.0),
                      ) : SizedBox(),
                      (element.serveType != null && element.serveType != "") ? Text(element.serveType,
                          style: TextStyle(fontSize: 12.0, color: Colors.grey)) : SizedBox(),
                    ],
                  ),
                ),
              ],
            ),
          ],
        );
      }).toList(),
    );
  }


  Widget _buildNappyListWidget(BuildContext context) {
    return Column(
      children: dailyChartModal.dailyChartNappyList.map<Widget>((element) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 1.2,
              height: 15.0,
              color: Colors.grey,
              margin: EdgeInsets.only(left: 18.0),
            ),
            SizedBox(height: 4.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 4.0),
                Container(
                  width: 30.0,
                  height: 30.0,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    backgroundImage: AssetImage("assets/iconsetpng/diaper.png"),
                    radius: 20.0,
                  ),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 1.7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      (element.username != null && element.username != "") ? Text(
                        "@${element.username}",
                        style: TextStyle(fontSize: 15.0),
                      ) : SizedBox(), //, fontWeight: FontWeight.w500
                      (element.date != null && element.date != "") ? Text(element.date, style: TextStyle(fontSize: 12.0)) : SizedBox(),
                      (element.catagory != null && element.catagory != "") ? Text(element.catagory,
                          style: TextStyle(fontSize: 12.0, color: Colors.grey)) : SizedBox(),
                      (element.trainingType != null && element.trainingType != "") ? Text("${element.trainingType} @ ${element.time}",
                          style: TextStyle(fontSize: 12.0, color: Colors.grey)) : SizedBox(),
                    ],
                  ),
                ),
              ],
            ),
          ],
        );
      }).toList(),
    );
  }

  Widget _buildBottleFeedListWidget(BuildContext context) {
    return Column(
      children: dailyChartModal.dailyChartBottleFeedList.map<Widget>((element) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 1.2,
              height: 15.0,
              color: Colors.grey,
              margin: EdgeInsets.only(left: 18.0),
            ),
            SizedBox(height: 4.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 4.0),
                Container(
                  width: 30.0,
                  height: 30.0,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    backgroundImage: AssetImage("assets/iconsetpng/bottle-feed.png"),
                    radius: 20.0,
                  ),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 1.7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      (element.bottleType != null && element.bottleType != "") ? Text(
                        element.bottleType,
                        style: TextStyle(fontSize: 15.0),
                      ) : SizedBox(), //, fontWeight: FontWeight.w500
                      Text(element.date, style: TextStyle(fontSize: 12.0)),
                      (element.bottleQty != null && element.bottleQty != "") ? Text("${element.bottleQty}(ml)",
                          style: TextStyle(fontSize: 12.0, color: Colors.grey)) : SizedBox(),
//                    Text("(08.00am - 08-30pm)",
//                        style: TextStyle(fontSize: 12.0, color: Colors.grey)),
                      (element.trainer != null && element.trainer != "") ? Row(
                        children: <Widget>[
                          new Icon(FontAwesomeIcons.userCircle,
                              size: 12.0, color: Colors.grey),
                          SizedBox(width: 1.9),
                          Text(
                            element.trainer,
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 12.0,
                            ),
                          )
                        ],
                      ) : SizedBox(),
                    ],
                  ),
                ),
              ],
            ),
          ],
        );
      }).toList(),
    );
  }

  Widget _buildRestListWidget(BuildContext context) {
    return Column(
      children: dailyChartModal.dailyChartRestList.map<Widget>((element) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 1.2,
              height: 15.0,
              color: Colors.grey,
              margin: EdgeInsets.only(left: 18.0),
            ),
            SizedBox(height: 4.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 4.0),
                Container(
                  width: 30.0,
                  height: 30.0,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    backgroundImage: AssetImage("assets/iconsetpng/bottle-feed.png"),
                    radius: 20.0,
                  ),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 1.7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      (element.restType != null && element.restType != "") ? Text(
                        element.restType,
                        style: TextStyle(fontSize: 15.0),
                      ) : SizedBox(), //, fontWeight: FontWeight.w500
                      (element.date != null && element.date != "") ? Text(element.date, style: TextStyle(fontSize: 12.0)) : SizedBox(),
                      (element.startTime != null && element.startTime != "") ? Text("(${element.startTime} - ${element.endTime ?? ''})",
                          style: TextStyle(fontSize: 12.0)) : SizedBox(),
                      (element.trainer != null && element.trainer != "") ? Row(
                        children: <Widget>[
                          new Icon(FontAwesomeIcons.userCircle,
                              size: 12.0, color: Colors.grey),
                          SizedBox(width: 1.9),
                          Text(
                            element.trainer,
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 12.0,
                            ),
                          )
                        ],
                      ) : SizedBox(),
                    ],
                  ),
                ),
              ],
            ),
          ],
        );
      }).toList(),
    );
  }

  Widget _buildSleepCheckListWidget(BuildContext context) {
    return Column(
      children: dailyChartModal.dailyChartSleepCheckList.map<Widget>((element) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 1.2,
              height: 15.0,
              color: Colors.grey,
              margin: EdgeInsets.only(left: 18.0),
            ),
            SizedBox(height: 4.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 4.0),
                Container(
                  width: 30.0,
                  height: 30.0,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    backgroundImage: AssetImage("assets/iconsetpng/crib-toy.png"),
                    radius: 20.0,
                  ),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 1.7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      (element.time != null && element.time != "") ? Text(
                        element.time,
                        style: TextStyle(fontSize: 15.0),
                      ) : SizedBox(), //, fontWeight: FontWeight.w500
                      (element.date != null && element.date != "") ? Text(element.date, style: TextStyle(fontSize: 12.0)) : SizedBox(),
                      (element.trainer != null && element.trainer != "") ? Row(
                        children: <Widget>[
                          new Icon(FontAwesomeIcons.userCircle,
                              size: 12.0, color: Colors.grey),
                          SizedBox(width: 1.9),
                          Text(
                            element.trainer,
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 12.0,
                            ),
                          )
                        ],
                      ) : SizedBox(),
                    ],
                  ),
                ),
              ],
            ),
          ],
        );
      }).toList(),
    );
  }

  Widget _buildSunScreenListWidget(BuildContext context) {
    return Column(
      children: dailyChartModal.dailyChartSunScreenList.map<Widget>((element) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 1.2,
              height: 15.0,
              color: Colors.grey,
              margin: EdgeInsets.only(left: 18.0),
            ),
            SizedBox(height: 4.0),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 4.0),
                Container(
                  width: 30.0,
                  height: 30.0,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    backgroundImage: AssetImage("assets/iconsetpng/alarm-clock.png"),
                    radius: 20.0,
                  ),
                ),
                SizedBox(
                  width: 8.0,
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 1.7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      (element.categoryName != null && element.categoryName != "") ? Text(
                        element.categoryName,
                        style: TextStyle(fontSize: 15.0),
                      ) : SizedBox(), //, fontWeight: FontWeight.w500
                      (element.date != null && element.date != "") ? Text(element.date, style: TextStyle(fontSize: 12.0)) : SizedBox(),
                      (element.educatorName != null && element.educatorName != "") ? Row(
                        children: <Widget>[
                          new Icon(FontAwesomeIcons.userCircle,
                              size: 12.0, color: Colors.grey),
                          SizedBox(width: 1.9),
                          Text(
                            element.educatorName,
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 12.0,
                            ),
                          )
                        ],
                      ) : SizedBox(),
                    ],
                  ),
                ),
              ],
            ),
          ],
        );
      }).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _buildFoodListWidget(context),
        _buildNappyListWidget(context),
        _buildBottleFeedListWidget(context),
        _buildRestListWidget(context),
        _buildSleepCheckListWidget(context),
        _buildSunScreenListWidget(context),
      ],
    );
  }
}