import 'package:flutter/material.dart';

//class Dailysummary extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//
//    return new Center(
//      child: new Text("Dailysummary"),
//    );
//  }
//
//}

//class Dailysummary extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Container(
////      margin: EdgeInsets.symmetric(vertical: 10.0),
//        padding: EdgeInsets.symmetric(horizontal:10.0 ),
//        height: 50.0,
//          child: ListView(
//            scrollDirection: Axis.horizontal,
//            children: <Widget>[
//              Container(
//                width: 60.0,
//                color: Colors.red,
//              ),
//              Container(
//                width: 60.0,
//                color: Colors.blue,
//              ),
//              Container(
//                width: 60.0,
//                color: Colors.green,
//              ),
//              Container(
//                width: 60.0,
//                color: Colors.yellow,
//              ),
//              Container(
//                width: 60.0,
//                color: Colors.orange,
//              ),
//            ],
//          ),
//        );
//  }
//}

class DynamicTabContent {
  IconData icon;
  String tooTip;
  String childimage;
  DynamicTabContent.name(this.icon, this.tooTip, this.childimage);
}

class Dailysummary_ extends StatefulWidget {
  @override
  _MainState createState() => new _MainState();
}

class _MainState extends State<Dailysummary_> with TickerProviderStateMixin {
  List<DynamicTabContent> myList = new List();

  TabController _cardController;

  TabPageSelector _tabPageSelector;

  @override
  void initState() {
    super.initState();

    myList.add(new DynamicTabContent.name(Icons.child_care, "child 1","assets/iconsetpng/newspaper2.png"));
    myList.add(new DynamicTabContent.name(Icons.child_care, "child 2","assets/iconsetpng/newspaper2.png"));
    myList.add(new DynamicTabContent.name(Icons.child_care, "child 3","assets/iconsetpng/newspaper2.png"));

    _cardController = new TabController(vsync: this, length: myList.length);
    _tabPageSelector = new TabPageSelector(controller: _cardController);
  }

  @override
  void dispose() {
    _cardController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: new AppBar(
          actions: <Widget>[
            new Padding(
              padding: const EdgeInsets.all(1.0),
              child: new IconButton(
                icon: const Icon(
                  Icons.add,
                  size: 30.0,
                  color: Colors.white,
                ),
//                tooltip: 'Add Tabs',
                onPressed: () {
                  List<DynamicTabContent> tempList = new List();

                  myList.forEach((dynamicContent) {
                    tempList.add(dynamicContent);
                  });

                  setState(() {
                    myList.clear();
                  });

                  if (tempList.length % 2 == 0) {
                    myList.add(new DynamicTabContent.name(Icons.child_care, "child 4","assets/iconsetpng/newspaper2.png"));
                  } else {
                    myList.add(new DynamicTabContent.name(Icons.child_care, "child 5","assets/iconsetpng/newspaper2.png"));
                  }

                  tempList.forEach((dynamicContent) {
                    myList.add(dynamicContent);
                  });

                  setState(() {
                    _cardController = new TabController(vsync: this, length: myList.length);
                    _tabPageSelector = new TabPageSelector(controller: _cardController);
                  });
                },
              ),
            ),
          ],
//          title: new Text("Title Here"),
          bottom: new PreferredSize(
              preferredSize: const Size.fromHeight(10.0),
              child: new Theme(
                data: Theme.of(context).copyWith(accentColor: Colors.grey),
                child: myList.isEmpty
                    ? new Container(
                  height: 30.0,
                )
                    : new Container(
                  height: 30.0,
                  alignment: Alignment.center,
                  child: _tabPageSelector,
                ),
              ))),
      body: new TabBarView(
        controller: _cardController,
        children: myList.isEmpty
            ? <Widget>[]
            : myList.map((dynamicContent) {
          return new Card(
            child: new Container(
                height: 450.0,
                width: 300.0,
                child: new IconButton(
                  icon: new Icon(dynamicContent.icon, size: 100.0),
                  tooltip: dynamicContent.tooTip,
                  onPressed: null,
                )),
          );
        }).toList(),
      ),
    );
  }
}