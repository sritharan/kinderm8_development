import 'dart:async';
import 'dart:convert';
import 'package:kinderm8/Theme.dart' as Kinderm8Theme;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/calendar/events.dart';
import 'package:kinderm8/pages/commondrawer.dart';
import 'package:kinderm8/pages/home/childdatacontent.dart';
import 'package:http/http.dart' as http;
import 'package:kinderm8/pages/home/dailysummary.dart';
import 'package:kinderm8/pages/home/dailysummarydata.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/newsfeed/newsfeedlist.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';

class Dailysummary extends StatefulWidget {
  @override
  _CollapsingTabState createState() => new _CollapsingTabState();
}

class _CollapsingTabState extends State<Dailysummary> implements HomePageContract{
  ScrollController scrollController;
  var childid, appuser, jwt, id, clientId;
  var email, password, client_id, center;
  var childrendata,image;
  var deviceSize;
  bool isLoading;
  List dailysummary_data;
  bool load = true;
  HomePagePresenter _presenter;
  final List<City> _allCities = City.allCities();
  TabController _tabController;

  Widget _buildActions() {
    Widget profile = new GestureDetector(
      onTap: () => showProfile(),
      child: new Container(
        height: 30.0,
        width: 45.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.grey,
          image: new DecorationImage(
            image: new ExactAssetImage("assets/logo.png"),
            fit: BoxFit.cover,
          ),
          border: Border.all(color: Colors.black, width: 2.0),
        ),
      ),
    );

    double scale;
    if (scrollController.hasClients) {
      scale = scrollController.offset / 300;
      scale = scale * 2;
      if (scale > 1) {
        scale = 1.0;
      }
    } else {
      scale = 0.0;
    }

    return new Transform(
      transform: new Matrix4.identity()..scale(scale, scale),
      alignment: Alignment.center,
      child: profile,
    );
  }

  _CollapsingTabState(){
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();

  }
  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    scrollController = new ScrollController();
    scrollController.addListener(() => setState(() {}));
    fetchDailysummaryData(10);
  }
  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Colors.white,
    containerColor: Kinderm8Theme.Colors.progresscontent,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  void _openchildrenpopup() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return CommonDrawer();
        },
        fullscreenDialog: true));
  }

  Future<DailysummaryUI> fetchDailysummaryData(int childid) async {
    print(childid);

    print('Going to get summary data for this id : >>>>> $widget.childid');
    ///data from GET method
    print("dailysummary data fetched $childid , $id, $client_id");

    String _eventsUrl =
        'http://13.55.4.100:7070/v2.1.1/getinitialload?userid=10&clientid=$client_id ';
    print('API call : $_eventsUrl');
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_eventsUrl, headers: headers).then((response) {
      var dailysummaryData;
      try {
        dailysummaryData = json.decode(response.body);
        print(dailysummaryData.length);
        print('res get ${response.body}');
        print('events Data $dailysummaryData');
      } on FormatException catch (e) {
        print("That string didn't look like Json.");
      } on NoSuchMethodError catch (e) {
        print('That string was null!');
      }


      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        print(isLoading);
        isLoading = false;
        print(isLoading);
//        if (childid != null) {
          setState(() {
            load = false;
            this.dailysummary_data = dailysummaryData;
            print('<<<<<<<<<< dailysummaryData >>>>>>>>>>>>> $dailysummaryData');
          });
//        }
//        else {
//          setState(() {
//            load = false;
//            data.addAll(eventsData);
//          });
//        }
//        k = data.length;
      } else if (response.statusCode == 500 &&
          dailysummaryData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      } else {
        fetchDailysummaryData(childrendata);
      }
    });
    return null;
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$id&clientid=$client_id';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);

      } on FormatException catch (e) {
        print("That string didn't look like Json.");
      } on NoSuchMethodError catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      if (childid != null) {
        fetchDailysummaryData(childid);
      } else {
        fetchDailysummaryData(childid);
      }
    });
  }

  @override
  Widget build(BuildContext ctxt) {
    return new MaterialApp(
        debugShowCheckedModeBanner: false,
        home: DefaultTabController(
          length: childrendata?.length ?? 0,
          child: new Scaffold(
            appBar: new AppBar(
              title: new Text("Daily Summary"),
              centerTitle: true,
              backgroundColor: Kinderm8Theme.Colors.appseconderycolour,
              bottom: new PreferredSize(
                preferredSize: new Size(0.0, 50.0),
                child: Container(
                  margin: const EdgeInsets.only(bottom:10.0),
                  child: new TabBar(
                      unselectedLabelColor: Colors.blue,
                      indicatorSize: TabBarIndicatorSize.label,
//                  indicatorPadding: EdgeInsets.all(15.0),
                      indicatorColor: Colors.white,
                      labelColor: Colors.black87,
                      isScrollable: true,
                      tabs: List<Widget>.generate(childrendata?.length ?? 0, (int index){

                    print("children $childrendata");
                    return new Tab(
                      child: Container(
                        height: 100.0,
                        child: Column(
                          children: <Widget>[
                            new Image(
                              image: NetworkImage('https://d212imxpbiy5j1.cloudfront.net/${childrendata[index]["image"]}'),
                              height: 40.0,
                              width: 40.0,
                              fit: BoxFit.cover,
                            ),
//                            new Text(
//                              "${children[index]["firstname"]}",
//                              style: TextStyle(
//                                  color: Colors.white,
//                                  fontSize: 12.00,
//                              ),
//                            ),
                          ],
                        ),
//                    padding: EdgeInsets.symmetric(),
                      ),
                    );

                  }),

            ),
                ),
              ),
          ),
          body:new TabBarView(
              controller: _tabController, children: <Widget>[
               new Scaffold(
                body: new Padding(
                    padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                    child: getHomePageBody(context))
            )
          ]
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.menu),
            backgroundColor: Kinderm8Theme.Colors.appcolour,
            onPressed: () {
              _openchildrenpopup();
//          ChildrenDrawer();
//          new Dailysummary();
//          showModalBottomSheet<Null>(
//            context: context,
//            builder: (BuildContext context) => const _DemoDrawer(),
//          );
            },
          ),
          bottomNavigationBar: BottomAppBar(
            shape: CircularNotchedRectangle(),
            notchMargin: 4.0,
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new FlatButton(
                  onPressed: () {
//                Navigator.pushNamed(context, "/home");
                    Navigator.push(context, new MaterialPageRoute(
                        builder: (context) =>
                        new Dailysummary())
                    );
                  },
                  padding: EdgeInsets.all(10.0),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Icon(Icons.format_list_bulleted),
//                  new Text("Dailysummary")
                    ],
                  ),
                ),
                new FlatButton(
                  onPressed: () {
//                Navigator.pushNamed(context, "/newsfeed");
                    Navigator.push(context, new MaterialPageRoute(
                        builder: (context) =>
                        new NewsFeedList())
                    );
                  },
                  padding: EdgeInsets.all(10.0),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Icon(Icons.chrome_reader_mode),
//                  new Text("Newsfeed")
                    ],
                  ),
                ),
                new FlatButton(
                  onPressed: () {
                    EventsData();
                    Navigator.pushNamed(context, "/calendar");
                    Navigator.push(context, new MaterialPageRoute(
                        builder: (context) =>
                        new EventsData())
                    );
                  },
                  padding: EdgeInsets.all(10.0),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Icon(Icons.date_range),
//                  new Text("Calendar")
                    ],
                  ),
                ),
                new FlatButton(
                  onPressed: () {
//                CommonDrawer();
//                Navigator.push(context, new MaterialPageRoute(
//                    builder: (context) =>
//                    new EventsList())
//                );
                  },
                  padding: EdgeInsets.all(10.0),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Icon(Icons.free_breakfast),
//                  new Text("Family ..")
                    ],
                  ),
                ),
//            IconButton(
//              icon: Icon(Icons.format_list_bulleted),
//              onPressed: () {
//                Dailysummary();
//              },
//            ),
//            IconButton(
//                icon: Icon(Icons.chrome_reader_mode),
//                onPressed: () {
//                  NewsfeedList();
//                },
//            ),
//            IconButton(
//              icon: Icon(Icons.date_range),
//              onPressed: () {
//                EventsList();
////                showModalBottomSheet<Null>(
////                  context: context,
////                  builder: (BuildContext context) => const _DemoDrawer(),
////                );
//              },
//            ),
//            IconButton(
//              icon: Icon(Icons.menu),
//              onPressed: () {
//                CommonDrawer();
////                showModalBottomSheet<Null>(
////                  context: context,
////                  builder: (BuildContext context) => CommonDrawer(),
////                );
//              },
//            ),
              ],
            ),
          ),
//          body: new TabBarView(
//              children: List<Widget>.generate(
//              children.length,
//                  (int index){
//              print('Insied TabBarView ------- $children');
////              return new Text("${children[index]["firstname"]}",
////                  style: TextStyle(
////                  color: Colors.blueAccent, fontSize: 24.00)
////              );
////            return new TabScreen('${children[index]["firstname"]}');
//            return new TabscreenUI();
////            return new Scaffold(
////                body: new Padding(
////                    padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
////                    child: getHomePageBody(context))
////            );
//            // Card design
////            return new Card(
////              child :new Column(
////                mainAxisAlignment: MainAxisAlignment.start,
////                mainAxisSize: MainAxisSize.min,
////                crossAxisAlignment: CrossAxisAlignment.stretch,
////                children: <Widget>[
////                  Padding(
////                    padding: const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 16.0),
////                    child: Row(
////                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
////                      children: <Widget>[
////                        Row(
////                          children: <Widget>[
////                            new Container(
////                              height: 40.0,
////                              width: 40.0,
////                              decoration: new BoxDecoration(
//////                                        shape: BoxShape.circle,
////                                image: new DecorationImage(
////                                    fit: BoxFit.fill,
////                                    image: new AssetImage(
////                                        "assets/iconsetpng/cubes.png")),
////                              ),
////                            ),
////                            new SizedBox(
////                              width: 10.0,
////                            ),
////                            new Text(
////                              'Journal Title',
////                              style: Theme.of(context).textTheme.headline.copyWith(color: Colors.black54,fontSize: 26.0),
////                            ),
////                          ],
////                        ),
////                        new IconButton(
////                          icon: Icon(Icons.more_vert),
////                          onPressed: null,
////                        )
////                      ],
////                    ),
////                  ),
////                  Padding(
////                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
////                    child: Text(
////                      "Liked by kinderm8",
////                      style: TextStyle(fontWeight: FontWeight.bold),
////                    ),
////                  ),
////                  Padding(
////                    padding: const EdgeInsets.fromLTRB(16.0, 16.0, 0.0, 8.0),
////                    child: Row(
////                      mainAxisAlignment: MainAxisAlignment.start,
////                      children: <Widget>[
//////                                new Container(
//////                                  height: 40.0,
//////                                  width: 40.0,
//////                                  decoration: new BoxDecoration(
//////                                    shape: BoxShape.circle,
//////                                    image: new DecorationImage(
//////                                        fit: BoxFit.fill,
//////                                        image: new AssetImage(
//////                                            "assets/iconsetpng/cubes.png")
//////                                    ),
//////                                  ),
//////                                ),
////                        new SizedBox(
////                          width: 10.0,
////                        ),
//////                                Expanded(
//////                                  child: new TextField(
//////                                    decoration: new InputDecoration(
//////                                      border: InputBorder.none,
//////                                      hintText: "Add a comment...",
//////                                    ),
//////                                  ),
//////                                ),
////                      ],
////                    ),
////                  ),
////                  Padding(
////                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
////                    child:
////                    Text("1 Day Ago", style: TextStyle(color: Colors.grey)),
////                  ),
////                  Padding(
////                    padding: const EdgeInsets.all(5.0),
////                  ),
////                ],
////              ),
////            );
//            // End of single card
//
//              }
//
//          ),
        )
//    )
    )
    );
  }

  getHomePageBody(BuildContext context) {
    print('getHomePageBody $context');
    return ListView.builder(
      itemCount: childrendata?.length ?? 0,
      itemBuilder: _getItemUI,
//      itemBuilder: (context, i) {
//        print(this.dailysummary_data[i]);
//        final data1 = this.dailysummary_data[i];
//           print('loading');
//            return DailysummaryData(data1, i,children['id']);
//      },
      padding: EdgeInsets.all(0.0),
    );
  }
  // First Attempt
  Widget _getItemUI(BuildContext context, int index) {
    return new Card(
              child :new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            new Container(
                              height: 40.0,
                              width: 40.0,
                              decoration: new BoxDecoration(
//                                        shape: BoxShape.circle,
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new AssetImage(
                                        "assets/iconsetpng/cubes.png")),
                              ),
                            ),
                            new SizedBox(
                              width: 10.0,
                            ),
                            new Text(
                              'Journal Title',
                              style: Theme.of(context).textTheme.headline.copyWith(color: Colors.black54,fontSize: 26.0),
                            ),
                          ],
                        ),
                        new IconButton(
                          icon: Icon(Icons.more_vert),
                          onPressed: null,
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Text(
                      "Liked by kinderm8",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(16.0, 16.0, 0.0, 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
//                                new Container(
//                                  height: 40.0,
//                                  width: 40.0,
//                                  decoration: new BoxDecoration(
//                                    shape: BoxShape.circle,
//                                    image: new DecorationImage(
//                                        fit: BoxFit.fill,
//                                        image: new AssetImage(
//                                            "assets/iconsetpng/cubes.png")
//                                    ),
//                                  ),
//                                ),
                        new SizedBox(
                          width: 10.0,
                        ),
//                                Expanded(
//                                  child: new TextField(
//                                    decoration: new InputDecoration(
//                                      border: InputBorder.none,
//                                      hintText: "Add a comment...",
//                                    ),
//                                  ),
//                                ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child:
                    Text("1 Day Ago", style: TextStyle(color: Colors.grey)),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                  ),
                ],
              ),
            );
  }

  @override
  Widget build_(BuildContext context) {
    var flexibleSpaceWidget = new SliverAppBar(
      expandedHeight: 200.0,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Text("Kinderm8 message",
              style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
              )
          ),
          background: Image.asset(
            "assets/Kinderm8.png",
          )),
      actions: <Widget>[
        new Padding(
          padding: EdgeInsets.all(5.0),
          child: _buildActions(),
        ),
      ],
    );

    return Scaffold(
        appBar: new AppBar(
        title: new Text("Dailysummary",
          style: TextStyle(
            color: Colors.white,
          )
        ),
        centerTitle: true,
      ),
      drawer: CommonDrawer(),
      body: new DefaultTabController(
        length: childrendata.length,
        child: NestedScrollView(
          controller: scrollController,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
//              flexibleSpaceWidget, // Not used in furture we can popup any message
              SliverPersistentHeader(
                delegate: _SliverAppBarDelegate(
                  TabBar(
                    unselectedLabelColor: Colors.blue,
                    indicatorSize: TabBarIndicatorSize.label,
                    indicatorPadding: EdgeInsets.all(5.0),
                    indicatorColor: Colors.red,
                    labelColor: Colors.black87,
                    tabs: [
                      Tab(
                        icon: Icon(Icons.child_care),
                        text: "Child 1",
                      ),
                      Tab(
                        child: Container(
                          margin: EdgeInsets.all(5.0),
                          child: Column(
                            children: <Widget>[
                              new Image(
                                image: AssetImage(
                                    "assets/iconsetpng/newspaper2.png"),
                                height: 20.0,
                                width: 20.0,
                              ),
                              new Text(
                                "Child 2",
                                style: TextStyle(
                                    color: Colors.blueAccent, fontSize: 14.00),
                              ),
                            ],
                          ),
                          padding: EdgeInsets.symmetric(),
                        ),
                      ),
                      Tab(icon: Icon(Icons.child_care), text: "Child 3"),
                      Tab(icon: Icon(Icons.child_care), text: "Child 4"),
                      Tab(icon: Icon(Icons.child_care), text: "Child 5"),
                    ],
                  ),
                ),
                pinned: true,
              ),
            ];
          },
          body: new TabBarView(
            children: <Widget>[
              Container(
                child: new ListView.builder(
                  padding: new EdgeInsets.all(8.0),
                  itemCount: 7,
                  itemBuilder: (BuildContext context, int index){

                    return new Card(
                      child :new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 16.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    new Container(
                                      height: 40.0,
                                      width: 40.0,
                                      decoration: new BoxDecoration(
//                                        shape: BoxShape.circle,
                                        image: new DecorationImage(
                                            fit: BoxFit.fill,
                                            image: new AssetImage(
                                                "assets/iconsetpng/cubes.png")),
                                      ),
                                    ),
                                    new SizedBox(
                                      width: 10.0,
                                    ),
                                    new Text(
                                      'Journal Title',
                                      style: Theme.of(context).textTheme.headline.copyWith(color: Colors.black54,fontSize: 26.0),
                                    ),
                                  ],
                                ),
                                new IconButton(
                                  icon: Icon(Icons.more_vert),
                                  onPressed: null,
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Text(
                              "Liked by kinderm8",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(16.0, 16.0, 0.0, 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
//                                new Container(
//                                  height: 40.0,
//                                  width: 40.0,
//                                  decoration: new BoxDecoration(
//                                    shape: BoxShape.circle,
//                                    image: new DecorationImage(
//                                        fit: BoxFit.fill,
//                                        image: new AssetImage(
//                                            "assets/iconsetpng/cubes.png")
//                                    ),
//                                  ),
//                                ),
                                new SizedBox(
                                  width: 10.0,
                                ),
//                                Expanded(
//                                  child: new TextField(
//                                    decoration: new InputDecoration(
//                                      border: InputBorder.none,
//                                      hintText: "Add a comment...",
//                                    ),
//                                  ),
//                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0),
                            child:
                            Text("1 Day Ago", style: TextStyle(color: Colors.grey)),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                          ),
                        ],
                      ),
                    );
                  },
                ),
//                  child :ChatItemScreen()
//                child: _buildContent(context),
              ),
//              new TabScreen("Child 1"),
              new TabScreen("Child 2"),
              new TabScreen("Child 3"),
              new TabScreen("Child 4"),
              new TabScreen("Child 5"),
            ],
          ),
        ),
      ),
    );
  }

  showProfile() {
    Navigator.pushNamed(context, '/profile');
  }

  @override
  void onDisplayUserInfo(User user) {
    email = user.email;
    password = user.password;
    client_id = user.client_id;
    center = user.center;
    setState(() {
      try {
        final parsed = json.decode(center);
        var details = parsed[0];
        var users = details["user"];
        jwt = details["jwt"];
        id = users["id"];
        client_id = users["client_id"];
        childrendata = details["children"];

//        print(jwt);
        image=childrendata[0]["image"];
//        print(id);

      } on FormatException catch (e) {
        print("That string didn't look like Json.");
      } on NoSuchMethodError catch (e) {
        print('That string was null!');
      }
    });
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }
}

Widget _buildContent(context) {
  final String title = 'Journal';
  final String imageAsset = 'assets/iconsetpng/cubes.png';
  final String imageAssetPackage = 'flutter_gallery_assets';
//  return ListView.builder(
//      itemExtent: 100.0,
//      itemCount: 7,
//      itemBuilder: (BuildContext content, int index) {
//        return Container(
//          padding: EdgeInsets.all(5.0),
//          child: Material(
//            elevation: 4.0,
//            borderRadius: BorderRadius.circular(3.0),
//            color: Colors.black12,
//            child: Center(
//              child: Text('Journal'),
//            ),
//          ),
//        );
//      });
  return new Card(

    child: new Row(
//      padding: const EdgeInsets.all(2.0),
//      child: new Row(
      crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new Align(
            alignment: 'H' == 'H'
                ? Alignment.centerLeft
                : Alignment.centerRight,
//            child: new CircleAvatar(child: new Image(
//              image: AssetImage("assets/iconsetpng/cubes.png"),
//              height: 30.0,
//              width: 32.0,
//            ),
//            ),
          ),
          new Expanded(
            child: new Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
              child: new DefaultTextStyle(
                softWrap: false,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.subhead,
                child: new Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                 new Image(
                image: AssetImage("assets/iconsetpng/cubes.png"),
                height: 40.0,
                width: 40.0,
              ),
//                    Container(
//                      child: Column(
//                        children: <Widget>[
//                      new Text(
//                      'Journal Title',
//                        style: Theme.of(context).textTheme.subhead.copyWith(color: Colors.black54,fontSize: 26.0)),
//                        ],
//                      ),
//                    ),
                    // three line description
                    new Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: new Text(
                        'Journal Title',
                        style: Theme.of(context).textTheme.headline.copyWith(color: Colors.black54,fontSize: 26.0),
                      ),
                    ),
//                    new Text('Subcontent'),
//                    new Text('Long content'),
                  ],
                ),
              ),
            ),
          ),

//          new Row(
//            children: <Widget>[
//              Container(
//                child:new Container(
//                  padding: new EdgeInsets.all(18.0),
//                  child: new Row(
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    children: <Widget>[
//                      new Text("Name",
//                        style: new TextStyle(
//                          color: Colors.black,
//                          fontWeight: FontWeight.w600,
//                          fontSize: 16.0,
//                        ),
//                      ),
//                      new Text("Hi whatsp?",
//                        style: new TextStyle(color: Colors.grey
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              )
//            ],
//          ),
//          new Column(
//            children: <Widget>[
//              new Text("9:50",
//                style: new TextStyle(
//                    color: Colors.lightGreen,
//                    fontSize: 12.0),),
//              new CircleAvatar(
//                backgroundColor: Colors.lightGreen,
//                radius: 10.0,
//                child: new Text("2",
//                  style: new TextStyle(color: Colors.white,
//                      fontSize: 12.0),),
//              )
//            ],
//          )
        ],
//      ),
    ),
  );

}

class ChatItemScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return new Container(
//      appBar: new AppBar(
//        title: new Text("Chat Item"),
//        backgroundColor: Colors.lightGreen,
//      ),
      child: new ChatItem(), //calling chat_item.dart
    );
  }
}

class ChatItem extends StatelessWidget{

  final leftSection = new Container();
  final middleSection = new Container();
  final rightSection = new Container();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        child: new Row(
          children: <Widget>[
            leftSection,
            middleSection,
            rightSection

          ],
        ),
      ),
    );

  }
}
final leftSection = new Container(
  child: new CircleAvatar(
    backgroundImage: new NetworkImage('assets/iconsetpng/cubes.png'),
    backgroundColor: Colors.lightGreen,
    radius: 24.0,
  ),
);

final middleSection = new Expanded(
  child: new Container(
    padding: new EdgeInsets.only(left: 8.0),
    child: new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        new Text("Name",
          style: new TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w600,
            fontSize: 16.0,
          ),),
        new Text("Hi whatsp?", style:
        new TextStyle(color: Colors.grey),),
      ],
    ),
  ),
);

final rightSection = new Container(
  child: new Column(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    children: <Widget>[
      new Text("9:50",
        style: new TextStyle(
            color: Colors.lightGreen,
            fontSize: 12.0),),
      new CircleAvatar(
        backgroundColor: Colors.lightGreen,
        radius: 10.0,
        child: new Text("2",
          style: new TextStyle(color: Colors.white,
              fontSize: 12.0),),
      )
    ],
  ),
);

//class Photo {
//  final int albumId;
//  final int id;
//  final String title;
//  final String url;
//  final String thumbnailUrl;
//
//  Photo({this.albumId, this.id, this.title, this.url, this.thumbnailUrl});
//
//  factory Photo.fromJson(Map<String, dynamic> json) {
//    return Photo(
//      albumId: json['albumId'] as int,
//      id: json['id'] as int,
//      title: json['title'] as String,
//      url: json['url'] as String,
//      thumbnailUrl: json['thumbnailUrl'] as String,
//    );
//  }
//}
//
//Future<List<Photo>> fetchPhotos(http.Client client) async {
//  final response =
//  await client.get('https://jsonplaceholder.typicode.com/photos');
//
//  // Use the compute function to run parsePhotos in a separate isolate
//  return compute(parsePhotos, response.body);
//}
//
//class PhotosList extends StatelessWidget {
//  final List<Photo> photos;
//
//  PhotosList({Key key, this.photos}) : super(key: key);
//
//  @override
//  Widget build(BuildContext context) {
//    return GridView.builder(
//      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//        crossAxisCount: 2,
//      ),
//      itemCount: photos.length,
//      itemBuilder: (context, index) {
//        return Image.network(photos[index].thumbnailUrl);
//      },
//    );
//  }
//}
//
//@override
//Widget build(BuildContext context) {
//  return Scaffold(
//    appBar: AppBar(
//      title: Text('title'),
//    ),
//    body: FutureBuilder<List<Photo>>(
//      future: fetchPhotos(http.Client()),
//      builder: (context, snapshot) {
//        if (snapshot.hasError) print(snapshot.error);
//
//        return snapshot.hasData
//            ? PhotosList(photos: snapshot.data)
//            : Center(child: CircularProgressIndicator());
//      },
//    ),
//  );
//}
//
//// A function that will convert a response body into a List<Photo>
//List<Photo> parsePhotos(String responseBody) {
//  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
//
//  return parsed.map<Photo>((json) => Photo.fromJson(json)).toList();
//}

class City {

  //--- Name Of City
  final String name;
  //-- image
  final String image;
  //--- population
  final String population;
  //--- country
  final String country;

  City({this.name,this.country,this.population,this.image});

  static List<City> allCities()
  {
    var lstOfCities = new List<City>();

    lstOfCities.add(new City(name:"Delhi",country: "India",population: "19 mill",image: "delhi.png"));
    lstOfCities.add(new City(name:"London",country: "Britain",population: "8 mill",image: "london.png"));
    lstOfCities.add(new City(name:"Vancouver",country: "Canada",population: "2.4 mill",image: "vancouver.png"));
    lstOfCities.add(new City(name:"New York",country: "USA",population: "8.1 mill",image: "newyork.png"));
    lstOfCities.add(new City(name:"Paris",country: "France",population: "2.2 mill",image: "paris.png"));
    lstOfCities.add(new City(name:"Berlin",country: "Germany",population: "3.7 mill",image: "berlin.png"));
    return lstOfCities;
  }
}
class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar _tabBar;

  _SliverAppBarDelegate(this._tabBar);

  @override
  double get minExtent => _tabBar.preferredSize.height;

  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}

class TabscreenUI extends StatelessWidget{
  String data;
  int i;
  int childid;

  @override
  Widget build(BuildContext context) {

    print('dailysummarydata >> data: $data');
    return new Column(
      children: <Widget>[
     new Card(
      child :new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  new Container(
                    height: 40.0,
                    width: 40.0,
                    decoration: new BoxDecoration(
//                                        shape: BoxShape.circle,
                      image: new DecorationImage(
                          fit: BoxFit.fill,
                          image: new AssetImage(
                              "assets/iconsetpng/cubes.png")),
                    ),
                  ),
                  new SizedBox(
                    width: 10.0,
                  ),
                  new Text(
                    'Journal Title',
                    style: Theme.of(context).textTheme.headline.copyWith(color: Colors.black54,fontSize: 26.0),
                  ),
                ],
              ),
              new IconButton(
                icon: Icon(Icons.more_vert),
                onPressed: null,
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Text(
            "Liked by kinderm8",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 16.0, 0.0, 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
//                                new Container(
//                                  height: 40.0,
//                                  width: 40.0,
//                                  decoration: new BoxDecoration(
//                                    shape: BoxShape.circle,
//                                    image: new DecorationImage(
//                                        fit: BoxFit.fill,
//                                        image: new AssetImage(
//                                            "assets/iconsetpng/cubes.png")
//                                    ),
//                                  ),
//                                ),
              new SizedBox(
                width: 10.0,
              ),
//                                Expanded(
//                                  child: new TextField(
//                                    decoration: new InputDecoration(
//                                      border: InputBorder.none,
//                                      hintText: "Add a comment...",
//                                    ),
//                                  ),
//                                ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child:
          Text("1 Day Ago", style: TextStyle(color: Colors.grey)),
        ),
        Padding(
          padding: const EdgeInsets.all(5.0),
        ),
      ],
    ),
    ),
      ],

    );
  }

}
class TabScreen extends StatelessWidget {
  TabScreen(this.listType);
  final String listType;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text(
              listType,
              style: Theme.of(context).textTheme.display2,
            ),
          ],
        ),
      ),
    );
  }
}

//class DailysummaryUI_ {
//
//  String nodata;
//  String empty_state_message;
//  // Journal
//  String id;
//  String journal_title;
//  String whathappeneddata;
//  // Observation
//  String journey_title;
//  String observationdata;
//  //DailyChartFoodList
//  String MenuCategory;
//  String FoodMenu;
//  String Servetype;
//  //DailyChartNappyList
//  String catagory;
//  String nappytrainer;
//  String nappytime;
//  //DailyChartBottlefeedList
//  String bottle_qty;
//  String bottletype;
//  String bottletrainer;
//  String bottle_time;
//  //DailyChartSleepCheckList
//  String sleeptime;
//  String sleepchecktrainer;
//  //Childnote
//  String notes;
//  //Learningstory
//  String learningstorytitle;
//  String educator;
//  String date;
//  //Medication
//  String medicationtitle;
//  //Gallery
//  String album;
//  String caption;
//  DailysummaryUI_(
//
//      // Empty
//      this.nodata, this.empty_state_message ,
//      // Journal
//      this.journal_title, this.whathappeneddata,
//      // Observation
//      this.journey_title, this.observationdata,
//      //DailyChart
//      this.MenuCategory, this.FoodMenu, this.Servetype,
//      this.catagory, this.nappytrainer, this.nappytime,
//      this.bottle_qty, this.bottletype,
//      this.bottletrainer, this.bottle_time,
//      this.sleeptime, this.sleepchecktrainer,
//      //Childnote
//      this.notes,
//      //Learningstory
//      this.learningstorytitle, this.educator, this.date,
//      //Medication
//      this.medicationtitle,
//      //Gallery
//      this.album,this.caption
//
//      );
//}
//
//class Emptyview {
//  String nodata;
//  String empty_state_message;
//
//  Emptyview(this.nodata, this.empty_state_message);
//}
//
//class Journal {
//  var id;
//  String journal_title;
//  String whathappeneddata;
//
//  Journal(this.id, this.journal_title, this.whathappeneddata);
//}
//
//class Observation {
//  var id;
//  String journey_title;
//  String observationdata;
//
//  Observation(this.id, this.journey_title, this.observationdata);
//}
//
//class DailyChartFoodList {
//  var id;
//  String MenuCategory;
//  String FoodMenu;
//  String Servetype;
//
//  DailyChartFoodList(this.id, this.MenuCategory, this.FoodMenu, this.Servetype);
//}
//
//class DailyChartNappyList {
//  var id;
//  String catagory;
//  String trainer;
//  String time;
//
//  DailyChartNappyList(this.id, this.catagory, this.trainer, this.time);
//}
//
//class DailyChartBottlefeedList {
//  var id;
//  String bottle_qty;
//  String bottletype;
//  String trainer;
//  String bottle_time;
//
//  DailyChartBottlefeedList(this.id, this.bottle_qty, this.bottletype,
//      this.trainer, this.bottle_time);
//}
//
//class DailyChartSleepCheckList {
//  var id;
//  String time;
//  String trainer;
//
//  DailyChartSleepCheckList(this.id, this.time, this.trainer);
//}
//
//class Childnote {
//  var id;
//  String notes;
//
//  Childnote(this.notes);
//}
//
//class Learningstory {
//  var id;
//  String title;
//  String educator;
//  String date;
//
//  Learningstory(this.id, this.title, this.educator, this.date);
//}
//
//class Medication {
//  var id;
//  String title;
//
//  Medication(this.id, this.title);
//}
//
//class Gallery {
//  var id;
//  String album;
//  String caption;
//
//  Gallery(this.id, this.album,this.caption);
//}
