import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/components/googlewebview_url.dart';
import 'dart:async';
import 'package:kinderm8/Theme.dart' as kinderm8Theme;
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class NewsletterData extends StatefulWidget {
  final newsletterdata;
  NewsletterData(this.newsletterdata);
  @override
  NewsletterState createState() => NewsletterState(newsletterdata);
}

class NewsletterState extends State<NewsletterData> {
  final newsletterdata;
  final flutterWebviewPlugin = new FlutterWebviewPlugin();
  NewsletterState(this.newsletterdata) {}


  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  openBrowserTabInWebview(String url) async {
    return new WebviewScaffold(
      url: url,
      appBar: new AppBar(
        title: const Text('Widget webview'),
      ),
      withZoom: true,
      withLocalStorage: true,
    );
//    flutterWebviewPlugin.launch(url, hidden: true);
  }


  @override
  Widget build(BuildContext context) {
    var parsedDate = DateTime.parse(newsletterdata["created_at"]);
    var date = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);
    var sender = newsletterdata["sender"];
    var fileurlencode = "https://docs.google.com/gview?embedded=true&url="+newsletterdata['fileurl'];



    var center = new Center(
        child: Column(
          children: <Widget>[
            new ListTile(
              leading: CircleAvatar(
                child: Container(
                  height: 45.0,
//                  padding: EdgeInsets.all(5.0),
                  margin: const EdgeInsets.all(10.0),
                  decoration: new BoxDecoration(
                    color: kinderm8Theme.Colors.appBarGradientEnd,
                    image: new DecorationImage(
                      image: new ExactAssetImage("assets/iconsetpng/newspaper.png"),
                      fit: BoxFit.cover,
                    ),

                  ),

                ),
                backgroundColor: kinderm8Theme.Colors.appBarGradientEnd,
              ),
              title: Container(
                padding: EdgeInsets.only(top: 5.0),
                child: new Text(
                  newsletterdata['filename'],
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: new TextStyle(
                      fontWeight: FontWeight.w500, fontSize: 14.0),
                ),
              ),
              subtitle: Container(
                padding: EdgeInsets.only(top: 5.0),
                child: new Text(
                  date,
                  style: new TextStyle(
                      fontWeight: FontWeight.w500, fontSize: 10.0),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context, new MaterialPageRoute(builder: (context) => new WebUrlPreview(fileurlencode, fileUrl: newsletterdata['fileurl'],)));
              },
            ),
            Divider(),
          ],
        ));


    return center;
  }


}
