import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:kinderm8/auth.dart';
import 'package:kinderm8/data/database_helper.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/dailysummarydatalist.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_summary_modal.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/pages/home/services/daily_summary_service.dart';
import 'package:kinderm8/utils/connectivity.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:http/http.dart' as http;

class UserChildDataInheritedWidget extends InheritedWidget {
  final _SplashPageState data;

  const UserChildDataInheritedWidget({
    Key key,
    this.data,
    @required Widget child,
  })  : assert(child != null),
        super(key: key, child: child);

  static UserChildDataInheritedWidget of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(UserChildDataInheritedWidget)
        as UserChildDataInheritedWidget;
  }

  @override
  bool updateShouldNotify(UserChildDataInheritedWidget old) {
    return old.child != child;
  }
}

class SplashPage extends StatefulWidget {
  @override
  State createState() => new _SplashPageState();
}

class _SplashPageState extends State<SplashPage>
    implements HomePageContract, AuthStateListener {
  _SplashPageState() {
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  var email, password, center;
  var childid, appuser, jwt, userId, clientId;
  var childrendata, image;
  var devicetoken;

  //fetch children data to a List from db
  BuildContext ctx;
  List<ChildViewModal> childrenData = [];
  bool isLoading;
  AuthState loginState;
  bool canProceed = true;

  bool isOffline = false;
  bool dialogIsVisible = false;

  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  ConnectionStatusSingleton connectionStatus;

  // Push
  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  @override
  void initState() {
    isLoading = true;
    loginState = AuthState.LOGGED_OUT;
    print("in initstate");

    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    // TODO: get push token and send to login
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print('onMessage $message');
      },
      onResume: (Map<String, dynamic> message) {
        print('onResume $message');
      },
      onLaunch: (Map<String, dynamic> message) {
        print('onLaunch $message');
      },
    );
    _firebaseMessaging.getToken().then((token) {
      print('device token : $token');
      devicetoken = token;
    });
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          isOffline = false;
          dialogIsVisible = false;
        });
        break;
      case ConnectivityResult.mobile:
        setState(() {
          isOffline = false;
          dialogIsVisible = false;
        });
        break;
      case ConnectivityResult.none:
        setState(() => isOffline = true);
        buildAlertDialog("Internet connection cannot be establised!");
        break;
      default:
        setState(() => isOffline = true);
        break;
    }
  }

  void buildAlertDialog(String message) {
    SchedulerBinding.instance.addPostFrameCallback((_) => setState(() {
      if (isOffline && !dialogIsVisible) {
        dialogIsVisible = true;
        showDialog(
          barrierDismissible: false,
            context: ctx,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(message, textAlign: TextAlign.center, style: TextStyle(fontSize: 14.0),),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Icon(Icons.portable_wifi_off, color: Colors.redAccent, size: 36.0,),
                    canProceed ? Text(
                      "Check your internet connection before proceeding.", textAlign: TextAlign.center, style: TextStyle(fontSize: 12.0),) : Text(
                      "Please! proceed by connecting to a internet connection", textAlign: TextAlign.center, style: TextStyle(fontSize: 12.0, color: Colors.red),),
                  ],
                ),
                actions: <Widget>[
                  RaisedButton(
                    onPressed: () {
                      SystemChannels.platform
                          .invokeMethod('SystemNavigator.pop');
                    },
                    child: Text(
                      "CLOSE THE APP",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  RaisedButton(
                    onPressed: () {
                      if (isOffline) {
                        setState(() {
                          canProceed = false;
                        });
                      } else {
                        Navigator.pop(ctx);
                        onAuthStateChanged(loginState);
                      }
                    },
                    child: Text(
                      "PROCEED",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              );
            });
      }
    }));
  }

  @override
  Widget build(BuildContext context) {
    print("in build");
    ctx = context;
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: Theme.Colors.appsplashscreen),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Image(
                        image: AssetImage(
                            "assets/kinderm8childcare-white-new.png"),
                        height: 100.0,
                        width: 400.0,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    isOffline ? SizedBox() : CircularProgressIndicator(
                      backgroundColor: Colors.amber,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    Text(
                      'Welcome to Kinder m8',
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0,
                          color: Colors.white),
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Future fetchData(List children) async {
    bool isImageAvailable = true;

    await getRefreshToken(userId: children[0]["id"].toString(), clientId: clientId);
    childrenData = children.map<ChildViewModal>((element) {
      precacheImage(
          NetworkImage(
              "https://d212imxpbiy5j1.cloudfront.net/${element["image"]}"),
          ctx,
          onError: (err, stackT) => isImageAvailable = false);
      return ChildViewModal(
        id: element["id"],
        iconAssetPath: isImageAvailable ? element["image"] : '',
        firstName: element["firstname"],
        lastName: element["lastname"],
        room: element["room"],
        gender: element["gender"],
        attendance: element["attendance"],
        dob: element["dob"],
      );
    }).toList();
    for (int i = 0; i < childrenData.length; i++) {
      await fetchDailySummaryData(
              childId: childrenData[i].id, clientId: clientId, userId: userId)
          .then((value) {
        ChildSummaryModal childSummaryModal = value;
        if (childSummaryModal?.childPhotoModals != null && childSummaryModal.childPhotoModals.length > 0 && i < 4) {
          precacheImage(
              NetworkImage(childSummaryModal.childPhotoModals[i].fileUrl), ctx,
              onError: (err, trace) {
                print(err);
              });
        }
        setState(() {
          childrenData[i].childSummaryVM = childSummaryModal;
//          childrenData[i].navigateFunctions = navigationFunctions;
        });
      });
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Future onAuthStateChanged(AuthState state) async {
//    setState(() {
      loginState = state;
//    });
    var db = new DatabaseHelper();
    User user = await db.getFirstUser();

    await checkIsPasswordChanged(user).then((flag) {
      if (flag) {
        state = AuthState.LOGGED_OUT;
        db.deleteUser();
      }
    });
    if (state == AuthState.LOGGED_IN) {
      clientId = user.client_id;
      final parsed = json.decode(user.center);
      var childDetails = parsed[0];
      await fetchData(childDetails['children']).timeout(Duration(seconds: 10), onTimeout: () {
        buildAlertDialog("Sorry! Cannot proceed with poor network connection.");//TODO: timeout implemented but did not test bcuz there no way to do that,now
        return;
      });
      Navigator.push(
          ctx,
          MaterialPageRoute(
              builder: (context) => new UserChildDataInheritedWidget(
                  data: this,
                  child: DailySummary(childrenData: childrenData))));
    } else {
      if (!isOffline) {
        Timer(Duration(seconds: 3),
                () => Navigator.of(ctx).pushReplacementNamed('/login'));
      }
    }
  }

  @override
  void onDisplayUserInfo(User user) {
    print("in display user");
    email = user.email;
    password = user.password;
    setState(() {
      clientId = user.client_id;
      var appUser = user.center;
      try {
        final parsed = json.decode(appUser);
        var details = parsed[0];
        var users = details["user"];
        jwt = details["jwt"];
        userId = users["id"];
        clientId = users["client_id"];
        childrendata = details["children"];
        // Setup first child to load daily summary
        if (users['id'] != null) {
          print('calling setup route');
          print(users.toString());
        }
      } catch (e) {
        print('That string was null!');
      }
    });
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  Future<bool> checkIsPasswordChanged(User user) {
    print("user email ${user?.email} password ${user?.password}");
    final baseUrl = 'http://13.55.4.100:7070/v2.1.0/setupuseroneapp';
    String requestBody = json.encode({"email": user?.email, "password": user?.password, "client_id": user?.client_id});
    return http.post(baseUrl, body: requestBody).then((resp) {
      if (resp.statusCode == 401) {
        return true;
      } else {
        return false;
      }
    });
  }
}

  @override
  void onDisplayUserInfo(User user) {
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }
