import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:kinderm8/Theme.dart' as kinderm8Theme;
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/privatemessage/inboxlist.dart';
import 'package:kinderm8/utils/network_util.dart';
//import 'package:multi_image_picker/multi_image_picker.dart';

class ReplyPrivateMessage extends StatefulWidget {
  final singlePrivateMessageData;
  final sender;
  ReplyPrivateMessage(this.singlePrivateMessageData, this.sender);

  @override
  ReplyPrivateMessageState createState() =>
      ReplyPrivateMessageState(singlePrivateMessageData, sender);
}

class ReplyPrivateMessageState extends State<ReplyPrivateMessage>
    implements HomePageContract {
  final singlePrivateMessageData;
  final sender;
  var appuser, jwt, userId, clientId;

  final formKey = new GlobalKey<FormState>();
  final TextEditingController _textController = new TextEditingController();
  HomePagePresenter presenter;
  bool error = false;
  bool send = true;
  var folder;
  List<Widget> receiver = [];
  List<String> receiverId = [];
  var messageId;

  List<File> fileList = new List();
  Future<File> _imageFile;
  String _path = '-';
  bool _pickFileInProgress = false;
  bool _iosPublicDataUTI = true;
  bool _checkByCustomExtension = false;
  bool _checkByMimeType = false;

//  List<Asset> images = List<Asset>();
  String _error;
  String _filePath;

  final _utiController = TextEditingController(
    text: 'com.sidlatau.example.mwfbak',
  );

  final _extensionController = TextEditingController(
    text: 'mwfbak',
  );

  final _mimeTypeController = TextEditingController(
    text: 'application/*',
  );

  ReplyPrivateMessageState(this.singlePrivateMessageData, this.sender) {
    presenter = new HomePagePresenter(this);
    presenter.getUserInfo();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getReceiverDetails();
  }

  getReceiverDetails() {
    for (int i = 0; i < sender.length; i++) {
// "to"
      receiver.add(
        new Chip(
          label: Text(
            '${sender[i]['fullname']}',
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.cyan,
        ),
      );
// "reply_receivers"
      receiverId.add(sender[i]['id'].toString());
    }
    print("Parm $receiverId ${receiverId.join(",")}");

// "parent_id"
    messageId = singlePrivateMessageData['id'];

// "folder"
    if (singlePrivateMessageData['attachments'] != '') {
      var attachment = singlePrivateMessageData['attachments'].split(':');
      print(attachment);
      var split = attachment[1].split(',');
      folder = split[0];
    } else {
      folder = "";
    }
  }

  pickImage(ctx) {
    // check size
    num size = fileList.length;
    if (size >= 9) {
      Scaffold.of(ctx).showSnackBar(new SnackBar(
        content: new Text("Only add up to 9 pictures!"),
      ));
      return;
    }
    showModalBottomSheet<void>(context: context, builder: _bottomSheetBuilder);
  }

  Widget _bottomSheetBuilder(BuildContext context) {
    return new Container(
        height: 182.0,
        child: new Padding(
          padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 30.0),
          child: new Column(
            children: <Widget>[
              _renderBottomMenuItem(
                  Icons.camera_alt, "Camera photo", ImageSource.camera),
              new Divider(
                height: 2.0,
              ),
              _renderBottomMenuItem(
                  Icons.image, "Gallery photo", ImageSource.gallery)
            ],
          ),
        ));
  }

  _renderBottomMenuItem(icon, title, ImageSource source) {
    var item = new Container(
      height: 60.0,
      child: Row(
        children: <Widget>[
          new Padding(
              padding: const EdgeInsets.all(10.0),
              child: new Icon(icon,
                  size: 50.0, color: kinderm8Theme.Colors.appcolour)),
          new Center(
              child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: new Text(
                    title,
                    style: new TextStyle(
                        fontSize: 22.0,
                        color: kinderm8Theme.Colors.appdarkcolour,
                        fontStyle: FontStyle.normal),
                  ))),
        ],
      ),
    );
    return new InkWell(
      child: item,
      onTap: () {
        Navigator.of(context).pop();
        setState(() {
          _imageFile = ImagePicker.pickImage(source: source);
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
//    print(singlePrivateMessageData['attachments']);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: kinderm8Theme.Colors.appdarkcolour,
        title: new Text(
          "Reply",
          style: TextStyle(
            color: Colors.white,
            fontSize: 21.0,
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 13.0),
            child: IconButton(
                icon: Icon(
                  Icons.attachment,
                  size: 25.0,
                  color: Colors.white,
                ),
                onPressed: () {
                  print("Have to navigate to pick files");
                  // add pictures
                  pickImage(context);
                }),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 13.0),
            child: send
                ? IconButton(
                    icon: Icon(
                      Icons.send,
                      size: 23.0,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      if (_textController.text.length == 0) {
                        setState(() {
                          error = true;
                        });
                      } else {
                        setState(() {
                          error = false;
                          send = false;
                        });
                        print(_textController.text);

                        submitReply();
                      }
                    })
                : CupertinoActivityIndicator(),
          ),
        ],
      ),

      ///
      ///
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: SingleChildScrollView(
          child: Column(children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(13.0),
              child: Row(
                children: <Widget>[
                  Container(child: Text("To:")),
                  Container(width: 5.0),
                  Expanded(child: Wrap(children: receiver))
                ],
              ),
            ),
            Container(height: 5.0),
            Padding(
                padding: const EdgeInsets.only(left: 13.0, right: 13.0),
                child: Form(
                    key: formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Text(
                            "Message*",
                            style: TextStyle(
                                color: error ? Colors.redAccent : Colors.black,
                                fontSize: 14.0),
                          ),
                        ),
//                      Container(height: 3.0),
                        Container(
                          child: TextField(
                              textInputAction: TextInputAction.newline,
                              controller: _textController,
//                              focusNode: FocusNode(),
                              maxLines: 10,
                              decoration: InputDecoration(
//                                  fillColor: Colors.grey.withOpacity(0.1),
//                                  filled: true,
                                  border: OutlineInputBorder(),
                                  contentPadding: EdgeInsets.all(3.0),
                                  hintText: "Type your message here",
                                  hintStyle: TextStyle(
                                      color: Colors.grey, fontSize: 11.0))),
                        ),
                      ],
                    ))),
            new Container(
              padding: const EdgeInsets.all(10.0),
//              margin: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
//              height: 200.0,
              child: new FutureBuilder(
                future: _imageFile,
                builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.data != null &&
                      _imageFile != null) {
                    //Selected image (photo or gallery selection), added to the list
                    fileList.add(snapshot.data);
                    _imageFile = null;
                  }
                  return new Builder(
                    builder: (context) {
                      return new GridView.count(
                        crossAxisCount: 4,
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        children:
                            new List.generate(fileList.length + 1, (index) {
                          // This method body is used to generate an item in the GridView.item
                          var content;
                          if (index == 0) {
                            // Add picture button
                            var addCell = new Center(
//                                  child: new Image.asset('ic_add_pics.png', width: 80.0, height: 80.0,)
                              child: new Icon(Icons.add,
                                  size: 50.0,
                                  color: kinderm8Theme.Colors.appcolour),
                            );
                            content = new GestureDetector(
                              onTap: () {
                                // add pictures
                                pickImage(context);
                              },
                              child: addCell,
                            );
                          } else {
                            // Selected image
                            content = new Center(
                                child: new Image.file(
                              fileList[index - 1],
//                              width: 80.0,
//                              height: 80.0,
                              fit: BoxFit.cover,
                            ));
                          }
                          return new Container(
                            margin: const EdgeInsets.all(2.0),
                            width: 80.0,
                            height: 80.0,
                            color: const Color(0xFFECECEC),
                            child: content,
                          );
                        }),
                      );
                    },
                  );
                },
              ),
            )
          ]),
        ),
      ),
    );
  }

  submitReply() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      FocusScope.of(context).requestFocus(new FocusNode());

      final submitcommentUrl =
          "http://13.55.4.100:7070/createmessagereplynew?clientid=$clientId";
      var body = {
        "message": _textController.text,
        "author_id": userId,
        "parent_id": messageId,
        "reply_receivers": receiverId.join(","),
        "folder": folder,
        "attachments": "",
        "isread": "0",
        "files": ""
      };
      var headers = {"x-authorization": jwt.toString()};

      NetworkUtil _netUtil = new NetworkUtil();
      _netUtil
          .post(submitcommentUrl,
              body: body, headers: headers, encoding: jwt.toString())
          .then((res) {
        print("resss  $res");
        if (res == 1) {
          String _refreshTokenUrl =
              'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

          print(_refreshTokenUrl);
          NetworkUtil _netutil = new NetworkUtil();

          _netutil.get(_refreshTokenUrl).then((response) {
            print('refresh get ${response.body}');
            var refreshJwtToken;
            try {
              refreshJwtToken = json.decode(response.body);
            } catch (e) {
              print('That string was null!');
            }
            this.jwt = refreshJwtToken;
            submitReply();
          });
        } else if (res == '') {
          print("may be 200");
          send = true;
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => PrivatemessageList()));
        } else {
          print("error");
        }
      });
    } else {
      print("else");
    }
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      userId = users["id"];

      print("iddd $userId");
      print(clientId);
    } catch (e) {
      print("That string didn't look like Json.");
    }
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
