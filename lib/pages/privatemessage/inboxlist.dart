import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';

import 'package:kinderm8/Theme.dart' as kinderm8Theme;
import 'package:kinderm8/pages/commondrawer.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/privatemessage/createmessage.dart';
import 'package:kinderm8/pages/privatemessage/inboxmessagedata.dart';
import 'package:kinderm8/pages/privatemessage/staffselection.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class PrivatemessageList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new PrivatemessageListState();
  }
}

class Privatemessage extends StatefulWidget {
  @override
  PrivatemessageListState createState() => PrivatemessageListState();
}

class PrivatemessageListState extends State<PrivatemessageList>
    implements HomePageContract {
  List Privatemessagedata;
  var privatemessageData;
  var k, appuser, jwt, id, clientId;
  ScrollController _scrollController = new ScrollController();
  bool isLoading;
  bool load = true;
  var delete = true;
  HomePagePresenter _presenter;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  PrivatemessageListState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  @override
  void initState() {
    super.initState();
  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: kinderm8Theme.Colors.appdarkcolour,
    containerColor: kinderm8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  Future<String> fetchPrivatemessageData(int s) async {
    ///data from GET method
    print("Privatemessage data fetched");

    String _privatemessageUrl =
        'http://13.55.4.100:7070/v4.4.2/getprivatemessagesmain?pmuserid=$id&step=$s&clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_privatemessageUrl, headers: headers).then((response) {
      privatemessageData = json.decode(response.body);
      print(privatemessageData.length);
      print('res get ${response.body}');
      print('PrivatemessageData $privatemessageData');
//      try {
//        privatemessageData = json.decode(response.body);
//        print(privatemessageData.length);
//        print('res get ${response.body}');
//        print('PrivatemessageData $privatemessageData');
//      } on FormatException catch (e) {
//        print("That string didn't look like Json.");
//      } on NoSuchMethodError catch (e) {
//        print('That string was null!');
//      }

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        print(isLoading);
        isLoading = false;
        print(isLoading);
        if (s == 0) {
          setState(() {
            load = false;
            this.Privatemessagedata = privatemessageData;
          });
        } else {
          setState(() {
            load = false;
            Privatemessagedata.addAll(privatemessageData);
          });
        }
        k = Privatemessagedata.length;
      } else if (response.statusCode == 500 &&
          privatemessageData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      } else {
        fetchPrivatemessageData(0);
      }
    });
    return null;
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } on FormatException catch (e) {
        print("That string didn't look like Json.");
      } on NoSuchMethodError catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      if (k != null) {
        fetchPrivatemessageData(k);
      } else {
        fetchPrivatemessageData(0);
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void deleteMessage(deleteMessageId, index) async {
    setState(() {
      delete = false;
    });
    String deleteUrl =
        'http://13.55.4.100:7070/deleteprivatemessage?clientid=$clientId';
    var body = {"message_id": deleteMessageId, "reply_id": 0, "user": id};
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.delete(deleteUrl, headers: headers, body: body).then((res) {
      print("res$res");
      print(res.statusCode);
      if (res.statusCode == 200) {
        setState(() {
          delete = true;
          Privatemessagedata.removeAt(index);
          print(Privatemessagedata.length);
//          Privatemessagedata.add(deleteCommentId);
        });
      } else if (res.statusCode == 500) {
        print("refreshing Token..");
        String _refreshTokenUrl =
            'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

        NetworkUtil _netutil = new NetworkUtil();

        _netutil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          var refreshJwtToken = json.decode(response.body);
          this.jwt = refreshJwtToken;
          deleteMessage(deleteMessageId, index);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(labelsConfig["privateMessageLabel"],
            style: TextStyle(
              color: Colors.white,
            )),
        backgroundColor: kinderm8Theme.Colors.appdarkcolour,
        centerTitle: true,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.refresh),
              tooltip: 'Refresh',
              onPressed: () {
                print("Reload..");
                setState(() {
                  load = true;
                  k = 0;
                });
                fetchPrivatemessageData(0);
              })
        ],
      ),
      drawer: CommonDrawer(),
      body: new RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: handleRefresh,
        child: new Center(
          child: load
              ? progress
              : new ListView.builder(
            itemCount: this.Privatemessagedata != null
                ? (this.Privatemessagedata.length + 1)
                : 0,
            itemBuilder: (context, i) {
              if (i == Privatemessagedata.length) {
                if (privatemessageData.length == 0 || privatemessageData.length < 10) {
                  print(privatemessageData.length);
                  return Container();
                } else {
                  return _buildCounterButton();
                }
              } else {
                final data = this.Privatemessagedata[i];

                return id == data['user']['id']
                    ? Slidable(
                  key: Key(data['id'].toString()),
                  closeOnScroll: true,
                  child: PrivateMessageData(data),
                  delegate: SlidableDrawerDelegate(),
                  actionExtentRatio: 0.2,
                  slideToDismissDelegate:
                  SlideToDismissDrawerDelegate(
                      onWillDismiss: (actionType) {
                        return showDialog<bool>(
                          context: context,
                          builder: (context) {
                            return new AlertDialog(
                              title: new Text('Do you want to Delete?'),
                              content:
                              new Text('Entire message will be deleted'),
                              actions: <Widget>[
                                new FlatButton(
                                  child: new Text('Cancel'),
                                  onPressed: () =>
                                      Navigator.of(context).pop(false),
                                ),
                                new FlatButton(
                                    child: new Text('Ok'),
                                    onPressed: () {
                                      Navigator.of(context).pop(true);
                                      deleteMessage(data['id'], i);
                                    }),
                              ],
                            );
                          },
                        );
                      }),
                  secondaryActions: <Widget>[
                    new IconSlideAction(
                      caption: 'Cancel',
                      color: Colors.grey.shade200,
                      icon: Icons.clear,
                    ),
                    delete
                        ? new IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () {
                        print(data['id']);

                        deleteMessage(data['id'], i);
                      },
                    )
                        : Container(
                      child: Column(
                        mainAxisAlignment:
                        MainAxisAlignment.center,
                        children: <Widget>[
                          CupertinoActivityIndicator(),
                          Text("Deleting..")
                        ],
                      ),
                      color: Colors.grey.shade200,
                    ),
                  ],
                )
                    : PrivateMessageData(data);
              }
            },
          ),
        ),
      ),
      floatingActionButton: _getMailFloatingActionButton(),
    );
  }

  FloatingActionButton _getMailFloatingActionButton() {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => StaffSelection()),
        );

      },
      child: Icon(Icons.add,size: 40.0,),
      backgroundColor: kinderm8Theme.Colors.appcolour,
    );
  }

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: isLoading
            ? new CupertinoActivityIndicator()
            : const Text('Load more...',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14.0,
                color: kinderm8Theme.Colors.app_white)),
        color: kinderm8Theme.Colors.appcolour.withOpacity(0.75),
//        splashColor: Colors.red,
        elevation: 4.0,
        onPressed: _counterButtonPress());
  }

  Function _counterButtonPress() {
    if (isLoading) {
      return null;
    } else {
      return () {
        setState(() {
          isLoading = true;
        });
        fetchPrivatemessageData(k);
      };
    }
  }

  Future<Null> handleRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      load = true;
      k = 0;
      fetchPrivatemessageData(0);
    });
    return null;
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      print(appusers);
      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];

      print("first page iddd $id");
      print(clientId);
      fetchPrivatemessageData(0);
    } on FormatException catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }
}