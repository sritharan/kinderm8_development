import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/components/googlewebview_url.dart';
import 'package:kinderm8/pages/privatemessage/detailmessageview.dart';
import 'package:kinderm8/utils/commonutils/utils.dart';
import 'dart:async';
//import 'package:photo_view/photo_view.dart';
import 'package:kinderm8/Theme.dart' as kinderm8Theme;
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class PrivateMessageData extends StatefulWidget {
  final singleprivatemessagedata;

  PrivateMessageData(this.singleprivatemessagedata);

  @override
  PrivateMessageState createState() =>
      PrivateMessageState(singleprivatemessagedata);
}

class PrivateMessageState extends State<PrivateMessageData> {
  final singleprivatemessagedata;
  final flutterWebviewPlugin = new FlutterWebviewPlugin();
  bool isread = true;

  PrivateMessageState(this.singleprivatemessagedata);

  @override
  void initState() {
    super.initState();
    if (singleprivatemessagedata['isread'] == '0') {
      isread = false;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  // /* Using webview*/ //
  openBrowserTabInWebview(String url) async {
    return new WebviewScaffold(
      url: url,
      appBar: new AppBar(
        title: const Text('Widget webview'),
      ),
      withZoom: true,
      withLocalStorage: true,
    );
//    flutterWebviewPlugin.launch(url, hidden: true);
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    var parsedcreatedDate =
    DateTime.parse(singleprivatemessagedata["created_at"]);
//    var date = new DateFormat.yMMMMEEEEd().add_jm().format(parsedcreatedDate);
    String formattedDate = DateFormat('MMM d').format(parsedcreatedDate);
//    String formattedDate = DateFormat('kk:mm:ss \n EEE d MMM').format(parsedcreatedDate);
    var sender = singleprivatemessagedata["sender"];

    // var fileurlencode = "https://docs.google.com/gview?embedded=true&url="+singleprivatemessagedata['fileurl'];
    /*var unescape = new HtmlUnescape();
    var fromData = unescape.convert(singleprivatemessagedata['message']);
    var convertedData = fromData.substring(5, fromData.length - 6);
    var message = convertedData.split('&');*/

    return Card(
      elevation: 0.0,
      margin: EdgeInsets.all(1.0),
      shape: Border(
          bottom: BorderSide(
              style: BorderStyle.solid, color: Colors.grey.withOpacity(0.5))),
      color: isread ? Colors.white : Colors.deepPurple.withOpacity(0.1),
      child: FlatButton(
        onPressed: () {
          setState(() {
            isread = true;
          });
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    DetailedMessage(singleprivatemessagedata)),
          );
        },
        child: Padding(
          padding: const EdgeInsets.only(top: 3.0, bottom: 3.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(5.0),
                height: 64.0,
                width: 64.0,
                alignment: Alignment.topLeft,
                child: singleprivatemessagedata['initialmsgreceiver'].length > 0
                    ? Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(64.0),
                      border: Border.all(
                          color: Colors.deepPurple,
                          style: BorderStyle.solid,
                          width: 1.0)),
                  child: ClipOval(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            singleprivatemessagedata['initialmsgreceiver']
                                .length >
                                0
                                ? singleprivatemessagedata[
                            'initialmsgreceiver'][0]
                            ['image'] ==
                                null
                                ? Image.asset(
                              "assets/nophoto.jpg",
                              height: singleprivatemessagedata[
                              'initialmsgreceiver']
                                  .length ==
                                  1 ||
                                  singleprivatemessagedata[
                                  'initialmsgreceiver']
                                      .length ==
                                      2
                                  ? 62.0
                                  : 30.0,
                              width: singleprivatemessagedata[
                              'initialmsgreceiver']
                                  .length ==
                                  1
                                  ? 62.0
                                  : 30.0,
                              fit: BoxFit.cover,
                            )
                                : Image.network(
                              "https://d212imxpbiy5j1.cloudfront.net/${singleprivatemessagedata['initialmsgreceiver'][0]['image']}",
                              height: singleprivatemessagedata[
                              'initialmsgreceiver']
                                  .length ==
                                  1 ||
                                  singleprivatemessagedata[
                                  'initialmsgreceiver']
                                      .length ==
                                      2
                                  ? 62.0
                                  : 30.0,
                              width: singleprivatemessagedata[
                              'initialmsgreceiver']
                                  .length ==
                                  1
                                  ? 62.0
                                  : 30.0,
                              fit: BoxFit.cover,
                            )
                                : Container(),
                            singleprivatemessagedata['initialmsgreceiver']
                                .length >
                                1
                                ? singleprivatemessagedata[
                            'initialmsgreceiver'][1]
                            ['image'] ==
                                null
                                ? Image.asset(
                              "assets/nophoto.jpg",
                              height: singleprivatemessagedata[
                              'initialmsgreceiver']
                                  .length ==
                                  2
                                  ? 62.0
                                  : 30.0,
                              width: 30.0,
                              fit: BoxFit.cover,
                            )
                                : Image.network(
                              "https://d212imxpbiy5j1.cloudfront.net/${singleprivatemessagedata['initialmsgreceiver'][1]['image']}",
                              height: singleprivatemessagedata[
                              'initialmsgreceiver']
                                  .length ==
                                  2
                                  ? 62.0
                                  : 30.0,
                              width: 30.0,
                              fit: BoxFit.cover,
                            )
                            /* Container(
                                        width: 30.0,
                                        height: 30.0,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(20.0),
                                            border: Border.all(
                                                color: Colors.deepPurple,
                                                style: BorderStyle.solid,
                                                width: 1.0),
                                            image: DecorationImage(
                                                fit: BoxFit.cover,
                                                image: CachedNetworkImageProvider(
                                                    "https://d212imxpbiy5j1.cloudfront.net/img/postuploads/production_1544181281-56fc9bbbf1cd3cfb47a788629bbdc71c.jpg"))),
                                      )*/
                                : Container(),
                          ],
                          mainAxisAlignment:
                          MainAxisAlignment.spaceAround,
                        ),
                        Row(
                          children: <Widget>[
                            singleprivatemessagedata['initialmsgreceiver']
                                .length >
                                2
                                ? singleprivatemessagedata[
                            'initialmsgreceiver'][2]
                            ['image'] ==
                                null
                                ? Image.asset(
                              "assets/nophoto.jpg",
                              height: 30.0,
                              width: singleprivatemessagedata[
                              'initialmsgreceiver']
                                  .length ==
                                  3
                                  ? 62.0
                                  : 30.0,
                              fit: BoxFit.cover,
                            )
                                : Image.network(
                              "https://d212imxpbiy5j1.cloudfront.net/${singleprivatemessagedata['initialmsgreceiver'][2]['image']}",
                              height: 30.0,
                              width: singleprivatemessagedata[
                              'initialmsgreceiver']
                                  .length ==
                                  3
                                  ? 62.0
                                  : 30.0,
                              fit: BoxFit.cover,
                            )
                                : Container(),
                            singleprivatemessagedata['initialmsgreceiver']
                                .length >
                                3
                                ? singleprivatemessagedata[
                            'initialmsgreceiver'][3]
                            ['image'] ==
                                null
                                ? Image.asset(
                              "assets/nophoto.jpg",
                              fit: BoxFit.cover,
                              height: 30.0,
                              width: 30.0,
                            )
                                : Image.network(
                              "https://d212imxpbiy5j1.cloudfront.net/${singleprivatemessagedata['initialmsgreceiver'][3]['image']}",
                              height: 30.0,
                              width: 30.0,
                              fit: BoxFit.cover,
                            )
                                : Container(),
                          ],
                          mainAxisAlignment:
                          MainAxisAlignment.spaceAround,
                        ),
                      ],
                    ),
                  ),
                )
                    : Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(64.0),
                      border: Border.all(
                          color: Colors.deepPurple,
                          style: BorderStyle.solid,
                          width: 1.0),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: singleprivatemessagedata['user']
                          ['image'] !=
                              null
                              ? CachedNetworkImageProvider(
                              "https://d212imxpbiy5j1.cloudfront.net/${singleprivatemessagedata['user']['image']}")
                              : AssetImage("assets/nophoto.jpg"))),
                ),
              ),
//              Icon(
//                Icons.account_circle,
//                size: 45.0,
//                color: kinderm8Theme.Colors.appcolour,
//              ),

              ///
              /// start expanded
              ///
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  singleprivatemessagedata['user']['fullname'],
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black87,
                                      fontSize: 16.0),
                                ),
                                Container(
                                  width: 5.0,
                                ),
                                singleprivatemessagedata['reply_count'] != 0
                                    ? Container(
                                    width: 16.0,
                                    height: 16.0,
                                    child: Center(
                                      child: Text(
                                        '${singleprivatemessagedata['reply_count']}',
                                        style: TextStyle(
                                            fontSize: 11.0,
                                            fontWeight: FontWeight.w500,
                                            color: kinderm8Theme
                                                .Colors.darkGrey),
                                      ),
                                    ),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.circular(16.0),
                                        border: Border.all(
                                            color: Colors.deepPurple,
                                            style: BorderStyle.solid,
                                            width: 1.0)))
                                    : Container()
                              ]),
                          Text(
                            formattedDate,
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                color: Colors.black54,
                                fontSize: 13.5),
                          ),
                        ],
                      ),
                      Container(
                        height: 5.0,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  width: screenSize.width - 150.0,
                                  child: Text(
                                    singleprivatemessagedata['subject'],
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontWeight: isread
                                            ? FontWeight.w400
                                            : FontWeight.bold,
                                        color: Colors.black54,
                                        fontSize: 14.0),
                                  ),
                                ),
//                                    SizedBox(
//                                      width: screenSize.width - 150.0,
//                                      child: Text(
//                                        singleprivatemessagedata['message'],
//                                        maxLines: 1,
//                                        overflow: TextOverflow.ellipsis,
//                                        style: TextStyle(
//                                            fontWeight: FontWeight.w400,
//                                            color: Colors.black54,
//                                            fontSize: 15.5),
//                                      ),
//                                    )
                              ],
                            ),
                          ),
                          singleprivatemessagedata['isAttachment'] == 1
                              ? Icon(Icons.attach_file, size: 13.5)
                              : Container(width: 0.0),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}