import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/privatemessage/createmessage.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:kinderm8/Theme.dart' as kinderm8Theme;

class StaffSelection extends StatefulWidget {
  @override
  StaffSelectionState createState() => StaffSelectionState();
}

class StaffSelectionState extends State<StaffSelection>
    implements HomePageContract {
  List staffByParentList, roomWithStaffList;
  bool load = true;

  var appuser, jwt, userId, clientId;
  List<String> rooms = [];
  String selectedValue;
  HomePagePresenter _presenter;

  bool checkboxValueAll = true;
  bool checkBoxValue = false;
  List _finalSelectedStaff = List();
  List _selectedStaff = List();

  StaffSelectionState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  void onChanged(bool value) {
    setState(() {
      checkBoxValue = value;
    });
  }

  Future<String> getStaffByParent() async {
    ///data from GET method
    print("getStaffByParentData data fetched");

    String _getStaffByParentUrl =
        'http://13.55.4.100:7070/getstaffbyparent?userid=$userId&clientid=$clientId';

    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_getStaffByParentUrl, headers: headers).then((response) {
      var getStaffByParentData;
      getStaffByParentData = json.decode(response.body);
      print('getStaffByParentData $getStaffByParentData');

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        staffByParentList = getStaffByParentData;
//        getStaff();
        setState(() {
          load = false;
        });
      } else if (response.statusCode == 500 &&
          getStaffByParentData["errorType"] == 'ExpiredJwtException') {
        String _refreshTokenUrl =
            'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

        NetworkUtil _netutil = new NetworkUtil();
        var refreshJwtToken;
        _netutil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          try {
            refreshJwtToken = json.decode(response.body);
            this.jwt = refreshJwtToken;
            getStaffByParent();
          } catch (e) {
            print('That string was null!');
          }
        });
      } else {
        print("error");
      }
    });
    return null;
  }

  Future<String> getRoomWithStaff() async {
    ///data from GET method
    print("getRoomWithStaffData data fetched");

    String _getRoomWithStaffUrl =
        'http://13.55.4.100:7070/getroomwithstaff?userid=$userId&clientid=$clientId';

    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_getRoomWithStaffUrl, headers: headers).then((response) {
      var getRoomWithStaffData;
      getRoomWithStaffData = json.decode(response.body);
//      print(getRoomWithStaffData.length);
//      print('res get ${response.body}');
      print('getRoomWithStaffData $getRoomWithStaffData');

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        roomWithStaffList = getRoomWithStaffData;
        setState(() {
          load = false;
        });
        getRooms();
      } else if (response.statusCode == 500 &&
          getRoomWithStaffData["errorType"] == 'ExpiredJwtException') {
        String _refreshTokenUrl =
            'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

        NetworkUtil _netutil = new NetworkUtil();
        var refreshJwtToken;
        _netutil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          try {
            refreshJwtToken = json.decode(response.body);
            this.jwt = refreshJwtToken;
            getRoomWithStaff();
          } catch (e) {
            print('That string was null!');
          }
        });
      } else {
        print("error");
      }
    });
    return null;
  }

  getRooms() {
    rooms.add('All');
    for (int i = 0; i < roomWithStaffList.length; i++) {
      rooms.add(roomWithStaffList[i]['title'].toString());
    }

    print("rooms $rooms");
  }

  void _onStaffSelected(bool selected, staff) {
    if (selected == true) {
      setState(() {
        _finalSelectedStaff.add(staff);
      });
    } else {
      setState(() {
        _finalSelectedStaff.remove(staff);
      });
    }
    print(_finalSelectedStaff);
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: new Text("Starff Selection "),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            // Image.asset("assets/BGI.png"),
            Column(
              children: <Widget>[
                Container(
                  height: 50.0,
//                  child: Padding(
//                    padding: const EdgeInsets.all(10.0),
//                    child: new  MaterialButton(
//                      child: Text('Staff Selection'),
//                      onPressed: null,
//                      color: kinderm8Theme.Colors.appcolour,
//                    ),
//                  )
                ),

                screenSize.width < 500
                    ? Container(
                  alignment: Alignment.topLeft,
                  height: 20.0,
                  width: 20.0,

                )
                    : Container(
                  height: 200.0,
                  width: 200.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 40.0, right: 40.0, top: 40.0, bottom: 40.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: 20.0),
                        height: 35.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                           border: Border.all(style: BorderStyle.solid)
                        ),

                        child: DropdownButtonHideUnderline(
                          child: new DropdownButton<String>(
                              elevation: 0,
                              hint: Center(child: Text('Select by room',
                                style: TextStyle(color: kinderm8Theme.Colors.appcolour),)),

                              value: selectedValue,
                              onChanged: (String changedValue) {
                                selectedValue = changedValue;
                                setState(() {
                                  _finalSelectedStaff = [];
                                  _selectedStaff = [];
//
                                  if (selectedValue == "All") {
                                    for (int i = 0;
                                    i < staffByParentList.length;
                                    i++) {
                                      _finalSelectedStaff
                                          .add(staffByParentList[i]);
                                      _selectedStaff.add(staffByParentList[i]);
                                      print(i);
                                    }
                                  } else {
                                    for (int i = 0;
                                    i < roomWithStaffList.length;
                                    i++) {
                                      if (selectedValue ==
                                          roomWithStaffList[i]['title']) {
                                        for (int j = 0;
                                        j <
                                            roomWithStaffList[i]['staff']
                                                .length;
                                        j++) {
                                          _finalSelectedStaff.add(
                                              roomWithStaffList[i]['staff'][j]);
                                          _selectedStaff.add(
                                              roomWithStaffList[i]['staff'][j]);
                                        }
                                      }
                                    }
                                  }
                                  print(_finalSelectedStaff);
                                  print(_selectedStaff);
                                  print("inside DropdownButton $selectedValue");
                                });
                              },
                              items: rooms.map((String value) {
                                return new DropdownMenuItem<String>(
                                  value: value,
                                  child: new Text(value),
                                );
                              }).toList()),
                        ),
                      )
                    ],
                  ),
                ),
                load
                    ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircularProgressIndicator(),
                )
                    : Row(
                  children: <Widget>[
                    Expanded(
                        child: Container(
                          child: selectedValue == null ||
                              selectedValue == 'All'
                              ? ListView.builder(
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              itemCount: staffByParentList?.length ?? 0,
                              itemBuilder:
                                  (BuildContext context, int index) {
                                return CheckboxListTile(
                                  value: _finalSelectedStaff
                                      .contains(staffByParentList[index]),
                                  onChanged: (bool selected) {
                                    _onStaffSelected(selected,
                                        staffByParentList[index]);
                                  },
                                  title: Row(
                                    children: <Widget>[
                                      CircleAvatar(
                                        backgroundImage: staffByParentList[
                                        index]['image'] !=
                                            null
                                            ? NetworkImage(
                                            "https://d212imxpbiy5j1.cloudfront.net/${staffByParentList[index]['image']}")
                                            : AssetImage(
                                            "assets/nophoto.jpg"),
                                        radius: 25.0,
                                      ),
                                      Container(width: 10.0),
                                      Text(staffByParentList[index]
                                      ['fullname'])
                                    ],
                                  ),
                                );
                              })
                              : ListView.builder(
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              itemCount: _selectedStaff.length,
                              itemBuilder:
                                  (BuildContext context, int index) {
                                return CheckboxListTile(
                                  value: _finalSelectedStaff
                                      .contains(_selectedStaff[index]),
                                  controlAffinity:
                                  ListTileControlAffinity.leading,
                                  onChanged: (bool selected) {
                                    _onStaffSelected(
                                        selected, _selectedStaff[index]);
                                  },
                                  title: Row(
                                    children: <Widget>[
                                      CircleAvatar(
                                        backgroundImage: _selectedStaff[
                                        index]['image'] !=
                                            null
                                            ? NetworkImage(
                                            "https://d212imxpbiy5j1.cloudfront.net/${_selectedStaff[index]['image']}")
                                            : AssetImage(
                                            "assets/nophoto.jpg"),
                                        radius: 25.0,
                                      ),
                                      Container(width: 10.0),
                                      Text(_selectedStaff[index]
                                      ['fullname'])
                                    ],
                                  ),
                                );
                              }),
                        ))
                  ],
                )
              ],
            )
          ],
        ),
      ),
      floatingActionButton: _finalSelectedStaff.length == 0
          ? Container()
          : _getMailFloatingActionButton(),
    );
  }

  FloatingActionButton _getMailFloatingActionButton() {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  CreateMessage(_finalSelectedStaff, staffByParentList)),
        );
      },
      child: Icon(
        Icons.message,
        size: 30.0,
      ),
      backgroundColor: kinderm8Theme.Colors.appcolour,
    );
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      userId = users["id"];

      print(" after the + click iddd  $userId");
      print(clientId);

      getStaffByParent();
      getRoomWithStaff();
    } catch (e) {
      print("That string didn't look like Json.");
    }
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
