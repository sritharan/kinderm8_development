import 'dart:async';
import 'dart:convert';
import 'dart:io';
//import 'package:url_launcher/url_launcher.dart';
//import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:connectivity/connectivity.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:kinderm8/auth.dart';
import 'package:kinderm8/data/database_helper.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/dailysummarydatalist.dart';
import 'package:kinderm8/pages/home/home.dart';
import 'package:kinderm8/pages/login/loginAnimation.dart';
import 'package:kinderm8/pages/login/login_presenter.dart';
import 'package:kinderm8/pages/login/multiple_login.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kinderm8/components/WhiteTick.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:kinderm8/components/SignUpLink.dart';

const SupportUrl = 'https://kinderm8.com.au/support';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage>
    with TickerProviderStateMixin
    implements AuthStateListener, LoginPageContract {
  AnimationController _loginButtonController;
  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _passController = new TextEditingController();

  final FocusNode myFocusNodeEmailLogin = FocusNode();
  final FocusNode myFocusNodePasswordLogin = FocusNode();

  final loginEditingController = TextEditingController();
  bool _buttonvisible = false;
  bool _obscureTextLogin = true;
  var devicetoken;
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  bool isOffline = false;
  AuthState loginState;
  bool canProceed = true;
  bool dialogIsVisible = false;
  void _toggleLogin() {
    setState(() {
      _obscureTextLogin = !_obscureTextLogin;
    });
  }

  // Push
  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  var animationStatus = 0;
  List sinid = new List();
  List sinemail = new List();
  List sinclients = List();
  List sinclient_id = List();
  List sincenter = List();
  List sinimage = List();
  List sincardList = List();

  LoginPagePresenter _presenter;
  LoginPageState() {
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
    _presenter = LoginPagePresenter(this);
  }

  @override
  void initState() {
    super.initState();
//    webview.close();
//    device token : elRj9gmBV3c:APA91bFhorY_JcORn7xk3gXpks0QNWK4-azf0_orMiqPiG_9GzvyUt8MLw1pJhH3TPJlpcRuGU1lI6TAvKm1EyQxjP6JyOwsw2WWQHy0vk3NL8vw8isNKNsBgh7xqu5BzySVPMAk5unf
    loginState = AuthState.LOGGED_OUT;
    _firebaseMessaging.autoInitEnabled();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
//    // TODO: get push token and send to login
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message){
        print('onMessage $message');
      },
      onResume: (Map<String, dynamic> message){
        print('onResume $message');
      },
      onLaunch: (Map<String, dynamic> message){
        print('onLaunch $message');
      },
    );
    _firebaseMessaging.getToken().then((token){
      print('device token : $token');
      devicetoken = token;
    });
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });

    _emailController.addListener(_LatestValue);
    _passController.addListener(_LatestValue);
    _loginButtonController = new AnimationController(
        duration: new Duration(milliseconds: 3000), vsync: this);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    _emailController.dispose();
    _passController.dispose();
    _loginButtonController.dispose();
    super.dispose();
  }
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          isOffline = false;
          dialogIsVisible = false;
        });
        break;
      case ConnectivityResult.mobile:
        setState(() {
          isOffline = false;
          dialogIsVisible = false;
        });
        break;
      case ConnectivityResult.none:
        setState(() => isOffline = true);
        buildAlertDialog();
        break;
      default:
        setState(() => isOffline = true);
        break;
    }
  }

  void buildAlertDialog() {
    SchedulerBinding.instance.addPostFrameCallback((_) => setState(() {
      if (isOffline && !dialogIsVisible) {
        dialogIsVisible = true;
        showDialog(
            barrierDismissible: false,
            context: _ctx,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Internet connection cannot be establised!", textAlign: TextAlign.center, style: TextStyle(fontSize: 14.0),),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Icon(Icons.portable_wifi_off, color: Colors.redAccent, size: 36.0,),
                    !canProceed ? Text(
                      "Check your internet connection before proceeding.", textAlign: TextAlign.center, style: TextStyle(fontSize: 12.0),) : Text(
                      "Please! proceed by connecting to a internet connection", textAlign: TextAlign.center, style: TextStyle(fontSize: 12.0, color: Colors.red),),
                  ],
                ),
                actions: <Widget>[
                  RaisedButton(
                    onPressed: () {
                      SystemChannels.platform
                          .invokeMethod('SystemNavigator.pop');
                    },
                    child: Text(
                      "CLOSE THE APP",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  RaisedButton(
                    onPressed: () {
                      if (isOffline) {
                        setState(() {
                          canProceed = false;
                        });
                      } else {
                        Navigator.pop(_ctx);
                        onAuthStateChanged(loginState);
                      }
                    },
                    child: Text(
                      "PROCEED",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              );
            });
        return;
      }
    }));
  }

  _LatestValue() {
    setState(() {
      if(_emailController.text.isEmpty || _passController.text.isEmpty ){
        _buttonvisible = false;
        print(" _buttonvisible: ${_emailController.text.isEmpty}");
      }else{
        _buttonvisible = true;
      }
      print("email text field: ${_emailController.value}");
      print("pass text field: ${_passController.value}");
    });
  }

  Future<Null> _playAnimation() async {
    try {
      _submit();
      await _loginButtonController.forward();
      await _loginButtonController.reverse();
    } on TickerCanceled {}
  }

  var progress = new ProgressBar(
//    backgroundColor: Colors.black12,
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  // Need help url open
//  Future launchURL() async {
//    const url = 'https://kinderm8.com.au/support';
//    if (await canLaunch(url)) {
//      await launch(url,forceSafariVC: true,forceWebView: true);
//    } else {
//      throw 'Could not launch $url';
//    }
//  }

  // Pop to ask do you wish to exit
  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          child: new AlertDialog(
            title: new Text('Are you sure do you want to exit?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => exit(0),
//                    Navigator.pushReplacementNamed(context, "/home"),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }

  // Email checker
  bool _isvalidEmail;

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value)) {
//      return 'Enter Valid Email';
      showInSnackBar("Enter Valid Email");
      _isvalidEmail = true;
      setState(() {
        _isLoading = false;
      });
    } else {
      _isvalidEmail = false;
      return null;
    }
  }

  BuildContext _ctx;
  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _email, _password;

  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      backgroundColor: Colors.redAccent,
    ));
    setState(() {
      _isLoading = false;
    });
  }

  void _submit() {
    print("Submit 2");
    print('_isvalidEmail >>>$_isvalidEmail');
    _email = _passController.text;
    _password = _passController.text;
    final form = formKey.currentState;
    if (form.validate()) {
      setState(() {
        if (!_isvalidEmail) {
          _isLoading = true;
          form.save();
          validatelogin(_email, _password);
        }
      });
    }
  }

  RegExp emailchecker = new RegExp(
      r"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$");
  validatelogin(email, password) {
    _email = _passController.text;
    _password = _passController.text;
    if (_email == "" && _password == "") {
      showInSnackBar("Email & Password can't be empty");
      setState(() {
        _isLoading = false;
      });
    } else if (_email == "" || _password == "") {
      if (_email == "") {
        showInSnackBar("Email can't be empty");
        setState(() {
          _isLoading = false;
        });
      }
      if (_password == "") {
        showInSnackBar("Password can't be empty");
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      final LOGIN_URL = 'http://13.55.4.100:7070/v2.1.0/setupuseroneapp';
      NetworkUtil _netutil = new NetworkUtil();

      _netutil.post(LOGIN_URL,
          body: {"email": email, "password": password}).then((res) {
        print(res);
        try {
          if (res != null) {
            List decodedList = json.decode(res);
            print("navigate to multiple");
            setState(() {
              Navigator.pushReplacement(
                  context,
                  new MaterialPageRoute(
                      builder: (context) =>
                          new MultipleLogin(password, decodedList)));
            });
          } else {
            final bar = SnackBar(
              content: Text(
                  "Unable to login, either your email or password is incorrect."),
              duration: Duration(seconds: 3),
              backgroundColor: Colors.redAccent,
            );
            scaffoldKey.currentState.showSnackBar(bar);
            setState(() {
              _isLoading = false;
            });
          }
        } catch (e) {
          print(e);
        }
      });
    }
  }

  void showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  @override
  void onAuthStateChanged(AuthState state) {
    setState(() {
      loginState = state;
    });
    if (state == AuthState.LOGGED_IN) {
//      Navigator.of(_ctx).pushReplacementNamed('/home');
      Navigator.push(
          _ctx, new MaterialPageRoute(builder: (context) => new HomePage()));
    }
  }

  Future loginWithEmail(email, password) async {
    _email = email;
    _password = password;
    print("ISLOAD=$_isLoading");

    print('Login Data >>>>>>>> $_email$_password');
    if (_email == "" && _password == "") {
      showInSnackBar("Email & Password can't be empty");
      setState(() {
//        _isLoading = false;
      });
    } else if (_email == "" || _password == "") {
      if (_email == "") {
        showInSnackBar("Email can't be empty");
        setState(() {
          _isLoading = false;
        });
      }
      if (_password == "") {
        showInSnackBar("Password can't be empty");
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      setState(() {
        _isLoading = true;
      });
      final LOGIN_URL = 'http://13.55.4.100:7070/v2.1.0/setupuseroneapp';
      NetworkUtil _netutil = new NetworkUtil();

      _netutil.post(LOGIN_URL,
          body: {"email": email, "password": password}).then((res) {
        print(res);
        try {
          if (res != null) {
            List decodedList = json.decode(res);

            if (decodedList.length == 1) {
              print("navigate to single");

              var user = decodedList[0]["user"];
              sinid.add(user["id"]);
              sinemail.add(user["email"]);
              sinclient_id.add(user["client_id"]);
              sincenter.add(user["center"]);
              sinimage.add(user["image"]);
              sinclients.add(user["client"]);

              var result = {
                "email": sinemail[0],
                "password": password,
                "client_id": sinclient_id[0],
                "center": sincenter[0],
                "image": sinimage[0]
              };
              _presenter.doLogin(result);
              print("navigate to single -> webloginWithEmail");
              webloginWithEmail(sinemail[0],password,sinclient_id[0]);
            } else {
              print("navigate to multiple");
              setState(() {
                _isLoading = false;
                Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                        builder: (context) =>
                            new MultipleLogin(password, decodedList)));
              });
            }
          } else {
            final bar = SnackBar(
              content: Text(
                  "Sorry, we couldn't find entered login credentials, please try again!"),
              duration: Duration(seconds: 3),
              backgroundColor: Colors.redAccent,
            );
            scaffoldKey.currentState.showSnackBar(bar);
            setState(() {
              _isLoading = false;
            });
          }
        } catch (e) {
          print(e);
        }
      });
    }
  }

  // Weblogin using client id
  Future webloginWithEmail(email, password, clientid) async {
    print('called webloginWithEmail');
    _email = email;
    _password = password;
    print("ISLOAD=$_isLoading");

    print('Login Data >>>>>>>> $_email$_password');
      try {
      setState(() {
        _isLoading = true;
      });
      final LOGIN_URL = 'https://${clientid}.kinderm8.com.au/api/v1/auth';
//      NetworkUtil _netutil = new NetworkUtil();
      print('LOGIN_URL $LOGIN_URL');
      http.post(LOGIN_URL, headers: {"mobile-api": "true", "Content-Type": "application/json"}, body: json.encode({"username": email, "password": password})).then((res) {
        print(res.toString());
        print(res.body);
        try {
          if (res != null) {
            List decodedList = json.decode(res.body);

            if (decodedList.length == 1) {
              print("navigate to single");
              print("web login success");
              var user = decodedList[0]["user"];
              print(decodedList.toString());

//              _presenter.doLogin(result);
            } else {
              print("navigate to multiple");
              setState(() {
                _isLoading = false;
//                Navigator.pushReplacement(
//                    context,
//                    new MaterialPageRoute(
//                        builder: (context) =>
//                        new MultipleLogin(password, decodedList)));
              });
            }
          } else {
            final bar = SnackBar(
              content: Text(
                  "Sorry, we couldn't find entered login credentials, please try again!"),
              duration: Duration(seconds: 3),
              backgroundColor: Colors.redAccent,
            );
            scaffoldKey.currentState.showSnackBar(bar);
            setState(() {
              _isLoading = false;
            });
          }
        } catch (e) {
          print(e);
        }
      });
    }
    catch(e){
        print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;

    final Size screenSize = MediaQuery.of(context).size;
    return WillPopScope(
        onWillPop: _onWillPop,
        child: new Scaffold(
          key: scaffoldKey,
          backgroundColor: Theme.Colors.app_white,
//        appBar: new AppBar(
//          backgroundColor: Colors.,
//          title: new GestureDetector(
//            onLongPress: () {},
//            child: new Text(
//              "Login",
//              style: new TextStyle(color: Colors.white),
//            ),
//          ),
//          centerTitle: true,
//        ),
          resizeToAvoidBottomPadding: true,
          body: new SingleChildScrollView(
              child: Container(
                height: screenSize.height,
//            width: MediaQuery.of(context).size.width,
//            height: MediaQuery.of(context).size.height >= 775.0
//                ? MediaQuery.of(context).size.height
//                : 775.0,
//          decoration: new BoxDecoration(
//            gradient: new LinearGradient(
//                colors: [
//                  Theme.Colors.app_white,
//                  Theme.Colors.appBarGradientEnd
//                ],
//                begin: const FractionalOffset(0.0, 0.0),
//                end: const FractionalOffset(1.0, 1.0),
//                stops: [0.0, 2.0],
//                tileMode: TileMode.clamp),
//          ),
            child: new Column(
              children: <Widget>[
                new ClipPath(
                  child: Container(
                    width: screenSize.width,
                    height: screenSize.width / 2,
                    decoration: new BoxDecoration(
                      color: Theme.Colors.appBarGradientEnd,
//                      borderRadius: new BorderRadius.only(
//                          topLeft: new Radius.circular(20.0),
//                          topRight: new Radius.circular(20.0)
//                      )
                    ),
//                  constraints: const BoxConstraints(maxHeight: 400.0),
                    child: Container(
                      margin: new EdgeInsets.only(
                          left: screenSize.width / 5,
                          right: screenSize.width / 5),
                      child: new SizedBox(
                        child: new Image.asset(
                          "assets/kinderm8childcare-white-new.png",
                          fit: BoxFit.contain,
                          width: screenSize.width / 6,
                        ),
                      ),
                    ),
                  ),
                  clipper: new DialogonalClipper(),
                ),
                new Container(
                  height: screenSize.width,
                  margin: new EdgeInsets.only(left: 5.0, right: 5.0),
//                  decoration: new BoxDecoration(
//                      color: Colors.white,
//                      borderRadius: new BorderRadius.only(
//                          topLeft: new Radius.circular(20.0),
//                          topRight: new Radius.circular(20.0))
//                  ),
//                  constraints: const BoxConstraints(maxHeight: 400.0),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        "Welcome ",
                        style: new TextStyle(
                            fontSize: 24.0, color: Theme.Colors.bigtext),
                      ),
                      new Text(
                        "It's good to see you! ",
                        style: new TextStyle(
                            fontSize: 16.0, color: Theme.Colors.boldText),
                      ),
                      new Container(
                        height: 60.0,
                        margin: new EdgeInsets.only(top: 5.0),
                        child: new Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: new Container(
                            width: screenSize.width,
                            margin: new EdgeInsets.only(
                                left: 10.0, right: 10.0, bottom: 2.0),
                            height: 60.0,
                            decoration: new BoxDecoration(
                                borderRadius: new BorderRadius.all(
                                    new Radius.circular(20.0))),
                            child: new TextFormField(
                              focusNode: myFocusNodeEmailLogin,
                              controller: _emailController,
                              keyboardType: TextInputType.emailAddress,
                              style: new TextStyle(
                                  color: Theme.Colors.logintext,
                                  fontSize: 18.0),
                              decoration: new InputDecoration(
                                prefixIcon: new Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: new Icon(
                                    Icons.email,
                                    size: 20.0,
                                  ),
                                ),
                                contentPadding: EdgeInsets.all(12.0),
                                labelText: "Email",
                                labelStyle: new TextStyle(
                                    fontSize: 20.0,
                                    color: Theme.Colors.textboxborder),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                    borderSide: new BorderSide(
                                        color: Theme.Colors.textboxborder)),
                              ),
                            ),
                          ),
                        ),
                      ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Padding(
//                            padding: const EdgeInsets.all(2.0),
                            padding:
                                const EdgeInsets.only(left: 20.0, right: 20.0),
                            child: new GestureDetector(
                              onTap: () {
                                Navigator.of(context)
                                    .pushNamed('/forgetpassword');
//                                Navigator.of(context).push(
//                                    new CupertinoPageRoute(
//                                        builder: (BuildContext context) =>
//                                        new GirliesSignUp()));
                              },
                              child: Container(
//                                margin: new EdgeInsets.only(right: 5.0),
                                child: new Text(
                                  "Forgot?",
                                  style: new TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.loginlabel),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      new Container(
//                        height: 60.0,
//                        margin: new EdgeInsets.only(top: 5.0),
                        child: new Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: new Container(
                            width: screenSize.width,
                            margin: new EdgeInsets.only(
                                left: 10.0, right: 10.0, bottom: 2.0),
                            height: 60.0,
                            decoration: new BoxDecoration(
                                borderRadius: new BorderRadius.all(
                                    new Radius.circular(20.0))),
                            child: new TextFormField(
                              controller: _passController,
                              focusNode: myFocusNodePasswordLogin,
                              obscureText: _obscureTextLogin,
//                            obscureText: true,
                              style: new TextStyle(
                                  color: Theme.Colors.logintext,
                                  fontSize: 18.0),
                              decoration: new InputDecoration(
                                prefixIcon: new Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: new Icon(
                                    Icons.lock,
                                    size: 20.0,
                                  ),
                                ),
                                contentPadding: EdgeInsets.all(12.0),
                                labelText: "Password",
                                labelStyle: new TextStyle(
                                    fontSize: 20.0,
                                    color: Theme.Colors.textboxborder),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                    borderSide: new BorderSide(
                                        color: Theme.Colors.textboxborder)),
                                suffixIcon: GestureDetector(
                                  onTap: _toggleLogin,
                                  child: Icon(
                                    Icons.remove_red_eye,
                                    size: 25.0,
                                    color: Theme.Colors.appcolour,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      new Padding(
                        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
//                            new Padding(
//                              padding: const EdgeInsets.all(2.0),
//                              child: new GestureDetector(
//                                onTap: () {
//                                  Navigator.of(context)
//                                      .pushNamed('/forgetpassword');
//                                },
//                                child: new Text(
//                                  "Forgot Password?",
//                                  style: new TextStyle(
//                                      fontSize: 14.0,
//                                      color: Theme.Colors.loginlabel),
//                                ),
//                              ),
//                            ),
                            new Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: new GestureDetector(
                                onTap: () {
//                                  Navigator.of(context).pushNamed('/support');
//                                Navigator.of(context).push(
//                                    new CupertinoPageRoute(
//                                        builder: (BuildContext context) =>
//                                        new Kinderm8Support()));
                                },
                                child: new Text(
                                  "Help?",
                                  style: new TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.loginlabel),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Padding(
                        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
//                          new Padding(
//                            padding: const EdgeInsets.all(2.0),
//                            child: new GestureDetector(
//                              onTap: () {
//                                Navigator.of(context).push(
//                                    new CupertinoPageRoute(
//                                        builder: (BuildContext context) =>
//                                        new GirliesSignUp()));
//                              },
//                              child: new Text(
//                                "SignUp",
//                                style: new TextStyle(
//                                    fontSize: 14.0, color: Colors.black),
//                              ),
//                            ),
//                          ),
                          ],
                        ),
                      ),
                      _buttonvisible ? _isLoading
                          ? new CircularProgressIndicator(
                              backgroundColor: Theme.Colors.appcolour,
                            )
                          : new InkWell(
                              onTap: () {
//                        _submit();
                                setState(() {
                                  _isLoading = true;
                                });
                                loginWithEmail(_emailController.text,
                                    _passController.text);
                              },
                              child: new Container(
                                height: 60.0,
                                margin: new EdgeInsets.only(top: 5.0),
                                child: new Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: new Container(
                                    width: screenSize.width,
                                    margin: new EdgeInsets.only(
                                        left: 10.0, right: 10.0, bottom: 2.0),
//                                    height: 60.0,
                                    decoration: new BoxDecoration(
                                        color: Theme.Colors.loginbutton,
                                        borderRadius: new BorderRadius.all(
                                            new Radius.circular(20.0))),
                                    child: new Center(
                                        child: new Text(
                                      "Login",
                                      style: new TextStyle(
                                          color: Colors.white, fontSize: 20.0),
                                    )),
                                  ),
                                ),
                              ),
                            ): new Container(),
//                      new Padding(
//                        padding: const EdgeInsets.all(10.0),
//                        child: new Row(
//                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                          children: <Widget>[
//                            new Container(
//                              height: 1.0,
//                              width: 100.0,
//                              color: Colors.black26,
//                            ),
//                            new Text(
//                              "",
//                              style: new TextStyle(
//                                fontSize: 14.0,
//                                color: Colors.black,
//                              ),
//                            ),
//                            new Container(
//                              height: 1.0,
//                              width: 100.0,
//                              color: Colors.black26,
//                            ),
//                          ],
//                        ),
//                      ),
//                      new Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                        children: <Widget>[
//                          /*new Column(
//                            children: <Widget>[
//                              new GestureDetector(
//                                onTap: () {
//                                  loginWithFacebook();
//                                },
//                                child: new Container(
//                                  decoration: new BoxDecoration(
//                                    borderRadius: new BorderRadius.circular(30.0),
//                                    color: Colors.blue[800],
//                                  ),
//                                  child: new CircleAvatar(
//                                    backgroundColor: Colors.white,
//                                    radius: 30.0,
//                                    backgroundImage: new AssetImage(
//                                        'assets/images/facebook.png'),
//                                  ),
//                                ),
//                              ),
//                              new Padding(
//                                padding: const EdgeInsets.all(5.0),
//                                child: new Text(
//                                  "Facebook",
//                                  style: new TextStyle(
//                                      fontSize: 18.0, color: Colors.blue[800]),
//                                ),
//                              ),
//                            ],
//                          ),*/
//                          new Column(
//                            children: <Widget>[
////                            new GestureDetector(
////                              onTap: () {
//////                                loginWithGoogle();
////                              },
////                              child: new Container(
////                                decoration: new BoxDecoration(
////                                  borderRadius: new BorderRadius.circular(30.0),
////                                  color: Colors.yellow[800],
////                                ),
////                                child: new CircleAvatar(
////                                  backgroundColor: Colors.white,
////                                  radius: 30.0,
////                                  backgroundImage: new AssetImage(
////                                      'assets/logo.png'),
////                                ),
////                              ),
////                            ),
//                              new Padding(
//                                padding: const EdgeInsets.all(5.0),
//                                child: new Text(
//                                  "V 5.0",
//                                  style: new TextStyle(
//                                      fontSize: 18.0,
//                                      color: Theme.Colors.versionlabel),
//                                ),
//                              ),
//                            ],
//                          ),
//                          /*new Column(
//                            children: <Widget>[
//                              new GestureDetector(
//                                onTap: () {
//                                  loginWithTwitter();
//                                },
//                                child: new Container(
//                                  decoration: new BoxDecoration(
//                                    borderRadius: new BorderRadius.circular(30.0),
//                                    color: Colors.blue[300],
//                                  ),
//                                  child: new CircleAvatar(
//                                    backgroundColor: Colors.white,
//                                    radius: 30.0,
//                                    backgroundImage: new AssetImage(
//                                        'assets/images/twitter.png'),
//                                  ),
//                                ),
//                              ),
//                              new Padding(
//                                padding: const EdgeInsets.all(5.0),
//                                child: new Text(
//                                  "Twitter",
//                                  style: new TextStyle(
//                                      fontSize: 18.0, color: Colors.blue[300]),
//                                ),
//                              ),
//                            ],
//                          ),*/
//                        ],
//                      )
                    ],
                  ),
                ),
                new ClipPath(
                  child: Container(
                    width: screenSize.width,
                    height: screenSize.width / 10,
                    decoration: new BoxDecoration(
                      color: Theme.Colors.appBarGradientEnd,
//                      borderRadius: new BorderRadius.only(
//                          topLeft: new Radius.circular(20.0),
//                          topRight: new Radius.circular(20.0)
//                      )
                    ),
//                  constraints: const BoxConstraints(maxHeight: 400.0),
                    child: Container(
                      margin: new EdgeInsets.only(
                          left: screenSize.width / 5,
                          right: screenSize.width / 5),
//                      width: 50.0,
                      height: screenSize.height,
                      child: new SizedBox(
                        child: new Image.asset(
                          "",
                          fit: BoxFit.contain,
                          width: screenSize.width,
                          height: screenSize.height,
                        ),
                      ),
                    ),
                  ),
                  clipper: new DialogonalClipper_bottom(),
                ),
                Flexible(
                  flex: 1,
                  child: Container(
                    color: Theme.Colors.appBarGradientEnd,
                  ),
                ),

//                new Container(
//                  alignment: Alignment.bottomCenter,
////                  decoration: new BoxDecoration(
////                    color: Colors.blue,
////                    border: new Border.all(),
////                    borderRadius: new BorderRadius.circular(25.0),
////                  ),
//                  child: new Container(
//                      constraints: const BoxConstraints(maxHeight: 600.0),
//                      color: Theme.Colors.appcolour,
//                      height: 100.0
//                  ),
//                )
              ],
            ),
          )
//    )
              ),
        ));
  }

  @override
  void onLoginSuccess(User user) async {
    print("on login single");
    var db = new DatabaseHelper();
    print('email ${user.email}');
    print('password ${user.password}');
    print('client_id${user.client_id}');

    print('webloginWithEmail');
    webloginWithEmail(user.email,user.password,user.client_id);

    await db.saveUser(user);
//    showSnackBar(user.toString());
    setState(() {
      Navigator.pushReplacement(context,
          new MaterialPageRoute(builder: (context) => new DailySummary()));
    });
    _isLoading = false;
  }

  @override
  void onLoginError(String error) {
    showSnackBar(error);
    setState(() => _isLoading = false);
  }
}

class DialogonalClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    // Since the wave goes vertically lower than bottom left starting point,
    // we'll have to make this point a little higher.
    path.lineTo(0.0, size.height);
    // TODO: The wavy clipping magic happens here, between the bottom left and bottom right points.
    // The bottom right point also isn't at the same level as its left counterpart,
    // so we'll adjust that one too.
    path.lineTo(size.width, size.height - 60);
    path.lineTo(size.width, 0.0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}

class DialogonalClipper_bottom extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.moveTo(size.width, 0.0);
    path.lineTo(0.0, size.height);
    path.lineTo(size.width, size.width);
    path.close();
    return path;
  }
//  Path getClip(Size size) {
//    Path path = new Path();
//    // Since the wave goes vertically lower than bottom left starting point,
//    // we'll have to make this point a little higher.
//    path.lineTo(0.0, size.height);
//    // TODO: The wavy clipping magic happens here, between the bottom left and bottom right points.
//    // The bottom right point also isn't at the same level as its left counterpart,
//    // so we'll adjust that one too.
////    path.lineTo(size.width, size.height - 60);
//    path.lineTo(size.width, 0.0);
//
//    path.lineTo(size.width, 0.0);
//    path.lineTo(size.width / 2, size.height);
//
//    path.close();
//    return path;
//  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
