import 'dart:convert';
import 'package:meta/meta.dart';

var data = [
  {
    "id": 499,
    "program_type_id": 4,
    "program_type": "News Feed",
    "program_color": "#EF6C00",
    "title": "Test 3",
    "room": {
      "id": 29,
      "title": "Room Silver",
      "description": "Room Silver",
      "created_at": "2018-07-27 19:09:19.0"
    },
    "educator": {
      "id": 21,
      "fullname": "Benson Smith",
      "second_email": "",
      "username": "benson",
      "isAdmin": "0",
      "isStaff": "1",
      "token":
          "9b1a9f8dd6aeaf20df9012d903f4f71416770dcf7a079a44a990969b0cc0e4be",
      "email": "sewwandi577@gmail.com",
      "image":
          "https://d212imxpbiy5j1.cloudfront.net/img/uploads/production_staffpic1502517862.jpg",
      "status": "0",
      "loginaccess": "0",
      "need_sec_email": "0",
      "client_id": "production",
      "center": "PRODUCTION",
      "client": {
        "id": 155,
        "clientid": "production",
        "name": "PRODUCTION",
        "connectionString":
            "jdbc:mysql://proitzenclouddb.c8fcq2q9we9x.ap-southeast-2.rds.amazonaws.com:3306/production",
        "AuthURI": "https://production.kinderm8.com.au/api/v1/auth",
        "kiosk": true,
        "active": true,
        "pincode": "PRO2590",
        "logo":
            "https://d212imxpbiy5j1.cloudfront.net/img/uploads/production_1499145575.Kinderm8.png?1499145695",
        "kisokparentemailnotification": false,
        "signature": true,
        "center_login_required": false,
        "timezone": "Australia/NSW",
        "kinderm8version": "1.8",
        "parent_kiosk_enable": false,
        "staff_kiosk_enable": false,
        "enable_all_rooms_child_checking": false,
        "enable_all_rooms_staff_checking": false,
        "staffap": false
      },
      "isparent": false,
      "created_at": "2017-08-12 16:04:24.0"
    },
    "subject_area": "123 the tree mangoes papaya oranges",
    "time": "10:34 AM - 10:49 AM",
    "description":
        "Water covers 71% of the Earth's surface, mostly in seas and oceans.[1] Small portions of water occur as groundwater (1.7%), in the glaciers and the ice caps of Antarctica and Greenland (1.7%), and in the air as vapor, clouds (formed of ice and liquid water suspended in air), and precipitation (0.001%).[2][3]",
    "start_date": "2018-11-06",
    "end_date": "2018-11-13",
    "start_time": "10:34 AM",
    "end_time": "10:49 AM",
    "created_user": {
      "id": 1,
      "fullname": "Production admin",
      "username": "admin",
      "isAdmin": "1",
      "isStaff": "0",
      "email": "kinderm84@hotmail.com",
      "image":
          "https://d212imxpbiy5j1.cloudfront.net/img/uploads/production_1525154496.png",
      "status": "0",
      "loginaccess": "0",
      "need_sec_email": "0",
      "client_id": "production",
      "center": "PRODUCTION",
      "client": {
        "id": 155,
        "clientid": "production",
        "name": "PRODUCTION",
        "connectionString":
            "jdbc:mysql://proitzenclouddb.c8fcq2q9we9x.ap-southeast-2.rds.amazonaws.com:3306/production",
        "AuthURI": "https://production.kinderm8.com.au/api/v1/auth",
        "kiosk": true,
        "active": true,
        "pincode": "PRO2590",
        "logo":
            "https://d212imxpbiy5j1.cloudfront.net/img/uploads/production_1499145575.Kinderm8.png?1499145695",
        "kisokparentemailnotification": false,
        "signature": true,
        "center_login_required": false,
        "timezone": "Australia/NSW",
        "kinderm8version": "1.8",
        "parent_kiosk_enable": false,
        "staff_kiosk_enable": false,
        "enable_all_rooms_child_checking": false,
        "enable_all_rooms_staff_checking": false,
        "staffap": false
      },
      "isparent": false,
      "created_at": "2017-06-02 17:06:43.0"
    },
    "shareable": 1,
    "complete_status": 0,
    "learning_tags": [],
    "outcomes": [
      {
        "id": 73,
        "title": "Outcome 1: Identity #3",
        "description":
            "Children develop knowledgeable and confident self-identities.",
        "clearningset_id": 3,
        "isfolder": "0",
        "created_at": "2016-02-17 03:46:45.0",
        "updated_at": "2016-02-17 03:46:57.0",
        "learning_set_title": "EYLF Outcomes"
      },
      {
        "id": 74,
        "title": "Outcome 1: Identity #4",
        "description":
            "Children learn to interact in relation to others with care, empathy &amp; respect.",
        "clearningset_id": 3,
        "isfolder": "0",
        "created_at": "2016-02-17 03:47:21.0",
        "updated_at": "2016-02-17 03:47:21.0",
        "learning_set_title": "EYLF Outcomes"
      },
      {
        "id": 84,
        "title": "Outcome 4: Learning #1",
        "description":
            "Children develop dispositions for learning such as curiosity, cooperation, confidence, creativity, commitment, enthusiasm, persistence, imagination &amp; reflexivity.",
        "clearningset_id": 3,
        "isfolder": "0",
        "created_at": "2016-02-17 03:50:01.0",
        "updated_at": "2016-02-17 03:50:01.0",
        "learning_set_title": "EYLF Outcomes"
      }
    ],
    "programdates": [
      {"id": 4193, "date": "2018-11-06", "custom_time": 0, "manual_status": 0},
      {"id": 4194, "date": "2018-11-08", "custom_time": 0, "manual_status": 0},
      {"id": 4195, "date": "2018-11-12", "custom_time": 0, "manual_status": 0}
    ],
    "created_at": "2018-10-05 15:21:35",
    "updated_at": "2018-10-05 15:21:35"
  }
];

List<Program> programFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<Program>.from(jsonData.map((x) => Program.fromJson(x)));
}

String programToJson(List<Program> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class Program {
  int id;
  int programTypeId;
  String programType;
  String programColor;
  String title;
  Room room;
  CreatedUser educator;
  String subjectArea;
  String time;
  String description;
  String startDate;
  String endDate;
  String startTime;
  String endTime;
  CreatedUser createdUser;
  int shareable;
  int completeStatus;
  List<dynamic> learningTags;
  List<Outcome> outcomes;
  List<Programdate> programdates;
  String createdAt;
  String updatedAt;

  Program({
    this.id,
    this.programTypeId,
    this.programType,
    this.programColor,
    this.title,
    this.room,
    this.educator,
    this.subjectArea,
    this.time,
    this.description,
    this.startDate,
    this.endDate,
    this.startTime,
    this.endTime,
    this.createdUser,
    this.shareable,
    this.completeStatus,
    this.learningTags,
    this.outcomes,
    this.programdates,
    this.createdAt,
    this.updatedAt,
  });

  factory Program.fromJson(Map<String, dynamic> json) => new Program(
        id: json["id"],
        programTypeId: json["program_type_id"],
        programType: json["program_type"],
        programColor: json["program_color"],
        title: json["title"],
        room: Room.fromJson(json["room"]),
        educator: CreatedUser.fromJson(json["educator"]),
        subjectArea: json["subject_area"],
        time: json["time"],
        description: json["description"],
        startDate: json["start_date"],
        endDate: json["end_date"],
        startTime: json["start_time"],
        endTime: json["end_time"],
        createdUser: CreatedUser.fromJson(json["created_user"]),
        shareable: json["shareable"],
        completeStatus: json["complete_status"],
        learningTags:
            new List<dynamic>.from(json["learning_tags"].map((x) => x)),
        outcomes: new List<Outcome>.from(
            json["outcomes"].map((x) => Outcome.fromJson(x))),
        programdates: new List<Programdate>.from(
            json["programdates"].map((x) => Programdate.fromJson(x))),
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "program_type_id": programTypeId,
        "program_type": programType,
        "program_color": programColor,
        "title": title,
        "room": room.toJson(),
        "educator": educator.toJson(),
        "subject_area": subjectArea,
        "time": time,
        "description": description,
        "start_date": startDate,
        "end_date": endDate,
        "start_time": startTime,
        "end_time": endTime,
        "created_user": createdUser.toJson(),
        "shareable": shareable,
        "complete_status": completeStatus,
        "learning_tags": new List<dynamic>.from(learningTags.map((x) => x)),
        "outcomes": new List<dynamic>.from(outcomes.map((x) => x.toJson())),
        "programdates":
            new List<dynamic>.from(programdates.map((x) => x.toJson())),
        "created_at": createdAt,
        "updated_at": updatedAt,
      };
}

class CreatedUser {
  int id;
  String fullname;
  String username;
  String isAdmin;
  String isStaff;
  String email;
  String image;
  String status;
  String loginaccess;
  String needSecEmail;
  String clientId;
  String center;
  Client client;
  bool isparent;
  String createdAt;
  String secondEmail;
  String token;

  CreatedUser({
    this.id,
    this.fullname,
    this.username,
    this.isAdmin,
    this.isStaff,
    this.email,
    this.image,
    this.status,
    this.loginaccess,
    this.needSecEmail,
    this.clientId,
    this.center,
    this.client,
    this.isparent,
    this.createdAt,
    this.secondEmail,
    this.token,
  });

  factory CreatedUser.fromJson(Map<String, dynamic> json) => new CreatedUser(
        id: json["id"],
        fullname: json["fullname"],
        username: json["username"],
        isAdmin: json["isAdmin"],
        isStaff: json["isStaff"],
        email: json["email"],
        image: json["image"],
        status: json["status"],
        loginaccess: json["loginaccess"],
        needSecEmail: json["need_sec_email"],
        clientId: json["client_id"],
        center: json["center"],
        client: Client.fromJson(json["client"]),
        isparent: json["isparent"],
        createdAt: json["created_at"],
        secondEmail: json["second_email"] == null ? null : json["second_email"],
        token: json["token"] == null ? null : json["token"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "fullname": fullname,
        "username": username,
        "isAdmin": isAdmin,
        "isStaff": isStaff,
        "email": email,
        "image": image,
        "status": status,
        "loginaccess": loginaccess,
        "need_sec_email": needSecEmail,
        "client_id": clientId,
        "center": center,
        "client": client.toJson(),
        "isparent": isparent,
        "created_at": createdAt,
        "second_email": secondEmail == null ? null : secondEmail,
        "token": token == null ? null : token,
      };
}

class Client {
  int id;
  String clientid;
  String name;
  String connectionString;
  String authUri;
  bool kiosk;
  bool active;
  String pincode;
  String logo;
  bool kisokparentemailnotification;
  bool signature;
  bool centerLoginRequired;
  String timezone;
  String kinderm8Version;
  bool parentKioskEnable;
  bool staffKioskEnable;
  bool enableAllRoomsChildChecking;
  bool enableAllRoomsStaffChecking;
  bool staffap;

  Client({
    this.id,
    this.clientid,
    this.name,
    this.connectionString,
    this.authUri,
    this.kiosk,
    this.active,
    this.pincode,
    this.logo,
    this.kisokparentemailnotification,
    this.signature,
    this.centerLoginRequired,
    this.timezone,
    this.kinderm8Version,
    this.parentKioskEnable,
    this.staffKioskEnable,
    this.enableAllRoomsChildChecking,
    this.enableAllRoomsStaffChecking,
    this.staffap,
  });

  factory Client.fromJson(Map<String, dynamic> json) => new Client(
        id: json["id"],
        clientid: json["clientid"],
        name: json["name"],
        connectionString: json["connectionString"],
        authUri: json["AuthURI"],
        kiosk: json["kiosk"],
        active: json["active"],
        pincode: json["pincode"],
        logo: json["logo"],
        kisokparentemailnotification: json["kisokparentemailnotification"],
        signature: json["signature"],
        centerLoginRequired: json["center_login_required"],
        timezone: json["timezone"],
        kinderm8Version: json["kinderm8version"],
        parentKioskEnable: json["parent_kiosk_enable"],
        staffKioskEnable: json["staff_kiosk_enable"],
        enableAllRoomsChildChecking: json["enable_all_rooms_child_checking"],
        enableAllRoomsStaffChecking: json["enable_all_rooms_staff_checking"],
        staffap: json["staffap"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "clientid": clientid,
        "name": name,
        "connectionString": connectionString,
        "AuthURI": authUri,
        "kiosk": kiosk,
        "active": active,
        "pincode": pincode,
        "logo": logo,
        "kisokparentemailnotification": kisokparentemailnotification,
        "signature": signature,
        "center_login_required": centerLoginRequired,
        "timezone": timezone,
        "kinderm8version": kinderm8Version,
        "parent_kiosk_enable": parentKioskEnable,
        "staff_kiosk_enable": staffKioskEnable,
        "enable_all_rooms_child_checking": enableAllRoomsChildChecking,
        "enable_all_rooms_staff_checking": enableAllRoomsStaffChecking,
        "staffap": staffap,
      };
}

class Outcome {
  int id;
  String title;
  String description;
  int clearningsetId;
  String isfolder;
  String createdAt;
  String updatedAt;
  String learningSetTitle;

  Outcome({
    this.id,
    this.title,
    this.description,
    this.clearningsetId,
    this.isfolder,
    this.createdAt,
    this.updatedAt,
    this.learningSetTitle,
  });

  factory Outcome.fromJson(Map<String, dynamic> json) => new Outcome(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        clearningsetId: json["clearningset_id"],
        isfolder: json["isfolder"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        learningSetTitle: json["learning_set_title"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "clearningset_id": clearningsetId,
        "isfolder": isfolder,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "learning_set_title": learningSetTitle,
      };
}

class Programdate {
  int id;
  String date;
  int customTime;
  int manualStatus;

  Programdate({
    this.id,
    this.date,
    this.customTime,
    this.manualStatus,
  });

  factory Programdate.fromJson(Map<String, dynamic> json) => new Programdate(
        id: json["id"],
        date: json["date"],
        customTime: json["custom_time"],
        manualStatus: json["manual_status"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "date": date,
        "custom_time": customTime,
        "manual_status": manualStatus,
      };
}

class Room {
  int id;
  String title;
  String description;
  String createdAt;

  Room({
    this.id,
    this.title,
    this.description,
    this.createdAt,
  });

  factory Room.fromJson(Map<String, dynamic> json) => new Room(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        createdAt: json["created_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "created_at": createdAt,
      };
}
