import 'dart:convert';
import 'package:meta/meta.dart';

class Events {
  Events({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.image_url,
    @required this.date,
    @required this.time,
    @required this.to_date,
    @required this.to_time,
    @required this.event_type,
    @required this.created_at,
  });

  final String id;
  final String title;
  final String description;
  final String image_url;
  final DateTime date;
  final DateTime time;
  final DateTime to_date;
  final DateTime to_time;
  final String event_type;
  final String created_at;

  static List<Events> allFromResponse(String response) {
    var decodedJson = json.decode(response)();
    print('decode ->>> $decodedJson');
    return decodedJson['results']
        .cast<Map<String, dynamic>>()
        .map((obj) => Events.fromMap(obj))
        .toList()
        .cast<Events>();
  }

  static Events fromMap(Map map) {

    return new Events(
      id:map['id'].toString(),
      title:map['title'].toString(),
      description:map['description'].toString(),
      image_url:map['image_url'],
      date:map['date'],
      time:map['time'],
      to_date:map['to_date'],
      to_time:map['to_time'],
      event_type:map['event_type'],
      created_at:map['created_at'],
    );
  }

  factory Events.fromJson(Map value) {
    return Events(
        id:value['id'].toString(),
        title:value['title'],
        description:value['description'],
        image_url:value['image_url'],
        date:value['date'],
        time:value['time'],
        to_date:value['to_date'],
        to_time:value['to_time'],
        event_type:value['event_type'],
        created_at:value['created_at']
    );
  }

}