import 'dart:math';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/pages/calendar/detailevent.dart';
//import 'package:photo_view/photo_view.dart';
import 'package:timeago/timeago.dart';
import 'dart:ui' as ui;

class EventData extends StatelessWidget {
  final singleeventdata;
  final jwt;
  EventData(this.singleeventdata,this.jwt);

  // https://stackoverflow.com/questions/52251853/building-and-sorting-a-new-list-based-on-provided-list-and-parameters/52252059
  TimeAgo timeAgoo = new TimeAgo();
  var status;
  bool bool_Yes;
  bool bool_Maybe;
  bool bool_No;

  @override
  Widget build(BuildContext context) {

    var S3URL = "https://d212imxpbiy5j1.cloudfront.net/";
    var rsvp_view;
    var EventStartDate = DateTime.parse(singleeventdata["date"]);
    var EventEndDate;

    if (singleeventdata['to_date'] != null) {
      EventEndDate = DateTime.parse(singleeventdata['to_date']);
    } else {
      EventEndDate = DateTime.now();
    }
    if (singleeventdata["rsvp"] != null) {
      rsvp_view = singleeventdata["rsvp"]['status'];
    }
    if(singleeventdata["rsvp"]!= null) {
      singleeventdata["rsvp"]['status'] == 1 ? status = 1 : 0 ;
      singleeventdata["rsvp"]['status'] == 2 ? status = 2 : 0 ;
      singleeventdata["rsvp"]['status'] == 3 ? status = 3 : 0 ;

      bool_Yes = singleeventdata["rsvp"]['status'] == 1 ? true : false;
      bool_Maybe = singleeventdata["rsvp"]['status'] == 2 ? true : false;
      bool_No = singleeventdata["rsvp"]['status'] == 3 ? true : false;
    }
    print('===================${singleeventdata}');
/* // Format */

    var timeStamp = new DateFormat("d MMM");
    String formatEventStartDate = timeStamp.format(EventStartDate);
    String formatEventEndDate = timeStamp.format(EventEndDate);
    return new GestureDetector(
      onTap: () {
        print("Navigate to event detail..");
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => new EventDetails(singleeventdata,jwt)));
      },
      child: new Card(
        elevation: 1.0,
        color: Colors.white,
        margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 14.0),
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Container(
              margin: const EdgeInsets.only(right: 15.0),
              width: 100.0,
              height: 150.0,
              child: singleeventdata['image_url'] != ""
                  ? new CachedNetworkImage(
                imageUrl: S3URL + singleeventdata['image_url'],
                placeholder: new CupertinoActivityIndicator(),
                errorWidget: new Image.asset("assets/nophoto.jpg"),
                fadeOutDuration: new Duration(seconds: 1),
                fadeInDuration: new Duration(seconds: 3),
              )
                  : _buildTripDate(context),
            ),
            new Expanded(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new Container(
                          margin:
                          const EdgeInsets.only(top: 12.0, bottom: 10.0),
                          child: new Text(
                            singleeventdata['title'],
                            style: new TextStyle(
                              fontSize: 17.0,
                              color: Colors.black,
                              fontWeight: FontWeight.w700,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      new Container(
                        margin: const EdgeInsets.only(right: 10.0),
                        child: new Text(
                          ''.toString(),
                          style: new TextStyle(
                            fontSize: 17.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ],
                  ),
                  new Container(
                    margin: const EdgeInsets.only(right: 10.0),
                    child:  singleeventdata['description']!= null ? new Text (
                      singleeventdata['description'],
                      style: new TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontSize: 15.0,
                      ),
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                    ):new Text("There is No Description"),
                  ),
                  new Container(
                    margin: const EdgeInsets.only(top: 10.0),
                    child: new Row(
                      children: <Widget>[
                        singleeventdata['time']!=null ?   Text (
                          formatEventStartDate.toString() +
                              ' ' +  singleeventdata['time'],
                          style: new TextStyle(
                            fontSize: 13.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ):new Text(
                            formatEventStartDate.toString()

                        ),
                        singleeventdata['to_date']!= null  && singleeventdata['to_time']!= null ? new Text(
                          ' - ' +
                              formatEventEndDate.toString() +
                              singleeventdata['to_time'],
                          style: new TextStyle(
                            fontSize: 13.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        )
                            : new Text(""),
                      ],
                    ),
                  ),

                  singleeventdata["is_rsvp"] == true ? new Chip(
                    labelPadding: EdgeInsets.only(left:8.0,right:8.0,top:0.0,bottom: 0.0),
                    label: bool_Yes?
                    Text( 'YES', style: TextStyle(color: Theme.Colors.app_dark)):
                    bool_No ? Text( 'No', style: TextStyle(color: Theme.Colors.app_dark)): bool_Maybe ?
                    Text( 'MayBe', style: TextStyle(color: Theme.Colors.app_dark)):Text( 'RSVP', style: TextStyle(color: Theme.Colors.app_dark)),
//                    label: singleeventdata["rsvp"]['status'] == 0 ? Text("No",style: TextStyle(color: Theme.Colors.app_white)): singleeventdata["rsvp"]['status'].toString() == "1" ? Text("YES",style: TextStyle(color: Theme.Colors.app_white)): Text("MayBe",style: TextStyle(color: Theme.Colors.app_white)),
                    backgroundColor : bool_Yes ? new Color(0xFF77DD77) : bool_Maybe? new Color(0xFFFFC433): bool_No ? new Color(0xFFff6f69):Theme.Colors.appcolour,
                  ): new Container()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTripDate(BuildContext context) {
    DateTime dateTo;
    var daydifference;
    String eventday;

    DateTime dateFrom = DateTime.parse(singleeventdata['date']);
    print(dateFrom);
    if (singleeventdata['to_date'] != null) {
      dateTo = DateTime.parse(singleeventdata['to_date']);
      print(dateTo);
      daydifference = dateTo.difference(dateFrom).inDays;
    } else {
      dateTo = DateTime.now();
      print(dateTo);
      daydifference = dateFrom.difference(dateTo).inDays;

    }

    print(daydifference);
    if (daydifference == 1) {
      eventday = daydifference.toString() + ' day';
      print(eventday);
    } else {
      eventday = daydifference.toString() + ' days';
      print(eventday);
    }
    var timeStamp = new DateFormat("yyyy-MM-dd");
    String formatdailychartdate = timeStamp.format(dateTo);

    return Padding(
      padding: const EdgeInsets.only(top: 50.0, left: 10.0, right: 10.0),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[

          Text(eventday.toString(),
              style: new TextStyle(
                color: Theme.Colors.appcolour,
                fontSize: 18.0,
              )),
          Text(formatdailychartdate,
              style: new TextStyle(
                color: Theme.Colors.appcolour,
                decoration: ui.TextDecoration.overline,
                fontSize: 14.0,
              )),
        ],
      ),
    );
  }
}