import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:kinderm8/pages/calendar/eventslist.dart';
import 'package:kinderm8/pages/calendar/programlist.dart';
import 'package:kinderm8/pages/childrenmenu.dart';
import 'package:kinderm8/pages/commondrawer.dart';
import 'package:kinderm8/pages/home/dailysummarydatalist.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/pages/newsfeed/newsfeedlist.dart';

class EventsData extends StatefulWidget {
  EventsData({this.childrenData});

  final List<ChildViewModal> childrenData;

  @override
  _EventsDataState createState() => new _EventsDataState();

}

class _EventsDataState extends State<EventsData> with SingleTickerProviderStateMixin {

//  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
//  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
//  new GlobalKey<RefreshIndicatorState>();

//  bool _initialized;
//  bool _readyToFetchNextPage;
//  ScrollController _scrollController;

  final List<Tab> tabs = <Tab>[
//    new Tab(text: "Events",icon: new Icon(Icons.calendar_today)),
//    new Tab(text: "Programs",icon: new Icon(Icons.calendar_today)),
    new Tab(text: labelsConfig["eventsLabel"],),
    new Tab(text: labelsConfig["programsLabel"]),
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: tabs.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  // Pop to ask do you wish to exit
  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      child: new AlertDialog(
        title: new Text('Are you sure?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () => exit(0),
//                    Navigator.pushReplacementNamed(context, "/home"),
            child: new Text('Yes'),
          ),
        ],
      ),
    ) ??
        false;
  }

  void _modalBottomSheetMenu(){
    showModalBottomSheet(
        context: context,
        builder: (builder){
          return new Container(
            height: 100.0,
            color: Colors.transparent,
            child: new Container(
                decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(10.0),
                        topRight: const Radius.circular(10.0))),
                child: new Center(
                  child: ChildrenDrawer(widget.childrenData),
                )),
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
          appBar: new AppBar(
            title: Text(labelsConfig["calendarLabel"]),
        backgroundColor: Theme.Colors.appdarkcolour,
            centerTitle: true,
            bottom: new TabBar(
              isScrollable: true,
          unselectedLabelColor: Theme.Colors.eventtabtitle,
          labelColor: Theme.Colors.eventtabtitle,
          labelStyle: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18.0,
              color: Theme.Colors.appBarDetailBackground),
          indicatorSize: TabBarIndicatorSize.tab,
          indicator: new BubbleTabIndicator(
            indicatorHeight: 35.0,
            indicatorColor: Theme.Colors.appcolour,
                tabBarIndicatorSize: TabBarIndicatorSize.tab,
              ),
              tabs: tabs,
              controller: _tabController,
            ),
          ),
          drawer: CommonDrawer(childrenData: widget.childrenData),
          body: WillPopScope(
            onWillPop: _onWillPop,
            child: new TabBarView(
              controller: _tabController,

              // Add tabs as widgets
              children: <Widget>[
                new EventsList(childrenData: widget.childrenData),
                new ProgramList(childrenData: widget.childrenData),
              ],
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.menu),
            backgroundColor: Theme.Colors.appcolour,
            onPressed: () {
              _modalBottomSheetMenu();
            },
          ),
          bottomNavigationBar: BottomAppBar(
            shape: CircularNotchedRectangle(),
            notchMargin: 4.0,
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new FlatButton(
                  onPressed: () {
//                Navigator.pushNamed(context, "/home");
                Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new DailySummary(childrenData: widget.childrenData,)));
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.format_list_bulleted),
//                  new Text("Dailysummary")
                    ],
                  ),
                ),
                new FlatButton(
                  onPressed: () {
//                Navigator.pushNamed(context, "/newsfeed");
                Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new NewsFeedList(childrenData: widget.childrenData,)));
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.chrome_reader_mode),
//                  new Text("Newsfeed")
                    ],
                  ),
                ),
                new FlatButton(
                  onPressed: () {
                Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new EventsData(childrenData: widget.childrenData)));
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.date_range),
//                  new Text("Calendar")
                    ],
                  ),
                ),
                new FlatButton(
                  onPressed: () {
//                CommonDrawer();
//                Navigator.push(context, new MaterialPageRoute(
//                    builder: (context) =>
//                    new EventsList())
//                );
                  },
                  padding: EdgeInsets.all(10.0),
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new Icon(Icons.free_breakfast),
//                  new Text("Family ..")
                    ],
                  ),
                ),
//            IconButton(
//              icon: Icon(Icons.format_list_bulleted),
//              onPressed: () {
//                Dailysummary();
//              },
//            ),
//            IconButton(
//                icon: Icon(Icons.chrome_reader_mode),
//                onPressed: () {
//                  NewsfeedList();
//                },
//            ),
//            IconButton(
//              icon: Icon(Icons.date_range),
//              onPressed: () {
//                EventsList();
////                showModalBottomSheet<Null>(
////                  context: context,
////                  builder: (BuildContext context) => const _DemoDrawer(),
////                );
//              },
//            ),
//            IconButton(
//              icon: Icon(Icons.menu),
//              onPressed: () {
//                CommonDrawer();
////                showModalBottomSheet<Null>(
////                  context: context,
////                  builder: (BuildContext context) => CommonDrawer(),
////                );
//              },
//            ),
              ],
            ),
          ),
    );

  }
}
