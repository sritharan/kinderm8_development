import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:kinderm8/auth.dart';
import 'package:kinderm8/models/user.dart';
import 'package:flutter/material.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/calendar/event.dart';
import 'package:kinderm8/pages/calendar/eventdata.dart';
import 'package:kinderm8/pages/commondrawer.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';

class EventsList extends StatefulWidget {
  EventsList({this.childrenData});

  final List<ChildViewModal> childrenData;

  @override
  _EventsState createState() => new _EventsState();
}

class _EventsState extends State<EventsList>
    with SingleTickerProviderStateMixin
    implements HomePageContract, AuthStateListener {
  List eventlistdata;
  var k,len, appuser, jwt, id, clientId;
  ScrollController _scrollController = new ScrollController();
  bool isLoading;
  bool load = true;
  HomePagePresenter _presenter;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  // Current Index of tab
  int _currentIndex = 0;
  _EventsState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  @override
  void initState() {
    fetchEventsData(0);
    super.initState();
  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Theme.Colors.appdarkcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  Future<Events> fetchEventsData(int s) async {
    ///data from GET method
    print("data fetched");

    String _eventsUrl =
        'http://13.55.4.100:7070/v2.1.0/event/getevents?userid=$id&step=$s&clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_eventsUrl, headers: headers).then((response) async {
      var eventsData;
      try {
        eventsData = json.decode(response.body);
        print(eventsData.length);
        print('res get ${response.body}');
        print('events Data $eventsData');
      } catch (e) {
        print("That string didn't look like Json.");
      }

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        isLoading = false;
        if (s == 0) {
          setState(() {
            len=eventsData.length;
            this.eventlistdata = eventsData;
          });
        } else {
          setState(() {
            load = false;
            len=eventsData.length;
            eventlistdata.addAll(eventsData);
          });
        }
        k = eventlistdata.length;
      } else if (response.statusCode == 500 &&
          eventsData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      } else {
        await fetchEventsData(0);
        setState(() {
          load = false;
        });
      }
    });
    return null;
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) async {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      if (k != null) {
        await fetchEventsData(k);
        setState(() {
          load = false;
        });
      } else {
        await fetchEventsData(0);
        setState(() {
          load = false;
        });
      }
    });
  }

  Future<Null> handleRefresh() async {
    setState(() {
      load = true;
    });
    await fetchEventsData(0);
    setState(() {
      load = false;
    });
    return null;
  }

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: isLoading
            ? new CupertinoActivityIndicator()
            : const Text('Load more...',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: Theme.Colors.app_white)),
        color: Theme.Colors.appcolour.withOpacity(0.75),
//        splashColor: Colors.red,
        elevation: 4.0,
        onPressed: _counterButtonPress());
  }

  Function _counterButtonPress() {
    if (isLoading) {
      return null;
    } else {
      return () {
        setState(() {
          isLoading = true;
        });
        fetchEventsData(k);
      };
    }
  }

  // Pop to ask do you wish to exit
  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          child: new AlertDialog(
            title: new Text('Are you sure?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => exit(0),
//                    Navigator.pushReplacementNamed(context, "/home"),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: new Scaffold(
        body: new RefreshIndicator(
          key: refreshIndicatorKey,
          onRefresh: handleRefresh,
          child: load
              ? progress
              : new ListView.builder(
                  shrinkWrap: true,
                  padding: const EdgeInsets.all(0.0),
                  scrollDirection: Axis.vertical,
                  primary: true,
                  itemCount: this.eventlistdata != null
                      ? (this.eventlistdata.length + 1)
                      : 0,
                  itemBuilder: (context, i) {
                    if (i == k) {
                      if (len == 0 ||
                          len < 10) {
                        return Container();
                      } else {
                        print("--------$len");
                        return _buildCounterButton();
                      }
                    } else {
                      final data = this.eventlistdata[i];
                      return EventData(data,jwt);
                    }
                  },
                ),
        ),
//      drawer: new CommonaDrawer(),
      ),
    );
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      print(appusers);
      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];

      print("iddd $id");
      print(clientId);
    } catch (e) {
      print(e);
    }
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    var authStateProvider = new AuthStateProvider();
    setState(() {
      authStateProvider.notify(AuthState.LOGGED_OUT);
      authStateProvider.dispose(this);
    });
    // TODO: implement onLogoutUser
  }

  @override
  void onAuthStateChanged(AuthState state) {
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }
    // TODO: implement onAuthStateChanged
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
