import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/newsfeed/learningoutcomes.dart';
import 'package:kinderm8/pages/newsfeed/learningtags.dart';
//import 'package:photo_view/photo_view.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:flutter/material.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:kinderm8/pages/home/data/config.dart';
//import 'package:device_calendar/device_calendar.dart';
import 'Package:intl/Date_symbol_data_local.Dart';
import 'package:date_format/date_format.dart';

class ProgramDetails extends StatefulWidget {
  final programdata;
//  Calendar _calendar;
//  Event _event;
  ProgramDetails(this.programdata);
  @override
  _ProgramPageState createState() {
    return new _ProgramPageState(programdata);
  }

//  @override
//  _EventPageState createState() => new _EventPageState(eventdata);
}

class _ProgramPageState extends State<ProgramDetails>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  ScrollController scrollController;
  Animation<Color> colorTween1;
  Animation<Color> colorTween2;
  final programdata;

  var learningTag, learningOutcomeDetail;

//  DeviceCalendarPlugin _deviceCalendarPlugin;

  final double statusBarSize = 24.0;
  final double imageSize = 264.0;
  double readPerc = 0.0;

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
//  Calendar _calendar;

  var color, splitColor, tag;
  int colorCode;

//  var now = DateTime.now();
//  var berlinWallFell = DateTime.utc(1989, 11, 9);
//  var moonLanding = DateTime.parse("1969-07-20 20:18:04Z");

  // Event
//  Event _event;
  DateTime _startDate;
  TimeOfDay _startTime;

  DateTime _endDate;
  TimeOfDay _endTime;

  bool _autovalidate = false;
//  _EventPageState(this.eventdata);
  _ProgramPageState(this.programdata);

  @override
  void initState() {
    super.initState();
//    _deviceCalendarPlugin = new DeviceCalendarPlugin();
    print(' ------------------ detail programdata $programdata ------------------');
    // Create the appbar colors animations
    animationController = new AnimationController(
        duration: new Duration(milliseconds: 500), vsync: this);
    animationController.addListener(() => setState(() {}));

    color = programdata["program_color"].toString();
    splitColor = '0xFF' + color.substring(1);
    colorCode = int.parse(splitColor);

    colorTween1 =
        new ColorTween(begin: Colors.black12, end: new Color(colorCode))
            .animate(new CurvedAnimation(
                parent: animationController, curve: Curves.easeIn));
    colorTween2 =
        new ColorTween(begin: Colors.transparent, end: new Color(0xFF001880))
            .animate(new CurvedAnimation(
                parent: animationController, curve: Curves.easeIn));

    scrollController = new ScrollController();
    scrollController.addListener(() {
      setState(() => readPerc =
          scrollController.offset / scrollController.position.maxScrollExtent);

      // Change the appbar colors
      if (scrollController.offset > imageSize - statusBarSize) {
        if (animationController.status == AnimationStatus.dismissed)
          animationController.forward();
      } else if (animationController.status == AnimationStatus.completed)
        animationController.reverse();
    });
  }

  ///
  ///

  learningTags() {
    learningTag = programdata["learning_tags"];
    learningOutcomeDetail = programdata["outcomes"];
    if (learningTag.length > 5) {
      for (int i = 0; i < 5; i++) {
        return Container(
          width: 60.0,
          height: 25.0,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 5,
              itemBuilder: (context, i) {
                var color = learningTag[i]["color_code"].toString();
                var splitColor = '0xFF' + color.substring(1);
                int colorCode = int.parse(splitColor);
                return Container(
                  child: new CircleAvatar(
                    backgroundColor: Color(colorCode),
                    radius: 6.0,
                  ),
                );
              }),
        );
      }
    } else {
      for (int i = 0; i < learningTag.length; i++) {
        return Container(
          width: 60.0,
          height: 25.0,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: learningTag.length,
              itemBuilder: (context, i) {
                var color = learningTag[i]["color_code"].toString();
                var splitColor = '0xFF' + color.substring(1);
                int colorCode = int.parse(splitColor);
                return new CircleAvatar(
                  backgroundColor: Color(colorCode),
                  radius: 6.0,
                );
              }),
        );
      }
    }
  }

  ///
  ///

  @override
  Widget build(BuildContext context) {
    var S3URL = "https://d212imxpbiy5j1.cloudfront.net/";
    var title = programdata["title"];
    var description = programdata["description"];
    var eventimage = programdata["image_url"];

    var ProgramStartDate = DateTime.parse(programdata["start_date"]);
    var ProgramEndDate = DateTime.parse(programdata["end_date"]);

//    if(programdata["start_time"] == null){
//      print('start_time empty');
//      programdata["start_time"] = "";
//      print(programdata["start_time"]);
//    }
//    if(programdata["end_time"] == ""){
//      print('end_time empty');
//      programdata["end_time"] = null ;
//      print(programdata["end_time"]);
//    }
//    var ProgramStartTime = DateTime.parse(programdata["start_time"]);
//    var ProgramEndTime= DateTime.parse(programdata["end_time"]);

    final Size screenSize = MediaQuery.of(context).size;
//    print(programdata["start_date"]);
//    print(programdata["end_date"]);
//    print(programdata["start_time"].length);
//    print(programdata["end_time"].length);

    /* // Format */
    var dateStamp = new DateFormat("yMMMEd");
    String formatEventStartDate = dateStamp.format(ProgramStartDate);
    String formatEventEndDate = dateStamp.format(ProgramEndDate);

    //    var timeStamp = new DateFormat("Hm");
//    String formatEventStartTime = timeStamp.format(ProgramStartTime);
//    String formatEventEndTime = timeStamp.format(ProgramEndTime);

//    print('programdata $programdata');

    var subject_area;
    if (programdata['subject_area'] != '' &&
        programdata['subject_area'] != null) {
      subject_area = html2md.convert(programdata['subject_area']);
    }

    return new Scaffold(
        body: new Hero(
      tag: 'Detailprogram',
      child: new Material(
        color: Colors.white,
        child:
            new Stack // I need to use a stack because otherwise the appbar blocks the content even if transparent
                (
          children: <Widget>[
            /// Image and text
            new ListView(
              padding: new EdgeInsets.all(0.0),
              controller: scrollController,
              children: <Widget>[
                new Container(
                  height: screenSize.width / 4,
                ),
                new Padding(
                  padding: new EdgeInsets.symmetric(horizontal: 32.0),
                  child: new Hero(
                    tag: 'Title',
                    child: new Text(title,
                        textAlign: TextAlign.center,
                        style: new TextStyle(
                            fontSize: 20.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w600)),
                  ),
                ),
                new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: new EdgeInsets.only(left: 5.0),
                            child: new IconButton(
                              onPressed: () {},
                              icon: new Icon(
                                Icons.today,
                                color: Theme.Colors.appcolour,
                                size: 35.0,
                              ),
                            ),
                          ),
                        ],
                      ),
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          programdata['start_date'] != null ?  Container(
                            child: Row(
                              children: <Widget>[
                                new Text('${labelsConfig["programStartDate"]} : ',
                                    textAlign: TextAlign.start,
                                    style: new TextStyle(
                                        fontSize: 14.0,
                                        color: Theme.Colors.darkGrey,
                                        fontWeight: FontWeight.w500)),
                                new Text(formatEventStartDate.toString(),
                                    textAlign: TextAlign.start,
                                    style: new TextStyle(
                                        fontSize: 14.0,
                                        color: Theme.Colors.bodyText,
                                        fontWeight: FontWeight.w400)),
                                programdata['start_time'].toString() != null || programdata['start_time'] == ''
                                    ? new Text(' at ',
                                        style: new TextStyle(
                                            fontSize: 14.0,
                                            color: Theme.Colors.bodyText,
                                            fontWeight: FontWeight.w400)) : new Text(''),
                                programdata['start_time'].toString() != null || programdata['start_time'] == ''
                                    ? new Text(programdata['start_time'],
                                    textAlign: TextAlign.start,
                                    style: new TextStyle(
                                        fontSize: 14.0,
                                        color: Theme.Colors.bodyText,
                                        fontWeight: FontWeight.w400)) : new Text('')
                              ],
                            ),
                          ) : new Container(),
                          programdata['end_date'] != null ? Container(
//                                margin: new EdgeInsets.only(left: 15.0),
                              child: Row(children: <Widget>[
                            new Text('End Date : ',
                                textAlign: TextAlign.start,
                                style: new TextStyle(
                                    fontSize: 14.0,
                                    color: Theme.Colors.darkGrey,
                                    fontWeight: FontWeight.w500)),
                            new Text(formatEventEndDate.toString(),
                                textAlign: TextAlign.start,
                                style: new TextStyle(
                                    fontSize: 14.0,
                                    color: Theme.Colors.bodyText,
                                    fontWeight: FontWeight.w400)),
                            programdata['end_time'] != null || programdata['end_time'] != ''
                                ? new Text(' at ',
                                    style: new TextStyle(
                                        fontSize: 14.0,
                                        color: Theme.Colors.bodyText,
                                        fontWeight: FontWeight.w400))
                                : new Text(''),
                                programdata['end_time'] != null || programdata['end_time'] != ''
                                    ? new Text(programdata['end_time'],
                                textAlign: TextAlign.start,
                                style: new TextStyle(
                                    fontSize: 14.0,
                                    color: Theme.Colors.bodyText,
                                    fontWeight: FontWeight.w400)): new Text('')
                          ])) : new Container()
                        ],
                      )
                    ]),
                new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Text("${labelsConfig["programTypeLabel"]}:",
                              textAlign: TextAlign.center,
                              style: new TextStyle(
                                  fontSize: 14.0,
                                  color: Theme.Colors.darkGrey,
                                  fontWeight: FontWeight.w700)),
                        ],
                      ),
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new Text(programdata['program_type'],
                              textAlign: TextAlign.center,
                              style: new TextStyle(
                                  fontSize: 14.0,
                                  color: Theme.Colors.bodyText,
                                  fontWeight: FontWeight.w700)),
                        ],
                      ),
                    ]),
                new Chip(
                  avatar: new CircleAvatar(
                    backgroundColor: Theme.Colors.app_white,
                    radius: 30.0,
                    backgroundImage: programdata['educator']["image"] != null
                        ? new NetworkImage(programdata['educator']["image"])
                        : new AssetImage(
                            "assets/nophoto.jpg",
                          ),
                  ),
                  label: Text(
                    '${programdata['educator']["fullname"]}',
                    style: TextStyle(color: Theme.Colors.app_white),
                  ),
                  backgroundColor: Color(colorCode),
                ),
                programdata['subject_area'] != null &&
                        programdata['subject_area'] != ''
                    ? Column(children: <Widget>[
                        new Text("${labelsConfig["programSubjectLabel"]}:",
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                                fontSize: 14.0,
                                color: Theme.Colors.darkGrey,
                                fontWeight: FontWeight.w700)),
                        new Container(
                          margin: new EdgeInsets.only(
                              left: 32.0, right: 32.0, bottom: 80.0),
                          child: new Text(programdata['subject_area'],
                              style: new TextStyle(
                                  color: Theme.Colors.bodyText,
                                  fontSize: 16.0)),
                        ),
                      ])
                    : Container(),
//                new Text("Subject Area:",
//                    textAlign: TextAlign.center,
//                    style: new TextStyle(
//                        fontSize: 14.0,
//                        color: Theme.Colors.darkGrey,
//                        fontWeight: FontWeight.w700)),
//                new Container(
//                  margin: new EdgeInsets.only(
//                      left: 32.0, right: 32.0, bottom: 80.0),
//                  child: new Text(programdata['subject_area'],
//                      style: new TextStyle(
//                          color: Theme.Colors.bodyText, fontSize: 16.0)),
//                ),
                new Container(
                  margin:new EdgeInsets.only(
		  left: 32.0, right: 32.0, bottom: 5.0),
                  child: new Text(programdata['description'],
                      style: new TextStyle(fontSize: 16.0)),
                ),
                new Container(
                  margin:
                      new EdgeInsets.only(left: 5.0, right: 5.0, bottom: 80.0),
                  padding: EdgeInsets.only(left: 0.0),
                  child: programdata["learning_tags"] != null &&
                          programdata["learning_tags"].length != 0
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                GestureDetector(
                                  child: Container(
                                      padding: EdgeInsets.only(left: 0.0),
//                                      width: ((screenSize.width * 3 / 8)),
                                      child: Row(
                                        children: <Widget>[
                                          Icon(FontAwesomeIcons.tags,
                                              size: 12.0,
                                              color: Colors.grey[600]),
                                          Container(width: 5.0),
                                          learningTags(),
                                          learningTag.length > 5
                                              ? Text(
                                                  " +${learningTag.length - 5}",
                                                  style: new TextStyle(
                                                      fontSize: 12.0),
                                                )
                                              : Text(""),
                                        ],
                                      )),
                                  onTap: () {
                                    print("have to navigte to Learning tags");
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                new LearningTags(learningTag)));
                                  },
                                )
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                GestureDetector(
                                  child: Container(
                                      padding: EdgeInsets.all(3.0),
                                      decoration: new BoxDecoration(
                                        borderRadius:
                                            new BorderRadius.circular(25.0),
                                        border: new Border.all(
                                          width: 2.0,
                                          color: Theme.Colors.appcolour,
                                        ),
                                      ),
//                                      width: ((screenSize.width   / 5)),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(FontAwesomeIcons.listAlt,
                                              size: 12.0,
                                              color: Colors.grey[600]),
                                          Container(width: 6.0),
                                          Text(labelsConfig["programLearningOutcomeLabel"],
                                              style: new TextStyle(
                                                  fontSize: 12.0)),
                                        ],
                                      )),
                                  onTap: () {
                                    print(
                                        "have to navigte to Learning Outcomes");
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                new LearningOutcomes(
                                                    learningOutcomeDetail)));
                                  },
                                )
                              ],
                            ),
                          ],
                        )
                      : Divider(),
                )
              ],
            ),

            /// Appbar
            new Align(
              alignment: Alignment.topCenter,
              child: new SizedBox.fromSize(
                size: new Size.fromHeight(90.0),
                child: new Stack(
                  alignment: Alignment.centerLeft,
                  children: <Widget>[
                    new SizedBox.expand // If the user scrolled over the image
                        (
                      child: new Material(
                        color: colorTween1.value,
                      ),
                    ),
                    new SizedBox.fromSize // TODO: Animate the reveal
                        (
                      size: new Size.fromWidth(
                          MediaQuery.of(context).size.width * readPerc),
                      child: new Material(
                        color: colorTween2.value,
                      ),
                    ),
                    new Align(
                        alignment: Alignment.center,
                        child: new SizedBox.expand(
                          child:
                              new Material // So we see the ripple when clicked on the iconbutton
                                  (
                            color: Colors.transparent,
                            child: new Container(
                                margin: new EdgeInsets.only(
                                    top: 24.0), // For the status bar
                                child: new Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    new IconButton(
                                      onPressed: () =>
                                          Navigator.of(context).pop(),
                                      icon: new Icon(Icons.arrow_back,
                                          color: Colors.white),
                                    ),
                                    Expanded(
                                      child: Text(labelsConfig["programDetailViewLabel"],
                                            textAlign: TextAlign.center,
                                            style: new TextStyle(
                                                fontSize: 14.0,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w700)),
                                    ),
//                                        new Hero
//                                          (
//                                            tag: 'Logo',
//                                            child: new Icon(Icons.event, color: Colors.white, size: 48.0)
//                                        ),
//                                        new IconButton
//                                          (
//                                          onPressed: () => Navigator.of(context).pop(),
//                                          icon: new Icon(Icons.event_note, color: Colors.white),
//                                        ),
                                  ],
                                )),
                          ),
                        ))
                  ],
                ),
              ),
            ),

            /// Fade bottom text
            new Align(
              alignment: Alignment.bottomCenter,
              child: new SizedBox.fromSize(
                  size: new Size.fromHeight(100.0),
                  child: new Container(
                    decoration: new BoxDecoration(
                        gradient: new LinearGradient(
                            colors: [Colors.white12, Colors.white],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            stops: [0.0, 1.0])),
                  )),
            )
          ],
        ),
      ),
    ));
  }

  String _validateTitle(String value) {
    if (value.isEmpty) {
      return 'Name is required.';
    }

    return null;
  }

  DateTime _combineDateWithTime(DateTime date, TimeOfDay time) {
    final dateWithoutTime =
        DateTime.parse(new DateFormat("y-MM-dd 00:00:00").format(_startDate));
    return dateWithoutTime
        .add(new Duration(hours: time.hour, minutes: time.minute));
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }
}
