import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:kinderm8/auth.dart';
import 'package:kinderm8/data/database_helper.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/calendar/program.dart';
import 'package:kinderm8/pages/calendar/programdata.dart';
import 'package:kinderm8/pages/commondrawer.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';

class ProgramList extends StatefulWidget {
  ProgramList({this.childrenData});

  final List<ChildViewModal> childrenData;

  @override
  _ProgramState createState() => new _ProgramState();
}

class _ProgramState extends State<ProgramList>
    with SingleTickerProviderStateMixin
    implements HomePageContract, AuthStateListener {
  var k, jwt, userId, clientId;
  List<ChildViewModal> childrenData;
  var load = true;
  bool isLoading;
  HomePagePresenter _presenter;
  List programListData = [];
  List<Widget> widgets = [];
  var programData;

  ChildViewModal selectedChild;
  var selectedChildId;
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  _ProgramState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  @override
  void initState() {
    childrenData = widget.childrenData;
    selectedChild = childrenData.length > 0 ? childrenData[0] : ChildViewModal();

    setState(() {
      load = true;
      selectedChild = childrenData[0];
      selectedChildId = selectedChild.id;
    });
    if(selectedChildId != null) {fetchProgramData(0);}
    super.initState();

  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  void fetchProgramData(int step) async {
//    selectedChildId = childrenData[0].id;
    print('selectedChildId >>>> $selectedChildId');
    if (selectedChildId != null) {
      String _eventsUrl =
          'http://13.55.4.100:7070/v2.1.0/program/getprograms/$selectedChildId?step=$step&clientid=$clientId';
      var headers = {"x-authorization": jwt.toString()};

      NetworkUtil _netutil = new NetworkUtil();
      await _netutil.get(_eventsUrl, headers: headers).then((response) {
//      final programData_ = programToJson(response);
//      print('programs programToJson  Data $programData_');

        try {
          programData = json.decode(response.body);
          print(programData.length);
          print('res get ${response.body}');
          print('programs  Data $programData');
        } catch (e) {
          print("That string didn't look like Json.");
        }

        print('jwt### $jwt');
        print(response.statusCode);
        if (response.statusCode == 200) {
          print(isLoading);
          isLoading = false;
          print(isLoading);
          if (step == 0) {
            setState(() {
              load = false;
              this.programListData = programData;
            });
          } else {
            setState(() {
              load = false;
              programListData.addAll(programData);
            });
          }
          k = programListData.length;
        } else if (response.statusCode == 500 &&
            programData["errorType"] == 'ExpiredJwtException') {
          print("retrying...");
          getRefreshToken();
        } else {
          print("error");
//          fetchProgramData(0);
        }
      });
      return null;
    }
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');	
      }
      this.jwt = refreshJwtToken;

      if (k != null) {
        fetchProgramData(k);
      } else {
        fetchProgramData(0);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<Null> handleRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      k = 0;
      load = true;
      fetchProgramData(0);
    });
    return null;
  }

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: isLoading
            ? new CupertinoActivityIndicator()
            : const Text('Load more...',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: Theme.Colors.progressbackground)),
        color: Theme.Colors.appseconderycolour,
        elevation: 4.0,
        onPressed: _counterButtonPress());
  }

  Function _counterButtonPress() {
    if (isLoading) {
      return null;
    } else {
      return () {
        setState(() {
          isLoading = true;
        });
        fetchProgramData(k);
      };
    }
  }

  // Pop to ask do you wish to exit
  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          child: new AlertDialog(
            title: new Text('Are you sure?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => exit(0),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }

  childList() {
    final Size screenSize = MediaQuery.of(context).size;
    if (childrenData != null && childrenData.length > 0) {
      return Center(
        child: Container(
          height: screenSize.width / 5.5,
          color: Colors.transparent,
          child: ListView.builder(
            itemCount: this.childrenData.length > 0 ? this.childrenData.length : 0,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return Container(
                width: screenSize.width / 5.5,
                color: Colors.transparent,
                child: GestureDetector(
                  onTap: () {
                    print('selectedChildId assigning =$selectedChildId');
                    setState(() {
                      print('selectedChildId assign =$selectedChildId');
                      load = true;
                      selectedChild = childrenData[index];
                      selectedChildId = selectedChild.id;
                    });
                    print('selectedChildId assigned =$selectedChildId');
                    fetchProgramData(0);
                  },
                  child: (childrenData[index].id != selectedChildId) ? Container(
                    margin: const EdgeInsets.all(5.0),
                    decoration: new BoxDecoration(
                      color: const Color(0xff7c94b6),
                      image: new DecorationImage(
                        image: CachedNetworkImageProvider(
                            'https://d212imxpbiy5j1.cloudfront.net/${childrenData[index].iconAssetPath}'),
                        fit: BoxFit.cover,
                      ),
                      borderRadius:
                          new BorderRadius.all(new Radius.circular(50.0)),
                      border: new Border.all(
                        color: Theme.Colors.appcolour,
                        width: 3.0,
                      ),
                    ),
                  ) : Container(
                    margin: const EdgeInsets.all(5.0),
                    decoration: new BoxDecoration(
                      color: const Color(0xff7c94b6),
                      image: new DecorationImage(
                        image: CachedNetworkImageProvider(
                            'https://d212imxpbiy5j1.cloudfront.net/${childrenData[index].iconAssetPath}'),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.all(new Radius.circular(50.0)),
                      border: new Border.all(
                        color: Colors.green,
                        width: 3.0,
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      );
    }
    else {
      print("error widgets");
      return SizedBox();
    }
  }

  @override
  Widget build(BuildContext context) {
//    final Size screenSize = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: WillPopScope(
        onWillPop: _onWillPop,
        child: new RefreshIndicator(
          key: refreshIndicatorKey,
          onRefresh: handleRefresh,
          child: Column(
            children: <Widget>[
              selectedChild != null
                  ? childList()
                  : CupertinoActivityIndicator(),
              Flexible(
                flex: 0,
                child: new Center(
                  child: load
                      ? progress
                      : new ListView.builder(
                          shrinkWrap: true,
                          physics: ClampingScrollPhysics(),
                          padding: const EdgeInsets.all(0.0),
                          scrollDirection: Axis.vertical,
                          primary: true,
                          itemCount: this.programListData != null
                              ? (this.programListData.length + 1)
                              : 0,
                          itemBuilder: (context, i) {
                            if (i == k) {
                              if (programData.length == 0 ||
                                  programData.length < 10) {
                                return Container();
                              } else {
                                return _buildCounterButton();
                              }
//                            return _buildCounterButton();
                            } else {
                              final singleProgramData = this.programListData[i];
                              return ProgramData(singleProgramData);
                            }
                          },
                        ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void onAuthStateChanged(AuthState state) {
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }
  }

  @override
  void onDisplayUserInfo(User user) {
    setState(() {
      var appuser = user.center;
      try {
        final parsed = json.decode(appuser);
        var details = parsed[0];
        var users = details["user"];
        jwt = details["jwt"];
        userId = users["id"];
        clientId = users["client_id"];
      } catch (e) {
        print(e);
      }
    });
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
