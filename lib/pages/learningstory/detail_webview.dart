import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/learningstory/learningstorycomment.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shimmer/shimmer.dart';
import 'package:flutter/services.dart';
import 'package:html2md/html2md.dart' as html2md;

class DetailWebView extends StatelessWidget {

  DetailWebView(this.savedauthtoken,this.jwt,this.clientId,this.singleStoryData, this.childid);
//  DetailWebView(this.savedauthtoken, {this.singleStoryData = "", this.jwt = "", this.clientId =""});

  final String savedauthtoken;
  final String clientId;
  final singleStoryData;
  final childid;
  final jwt;

  final flutterWebViewPlugin = FlutterWebviewPlugin();
//  final flutterWebviewPlugin = new FlutterWebviewPlugin();
  var webdata;
  var hml;

  // // Weblogin using client id
    Future<dynamic> webloginLearningStory_HTML(story_id, child_id, savedauthtoken) async {
    print('From ->DetailWebView ->called webloginLearningStory_HTML');
    print(singleStoryData['id']);
    print(savedauthtoken);
    print(child_id);
    try {
      NetworkUtil _netUtil = new NetworkUtil();
      final WEBSTORY_URL = 'https://${clientId}.kinderm8.com.au/api/v1/single-story-view';
      print('LOGIN_URL $WEBSTORY_URL');
      var body = {"email": "narmada@proitzen.com","pwd": "abc123","child_id":child_id,"id":story_id};

      //      http.post(WEBSTORY_URL, headers: {"mobile-api": "true", "Content-Type": "application/json","X-Authorization": savedauthtoken},
      _netUtil.post(WEBSTORY_URL, headers: {"Content-Type": "text/html","X-Authorization": savedauthtoken},
          body: {"email": "narmada@proitzen.com","pwd": "abc123","child_id":child_id,"id":singleStoryData['id']}
//          body : jsonEncode(body) //{"email": "narmada@proitzen.com","pwd": "abc123","child_id":child_id,"id":singleStoryData['id']}
          ).then((res) {
//      http.post(WEBSTORY_URL, headers: {"mobile-api": "true", "Content-Type": "application/json","X-Authorization": savedauthtoken}).then((res) {
        print("----------------###############------------------------------------------###############--------------------------");
        print("res$res");
        webdata = res;
        print("----------------###############------------------------------------------###############--------------------------");
        return res;
      });
    }
    catch(e){
      print(e);
    }
  }

  String loadLocal() {
    hml = webloginLearningStory_HTML(singleStoryData['id'],childid,savedauthtoken);
    return hml;
//    var unescape = new HtmlUnescape();
////    var text = unescape.convert("&lt;div class=\"item-header\"&gt;  &lt;div class=\"row\"&gt;   &lt;div class=\"lheader\"&gt;    &lt;p&gt;drawing day &lt;/p&gt;   &lt;/div&gt;  &lt;/div&gt; &lt;/div&gt; &lt;div class=\"row\"&gt;  &lt;div class=\"col story_video\"&gt;   &lt;iframe frameborder=\"0\" allowfullscreen src=\"https://youtube.com/embed/wC4owOOHsZU\"&gt;&lt;/iframe&gt;  &lt;/div&gt; &lt;/div&gt; &lt;div class=\"row\"&gt;  &lt;div class=\"col story_video\"&gt;   &lt;iframe frameborder=\"0\" allowfullscreen src=\"https://youtube.com/embed/prDzi0XyuQ4\"&gt;&lt;/iframe&gt;  &lt;/div&gt; &lt;/div&gt; &lt;div class=\"row\"&gt;  &lt;div class=\"col story_video\"&gt;   &lt;iframe frameborder=\"0\" allowfullscreen src=\"https://youtube.com/embed/sIlDNONyrZo\"&gt;&lt;/iframe&gt;  &lt;/div&gt; &lt;/div&gt;");
////    var text =  Uri.dataFromString(
////        webdata,
////        mimeType: 'text/html',
////        encoding: Encoding.getByName('utf-8')
////    ).toString();
//    print("------------------------------------------");
//    var htmltext = html2md.convert(webdata);
//    print(htmltext);
//    print("-----*******************---------");
//    var htmltext2 = html2md.convert(hml);
//
//    print(htmltext2);
//    print("----------------###############--------------------------");
//    print(webdata);
//    var text_new = unescape.convert(webdata);
//    return webloginLearningStory_HTML(singleStoryData['id'],childid,savedauthtoken);
  }


  @override
  Widget build(BuildContext context) {
//    final WEBSTORY_URL = 'https://${clientId}.kinderm8.com.au/parent/story/${singleStoryData['id']}?child_id$childid';
    var s_id = singleStoryData['id'];
    return WebviewScaffold(

//      url: new Uri.dataFromString(webloginLearningStory_HTML(s_id,childid,savedauthtoken), mimeType: 'text/html').toString(),
//      url: Uri.dataFromString(loadLocal(), mimeType: 'text/html', encoding: Encoding.getByName("UTF-8")).toString(), // maybe you Uri.dataFromString(snapshot.data, mimeType: 'text/html', encoding: Encoding.getByName("UTF-8")).toString()
      url: loadLocal(),
      headers: { "_token": savedauthtoken} ,
      withJavascript: true,
      initialChild: _shimmer,
      appBar: PreferredSize(
        preferredSize: Size(double.infinity, 55.0),//I have changed the app bar height here you can change or remove preferredSize: Asanka
        child: AppBar(
          title: Container(
              height: 25.0,
              child: new Text(labelsConfig["learningStoryDetailLabel"])
          ),
          backgroundColor: Theme.Colors.appcolour,
          actions: <Widget>[
//          new IconButton(
//            onPressed: () =>
//                Navigator.of(context).pop(),
//            icon: new Icon(Icons.arrow_back,
//                color: Colors.white),
//          ),
//          Center(
//            child: new Text("Learnig story detail" ,style: TextStyle(
//                fontSize: 20.0,
//                color: Theme
//                    .Colors
//                    .app_white)),
//          ),
            new IconButton(
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(
                    new MaterialPageRoute<Null>(
                      builder:
                          (BuildContext context) {
                        return LearningStoryComment(
                            singleStoryData,
                            jwt);
                      },
                    ));
              },
              icon: CircleAvatar(
                backgroundColor: Colors.white,
                child: new Icon(
                  Icons.comment,
                  color: Theme
                      .Colors.appcolour,
                  size: 25.0,
                ),
              ),
            ),
          ],
        ),
      ),
//      withZoom: true,
      withLocalStorage: true,
//      bottomNavigationBar: BottomAppBar(
//        child: Row(
//          children: <Widget>[
//            IconButton(
//              icon: const Icon(Icons.arrow_back_ios),
//              onPressed: () {
//                flutterWebViewPlugin.goBack();
//              },
//            ),
//            IconButton(
//              icon: const Icon(Icons.arrow_forward_ios),
//              onPressed: () {
//                flutterWebViewPlugin.goForward();
//              },
//            ),
//            IconButton(
//              icon: const Icon(Icons.autorenew),
//              onPressed: () {
//                flutterWebViewPlugin.reload();
//              },
//            ),
//          ],
//        ),
//      ),
    );
  }

  var _shimmer =  Scaffold(
    body: Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[100],
        child: Column(
          children: [0, 1, 2, 3, 4, 5, 6]
              .map((_) => Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 48.0,
                  height: 48.0,
                  color: Colors.white,
                ),
                Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 8.0),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: double.infinity,
                        height: 8.0,
                        color: Colors.white,
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.symmetric(vertical: 2.0),
                      ),
                      Container(
                        width: double.infinity,
                        height: 8.0,
                        color: Colors.white,
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.symmetric(vertical: 2.0),
                      ),
                      Container(
                        width: 40.0,
                        height: 8.0,
                        color: Colors.white,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ))
              .toList(),
        ),
      ),
    ),
  );
}

class LoadingListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Loading List'),
      ),
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        child: Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          child: Column(
            children: [0, 1, 2, 3, 4, 5, 6]
                .map((_) => Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 48.0,
                    height: 48.0,
                    color: Colors.white,
                  ),
                  Padding(
                    padding:
                    const EdgeInsets.symmetric(horizontal: 8.0),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: double.infinity,
                          height: 8.0,
                          color: Colors.white,
                        ),
                        Padding(
                          padding:
                          const EdgeInsets.symmetric(vertical: 2.0),
                        ),
                        Container(
                          width: double.infinity,
                          height: 8.0,
                          color: Colors.white,
                        ),
                        Padding(
                          padding:
                          const EdgeInsets.symmetric(vertical: 2.0),
                        ),
                        Container(
                          width: 40.0,
                          height: 8.0,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ))
                .toList(),
          ),
        ),
      ),
    );
  }
}