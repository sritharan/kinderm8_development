import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:kinderm8/Theme.dart' as Theme;
import 'package:flutter/material.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/pages/home/data/config.dart';
import 'package:kinderm8/pages/home/home_presenter.dart';
import 'package:kinderm8/pages/home/modals/child_view_modal.dart';
import 'package:kinderm8/pages/learningstory/learningstorydata.dart';
import 'package:kinderm8/utils/commonutils/progress.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


class LearningStoryList extends StatefulWidget {
  final ChildViewModal childData;
  final jwt;
  LearningStoryList(this.childData, this.jwt);
  @override
  LearningStoryState createState() => LearningStoryState();
}

class LearningStoryState extends State<LearningStoryList>
    implements HomePageContract {
  var k, appuser, jwt, id, clientId;
  List data;
  var childId;
  bool isLoading;
  bool load = true;

  HomePagePresenter _presenter;
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String savedauthtoken;
  LearningStoryState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getauthtoken();
  }

  void getauthtoken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var new_savedauthtoken = prefs.getString('authtoken') ?? null;
    setState(() {
      savedauthtoken = new_savedauthtoken;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(labelsConfig["learningStoryLabel"],
            style: TextStyle(
              color: Colors.white,
            )),
        backgroundColor: Theme.Colors.appcolour,
        centerTitle: true,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.refresh),
              tooltip: 'Refresh',
              onPressed: () {
                print("Reload..");
                setState(() {
                  load = true;
                });
                fetchLearningStoryData(0);
              })
        ],
      ),
//      drawer: new CommonDrawer(),
      body: new RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: handleRefresh,
        child: new Center(
          child: load
              ? progress
              : new ListView.builder(
                  itemCount: this.data != null ? (this.data.length + 1) : 0,
                  itemBuilder: (context, i) {
                    if (i == k) {
                      return _buildCounterButton();
                    } else {
                      final singleData = this.data[i];
//                      singleData.childid = widget.childData.id;
                      return LearningStoryData(singleData, jwt,savedauthtoken,widget.childData.id,clientId);
                    }
                  },
                ),
        ),
      ),
    );
  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: isLoading
            ? new CupertinoActivityIndicator()
            : const Text('Load more...',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: Theme.Colors.app_white)),
        color: Theme.Colors.appcolour.withOpacity(0.75),
//        splashColor: Colors.red,
        elevation: 4.0,
        onPressed: _counterButtonPress());
  }

  Function _counterButtonPress() {
    if (isLoading) {
      return null;
    } else {
      return () {
        setState(() {
          isLoading = true;
        });
        fetchLearningStoryData(k);
      };
    }
  }

  Future<String> fetchLearningStoryData(int s) async {
    ///data from GET method
    print("Learning Story data fetched");
    childId = widget.childData.id;
    String _dailyJournalUrl =
        'http://13.55.4.100:7070/v2.1.0/getLearningStoriesWithFollowup/$childId?userid=$id&step=$s&clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_dailyJournalUrl, headers: headers).then((response) {
      var LearningStoryData;
      try {
        LearningStoryData = json.decode(response.body);
        print(LearningStoryData.length);
        print('res get ${response.body}');
        print('Learning StoryUrlData ${LearningStoryData[0]}');
        print('Learning StoryUrlData $LearningStoryData');
      } catch (e) {
        print('That string was null!');
      }

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        print(isLoading);
        isLoading = false;
        print(isLoading);
        if (s == 0) {
          setState(() {
            load = false;
            this.data = LearningStoryData;
          });
        } else {
          setState(() {
            load = false;
            data.addAll(LearningStoryData);
          });
        }
        k = data.length;
      } else if (response.statusCode == 500 &&
          LearningStoryData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      } else {
        print("Learning Story Data errorType ${LearningStoryData["errorType"]}");
//        fetchObservationData(0);
      }
    });
    return null;
  }

  getRefreshToken() {
    print("refreshing Token..");
    String _refreshTokenUrl =
        'http://api.kinderm8.com.au/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      if (k != null) {
        fetchLearningStoryData(k);
      } else {
        fetchLearningStoryData(0);
      }
    });
  }

  Future<Null> handleRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      load = true;
      fetchLearningStoryData(0);
    });
    return null;
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      print(appusers);
      jwt = widget.jwt.toString();
//      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];

      print("iddd $id");
      print(clientId);

      fetchLearningStoryData(0);
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }

    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }
}
