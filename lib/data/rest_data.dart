import 'dart:async';
import 'dart:convert';

import 'package:kinderm8/models/appuser.dart';
import 'package:kinderm8/models/user.dart';
import 'package:kinderm8/utils/network_util.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RestData {
  NetworkUtil _netUtil = new NetworkUtil();
  static final BASE_URL = 'http://13.55.4.100:7070/v2.1.0/setupuseroneapp';

  // User
  Future<User> login(result) {
    var email = result["email"];
    var password = result["password"];
    var client_id = result["client_id"];
    var center = result["center"];
    print("$email,$password,$center,$client_id");

    return _netUtil.post(BASE_URL, body: {
      "email": email,
      "password": password,
      "client_id": client_id
    }).then((dynamic resp) async {

      List res = json.decode(resp);
      var outRes = resp.toString();
      var output = res[0];
      var appUser = new AppUser.fromJson(output);

//      String jwt = output["jwt"];
//      var user = output["user"];
//      List children = output["children"];
//      var client = user["client"];

      var globalConfig = output["globalconfig"];
      await saveGlobalConfig(globalConfig);
      print('globalconfig$globalConfig');

      var dummy = {
        "email": email,
        "password": password,
        "client_id": client_id,
        "center": outRes,
        "children": appUser.children
      };
      return new Future.value(new User.map(dummy));
    });
  }

  Future saveGlobalConfig(var configToSave) async {
    String encodedClientSectionConfig = json.encode(configToSave["client_section"]);
    String encodedNavigationConfig = json.encode(configToSave["navigation_set"]);
    String encodedProgramPlanSectionConfig = json.encode(configToSave["program_planning_section"]);
    String encodedNotificationSectionConfig = json.encode(configToSave["notification"]["nav_notification_index_title"]);
    String encodedPrivateMessageSectionConfig = json.encode(configToSave["private_message_section"]);
    String encodedChildProfileConfig = json.encode(configToSave["child_profile"]["nav_centermanagement_quicklink_label"]);
    String encodedStaffDocumentsSection = json.encode(configToSave["staff_documents_upload_section"]);
    String encodedMedicationSection = json.encode(configToSave["medication_section"]);
    String encodedNonPrescribedSection = json.encode(configToSave["non_prescribed_medication_section"]);
    String encodedCenterManagementSection= json.encode(configToSave["center_management_journal_section"]);
    String encodedCenterManagementJourneySection= json.encode(configToSave["center_management_journey_section"]);
    String encodedNewsFeedSection = json.encode(configToSave["newsfeed_section"]["nav_newsfeed_label"]);
    String encodedAppSettings = json.encode(configToSave["application_settings"]);
//    String encodedJourneyAdditionalTextFieldOne = json.encode(configToSave["application_settings"]["observation_additionl_text_field_1"]["label"]);
//    String encodedJourneyAdditionalTextFieldTwo = json.encode(configToSave["application_settings"]["observation_additionl_text_field_2"]["label"]);


    SharedPreferences prefs = await SharedPreferences.getInstance();
//    print('savedGlobalConfig $savedGlobalConfig');

    await prefs.setString('globalConfig_clientSection', encodedClientSectionConfig);
    await prefs.setString("globalConfig_navigationSet", encodedNavigationConfig);
    await prefs.setString("globalConfig_programPlanSection", encodedProgramPlanSectionConfig);
    await prefs.setString("globalConfig_notificationSection", encodedNotificationSectionConfig);
    await prefs.setString("globalConfig_privateMessageSection", encodedPrivateMessageSectionConfig);
    await prefs.setString("globalConfig_childProfileSection", encodedChildProfileConfig);
    await prefs.setString("globalConfig_StaffDocumentsSection", encodedStaffDocumentsSection);
    await prefs.setString("globalConfig_medicationSection", encodedMedicationSection);
    await prefs.setString("globalConfig_nonPrescribedMedicationSection", encodedNonPrescribedSection);
    await prefs.setString("globalConfig_centerManagementJournalSection", encodedCenterManagementSection);
    await prefs.setString("globalConfig_centerManagementjourneySection", encodedCenterManagementJourneySection);
    await prefs.setString("globalConfig_newsFeedSection", encodedNewsFeedSection);
//    await prefs.setString("globalConfig_journeyAdditionalTextFieldOne", encodedJourneyAdditionalTextFieldOne);
//    await prefs.setString("globalConfig_journeyAdditionalTextFieldTwo", encodedJourneyAdditionalTextFieldTwo);
    await prefs.setString("globalConfig_newsFeedDetailLabel", json.encode(configToSave["newsfeed_section"]["nav_newsfeed_tab_wall"]));
    await prefs.setString("globalConfig_appSettings", encodedAppSettings);
  }
}