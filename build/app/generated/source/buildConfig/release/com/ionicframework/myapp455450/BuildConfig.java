/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.ionicframework.myapp455450;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.ionicframework.myapp455450";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 21;
  public static final String VERSION_NAME = "1.0.18";
}
