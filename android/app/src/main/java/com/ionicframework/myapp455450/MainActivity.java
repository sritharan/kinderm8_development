package com.ionicframework.myapp455450;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.CognitoCredentialsProvider;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {

    //Asanka s3 upload

    private static final String CHANNEL = "proitzen.kinderm8/s3_image";
    String imagePath = "";
    int imageNumber = 0;
    String clientId = "";
    String awsFolder = "";
    String fileExtension = "";
    String excep = "";
    final int[] sendStatus = {0};
    String key = "";
    MethodChannel.Result s3ParentResult;

    private static int REQUEST_CODE_CHOOSE = 1000;
    private static final int REQUEST_CODE_GRANT_PERMISSIONS = 1111;

    List<Uri> mSelected;
    List<String> selectedImages;

    CognitoCredentialsProvider credentialsProvider;
    ClientConfiguration clientConfiguration = new ClientConfiguration();
    public static final String POOL_ID = "ap-southeast-2:4ebdae8f-00cd-4ffb-b3a1-44173b9b7dda";
    public static final Regions REGION = Regions.AP_SOUTHEAST_2;

    public static final String BUCKET_NAME = "proitzencloudcdn";

    //Thiruban video download
    BroadcastReceiver receiver;
    long queueid;
    DownloadManager dm;
    MethodChannel.Result videoParentResult;

    Uri downloadedUrl;

    // Thirubaran profile picture change

    String client_id = "";
    String _image = "";
    String _imagePath = "";





    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_GRANT_PERMISSIONS && permissions.length == 3) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                pickImages();
            } else {
                if (
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                    Log.d("PERMISSION_DENIED", "Read, write or camera permission granting..");
                } else {
                    Log.d("PERMANENTLY_DENIED", "Please enable access to the storage and the camera.");
                }
            }
        }
        Log.d("PERMISSION_DENIED", "Read, write or camera permission was not granted");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Asanka method channel call for s3

        new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
                new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
                        s3ParentResult = result;
                        if (call.method.equals("pickImages")) {
                            requestPermissionIfNoPermission();
                        } else if (call.method.equals("sendImage")) {
                            imagePath = call.argument("imagePath");
                            imageNumber = call.argument("imageNumber");
                            clientId = call.argument("clientId");
                            awsFolder = call.argument("awsFolder");
                            fileExtension = call.argument("fileExtension");
                            sendImage();
                        } else if (call.method.equals("addToCalendar")) {
                            addToCalendar(call.argument("start_date")+"", call.argument("end_date")+"", call.argument("title")+"", call.argument("description")+"", call.argument("location")+"", call.argument("email")+"");
                        } else if (call.method.equals("getVideoDownloadLevel")) {
                            //Thiruban video download broadcast reciever
                            receiver = new BroadcastReceiver() {
                                @TargetApi(Build.VERSION_CODES.GINGERBREAD)
                                @Override
                                public void onReceive(Context context, Intent intent) {
                                    String action = intent.getAction();
                                    if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {

                                        DownloadManager.Query req_query = new DownloadManager.Query();
                                        req_query.setFilterById(queueid);

                                        Cursor c = dm.query(req_query);
                                        if (c.moveToFirst()) {

                                            int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);

                                            if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                                                System.out.println("Downlod completed");
                                            }
                                        }
                                    }
                                }
                            };

                            String url = call.argument("url");
                            downloadedUrl = getvideoDownloadLevel(url);
                            videoParentResult = result;
                        } else if (call.method.equals("getPhotoDownloadLevel")) {
                            receiver = new BroadcastReceiver() {
                                @TargetApi(Build.VERSION_CODES.GINGERBREAD)
                                @Override
                                public void onReceive(Context context, Intent intent) {
                                    String action = intent.getAction();
                                    if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {

                                        DownloadManager.Query req_query = new DownloadManager.Query();
                                        req_query.setFilterById(queueid);

                                        Cursor c = dm.query(req_query);
                                        if (c.moveToFirst()) {

                                            int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);

                                            if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                                                System.out.println("Downlod completed");
                                            }
                                        }
                                    }
                                }
                            };

                            String url = call.argument("url");
                            downloadedUrl = getphotoDownloadLevel(url);
                            videoParentResult = result;
                        } else if(call.method.equals("showDialog")){
                            client_id = call.argument("clientId");
                            changeProfileImage(call.argument("imagePath").toString());

                            System.out.print("profile upload method ");

                        }else if (call.method.equals("pdfDownload")){
                            System.out.println("pdf downloadingggggggggggggggggggggggggg");
                            receiver = new BroadcastReceiver() {
                                @TargetApi(Build.VERSION_CODES.GINGERBREAD)
                                @Override
                                public void onReceive(Context context, Intent intent) {
                                    String action = intent.getAction();

                                }
                            };

                            String url = call.argument("url");
                            new DownloadTask(MainActivity.this, url);
//                            downloadedUrl = Uri.fromFile(task.getFile());
                            videoParentResult = result;
                            System.out.println("pdf download url - "+url);
                        }


                        else {
                            result.notImplemented();
                        }
                    }
                });
        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        GeneratedPluginRegistrant.registerWith(this);

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    Uri getphotoDownloadLevel(String url) {

        dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse
                (url));

        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Image ")
                .setDescription("Baby Image Is Downloading.......")
                .setDestinationInExternalPublicDir("/BabyImages", "image.png");

        queueid = dm.enqueue(request);

        return dm.getUriForDownloadedFile(queueid);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    Uri pdfDownload(String url) {

        dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse
                (url));

        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Image ")
                .setDescription("Kinerm8 Image Is Downloading.......")
                .setDestinationInExternalPublicDir("/Kinerm8Images", "image.png");

        queueid = dm.enqueue(request);

        return dm.getUriForDownloadedFile(queueid);
    }

    //Thiruban video dowload get download level method
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    Uri getvideoDownloadLevel(String url) {

        dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse
                (url));
        // Environment.getExternalStorageDirectory(Environment().MEDIA_MOUNTED);
        //  request.setDestinationInExternalFilesDir(getApplicationContext(), Environment.DIRECTORY_DOWNLOADS , "video.mp4");

        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Video")
                .setDescription("Kinerm8 Video Is Downloading.")
                .setDestinationInExternalPublicDir("/Kinerm8Videos", "videos.mp4");

        queueid = dm.enqueue(request);

        return dm.getUriForDownloadedFile(queueid);
    }

    //Asanka snedImage to s3 upload method

    private void sendImage() {
        clientConfiguration.setConnectionTimeout(250000);
        clientConfiguration.setSocketTimeout(250000);
        credentialsProvider = new CognitoCredentialsProvider(POOL_ID, REGION, clientConfiguration);
        final TransferUtility transferUtility1 = TransferUtility.builder().context(getApplicationContext()).awsConfiguration(AWSMobileClient.getInstance().getConfiguration()).s3Client(new AmazonS3Client(credentialsProvider)).build();

        key = awsFolder + "/" + clientId + "_" + imageNumber + "img_" + System.currentTimeMillis() + fileExtension;
        final TransferObserver transferObserver1 = transferUtility1
                .upload(BUCKET_NAME, key, new File(imagePath), CannedAccessControlList.PublicRead);

        transferObserver1.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.equals(TransferState.COMPLETED)) {
                    sendStatus[0] = 1;
                    s3ParentResult.success(key);
//          System.out.println("COMPLETED, image available at: " + key);
                } else if (state.equals(TransferState.WAITING)) {
                    sendStatus[0] = 0;
                } else if (state.equals(TransferState.FAILED)) {
                    sendStatus[0] = -1;
                    s3ParentResult.success(null);
                } else {
                    sendStatus[0] = -2;
                }
            }

                                                  @Override
                                                  public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                                                      sendStatus[0] = 0;
                                                  }

                                                  @Override
                                                  public void onError(int id, Exception ex) {
                                                      sendStatus[0] = -3;
                                                      excep = ex.toString();
                                                      System.out.println("onError: " + excep);
                                                  }
                                              }
        );
    }

    // Thirubaran  user profile picture change

    public void changeProfileImage(String path) {
        credentialsProvider = new CognitoCredentialsProvider(POOL_ID, REGION, clientConfiguration);
        final TransferUtility transferUtility1 = TransferUtility.builder().context(getApplicationContext())
                .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                .s3Client(new AmazonS3Client(credentialsProvider)).build();

        String [] temp = path.split("/");
        key = "img/uploads/"+temp[temp.length-1];
        final TransferObserver transferObserver1 = transferUtility1
                .upload(BUCKET_NAME, key, new File(path), CannedAccessControlList.PublicRead);

        transferObserver1.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.equals(TransferState.COMPLETED)) {
                    sendStatus[0] = 1;
                    s3ParentResult.success(key);
                    System.out.print("img/uploads/$imageName");
                } else if (state.equals(TransferState.WAITING)) {
                    sendStatus[0] = 0;
                } else if (state.equals(TransferState.FAILED)) {
                    sendStatus[0] = -1;
                    s3ParentResult.success(null);
                } else {
                    sendStatus[0] = -2;
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                sendStatus[0] = 0;
            }

            @Override
            public void onError(int id, Exception ex) {
                sendStatus[0] = -3;
                excep = ex.toString();
                System.out.println("onError: " + excep);
            }
        });
    }

    private void requestPermissionIfNoPermission() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA
                    },
                    REQUEST_CODE_GRANT_PERMISSIONS);

        } else {
            pickImages();
        }

    }

    private void pickImages() {
        Matisse.from(MainActivity.this)
                .choose(MimeType.ofAll())
                .countable(true)
                .maxSelectable(9)
//                        .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
//                        .gridExpectedSize(120)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .thumbnailScale(0.85f)
                .imageEngine(new GlideEngine())
                .forResult(REQUEST_CODE_CHOOSE);
    }

    private String getFileName(Uri uri) {
        String fileName = "";
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor metaCursor = getApplicationContext().getContentResolver().query(uri, projection, null, null, null);
        if (metaCursor != null) {
            try {
                if (metaCursor.moveToFirst()) {
                    fileName = metaCursor.getString(0);
                }
            } finally {
                metaCursor.close();
            }
        }

        return fileName;
    }

    private void addToCalendar(String beginTime, String endTime, String title, String description, String location, String email) {
//        Calendar cal = Calendar.getInstance();//cal.getTimeInMillis()
        Date startDate = new Date(), endDate = new Date();
        try {
            startDate =new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(beginTime);
            endDate =new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= 14) {
            Intent intent = new Intent(Intent.ACTION_INSERT)
                    .setData(CalendarContract.Events.CONTENT_URI)
                    .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startDate)
                    .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endDate)
                    .putExtra(CalendarContract.Events.TITLE, title)
                    .putExtra(CalendarContract.Events.DESCRIPTION, description)
                    .putExtra(CalendarContract.Events.EVENT_LOCATION, location)
                    .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
//                    .putExtra(Intent.EXTRA_EMAIL, email);
            startActivity(intent);
        } else {
            Intent intent = new Intent(Intent.ACTION_EDIT);
            intent.setType("vnd.android.cursor.item/event");
            intent.putExtra("beginTime", startDate);
            intent.putExtra("allDay", true);
            intent.putExtra("rrule", "FREQ=YEARLY");
            intent.putExtra("endTime", endDate);
            intent.putExtra("title", title);
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        selectedImages = new ArrayList<>();
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            mSelected = Matisse.obtainResult(data);
            for (Uri uri : mSelected) {
                File file = new File(getFileName(uri));
                selectedImages.add(file.getAbsolutePath());
            }
            s3ParentResult.success(selectedImages);
            Log.d("Matisse", "mSelected: " + mSelected);
        }
    }
}
